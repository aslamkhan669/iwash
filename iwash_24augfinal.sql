-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2017 at 02:36 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iwash`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `url`, `status`, `createdAt`, `updatedAt`) VALUES
(5, 'uploads/banners/banner_2017-08-02 10:57:44.jpg', '1', '2017-08-02 10:57:44', '2017-08-02 10:57:44'),
(6, 'uploads/banners/banner_2017-08-02 10:57:48.jpg', '1', '2017-08-02 10:57:48', '2017-08-02 10:57:48');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch`, `location`, `createdAt`, `updatedAt`) VALUES
(1, 'branch1', 'location1', '2017-08-17 10:43:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_app` varchar(255) NOT NULL,
  `image_website` varchar(255) NOT NULL,
  `image_banner` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `per` varchar(100) NOT NULL,
  `rank_weight` int(10) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `description`, `image`, `image_app`, `image_website`, `image_banner`, `price`, `per`, `rank_weight`, `createdAt`, `updatedAt`) VALUES
(1, 'Wash & Fold', 0, 'Wash & Fold', 'uploads/categories/cat_df37d0533e764b1eed6e0e324a9afb58.png', '', '', '', 0.00, 'Piece', 0, '2017-06-29 07:31:49', '0000-00-00 00:00:00'),
(2, 'Wash & Iron', 0, 'Wash & Iron', 'uploads/categories/cat_499efbe825d993c93abdbd588aa1ad18.png', '', '', '', 0.00, 'Piece', 0, '2017-06-29 07:33:17', '0000-00-00 00:00:00'),
(3, 'Premium Laundry', 0, 'Premium Laundry', 'uploads/categories/cat_ed89033621d3fce0fdded313720514fc.png', '', '', '', 0.00, 'Piece', 0, '2017-06-29 07:33:32', '0000-00-00 00:00:00'),
(4, 'Dry Cleaning', 0, 'Dry Cleaning', 'uploads/categories/cat_97ab038dc0bce966a3bed6474da6843b.png', '', '', '', 0.00, 'Piece', 0, '2017-06-29 07:33:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `mobile`, `email`, `seen`, `message`, `createdAt`, `updatedAt`) VALUES
(1, 'Rakesh Bisht', '9650719414', 'raks.bisht@gmail.com', 0, 'hello testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `deliverytime`
--

CREATE TABLE `deliverytime` (
  `id` int(11) NOT NULL,
  `timebetween` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `deliverytime`
--

INSERT INTO `deliverytime` (`id`, `timebetween`, `status`, `createdAt`, `updatedAt`) VALUES
(1, '12:00 PM to 03:00 PM', 1, '2017-07-27 11:10:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `faq_category` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `faq_category`, `question`, `answer`, `createdAt`, `updatedAt`) VALUES
(2, 1, 'question1', 'a', '2017-06-30 11:02:47', '0000-00-00 00:00:00'),
(3, 2, 'question2', 'aa', '2017-06-30 11:02:56', '0000-00-00 00:00:00'),
(4, 1, 'question1_2', 'a', '2017-07-08 10:34:57', '0000-00-00 00:00:00'),
(5, 1, 'aa', 'aa', '2017-07-20 07:44:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `name`, `parent_id`, `description`, `image`, `createdAt`, `updatedAt`) VALUES
(1, 'faq category 1', 0, 'faq category 1', 'uploads/faqcat/cat_2017-08-02 11:36:26.jpg', '2017-07-03 09:28:25', '0000-00-00 00:00:00'),
(3, 'testfaq', 0, 'testfaq', 'uploads/faqcat/faqcat_2017-08-02 11:37:42.jpg', '2017-08-02 11:37:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `monthly_packages`
--

CREATE TABLE `monthly_packages` (
  `id` int(11) NOT NULL,
  `package` varchar(255) NOT NULL,
  `usage_limit` varchar(255) NOT NULL,
  `pickup_options` varchar(255) NOT NULL,
  `available_packages` varchar(255) NOT NULL,
  `pricing_for_each_month` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monthly_packages`
--

INSERT INTO `monthly_packages` (`id`, `package`, `usage_limit`, `pickup_options`, `available_packages`, `pricing_for_each_month`, `createdAt`, `updatedAt`) VALUES
(1, 'Traditional', '50 Thobs, 50 Srwal', 'Alternative,Weekly', '1,3,6,12 months', 600, '2017-08-22 08:43:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_cat_id` int(11) NOT NULL,
  `totalamount` float(10,2) DEFAULT NULL,
  `flatandstreet` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `addressType` enum('home','office','other') NOT NULL,
  `pickupDate` date NOT NULL,
  `deliveryDate` date NOT NULL,
  `timeOfDelivery` tinyint(1) NOT NULL,
  `timeOfPickup` tinyint(1) NOT NULL,
  `status` enum('cancelled','pending','placed','paid','completed') NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `service_cat_id`, `totalamount`, `flatandstreet`, `landmark`, `addressType`, `pickupDate`, `deliveryDate`, `timeOfDelivery`, `timeOfPickup`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 6, 1, 22.00, 'demoaddress', 'demolandmark', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-07-26 06:37:34', '0000-00-00 00:00:00'),
(2, 6, 1, 22.00, 'demoaddress', 'demolandmark', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-07-26 01:07:34', '0000-00-00 00:00:00'),
(3, 6, 2, NULL, 'dgdfgdfg', 'dgdf', 'home', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:00:30', '0000-00-00 00:00:00'),
(4, 6, 2, NULL, 'dgdfgdfg', 'dgdf', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-01 12:03:15', '0000-00-00 00:00:00'),
(5, 6, 2, NULL, 'dgdfgdfg', 'dgdf', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-01 12:06:16', '0000-00-00 00:00:00'),
(6, 6, 3, NULL, 'sfsdf', 'afafaf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:07:43', '0000-00-00 00:00:00'),
(7, 6, 2, NULL, 'afafa', 'sdfsdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:17:41', '0000-00-00 00:00:00'),
(8, 6, 3, NULL, 'sdfsdf', 'sdfsdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:18:50', '0000-00-00 00:00:00'),
(9, 6, 2, NULL, 'sdfsd', 'sdfs', 'other', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:21:21', '0000-00-00 00:00:00'),
(10, 6, 2, NULL, 'fa', 'af', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:24:29', '0000-00-00 00:00:00'),
(11, 6, 3, NULL, 'fds', 'fsdfsdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:28:40', '0000-00-00 00:00:00'),
(12, 6, 1, NULL, 'sfs', 'fsdfsd', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:29:51', '0000-00-00 00:00:00'),
(13, 6, 4, NULL, 'sdfsd', 'fsdfsdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-01 12:30:45', '0000-00-00 00:00:00'),
(14, 13, 2, NULL, '305 badar bagh', 'Aligarh', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-05 14:49:40', '0000-00-00 00:00:00'),
(15, 6, 1, NULL, 'Uquwuwuw', 'Hajajaj', 'home', '0000-00-00', '0000-00-00', 1, 2, 'cancelled', '2017-08-07 13:54:08', '0000-00-00 00:00:00'),
(16, 15, 1, NULL, 'Ddddd', 'Fffff', 'home', '0000-00-00', '0000-00-00', 1, 2, 'cancelled', '2017-08-07 14:01:52', '0000-00-00 00:00:00'),
(17, 6, 2, NULL, 'fs', 'fsdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-07 16:44:15', '0000-00-00 00:00:00'),
(18, 6, 4, NULL, 'a', 'a', 'office', '0000-00-00', '0000-00-00', 1, 2, 'placed', '2017-08-07 17:33:12', '0000-00-00 00:00:00'),
(19, 6, 3, NULL, 'gg', 'vv', 'office', '0000-00-00', '0000-00-00', 1, 2, 'placed', '2017-08-07 17:54:06', '0000-00-00 00:00:00'),
(20, 6, 2, NULL, 'sfsdf', 'sdfsdfsdf', 'other', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-08 06:20:06', '0000-00-00 00:00:00'),
(21, 6, 2, NULL, 'sdf', 'sdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-08 07:08:32', '0000-00-00 00:00:00'),
(22, 6, 2, NULL, 'sdfsd', 'fsdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-08 07:10:59', '0000-00-00 00:00:00'),
(23, 6, 1, NULL, 'Hello', 'Hello', 'home', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-08 08:32:58', '0000-00-00 00:00:00'),
(24, 15, 1, NULL, 'Hahahah', 'Hahahjjjwjw', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 12:10:35', '0000-00-00 00:00:00'),
(25, 15, 1, NULL, 'Hsueususuu', 'Ususususuus', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 12:31:57', '0000-00-00 00:00:00'),
(26, 15, 1, NULL, 'Ffff', 'Ffrt', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 12:52:23', '0000-00-00 00:00:00'),
(27, 6, 1, NULL, 'Hello', 'Hello', 'other', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 13:23:42', '0000-00-00 00:00:00'),
(28, 15, 1, NULL, 'hahahah', 'hshzhshbs', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 13:35:43', '0000-00-00 00:00:00'),
(29, 6, 3, NULL, 'Aslam', 'Near', 'office', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 13:36:52', '0000-00-00 00:00:00'),
(30, 16, 4, NULL, 'Dhorra', 'Aligarh', 'home', '0000-00-00', '0000-00-00', 1, 2, 'cancelled', '2017-08-08 13:40:29', '0000-00-00 00:00:00'),
(31, 17, 1, NULL, '305 badar bhag', 'shamsad', 'office', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 13:41:52', '0000-00-00 00:00:00'),
(32, 16, 4, NULL, 'Abc', 'Hfh', 'other', '0000-00-00', '0000-00-00', 1, 2, 'cancelled', '2017-08-08 13:45:00', '0000-00-00 00:00:00'),
(33, 13, 1, NULL, 'Bzbs', 'Hzhsh', 'office', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-08 14:56:10', '0000-00-00 00:00:00'),
(34, 13, 1, NULL, 'Add', 'Jdjdj', 'other', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-08 15:03:02', '0000-00-00 00:00:00'),
(35, 6, 2, NULL, 'vxvxcvxv', 'xcvxcvxc', 'home', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-08 15:54:10', '0000-00-00 00:00:00'),
(36, 6, 2, NULL, 't', 't', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-08 16:04:03', '0000-00-00 00:00:00'),
(37, 6, 4, NULL, 'sdf', 'sdfs', 'home', '2017-08-19', '2017-08-25', 1, 1, 'placed', '2017-08-08 16:15:33', '0000-00-00 00:00:00'),
(38, 6, 2, NULL, 'sdf', 'ffffffffffffff', 'other', '2017-08-18', '2017-08-24', 1, 1, 'cancelled', '2017-08-11 10:05:32', '0000-00-00 00:00:00'),
(39, 15, 1, NULL, 'najaj', 'hahaja', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-13 17:50:21', '0000-00-00 00:00:00'),
(40, 15, 1, NULL, 'najaj', 'hahaja', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-13 17:50:22', '0000-00-00 00:00:00'),
(41, 15, 1, NULL, 'najaj', 'hahaja', 'home', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-13 17:50:27', '0000-00-00 00:00:00'),
(42, 6, 2, NULL, '305 badar bhag aligarh', 'dena bank', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-17 09:59:18', '0000-00-00 00:00:00'),
(43, 6, 2, NULL, 'flat11', 'aligarh', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-17 10:19:19', '0000-00-00 00:00:00'),
(44, 6, 2, NULL, '222', 'delhi', 'home', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-17 10:50:43', '0000-00-00 00:00:00'),
(45, 6, 1, NULL, 'fsdfs', 'fsdf', 'office', '0000-00-00', '0000-00-00', 1, 1, 'cancelled', '2017-08-17 11:08:08', '0000-00-00 00:00:00'),
(46, 6, 3, NULL, '111', 'aligarh', 'office', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-17 11:12:19', '0000-00-00 00:00:00'),
(47, 6, 2, NULL, 'sdf', 'sfsdf', 'office', '2017-08-18', '2017-08-31', 1, 1, 'placed', '2017-08-17 11:13:27', '0000-00-00 00:00:00'),
(48, 6, 3, NULL, '11', 'abc', 'other', '0000-00-00', '0000-00-00', 1, 2, 'placed', '2017-08-17 11:15:21', '0000-00-00 00:00:00'),
(49, 6, 4, NULL, '11', 'delhi', 'other', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-17 11:17:14', '0000-00-00 00:00:00'),
(50, 6, 2, NULL, 'address', 'landmark', 'home', '2017-08-26', '2017-08-30', 1, 2, 'placed', '2017-08-17 11:23:50', '0000-00-00 00:00:00'),
(51, 6, 3, NULL, '111', 'landmark', 'home', '0000-00-00', '0000-00-00', 1, 1, 'placed', '2017-08-17 11:32:11', '0000-00-00 00:00:00'),
(52, 6, 2, NULL, '1102 civil line', 'landmark', 'home', '2017-08-19', '2017-08-24', 1, 1, 'placed', '2017-08-17 11:36:17', '0000-00-00 00:00:00'),
(53, 6, 3, NULL, '123', 'dena babk', 'home', '2017-08-18', '2017-08-30', 1, 2, 'placed', '2017-08-17 11:40:55', '0000-00-00 00:00:00'),
(54, 6, 4, NULL, '12', 'bank', 'office', '2017-08-18', '2017-08-30', 1, 1, 'placed', '2017-08-17 13:14:02', '0000-00-00 00:00:00'),
(55, 6, 1, NULL, 'faf', 'afa', 'office', '2017-08-19', '2017-08-31', 1, 1, 'placed', '2017-08-17 18:44:09', '0000-00-00 00:00:00'),
(56, 6, 1, NULL, 'fsdf', 'fsdf', 'office', '2017-08-19', '2017-08-22', 1, 1, 'placed', '2017-08-17 18:46:51', '0000-00-00 00:00:00'),
(57, 6, 2, NULL, 'sdf', 'sdf', 'office', '2017-08-25', '2017-08-23', 1, 1, 'placed', '2017-08-17 18:48:25', '0000-00-00 00:00:00'),
(58, 6, 2, NULL, 'sdf', 'sdfsd', 'office', '2017-08-19', '2017-08-29', 1, 2, 'placed', '2017-08-17 18:49:17', '0000-00-00 00:00:00'),
(59, 6, 2, NULL, 'dfg', 'gdgd', 'other', '2017-08-25', '2017-08-30', 1, 2, 'placed', '2017-08-17 18:51:13', '0000-00-00 00:00:00'),
(60, 6, 2, NULL, 'sdf', 'sdfsd', 'office', '2017-08-30', '2017-08-26', 1, 1, 'placed', '2017-08-18 09:14:30', '0000-00-00 00:00:00'),
(61, 6, 4, NULL, '12', 'landmarks', 'other', '2017-08-25', '2017-08-21', 1, 1, 'placed', '2017-08-18 09:36:10', '0000-00-00 00:00:00'),
(62, 6, 2, NULL, '11', 'landmark', 'home', '2017-08-19', '2017-08-23', 1, 2, 'placed', '2017-08-18 09:49:02', '0000-00-00 00:00:00'),
(63, 6, 2, NULL, '13', 'landmark', 'office', '2017-08-22', '2017-08-31', 1, 1, 'placed', '2017-08-18 10:03:40', '0000-00-00 00:00:00'),
(64, 6, 4, NULL, '14', 'aligarh', 'home', '2017-08-19', '2017-08-31', 1, 1, 'cancelled', '2017-08-18 10:09:23', '0000-00-00 00:00:00'),
(65, 6, 2, NULL, '0099', 'landmark', 'office', '2017-08-25', '2017-08-31', 1, 1, 'placed', '2017-08-18 10:16:33', '0000-00-00 00:00:00'),
(66, 6, 2, NULL, '11', 'landmark', 'office', '2017-08-18', '2017-08-21', 1, 1, 'placed', '2017-08-18 11:01:37', '0000-00-00 00:00:00'),
(67, 6, 2, NULL, '1100001 line', 'landmark', 'other', '2017-08-19', '2017-08-31', 1, 1, 'cancelled', '2017-08-18 11:44:01', '0000-00-00 00:00:00'),
(68, 6, 30, NULL, '111', 'delhi', 'office', '2017-08-19', '2017-08-30', 1, 1, 'cancelled', '2017-08-18 12:17:31', '0000-00-00 00:00:00'),
(69, 6, 29, NULL, '12', 'landmark', 'home', '2017-08-19', '2017-08-23', 1, 1, 'placed', '2017-08-18 13:14:59', '0000-00-00 00:00:00'),
(70, 6, 28, NULL, 'jjj', 'hhh', 'home', '0000-00-00', '0000-00-00', 1, 2, 'placed', '2017-08-19 08:30:06', '0000-00-00 00:00:00'),
(71, 6, 28, NULL, 'hahah', 'hahah', 'home', '2017-08-26', '2017-08-25', 1, 2, 'cancelled', '2017-08-19 11:09:26', '0000-00-00 00:00:00'),
(72, 6, 28, NULL, '123', 'landmark', 'other', '2017-08-22', '2017-08-31', 1, 1, 'placed', '2017-08-21 06:06:45', '0000-00-00 00:00:00'),
(73, 6, 28, NULL, 'sdfds', 'fsdf', 'office', '2017-08-24', '2017-08-30', 1, 2, 'cancelled', '2017-08-21 12:13:20', '0000-00-00 00:00:00'),
(74, 6, 30, NULL, 'aaa', 'landmark', 'home', '2017-08-22', '2017-08-23', 1, 2, 'cancelled', '2017-08-21 12:31:19', '0000-00-00 00:00:00'),
(75, 6, 31, NULL, 'q12', 'aligarh', 'home', '2017-08-22', '2017-08-31', 1, 1, 'cancelled', '2017-08-21 12:50:15', '0000-00-00 00:00:00'),
(76, 6, 30, NULL, 'zxz', 'Taj Hotel, Rakabganj, 9, Mahatma Gandhi Rd, Chhipitola, Agra, Uttar Pradesh 282001, India', 'office', '2017-08-22', '2017-08-22', 1, 2, 'cancelled', '2017-08-22 18:01:14', '0000-00-00 00:00:00'),
(77, 6, 30, NULL, 'ghhh', 'SAAD APARTMENT, Loco Rd, Aligarh, Uttar Pradesh 202001, India', 'office', '2017-08-25', '2017-08-25', 1, 1, 'cancelled', '2017-08-23 08:23:41', '0000-00-00 00:00:00'),
(78, 6, 28, NULL, 'dsfjdskfjdsklfj', 'SAAD APARTMENT, Loco Rd, Aligarh, Uttar Pradesh 202001, India', 'home', '2017-08-23', '2017-08-23', 1, 1, 'cancelled', '2017-08-23 08:57:44', '0000-00-00 00:00:00'),
(79, 6, 28, NULL, 'Fucuf', 'Als', 'home', '2017-08-23', '2017-08-23', 1, 1, 'placed', '2017-08-23 11:02:36', '0000-00-00 00:00:00'),
(80, 6, 28, NULL, 'hh', 'Hyderabad, Telangana, India', 'home', '2017-08-25', '2017-08-30', 1, 1, 'cancelled', '2017-08-23 12:00:00', '0000-00-00 00:00:00'),
(81, 6, 30, NULL, 'hh', 'Jaipur, Rajasthan, India', 'home', '2017-08-26', '2017-08-24', 1, 2, 'placed', '2017-08-23 12:10:13', '0000-00-00 00:00:00'),
(82, 6, 28, NULL, 'aslam', 'SAAD APARTMENT, Loco Rd, Aligarh, Uttar Pradesh 202001, India', 'home', '2017-08-30', '2017-08-23', 1, 1, 'cancelled', '2017-08-23 13:03:44', '0000-00-00 00:00:00'),
(83, 6, 3, NULL, 'sdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-25', 1, 1, 'placed', '2017-08-24 10:07:41', '0000-00-00 00:00:00'),
(84, 6, 3, NULL, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 1, 'placed', '2017-08-24 10:09:29', '0000-00-00 00:00:00'),
(85, 6, 2, NULL, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, Indiasdf', 'office', '2017-08-25', '2017-08-25', 1, 2, 'placed', '2017-08-24 10:14:10', '0000-00-00 00:00:00'),
(86, 6, 4, NULL, 'sdfs', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-26', 1, 1, 'placed', '2017-08-24 10:17:06', '0000-00-00 00:00:00'),
(87, 6, 3, NULL, 'sdfs', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-30', '2017-08-25', 1, 2, 'placed', '2017-08-24 10:18:20', '0000-00-00 00:00:00'),
(88, 6, 2, NULL, 's', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 1, 'placed', '2017-08-24 10:21:03', '0000-00-00 00:00:00'),
(89, 6, 2, NULL, 'sdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'other', '2017-08-26', '2017-08-25', 1, 2, 'placed', '2017-08-24 10:23:36', '0000-00-00 00:00:00'),
(90, 6, 2, NULL, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 2, 'placed', '2017-08-24 10:27:08', '0000-00-00 00:00:00'),
(91, 6, 2, NULL, 'sfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 2, 'placed', '2017-08-24 10:32:13', '0000-00-00 00:00:00'),
(92, 6, 2, NULL, 'sfsfs', 'sfsf', 'office', '2017-08-26', '2017-08-25', 1, 1, 'placed', '2017-08-24 10:34:07', '0000-00-00 00:00:00'),
(93, 6, 2, NULL, 'sfssdfs', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-25', 1, 1, 'placed', '2017-08-24 10:36:23', '0000-00-00 00:00:00'),
(94, 6, 2, NULL, 'fsdfsf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-26', 1, 1, 'placed', '2017-08-24 10:46:15', '0000-00-00 00:00:00'),
(95, 6, 1, NULL, 'sdfs', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-26', 1, 2, 'placed', '2017-08-24 10:54:56', '0000-00-00 00:00:00'),
(96, 6, 2, NULL, 'afa', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-25', 1, 1, 'placed', '2017-08-24 10:57:44', '0000-00-00 00:00:00'),
(97, 6, 2, NULL, 'sdfsdfsfsd', 'sdfsv, Rayagada, Odisha, India', 'office', '2017-08-25', '2017-08-26', 1, 2, 'placed', '2017-08-24 10:59:18', '0000-00-00 00:00:00'),
(98, 6, 2, NULL, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-25', 1, 1, 'placed', '2017-08-24 11:00:07', '0000-00-00 00:00:00'),
(99, 6, 2, NULL, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-30', 1, 2, 'placed', '2017-08-24 11:01:47', '0000-00-00 00:00:00'),
(100, 6, 3, NULL, 'afaf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-25', 1, 2, 'placed', '2017-08-24 11:02:16', '0000-00-00 00:00:00'),
(101, 6, 2, NULL, 'aa', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 2, 'placed', '2017-08-24 11:04:32', '0000-00-00 00:00:00'),
(102, 6, 2, NULL, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-24', '2017-08-31', 1, 1, 'placed', '2017-08-24 11:07:52', '0000-00-00 00:00:00'),
(103, 6, 2, NULL, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 2, 'placed', '2017-08-24 11:08:50', '0000-00-00 00:00:00'),
(104, 6, 2, NULL, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-25', 1, 2, 'placed', '2017-08-24 11:13:06', '0000-00-00 00:00:00'),
(105, 6, 2, NULL, 'afaf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-25', 1, 1, 'placed', '2017-08-24 11:13:43', '0000-00-00 00:00:00'),
(106, 6, 3, NULL, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'other', '2017-08-25', '2017-08-26', 1, 1, 'placed', '2017-08-24 11:15:04', '0000-00-00 00:00:00'),
(107, 6, 3, NULL, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-31', 1, 1, 'placed', '2017-08-24 11:16:18', '0000-00-00 00:00:00'),
(108, 6, 2, NULL, 'sdfsd', '2, Pocket G-27, Sector 3G, Rohini, Delhi, 110085, India', 'office', '2017-08-31', '2017-08-25', 1, 1, 'placed', '2017-08-24 11:17:47', '0000-00-00 00:00:00'),
(109, 6, 1, NULL, 'sdf', 'sfsdf', 'office', '2017-08-25', '2017-08-29', 1, 2, 'placed', '2017-08-24 11:39:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_service`
--

CREATE TABLE `order_service` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_service`
--

INSERT INTO `order_service` (`id`, `order_id`, `service_id`, `quantity`, `price`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, 1, 0.00, '2017-07-26 08:39:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pickuptime`
--

CREATE TABLE `pickuptime` (
  `id` int(11) NOT NULL,
  `timebetween` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickuptime`
--

INSERT INTO `pickuptime` (`id`, `timebetween`, `status`, `createdAt`, `updatedAt`) VALUES
(1, '12:00 PM to 02:00 PM', 1, '2017-07-27 11:10:14', '0000-00-00 00:00:00'),
(2, '12:00 PM to 03:00 PM', 1, '2017-08-07 11:38:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'User', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Employee', 'employee', '2017-08-16 12:24:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `per` varchar(100) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image_website` varchar(255) DEFAULT NULL,
  `image_app` varchar(255) DEFAULT NULL,
  `image_banner` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `category_id`, `description`, `image`, `price`, `per`, `createdAt`, `updatedAt`, `image_website`, `image_app`, `image_banner`) VALUES
(8, 'Pants', 2, 'some ', 'uploads/services/service_7c9f77d931c13b2dec39ca1066050f23.jpg', 5.00, 'item', '2017-08-18 10:15:41', '0000-00-00 00:00:00', 'uploads/services/service_webb60753d2a98f1652c221bdcb55b94b1a.jpg', 'uploads/services/service_appb60753d2a98f1652c221bdcb55b94b1a.jpg', 'uploads/services/service_banner61e48ebb928c3501b601d78912d5e657.png'),
(9, 'Jeans', 2, 'some', 'uploads/services/service_76825781ae82b4a0bbb2cf5cebb5e00f.jpg', 5.00, 'item', '2017-08-18 10:18:22', '0000-00-00 00:00:00', NULL, NULL, NULL),
(10, 'Formal Pants', 2, 'some', 'uploads/services/service_8bf18fa5f855f64700432972986342fd.jpg', 5.00, 'item', '2017-08-18 10:19:19', '0000-00-00 00:00:00', NULL, NULL, NULL),
(11, 'Formal Pants', 2, 'some', 'uploads/services/service_09c75fc19475689dfcf8d3fef03c0db6.jpg', 5.00, 'item', '2017-08-18 10:26:44', '0000-00-00 00:00:00', NULL, NULL, NULL),
(12, 'Abayas', 3, 'some', 'uploads/services/service_595ed86c9476970912fe3a13233c6bb6.jpg', 5.00, 'item', '2017-08-18 10:28:14', '0000-00-00 00:00:00', NULL, NULL, NULL),
(13, 'Towels', 4, 'some', 'uploads/services/service_e491974c4e283f54a422fcb6746931c7.jpg', 3.00, 'item', '2017-08-18 10:29:10', '0000-00-00 00:00:00', NULL, NULL, NULL),
(14, 'Tops', 3, 'some', 'uploads/services/service_43368bc0ce18998606367e2cfd5bda56.jpg', 5.00, 'item', '2017-08-18 10:30:08', '0000-00-00 00:00:00', NULL, NULL, NULL),
(15, 'Shorts', 1, 'some', 'uploads/services/service_79445eff3413f7d548d2066fc970b69d.jpg', 5.00, 'item', '2017-08-18 10:32:13', '0000-00-00 00:00:00', NULL, NULL, NULL),
(16, 'Suit Jackets', 3, 'some', 'uploads/services/service_c8f2dc7d0e0baca63f012eeb21cca322.jpg', 15.00, 'item', '2017-08-18 10:37:44', '0000-00-00 00:00:00', NULL, NULL, NULL),
(17, 'Pillow Case', 4, 'some', 'uploads/services/service_68db50e7b2f6c3e2e22da49eca05e484.jpg', 3.00, 'item', '2017-08-18 10:41:59', '0000-00-00 00:00:00', NULL, NULL, NULL),
(18, 'Bed Cover', 4, 'some', 'uploads/services/service_3b02f69aa707796530a293c5775b0c54.jpg', 5.00, 'item', '2017-08-18 10:42:48', '0000-00-00 00:00:00', NULL, NULL, NULL),
(19, 'Comforter', 4, 'some', 'uploads/services/service_bc631807d8f0fc5873b9fb58dfe2dba3.jpg', 10.00, 'i', '2017-08-18 10:43:31', '0000-00-00 00:00:00', NULL, NULL, NULL),
(20, 'Bedsheets', 4, 'some', 'uploads/services/service_a9f16222440888c7b0104ddf99e44edf.jpg', 6.00, 'item', '2017-08-18 10:44:20', '0000-00-00 00:00:00', NULL, NULL, NULL),
(21, 'Formal Shirts', 2, 'some', 'uploads/services/service_3f5b4d20bcf2f7f9aad3eb3536d34c7c.jpg', 5.00, 'item', '2017-08-18 10:45:15', '0000-00-00 00:00:00', NULL, NULL, NULL),
(22, 'Formal Shirts', 2, 'some', 'uploads/services/service_be87188b12a2122740159638dd7021b1.jpg', 5.00, 'item', '2017-08-18 10:45:21', '0000-00-00 00:00:00', NULL, NULL, NULL),
(23, 'Thobes', 2, 'some', 'uploads/services/service_8956916c2ca9173eca3ac79724455c1c.jpg', 5.00, 'item', '2017-08-18 10:47:39', '0000-00-00 00:00:00', NULL, NULL, NULL),
(24, 'Qtrah', 1, 'some', 'uploads/services/service_47022791d60b677cd68f2e2666a26095.jpg', 3.00, 'item', '2017-08-18 10:50:01', '0000-00-00 00:00:00', NULL, NULL, NULL),
(25, 'Fnyla', 1, 'some', 'uploads/services/service_62cf50a6ed4be3f4540f878e7de9f90e.jpg', 3.00, 'item', '2017-08-18 10:50:58', '0000-00-00 00:00:00', NULL, NULL, NULL),
(26, 'Srwal', 3, 'some', 'uploads/services/service_76b249a8039af5641c47be0e83af46ba.jpg', 3.00, 'item', '2017-08-18 10:51:32', '0000-00-00 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_features`
--

CREATE TABLE `service_features` (
  `id` int(11) NOT NULL,
  `service_cat_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_features`
--

INSERT INTO `service_features` (`id`, `service_cat_id`, `title`, `image`, `description`, `createdAt`, `updatedAt`) VALUES
(3, 1, 'Garments', 'uploads/servicefeatures/service_630359e397c21d9984362775201a8edf.png', 'For Household Wears', '2017-07-22 06:55:17', '0000-00-00 00:00:00'),
(5, 2, 'Garments', 'uploads/servicefeatures/service_c6d73f786d20411535fbaabd2f3ee278.png', 'For Household wears', '2017-08-02 13:40:59', '0000-00-00 00:00:00'),
(6, 3, 'Wash & Iron', 'uploads/servicefeatures/service_0d4b5fb2539a501d16ea265ab8330d8d.png', 'This is some Demo Description', '2017-08-23 08:44:43', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `souce_account_source_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`id`, `provider_id`, `username`, `avatar`, `souce_account_source_id`, `user_id`, `createdAt`, `updatedAt`) VALUES
(1, '1679446768753998', 'aslamkhan669@gmail.com', NULL, 1, 13, '2017-08-05 11:56:57', '0000-00-00 00:00:00'),
(2, '1341651595947567', 'sid.allthebest91@gmail.com', NULL, 1, 16, '2017-08-08 13:34:11', '0000-00-00 00:00:00'),
(3, '853362528154534', NULL, NULL, 1, 17, '2017-08-08 13:39:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_account_sources`
--

CREATE TABLE `social_account_sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_account_sources`
--

INSERT INTO `social_account_sources` (`id`, `source`, `createdAt`, `updatedAt`) VALUES
(1, 'facebook', '2016-04-15 02:08:58', '0000-00-00 00:00:00'),
(2, 'gplus', '2016-04-15 02:09:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_package`
--

CREATE TABLE `subscribe_package` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe_package`
--

INSERT INTO `subscribe_package` (`id`, `package_id`, `user_id`, `createdAt`, `updatedAt`) VALUES
(1, 1, 6, '2017-08-22 15:05:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `profile` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `designation`, `message`, `profile`, `createdAt`, `updatedAt`) VALUES
(2, 'Waseem', 'Software Engineer', 'Iwash doing a great job!', 'uploads/testimonials/testimonial_2017-08-24 07:31:39.jpg', '2017-08-02 11:28:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED NOT NULL,
  `confirmed` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `mobile_confirmed` tinyint(1) NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `mobile`, `password`, `location`, `active`, `role_id`, `confirmed`, `mobile_confirmed`, `confirmation_code`, `remember_token`, `createdAt`, `updatedAt`) VALUES
(1, 'raks.bisht@gmail.com', '', '$2y$10$VAbzPbeTcGKp5OspvuezlOXaAhPQxPj1O0PPtsA/.AAOTo.ka57wK', NULL, '0', 1, '1', 1, NULL, '2Npj4LTZvwMfmazK0IQJFbJOwgSmXEYCARzarqrQX5awLZ7nLI7R0nvDUPTw', '2017-06-30 09:10:52', '0000-00-00 00:00:00'),
(2, 'raks@test.com', '', 'db59555ae86e3ca1626305eb1b8b8d7a', NULL, '1', 2, '1', 1, NULL, NULL, '2017-07-12 10:52:57', '0000-00-00 00:00:00'),
(5, 'bisht.raksA@gmail.com', NULL, 'cd9d48e9f242ee364e39de55a811dcf1', NULL, '1', 2, '', 0, '35ce6f72f4b5e11a0b8ed35f685132ca', NULL, '2017-07-24 10:48:08', '0000-00-00 00:00:00'),
(6, 'bisht.raks@gmail.com', NULL, 'cd9d48e9f242ee364e39de55a811dcf1', NULL, '0', 2, '', 0, '35ce6f72f4b5e11a0b8ed35f685132ca', NULL, '2017-07-24 10:50:35', '0000-00-00 00:00:00'),
(7, 'raks.bishtsw@gmail.com', NULL, 'db59555ae86e3ca1626305eb1b8b8d7a', NULL, '1', 2, '1', 1, NULL, NULL, '2017-08-02 13:51:44', '0000-00-00 00:00:00'),
(8, 'aslam@gmail.com', NULL, 'bc0595a6c1a1d7246c23333b0201eea6', NULL, '1', 2, '1', 1, NULL, NULL, '2017-08-03 09:04:51', '0000-00-00 00:00:00'),
(9, 'junaid@sofyrus.com', NULL, 'bc0595a6c1a1d7246c23333b0201eea6', NULL, '1', 2, '1', 1, NULL, NULL, '2017-08-03 09:05:26', '0000-00-00 00:00:00'),
(10, 'rakesh@sofyrus.com', NULL, '0fccae940ba99190da5507470fca915b', NULL, '0', 2, '0', 0, '17bf03043247afaf4d8283a1515fe5e2', NULL, '2017-08-05 08:14:41', '2017-08-05 08:14:41'),
(11, 'raks.bisht@outlook.com', NULL, '89f2868773bff8b9669c7784da388098', NULL, '0', 2, '0', 0, '0955706dd7d85b6f3706b6708a27a7aa', NULL, '2017-08-05 08:17:12', '2017-08-05 08:17:12'),
(12, 'test@test.com', NULL, '7303ac0b9d74e6b23f8db16ab760aa26', NULL, '0', 2, '0', 0, '2b276d70000f20362c75bce9e601527e', NULL, '2017-08-05 08:26:34', '2017-08-05 08:26:34'),
(13, 'aslamkhan669@gmail.com', NULL, NULL, NULL, '0', 2, '0', 0, NULL, NULL, '2017-08-05 11:56:57', '2017-08-05 11:56:57'),
(14, 'test@tester.com', NULL, 'acae54c95331fe242aaf9a4a733ba506', NULL, '1', 2, '1', 1, NULL, NULL, '2017-08-06 16:55:10', '0000-00-00 00:00:00'),
(15, 'waseemzaidi85@gmail.com', NULL, '3c1ef886bbf276b2bafb68225b1beca8', NULL, '0', 2, '0', 0, '67ecc812693e190de811a77d4a31d35f', NULL, '2017-08-07 13:57:21', '2017-08-07 13:57:21'),
(16, 'sid.allthebest91@gmail.com', NULL, NULL, NULL, '0', 2, '0', 0, NULL, NULL, '2017-08-08 13:34:11', '2017-08-08 13:34:11'),
(17, NULL, NULL, NULL, NULL, '0', 2, '0', 0, NULL, NULL, '2017-08-08 13:39:16', '2017-08-08 13:39:16'),
(18, 'aliawasif1983@gmail.com', NULL, 'd00f763cf69a1c093d962d63e2df23fe', NULL, '0', 2, '0', 0, '1722cb732e3269cdce24dd63a6d0f1f4', NULL, '2017-08-08 19:21:10', '2017-08-08 19:21:10'),
(19, 'gilaniwasif@gmail.com', NULL, 'e4eb11936dd8a70cb6c01cd6b002a16f', NULL, '0', 2, '0', 0, 'c44b55e28b5afabde033bed8e4df3ca8', NULL, '2017-08-14 06:55:54', '2017-08-14 06:55:54'),
(20, 'aamir@iul.ac.in', NULL, 'ac9f8befe1aa0844b2ba26fd9cf503d8', NULL, '0', 2, '0', 0, '65d854107216faf8e7373c8267722d80', NULL, '2017-08-17 10:01:54', '2017-08-17 10:01:54'),
(21, 'employee1@test.com', '9643896566', '$2y$10$ZbYSIfS/KNVwoc0DY0srz.9DTJaXzo1SvEEH.Sh66XT7M4U1ApZCq', 1, '1', 3, '1', 1, NULL, 'Ds8HrUo1P5drDu35HSK1j11ji7aDw6L508wtHEJl8DsXctXMCUpWbCcxFDtd', '2017-08-17 10:44:09', '0000-00-00 00:00:00'),
(22, 'al.naimi97@hotmail.com', NULL, 'bfdc4cf9fad1a42275555c50cefbe3a5', NULL, '0', 2, '0', 0, 'bdb3c63a0bd1261073c35244d7fbeef8', NULL, '2017-08-21 19:01:14', '2017-08-21 19:01:14'),
(23, 'qatari._.97@hotmail.com', '33300033', '$2y$10$rbCe2r5F.5FWO/4L5zZr2uIYob50j6.xx1bYrdMJWepPe9llDpSJG', 1, '1', 3, '1', 1, NULL, 'kU45OuZH7rrdnPqBPsQaqZeU44ebQGACPvoXbLiefCbAxSHosNoLZiQ75QIt', '2017-08-22 23:17:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_me` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `first_name`, `last_name`, `about_me`, `avatar`, `user_id`, `createdAt`, `updatedAt`) VALUES
(1, 'Rakesh', 'Bisht', '', '', 1, '2017-06-30 09:12:26', '0000-00-00 00:00:00'),
(2, 'Rakesh', 'Bisht', '', '', 2, '2017-07-12 10:52:57', '0000-00-00 00:00:00'),
(3, 'ra', 'faf', '', '', 5, '2017-07-24 10:48:08', '0000-00-00 00:00:00'),
(4, 'RA', 'RA', '', '', 6, '2017-07-24 10:50:35', '0000-00-00 00:00:00'),
(5, 'Rakesh', 'Bisht', '', '', 7, '2017-08-02 13:51:44', '0000-00-00 00:00:00'),
(6, 'aslam', 'khan', '', '', 8, '2017-08-03 09:04:51', '0000-00-00 00:00:00'),
(7, 'junaid', 'siddiqui', '', '', 9, '2017-08-03 09:05:26', '0000-00-00 00:00:00'),
(8, 'fsdfsd', 'fsdf', '', '', 12, '2017-08-05 08:26:34', '0000-00-00 00:00:00'),
(9, 'Aslam', 'Khan', '', '', 13, '2017-08-05 11:56:57', '0000-00-00 00:00:00'),
(10, 'test', 'test', '', '', 14, '2017-08-06 16:55:10', '0000-00-00 00:00:00'),
(11, 'Waseem', 'Ahmad', '', '', 15, '2017-08-07 13:57:21', '0000-00-00 00:00:00'),
(12, 'Junaid', 'Sid', '', '', 16, '2017-08-08 13:34:11', '0000-00-00 00:00:00'),
(13, 'Amir', 'Ansari', '', '', 17, '2017-08-08 13:39:16', '0000-00-00 00:00:00'),
(14, 'Alia', 'Khan', '', '', 18, '2017-08-08 19:21:10', '0000-00-00 00:00:00'),
(15, 'Wasif', 'Gilani', '', '', 19, '2017-08-14 06:55:54', '0000-00-00 00:00:00'),
(16, 'md', 'amir', '', '', 20, '2017-08-17 10:01:54', '0000-00-00 00:00:00'),
(17, 'newemp1', 'newemp1', '', '', 21, '2017-08-17 10:44:09', '0000-00-00 00:00:00'),
(18, 'hassan', 'alnaimi', '', '', 22, '2017-08-21 19:01:14', '0000-00-00 00:00:00'),
(19, 'h', 'h', '', '', 23, '2017-08-22 23:17:37', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliverytime`
--
ALTER TABLE `deliverytime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthly_packages`
--
ALTER TABLE `monthly_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_service`
--
ALTER TABLE `order_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `pickuptime`
--
ALTER TABLE `pickuptime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_features`
--
ALTER TABLE `service_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_accounts_provider_id_unique` (`provider_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `souce_account_source_id` (`souce_account_source_id`);

--
-- Indexes for table `social_account_sources`
--
ALTER TABLE `social_account_sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_package`
--
ALTER TABLE `subscribe_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_profiles_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `deliverytime`
--
ALTER TABLE `deliverytime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `monthly_packages`
--
ALTER TABLE `monthly_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `order_service`
--
ALTER TABLE `order_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pickuptime`
--
ALTER TABLE `pickuptime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `service_features`
--
ALTER TABLE `service_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social_account_sources`
--
ALTER TABLE `social_account_sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subscribe_package`
--
ALTER TABLE `subscribe_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `social_accounts_ibfk_2` FOREIGN KEY (`souce_account_source_id`) REFERENCES `social_account_sources` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
