//import { newpackageRepository } from '../dao/newpackageDao';

var orderDao = require('../dao/orderDao');
var orderRepository = new orderDao.orderRepository();
var express = require('express');
var qr = require('qr-image');
var crypto = require('qr-image'); ('crypto');
var router = express.Router();
var models = require('../models');
var categoryDao = require('../dao/categoryDao');
var categoryRepository = new categoryDao.categoryRepository();
var newpackageDao = require('../dao/newpackageDao');
var newpackageRepository = new newpackageDao.newpackageRepository();

router.use(function (req, res, next) {
    next();
});

router.get('/branches', function (req, res) {

    orderRepository.getBranches().then(function (result) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});

router.get('/deliverytime', function (req, res) {

    orderRepository.getDeliveryTime().then(function (result) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});
router.get('/pickuptime', function (req, res) {

    orderRepository.getPickupTime().then(function (result) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});



router.post('/getAll', function (req, res) {
    var authuser = req.authuser.username;
    console.log(req.authuser);
    orderRepository.getAll(authuser, req.body).then(function (result) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});

router.get('/get/:orderid', function (req, res) {
    var authuser = req.authuser.username;
    console.log(req.authuser);
    orderRepository.findById(authuser, req.params.orderid).then(function (result) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});

router.post('/place', function (req, res) {
    var authuser = req.authuser.username;
    console.log(req.authuser);
     var currDate  = new Date();
      var currDay   = currDate.getDate();
      var currMonth = currDate.getMonth() + 1;
      var currYear  = currDate.getFullYear();
     var  currDateStr   = currYear+"-"+currMonth + "-" + currDay ;

    var data = {};
    var orderService = {};
    data.user_id = authuser;
    data.qty = req.body.qty;
    data.totalamount = req.body.cartPrice;
    data.flatandstreet = req.body.address.street;
    data.landmark = req.body.address.landmark;
    data.addressType = req.body.address.addresstype;
    data.branch = req.body.branch.id;
    data.order_type = 'simple';
    data.status = 'accepted';
    data.service_cat_id = 2;
    data.added_by = 1;
    data.pickupDate = 0;
    data.deliveryDate = 0;
    data.timeOfDelivery = 1;
    data.timeOfPickup = 1;
    data.payment_date = currDateStr;
    //data.order_id=req.body.data[0].id;
    data.service_id = 1;
    data.quantity = 0;
    data.price = 0;
    data.payment_status=1;
    data.payment_mode="card";



    orderRepository.placeorder(data).then(function (order) {

        var orderservice = [];
        var cart = req.body.array;
        for (var i = 0; i < req.body.array.length; i++) {
            var service = {};
            service.order_id = order.id;
            service.service_id =cart[i].id;
            service.quantity = cart[i].cart_qty;
            service.price=cart[i].price;
            service.qty_w=cart[i].qty_w;
            service.qty_ir=cart[i].qty_ir;
            service.qty_dc=cart[i].qty_dc;
            service.price_w=cart[i].price_wash;
            service.price_ir=cart[i].price_iron;
            service.price_dc=cart[i].price_dryclean;
            service.createdAt=cart[i].createdAt;
            service.updatedAt=cart[i].updatedAt;
            orderservice.push(service);

           
            
        }

        orderRepository.addOrderServices(orderservice).then(function () {
            
        
                res.json(order);
            
            



        })


    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });
});


router.post('/placeWithoutSubscription', function (req, res) {
    var authuser = req.authuser.username;
    console.log(req.authuser);
     var currDate  = new Date();
      var currDay   = currDate.getDate();
      var currMonth = currDate.getMonth() + 1;
      var currYear  = currDate.getFullYear();
     var  currDateStr   = currYear+"-"+currMonth + "-" + currDay ;
    var data = {};
    var orderService = {};
    data.user_id = authuser;
    data.qty = req.body.qty;
    data.totalamount = req.body.cartPrice;
    data.flatandstreet = req.body.address.street;
    data.landmark = req.body.address.landmark;
    data.addressType = req.body.address.addresstype;
    data.branch = req.body.branch.id;
    data.order_type = 'simple';
    data.status = 'accepted';
    data.service_cat_id = 2;
    data.added_by = 1;
    data.pickupDate = 0;
    data.deliveryDate = 0;
    data.timeOfDelivery = 1;
    data.timeOfPickup = 1;
    data.payment_date =currDateStr;
    //data.order_id=req.body.data[0].id;
    data.service_id = 1;
    data.quantity = 0;
    data.price = 0;
    data.payment_status=1;
    data.payment_mode="card";




    orderRepository.placeorder(data).then(function (order) {

        var orderservice = [];
        var cart = req.body.array;
        for (var i = 0; i < req.body.array.length; i++) {
            var service = {};
            service.order_id = order.id;
            service.service_id = cart[i].id;
            service.quantity = cart[i].cart_qty;
            service.price=cart[i].price;
            service.qty_w=cart[i].qty_w;
            service.qty_ir=cart[i].qty_ir;
            service.qty_dc=cart[i].qty_dc;
            service.price_w=cart[i].price_wash;
            service.price_ir=cart[i].price_iron;
            service.price_dc=cart[i].price_dryclean;
            service.createdAt=cart[i].createdAt;
            service.updatedAt=cart[i].updatedAt;
            orderservice.push(service);

           
            
        }

        orderRepository.addOrderServices(orderservice).then(function (result) {
           
            
            res.json(order);
            
            



            })


    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });
});




router.post('/placeWithSubscription', function (req, res) {
     var authuser = req.authuser.username;
    console.log(req.authuser);
     var currDate  = new Date();
      var currDay   = currDate.getDate();
      var currMonth = currDate.getMonth() + 1;
      var currYear  = currDate.getFullYear();
     var  currDateStr   = currYear+"-"+currMonth + "-" + currDay ;

    var data = {};
    var orderService = {};
    data.user_id = authuser;
    data.qty = req.body.qty;
    data.totalamount = req.body.cartPrice;
    data.flatandstreet = req.body.address.street;
    data.landmark = req.body.address.landmark;
    data.addressType = req.body.address.addresstype;
    data.branch = req.body.branch.id;
    
    data.order_type = 'simple';
    data.status = 'accepted';
    data.service_cat_id = 2;
    data.added_by = 1;
    data.pickupDate = currDateStr;
    data.deliveryDate = 0;
    data.timeOfDelivery = 0;
    data.timeOfPickup = 0;
    data.payment_date = 0;
    //data.order_id=req.body.data[0].id;
    data.service_id = 1;
    data.quantity = 0;
    data.price = 0;
    data.payment_status=1;
data.payment_mode="card";
    //newdata=req.body.array


    orderRepository.placeorder(data).then(function (order) {
        var orderservice = [];
        var cart = req.body.array;
        for (var i = 0; i < req.body.array.length; i++) {
            var service = {};
            service.order_id = order.id;
            service.service_id = cart[i].id;
            service.quantity = cart[i].cart_qty;
            service.price=cart[i].price;
            service.qty_w=cart[i].qty_w;
            service.qty_ir=cart[i].qty_ir;
            service.qty_dc=cart[i].qty_dc;
            service.price_w=cart[i].price_wash;
            service.price_ir=cart[i].price_iron;
            service.price_dc=cart[i].price_dryclean;
            service.createdAt=cart[i].createdAt;
            service.updatedAt=cart[i].updatedAt;
            orderservice.push(service);

           
            
        }

        orderRepository.addOrderServices(orderservice).then(function () {

            newpackageRepository.Update(req.body.mysubscription).then(function (result) {
                res.json(order);
            })

 })


    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });

});




function setSubscription(user_id, order_id, category_id, subscribe_id, service_id, quantity) {


    return models.sequelize.query("UPDATE  subscribe_services SET quantity=(quantity-" + quantity + ")  WHERE subscribe_id =" + subscribe_id + " and service_id=" + service_id + " and quantity>=" + quantity + "").spread(function (results, metadata) {

        console.log('metadata');
        console.log(metadata);
        console.log(metadata.affectedRows);
        if (metadata.affectedRows) {
            return orderRepository.addOrderService({ order_id: order_id, service_id: service_id, quantity: quantity }).then(function (orderresults) {



            })
        }
    });
}

router.get('/cancelled/:orderid', function (req, res) {
    var authuser = req.authuser.username;
    var data = {};
    data.orderid = req.params.orderid;
    data.user_id = authuser;
    data.status = 'cancelled';


    orderRepository.cancelorder(data).then(function (result) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});



module.exports = router;
    //# sourceMappingURL=user.js.map