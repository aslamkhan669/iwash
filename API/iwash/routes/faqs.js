var faqsDao = require("../dao/faqsDao");
var faqsRepository = new faqsDao.faqsRepository();

var faqcategoriesDao = require("../dao/faq_categoriesDao");
var faqscategoriesRepository = new faqcategoriesDao.faqscategoriesRepository();
var express = require("express");
var router = express.Router();

router.use(function(req, res, next) {
  next();
});

router.get("/list", function(req, res) {
  faqsRepository.getAll().then(function(result) {
      res.json(result);
    })
    .error(function(error) {
      if (!error || error == null) {
        return new Error("Error fetching records!");
      }
    });
});
router.get("/categories", function(req, res) {
  faqscategoriesRepository.getAll().then(function(result) {
      res.json(result);
    })
    .error(function(error) {
      if (!error || error == null) {
        return new Error("Error fetching records!");
      }
    });
});

router.get("/catwithQuestion", function(req, res) {
  faqscategoriesRepository.getAllWithQuestions().then(function(result) {
      res.json(result);
    })
    .error(function(error) {
      if (!error || error == null) {
        return new Error("Error fetching records!");
      }
    });
});

  router.get('/:categoryid',function(req,res){
        faqsRepository.findByCatId(req.params.categoryid).then(function (result) {
            res.json(result);

        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching records!');
            }
        });
    });
module.exports = router;
//# sourceMappingURL=user.js.map
