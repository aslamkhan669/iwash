    var testimonialsDao = require('../dao/testimonialsDao');
    var testimonialsRepository = new testimonialsDao.testimonialsRepository();
  var express = require('express');

    var router = express.Router();
	
    router.use(function (req, res, next) {
        next();
    });
  

	
    router.get('/', function (req, res) {
        
        testimonialsRepository.getAll().then(function (result) {
            if(result){
                res.json({"status":true,"result":result});

            }else{

           res.json({"status":false});
            }
          
        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching records!');
            }
        });
    });

    

    module.exports = router;
    //# sourceMappingURL=user.js.map