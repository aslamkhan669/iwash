var newpackageDao = require('../dao/newpackageDao');
var newpackageRepository = new newpackageDao.newpackageRepository();
var subscribepackageDao = require("../dao/subscribepackageDao");
var subscribepackageRepository = new subscribepackageDao.subscribepackageRepository();

var userDao = require('../dao/userDao');
var userRepository = new userDao.userRepository();

var express=require('express');
var router = express.Router();
var models = require('../models');




router.use(function (req, res, next) {
    next();
});


router.post('/packagesadd', function (req, res) {
       
     // var createdAt=new Date();
     var x =parseInt(req.body.noofmonths)     
      var currDate  = new Date();
      var currDay   = currDate.getDate();
      var currMonth = currDate.getMonth() + 1;
      var currYear  = currDate.getFullYear();
     var  currDateStr   = currMonth + "/" + currDay + "/" + currYear;
     /// document.write(currDateStr);
     // document.write("<p>");
      var ModMonth = currMonth + x;
      if (ModMonth > 12)
       { 
         ModMonth = ModMonth-12;
         currYear = currYear + 1;
       }
    
     var ModDateStr = ModMonth + "/" + currDay + "/" + currYear
     var somedate = new Date(ModDateStr);
     var data={};  
    models.package.find({
        where: {
          id: req.body.pid
        }
      }).then(function(x){
     var price =  x.price;
        
      

      data.package_id = req.body.pid;
      data.end_date= somedate;
      data.quantity_used=req.body.quantity_used;
      data.duration=req.body.noofmonths;
      data.amount_paid=price*req.body.noofmonths;
      //data.enddate=created
      data.user_id=req.body.userId;
      data.payment_status=0;
      data.isExpired=0;
      data.isLimitUsed=0;
      data.status=1;
      data.active=1;
      data.start_date=currDate;
    

      subscribepackageRepository.create(data).then(function (result) {
        res.json(result);

    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });
      });
      
      
      
     
     
   

   
});

router.post('/UpdatePackage', function (req, res) {
    
  
  var data={};  
 models.package.find({
     where: {
       id: req.body.pid
     }
   }).then(function(x){
  var price =  x.price;
     
   

   data.package_id = req.body.pid;
   data.quantity_used=req.body.quantity_used;
   data.duration=req.body.noofmonths;
   data.payment_amount=price*req.body.noofmonths;
   data.user_id=req.body.userId;
   data.payment_status=0;
   data.isExpired=0;
   data.isLimitUsed=0;
   data.status=1;
   data.active=1;

   subscribepackageRepository.Update(data).then(function (result) {
     res.json(result);

 }).error(function (error) {
     if (!error || error == null) {
         return new Error('Error Fetching Records');
     }
 });
   });
   
   
   
  
  



});

   



router.get('/packages', function (req, res) {
  
   newpackageRepository.getPackages().then(function (result) {
    res.json(result);
}).error(function (error) {
    if (!error || error == null) {
        return new Error('Error fetching records!');
    }
});
});

module.exports = router;


