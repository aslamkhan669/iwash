var express = require('express');
var router = express.Router();
var models = require('../models');
var crypto = require('crypto');
var userDao = require("../dao/userDao");

var userRepository = new userDao.userRepository();
router.use(function (req, res, next) {
    next();
});

router.get('/Tests', function (req, res) {
    
     userRepository.getUserDetailsByTahseen().then(function (result) {
      res.json(result);
  }).error(function (error) {
      if (!error || error == null) {
          return new Error('Error fetching records!');
      }
  });
});

module.exports = router;

