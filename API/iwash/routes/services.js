    var servicesDao = require('../dao/servicesDao');
    var servicesRepository = new servicesDao.servicesRepository();
  var express = require('express');

    var router = express.Router();
	
    router.use(function (req, res, next) {
        next();
    });
  

	 router.get('/features/list', function (req, res) {
        console.log('features');



        servicesRepository.findWithFeatures().then(function (result) {
            res.json(result);
        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching records!');
            }
        });
    });


    router.get('/catid/:id', function (req, res) {
        
        
        servicesRepository.findByCatId(req.params.id).then(function (result) {
            res.json(result);
        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching records!');
            }
        });
    });

   

router.get('/getAllItems',function(req,res){
    
    servicesRepository.getAll().then(function (result) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});

  
    

    module.exports = router;
    //# sourceMappingURL=user.js.map