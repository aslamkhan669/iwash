var express = require('express');
var router = express.Router();
var models = require('../models');
var crypto = require('crypto');
var phonebookDao = require("../dao/phonebookDao");

var pbookRepository = new phonebookDao.pbookRepository();

router.use(function (req, res, next) {
    next();
});

router.get('/getAll', function (req, res) {
    pbookRepository.getAll().then(function (result) {
        res.json(result);

    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });
});


router.post('/create', function (req, res) {
    pbookRepository.Create(req.body).then(function (result) {
        res.json(result);

    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });
});

router.post('/update', function (req, res) {
    pbookRepository.Update(req.body).then(function (result) {
        res.json(result);

    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });
});

router.get('/delete/:id', function (req, res) {

    var data = {};

    data.id = id;
    data.status = 'deleted';


    pbookRepository.Delete(data).then(function (req, res) {
        res.json(result);
    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error fetching records!');
        }
    });
});



   
  module.exports = router;
