    var express = require('express');
    var categoryDao = require('../dao/categoryDao');
    var categoryRepository = new categoryDao.categoryRepository();
    var servicesDao = require('../dao/servicesDao');
    var servicesRepository = new servicesDao.servicesRepository();
    var router = express.Router();
	
    router.use(function (req, res, next) {
        next();
    });
  
	
    router.get('/', function (req, res) {
        
        categoryRepository.getAll().then(function (result) {

            res.json(result);
        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching users!');
            }
        });
    });

   
 router.get('/parent', function (req, res) {
        
        categoryRepository.getAllParentcats().then(function (result) {

           if(result.length==0){
        res.json({status:false,message:"No Categories"});
      }
      else{
        res.json({status:true,result:result});

      }
        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching Categories!');
            }
        });
    });

    router.get('/parent/:parentid',function(req,res){
        categoryRepository.getSubcatByParent(req.params.parentid,req.body.page,req.body.limit).then(function (result) {
                if(result.length==0){
                res.json({status:false,message:"No Sub Categories"});
                }
                else{
        servicesRepository.findByCatId(req.params.parentid,req.body.page,req.body.limit).then(function (result) {
            if(result.length==0){
                res.json({status:false,message:"No Services"});
                }
                else{
        res.json({status:true,result:result});

                }


        });
          
        
         
       }
        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching Sub Categories!');
            }
        });

      });
 

    router.get('/subcategory/:parentid',function(req,res){
        categoryRepository.getSubcatByParentId(req.params.parentid).then(function (result) {
            res.json(result);

        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching Sub Categories!');
            }
        });
    });



     //get category details
    router.get('/:id',function(req,res){
        categoryRepository.findById(req.params.id).then(function (result) {
            res.json(result);

        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching Categories!');
            }
        });
    });

    router.post('/topCategories',function(req,res){
        if(req.body.limit){
            categoryRepository.findByRank(req.body.limit).then(function (result) {
                res.json(result);

            }).error(function (error) {
                if (!error || error == null) {
                    return new Error('Error fetching Categories!');
                }
            });
        }
        else{
            categoryRepository.findByRank(3).then(function (result) {
                res.json(result);

            }).error(function (error) {
                if (!error || error == null) {
                    return new Error('Error fetching Categories!');
                }
            });
        }
    });


    module.exports = router;
    //# sourceMappingURL=user.js.map