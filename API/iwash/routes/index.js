var express = require('express');
var app = express();
var router = express.Router();
var jwt    = require('jsonwebtoken');
var models = require('../models');


router.use(function (req, res, next) {
	
				if(
					req.method!='OPTIONS' && 
					req._parsedUrl.pathname!='/api/user/loginsocial' &&
					req._parsedUrl.pathname!='/api/phonebook/create' &&
					req._parsedUrl.pathname!='/api/phonebook/update' &&
					req._parsedUrl.pathname!='/api/user/authenticate' &&
					req._parsedUrl.pathname!='/api/user/register' &&
					req._parsedUrl.pathname!='/api/testimonials' &&
					req._parsedUrl.pathname!='/api/featured-services' &&
					req._parsedUrl.pathname.indexOf('/api/feature')==-1 &&
					req._parsedUrl.pathname.indexOf('/api/categories')==-1 &&
					req._parsedUrl.pathname.indexOf('/api/services')==-1 &&
					req._parsedUrl.pathname.indexOf('/api/faqs')==-1 &&
					req._parsedUrl.pathname!='/api/package/list' &&
					req._parsedUrl.pathname!='/api/user/resetpassword' &&
					req._parsedUrl.pathname!='/api/user/resetpasswordconfirm'  && 
					req._parsedUrl.pathname!='/api/user/forgotpassword/tokenverify' && 
					req._parsedUrl.pathname!='/api/user/verifymobile' &&
					req._parsedUrl.pathname!='/api/banner' &&
					req._parsedUrl.pathname!='/api/user/verifymobile/confirm' &&
					req._parsedUrl.pathname!='/api/package/getsub'
				
				
				){


					var token = req.body.xaccesstoken || req.query.xaccesstoken || req.headers.xaccesstoken || req.body.xaccesstoken;

			//	console.log(token);
				  if (token) {
														
				jwt.verify(token,'eyJ1c2VybmFtZSI6ILTE5VDA3OjIyOjE5LjgImRhdGUiOiIyMDE2LTELTE5VDA3OjIyOjE5Lj3MloiLCJyYW5kb20iiIsg3MloiLCJyYW5kb20ixOjEwNTg1NzEzODUsImlhdCI6MTQ3OTU0MDEzOSwiZXhwI', function(err, decoded) {      
				  	if (err) {
					  if(err.name='TokenExpiredError'){
						  	return res.status(401).send({ 
								success: false, 
								type:'token_expired',
								message: 'Token Expired.' 
								});
					  }
					  if(err.name='JsonWebTokenError'){
						  return res.status(401).send({ 
								success: false, 
								type:'token_invalid',
								message: 'Token Invalid.' 
								});
					  }
					
				
				
				  } else {
				//	console.log(decoded);
					req.authuser = decoded;    
					req.decoded = decoded;    
					next();
				  }
				});

			  } else {

				  // console.log(req._parsedUrl.pathname);
				  // console.log('No token provided.');

				return res.status(401).send({ 
					success: false, 
					type:'no_token',
					message: 'No token provided.' 
				});
				
			  }
			}
			else{

console.log(33);
		
next();
				  }
			
});

module.exports = router;




