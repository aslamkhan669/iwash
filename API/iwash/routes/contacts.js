    var express = require('express');
    var contactsDao = require('../dao/contactsDao');
    var contactsRepository = new contactsDao.contactsRepository();

    var router = express.Router();
    router.use(function (req, res, next) {
        next();
    });
   
    router.get('/', function (req, res) {
        contactsRepository.getAll().then(function (result) {

            res.json(result);
        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching users!');
            }
        });
    });

    router.post('/add',function(req,res){

         contactsRepository.addcontacts(req.body).then(function (result) {
            res.json(result);

        }).error(function (error) {
            if (!error || error == null) {
                return new Error('Error fetching Suggested service provider!');
            }
        });
    });

   
  


    module.exports = router;
    //# sourceMappingURL=user.js.map