var packagesDao = require("../dao/packagesDao");
var packagesRepository = new packagesDao.packagesRepository();
var models = require('../models');

var subscribepackageDao = require("../dao/subscribepackageDao");
var subscribepackageRepository = new subscribepackageDao.subscribepackageRepository();

var express = require("express");
var router = express.Router();

router.use(function(req, res, next) {
  next();
});

router.get("/current/subscription", function(req, res) {
	  var authuser=req.authuser.username;
  

			models.sequelize.query("SELECT subscribe_package.*,CURRENT_DATE() as currentdate,(DATE(createdAt) + INTERVAL duration MONTH) as expdate FROM subscribe_package where status=1 and payment_status=1 and user_id="+authuser+" HAVING currentdate < expdate",{ type: models.sequelize.QueryTypes.SELECT}).then(function(issub){ 

           
                if(issub.length){
                    models.sequelize.query("SELECT  subscribe_package.id as package, subscribe_services.*, sum(subscribe_services.quantity) as remaining FROM `subscribe_package` join subscribe_services on subscribe_package.id=subscribe_services.subscribe_id WHERE subscribe_package.payment_status=1 && subscribe_package.id="+issub[0].id+" having sum(subscribe_services.quantity) <=0",{ type: models.sequelize.QueryTypes.SELECT}).then(function(isLimitUsed){
                        console.log(isLimitUsed);
						
						subscribepackageRepository.findPackageUserId(req.authuser.username,issub[0].id).then(function(result) {

							if(result!=null){
								if(isLimitUsed.length){
                                     res.json({status:false,message:"No result"});

								}else{
								    res.json({status:true,result:result});
								}

							}  else{
								res.json({status:false,message:"No result"});
								
							}

							});
							
                      
                });
                    
                }
                else{
                    res.json({status:false,message:"No result"});
                    
                }


                });
	
});	




router.get("/subscriptions", function(req, res) {
    var authuser=req.authuser.username;
  

			models.sequelize.query("SELECT subscribe_package.*,CURRENT_DATE() as currentdate,(DATE(createdAt) + INTERVAL duration MONTH) as expdate FROM subscribe_package where status=1 and user_id="+authuser+" HAVING currentdate < expdate",{ type: models.sequelize.QueryTypes.SELECT}).then(function(issub){ 
			console.log(issub);

           
                if(issub.length){
                    models.sequelize.query("SELECT  subscribe_package.id as package, subscribe_services.*, sum(subscribe_services.quantity) as remaining FROM `subscribe_package` join subscribe_services on subscribe_package.id=subscribe_services.subscribe_id WHERE subscribe_package.payment_status=1 && subscribe_package.id="+issub[0].id+" having sum(subscribe_services.quantity) <=0",{ type: models.sequelize.QueryTypes.SELECT}).then(function(isLimitUsed){
                        console.log(isLimitUsed);
						console.log('isLimitUsed');
						subscribepackageRepository.findPackageUserId(req.authuser.username,issub[0].id).then(function(result) {
                        console.log(result);
							if(result!=null){
								if(isLimitUsed.length){
								     res.json({status:true,result:result,subscription:true,isLimitUsed:true});

								}else{
								    res.json({status:true,result:result,subscription:true,isLimitUsed:false});
								}

							}  else{
								res.json({status:false,message:"No result"});
								
							}

							});
                        
                      
                });
                    
                }
                else{
                    res.json({status:false,message:"No result"});
                    
                }


                });
	

});



router.get('/getsub', function (req, res) {
    
    
    var uid= req.query.user_id;
    
    packagesRepository.getAll(uid).then(function (result) {
             
        res.json(result);

    }).error(function (error) {
        if (!error || error == null) {
            return new Error('Error Fetching Records');
        }
    });
});



router.get("/single/:id", function(req, res) {
    packagesRepository.getById(req.params.id).then(function(result) {

                res.json({status:true,result:result});

    });

  });
router.get("/list", function(req, res) {
    packagesRepository.getAll().then(function(result) {

     
        var a = [],allResults = [],packageobj;
        
         if(result!=null){
            if(req.authuser){
            var authuser=req.authuser.username;
          
            models.sequelize.query("SELECT subscribe_package.*,CURRENT_DATE() as currentdate,(DATE(createdAt) + INTERVAL duration MONTH) as expdate FROM subscribe_package where status=1  and user_id="+authuser+" HAVING currentdate < expdate",{ type: models.sequelize.QueryTypes.SELECT}).then(function(issub){
                console.log(issub);
                    if(issub.length){
                        models.sequelize.query("SELECT  subscribe_package.id as package, subscribe_services.*, sum(subscribe_services.quantity) as remaining FROM `subscribe_package` join subscribe_services on subscribe_package.id=subscribe_services.subscribe_id WHERE subscribe_package.payment_status=1 && subscribe_package.id="+issub[0].id+" having sum(subscribe_services.quantity) <=0",{ type: models.sequelize.QueryTypes.SELECT}).then(function(isLimitUsed){
                            console.log(isLimitUsed);
                            
                            if(isLimitUsed.length){
                                res.json({status:true,result:result,subscription:issub[0],isLimitUsed:true});
                                
                                
                            }else{

                                res.json({status:true,result:result,subscription:issub[0],isLimitUsed:false});
                            }
                    });
                        
                    }
                    else{
                    res.json({status:true,result:result,subscription:false});
                    
                    }


                    });
                
             }else{
                res.json({status:true,result:result,subscription:false});
                
                            
           }
          

       }else{

                res.json({status:false,message:"No result"});

       }
    })
    .error(function(error) {
      if (!error || error == null) {
        return new Error("Error fetching records!");
      }
    });
});


router.get('/unsubscribe/:subscribeid', function (req, res) {
    
    var authuser=req.authuser.username;
   subscribepackageRepository.unsubscribe(authuser,req.params.subscribeid).then(function (result) {

       res.json(result);
   });
    

});
router.post('/subscribe', function (req, res) {
    
    var authuser=req.authuser.username;
    console.log(req.authuser);
    packagesRepository.getByIdBasic(req.body.package_id).then(function(packdetails){
        if(packdetails!=null){
            debugger;
        var price=packdetails.dataValues.pricing_for_each_month;
        var packageServices=packdetails.dataValues.package_services;
            var data={};
            // data=req.body;
             data.package_id=req.body.package_id;
             //data.pickup_option=req.body.pickup_option;
             data.duration=req.body.duration;             
             data.payment_amount=price*req.body.duration;             
             data.payment_status=0;
             data.isExpired=0;
             data.isLimitUsed=0;
             data.status=1;
             data.user_id=authuser;
             
           //  subscribepackageRepository.findIsSubscribed(authuser,req.body.package_id).then(function(subscription){

            models.sequelize.query("SELECT  subscribe_package.*,CURRENT_DATE() as currentdate,(DATE(createdAt) + INTERVAL duration MONTH) as expdate FROM subscribe_package where status=1  and user_id="+authuser+" and package_id="+req.body.package_id+" HAVING currentdate < expdate",{ type: models.sequelize.QueryTypes.SELECT}).then(function(issub){
                
             //   user_id:authuser,status:1,package_id:package_id


                    if(issub.length){
 
                        models.sequelize.query("SELECT  subscribe_package.id as package, subscribe_services.*, sum(subscribe_services.quantity) as remaining FROM `subscribe_package` join subscribe_services on subscribe_package.id=subscribe_services.subscribe_id WHERE subscribe_package.payment_status=1 && subscribe_package.id="+issub[0].id+" having sum(subscribe_services.quantity) <=0",{ type: models.sequelize.QueryTypes.SELECT}).then(function(isLimitUsed){
                            console.log(isLimitUsed);
                            
                            if(isLimitUsed.length){
                                subscribepackageRepository.create(data).then(function (result) {
                                    if(packdetails.dataValues.package_services!=null){
                                            
      
                                          var subscriptionid=result.dataValues.id;
                                          var servicesArray=[];
                                          for(var j=0;j<packageServices.length;j++){
                                                var  servicesobj=packageServices[j].dataValues;    
                                                
                                                servicesArray.push( { user_id:authuser, subscribe_id: subscriptionid, category_id: servicesobj.category_id, service_id: servicesobj.service_id, package_id: servicesobj.package_id, quantity: servicesobj.quantity });     
                                           }
      
                                              subscribepackageRepository.addSubscribeServices(servicesArray).then(function (serviceresult) {
                                                  
                                                  res.json({status:true,result:result,message:'Package Subscribed'});
                                                  
      
                                              })
      
                                      
      
                                    }
      
                                   
                               })
                                
                                
                            }else{

                                res.json({status:false,message:'Already subscribed!'});
                            }
                    });

                    }else{
                   subscribepackageRepository.create(data).then(function (result) {
                              if(packdetails.dataValues.package_services!=null){
                                      

                                    var subscriptionid=result.dataValues.id;
                                    var servicesArray=[];
                                    for(var j=0;j<packageServices.length;j++){
                                          var  servicesobj=packageServices[j].dataValues;    
                                            servicesArray.push( { user_id:authuser, subscribe_id: subscriptionid, category_id: servicesobj.category_id, service_id: servicesobj.service_id, package_id: servicesobj.package_id, quantity: servicesobj.quantity });     

                                     }

                                        subscribepackageRepository.addSubscribeServices(servicesArray).then(function (serviceresult) {
                                            
                                            res.json({status:true,result:result,message:'Package Subscribed'});
                                            

                                        })

                                

                              }

                             
                         }).error(function (error) {
                             if (!error || error == null) {
                                 return new Error('Error fetching records!');
                             }
                         });
         
                     }
         
         });
        }

        
    });
 



});



module.exports = router;
//# sourceMappingURL=user.js.map
