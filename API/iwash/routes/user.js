var express = require("express");
var userDao = require("../dao/userDao");
var models = require('../models');

var userRepository = new userDao.userRepository();

var social_account_sourcesDao = require("../dao/social_account_sourcesDao");
var social_account_sourcesRepository = new social_account_sourcesDao.social_account_sourcesRepository();
var social_accountsDao = require("../dao/social_accountsDao");
var social_accountsRepository = new social_accountsDao.social_accountsRepository();
var config = require('../config/config.json')
var crypto = require("crypto");
var app = express();
var jwt = require("jsonwebtoken");
var request = require("request");

var router = express.Router();

router.use(function (req, res, next) {
  next();
});

router.post("/loginsocial", function (req, res) {
  
  var network = req.body.network;
  var socialToken = req.body.socialToken;

  function validateWithProvider(network, socialToken, callback) {
    if (network == "facebook") {
      var profileresponse = "";
      request(
        {
          url: "https://graph.facebook.com/v2.2/me", //URL to hit
          qs: {
            access_token: socialToken,
            fields:
              "id,name,first_name,last_name,about,email,gender,location,picture",
            format: "json"
          },
          method: "GET" //Specify the method
        },
        function (error, response, body) {
          console.log(body);
          console.log(error);
          console.log(response.statusCode);
          if (!error && response.statusCode == 200) {
            var socialprofile = JSON.parse(body);
            var finalprofile = {};
            if (socialprofile.email != undefined) {
              finalprofile.email = socialprofile.email;
            } else {
              finalprofile.email = null;
            }
            finalprofile.first_name = socialprofile.first_name;
            finalprofile.last_name = socialprofile.last_name;
            if (socialprofile.mobile != undefined) {
              finalprofile.mobile = socialprofile.mobile;
            } else {
              finalprofile.mobile = null;
            }
            finalprofile.gender = socialprofile.gender;
            finalprofile.about_me = socialprofile.about;
            finalprofile.location = socialprofile.location;
            finalprofile.avatar = socialprofile.picture.data.url;
            //finalprofile.avatar = null;

            finalprofile.provider_id = socialprofile.id;
            finalprofile.username = socialprofile.email;
            profileresponse = finalprofile;
          } else {
            profileresponse = false;
          }

          return callback(profileresponse);
        }
      );
      //  return profileresponse;
    } else if (network == "gplus") {
      var profileresponse = "";
      request(
        {
          url: "https://www.googleapis.com/oauth2/v1/userinfo?alt=json",
          qs: { access_token: socialToken, format: "json" },
          method: "GET" //Specify the method
        },
        function (error, response, body) {
          console.log(body);
          console.log(error);
          console.log(response.statusCode);

          console.log("verify");
          if (!error && response.statusCode == 200) {
            console.log("verify in");

            var socialprofile = JSON.parse(body);
            var finalprofile = {};
            if (socialprofile.email != undefined) {
              finalprofile.email = socialprofile.email;
            } else {
              finalprofile.email = null;
            }
            finalprofile.first_name = socialprofile.name;
            finalprofile.last_name = null;
            if (socialprofile.mobile != undefined) {
              finalprofile.mobile = socialprofile.mobile;
            } else {
              finalprofile.mobile = null;
            }
            finalprofile.gender = socialprofile.gender;
            finalprofile.about_me = null;
            finalprofile.location = null;
            finalprofile.avatar = socialprofile.picture;
            finalprofile.provider_id = socialprofile.id;
            finalprofile.username = socialprofile.email;
            profileresponse = finalprofile;
            // return finalprofile;
          } else {
            profileresponse = false;
          }
          return callback(profileresponse);
        }
      );

      //  console.log(profileresponse);
      //return profileresponse;
    }
  }

  function generateToken(uniqueID, network) {
    var raw = {};
    raw.username = uniqueID;
    raw.logintype = network;
    raw.date = new Date();
    raw.random = Math.floor(
      Math.random() * (10000000000 - 50000000) + 50000000
    );
    var token = jwt.sign(
      raw,
      "eyJ1c2VybmFtZSI6ILTE5VDA3OjIyOjE5LjgImRhdGUiOiIyMDE2LTELTE5VDA3OjIyOjE5Lj3MloiLCJyYW5kb20iiIsg3MloiLCJyYW5kb20ixOjEwNTg1NzEzODUsImlhdCI6MTQ3OTU0MDEzOSwiZXhwI",
      {
        expiresIn: "60 days" // expires in 7 days
      }
    );
    return token;
  }

  function CreateUpdateProfile(network, socialprofile) {
    social_account_sourcesRepository
      .findBySource(network)
      .then(function (source) {
        social_accountsRepository
          .findUserSocialAccountByProvider(
          socialprofile.provider_id,
          source.dataValues.id
          )
          .then(function (socialresult) {
            console.log(socialresult);
            if (socialresult === null) {
              if (socialprofile.email != null) {
                userRepository
                  .findByEmail(socialprofile.email)
                  .then(function (user) {

                    if (user === null) {
                      var userdata = {};
                      userdata.email = socialprofile.email;
                      var profiledata = {};
                      profiledata.first_name = socialprofile.first_name;
                      profiledata.last_name = socialprofile.last_name;
                      //profiledata.mobile = socialprofile.mobile;
                      profiledata.about_me = socialprofile.about;
                      profiledata.location = socialprofile.location;

                      userRepository
                        .addSocialUser(userdata)
                        .then(function (result) {
                          userRepository
                            .addProfile(profiledata, result)
                            .then(function (profile) {
                              //social_account_sourcesRepository.findBySource(network).then(function (source) {
                              var socialdata = {};
                              socialdata.user_id = result.dataValues.id;
                              socialdata.souce_account_source_id =
                                source.dataValues.id;
                              socialdata.avatar = socialprofile.avatar;
                              socialdata.provider_id =
                                socialprofile.provider_id;
                              socialdata.username = socialprofile.username;
                              social_accountsRepository
                                .addSocial(socialdata)
                                .then(function (usercreated) {
                                  res.json({
                                    success: true,
                                    message: "Successfully logged in ",
                                    userId: result.dataValues.id,
                                    roleId: 2,
                                    token: generateToken(result.dataValues.id, network)
                                  });
                                });
                              //});
                            });
                        });
                    } else {

                      if (user.dataValues.role_id == 2) {
                        var socialdata = {};
                        socialdata.user_id = user.dataValues.id;
                        socialdata.souce_account_source_id = source.dataValues.id;
                        socialdata.avatar = socialprofile.avatar;
                        socialdata.provider_id = socialprofile.provider_id;
                        socialdata.username = socialprofile.username;
                        social_accountsRepository
                          .addSocial(socialdata)
                          .then(function (usercreated) {
                            res.json({
                              success: true,
                              message: "Successfully logged in ",
                              userId: user.dataValues.id,
                              roleId: 2,
                              token: generateToken(user.dataValues.id, network)
                            });
                          });

                      } else {
                        res.json({ success: false, message: 'Authentication failed.' });


                      }

                    }
                  });
              } else {
                var userdata = {};
                userdata.email = socialprofile.email;
                var profiledata = {};
                profiledata.first_name = socialprofile.first_name;
                profiledata.last_name = socialprofile.last_name;
                //profiledata.mobile = socialprofile.mobile;
                profiledata.about_me = socialprofile.about;
                profiledata.location = socialprofile.location;

                userRepository.addSocialUser(userdata).then(function (result) {
                  userRepository
                    .addProfile(profiledata, result)
                    .then(function (profile) {
                      //social_account_sourcesRepository.findBySource(network).then(function (source) {
                      var socialdata = {};
                      socialdata.user_id = result.dataValues.id;
                      socialdata.souce_account_source_id = source.dataValues.id;
                      socialdata.avatar = socialprofile.avatar;
                      socialdata.provider_id = socialprofile.provider_id;
                      socialdata.username = socialprofile.username;
                      social_accountsRepository
                        .addSocial(socialdata)
                        .then(function (usercreated) {
                          res.json({
                            success: true,
                            message: "Successfully logged in ",
                            userId: result.dataValues.id,
                            roleId: 2,
                            token: generateToken(result.dataValues.id, network)
                          });
                        });
                      //});
                    });
                });
              }
            } else {
              res.json({
                success: true,
                message: "Successfully logged in ",
                userId: socialresult.dataValues.user_id,
                roleId: 2,
                token: generateToken(socialresult.dataValues.user_id, network)
              });
            }
          });
      });
  }

  if (network != null && socialToken != null) {
    validateWithProvider(network, socialToken, function (profile) {
      console.log("callback");

      console.log("profile");
      console.log(profile);
      console.log("profile");

      if (profile != false && profile != undefined) {
        CreateUpdateProfile(network, profile);
      } else {
        res.json({ success: false, message: "Authentication failed." });
      }

      console.log(profile);
    });
  } else {
    res.json({ success: false, message: "Authentication failed." });
  }
});





router.post('/register', function (req, res) {

  userRepository.findbyMobile(req.body.mobile).then(function (result) {
    if (result === null) {

      userRepository.findByEmail(req.body.email).then(function (userexists) {
        if (userexists === null) {

          var userdata = {};
          userdata.email = req.body.email;
          userdata.mobile = req.body.mobile;
          userdata.password = req.body.password;
          userdata.mobile_confirm_code = 'BM' + Math.floor(100000 + Math.random() * 900000).toString();
          userdata.mobile_confirmed = 0;
          userdata.confirmed = '1';
          userdata.active = '1';
          var profiledata = {};
          profiledata.first_name = req.body.fname;
          profiledata.last_name = req.body.lname;
          profiledata.about_me = '';
          profiledata.avatar = '';


          userRepository.addUser(userdata).then(function (result) {
            userRepository.addProfile(profiledata, result).then(function (profile) {
              res.json({ success: true, message: "success" });;



            });
          });


        }
        else {
          res.json({ success: false, message: "User with this email  already registered" });

        }


      });





    } else {

      res.json({ success: false, message: "User with this mobile no already registered" });
    }


  });




});






//get user profile details by id
router.post('/details/:id', function (req, res) {
  var authuser = req.authuser.username;
  var logintype = '';
  if (req.authuser.logintype != undefined && req.authuser.logintype == 'facebook') {

    logintype = 'facebook';
  } else if (req.authuser.logintype != undefined && req.authuser.logintype == 'gplus') {

    logintype = 'gplus';
  } else {
    logintype = 'simple';

  }
  userRepository.getUserDetails(authuser).then(function (result) {

    if (result === null) {
      res.send({ success: false, message: "No records found" });
    } else {
      if (result.dataValues.user_profile) {
        var fname = result.dataValues.user_profile.dataValues.first_name;
        var lname = result.dataValues.user_profile.dataValues.last_name;
        if (logintype == 'simple') {
          res.send({

            email: result.dataValues.email,
            name: fname + " " + lname,
            role: result.dataValues.role.dataValues,
            avatar: result.dataValues.user_profile.dataValues.avatar

          });
        } else {
          models.sequelize.query("SELECT social_accounts.avatar from social_accounts join  social_account_sources on social_accounts.souce_account_source_id=social_account_sources.id  where social_accounts.user_id=" + authuser + " AND social_account_sources.source='" + logintype + "'",
            { type: models.sequelize.QueryTypes.SELECT }).then(function (socialresult) {
              if (socialresult) {
                res.send({

                  email: result.dataValues.email,
                  name: fname + " " + lname,
                  role: result.dataValues.role.dataValues,
                  avatar: null,
                  social_avatar: socialresult[0].avatar

                });
              } else {
                res.send({

                  email: result.dataValues.email,
                  name: fname + " " + lname,
                  role: result.dataValues.role.dataValues,
                  avatar: result.dataValues.user_profile.dataValues.avatar,
                  social_avatar: null

                });
              }

            });

        }


      }
    }

  });
});



router.post('/authenticate', function (req, res) {

  //console.log(req.body.email);
  if (req.body.username.toString().indexOf("@") != -1) {

    userRepository.findByEmail(req.body.username).then(function (user) {

      if (!user) {
        res.json({ success: false, message: 'Authentication failed. Email not found.' });
      } else if (user) {
        if (user.dataValues.active == '1') {
          if (user.dataValues.password != crypto.createHmac("md5", config.salt).update(req.body.password).digest('hex')) {
            res.json({ success: false, message: 'Authentication failed. Invalid credentials.' });
          } else {

            // if user is found and password is right
            // create a token
            var raw = {};
            raw.logintype = 'simple';
            raw.username = user.dataValues.id;
            raw.date = new Date();
            raw.random = Math.floor(Math.random() * (10000000000 - 50000000) + 50000000);
            var token = jwt.sign(raw, 'eyJ1c2VybmFtZSI6ILTE5VDA3OjIyOjE5LjgImRhdGUiOiIyMDE2LTELTE5VDA3OjIyOjE5Lj3MloiLCJyYW5kb20iiIsg3MloiLCJyYW5kb20ixOjEwNTg1NzEzODUsImlhdCI6MTQ3OTU0MDEzOSwiZXhwI', {
              expiresIn: '60 days' // expires in 7 days

            });


            // return the information including token as JSON
            res.json({
              success: true,
              message: 'success',
              userId: user.dataValues.id,
              roleId: user.dataValues.role_id,
              email: user.dataValues.email,
              token: token
            });
          }


        } else {
          res.json({ success: false, message: 'Authentication failed. Email id not verified.' });

        }



      }
    });

  } else {
    res.json({ success: false, message: 'Authentication failed.' });

  }
});
module.exports = router;
