module.exports = function (sequalize, DataTypes) {
    return sequalize.define('crud', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

        name: {
            type: DataTypes.STRING(255),
            allowNull: false


        },

        city: {
            type: DataTypes.STRING(255),
            allowNull: false
        }

  },
  {
    timestamps: false,
    freezeTableName:true
},
  {
     tableName: 'crud'
  });
};
