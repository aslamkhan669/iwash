/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('monthly_packages', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    package: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    usage_limit: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    pickup_options: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    available_packages: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    pricing_for_each_month: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    }
  }, {
    tableName: 'monthly_packages'
  });
};
