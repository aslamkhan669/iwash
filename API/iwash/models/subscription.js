/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('subscription', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    package_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    // pickup_option: {
    //   type: DataTypes.STRING(255),
    //   allowNull: false
    // },
    duration: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },

    quantity_used: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },

    payment_status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
      
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
      
    },
    active: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      
      
    },
    isExpired: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
      
    },

    end_date: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    },
    
    start_date: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    },
    
    isLimitUsed: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
      
    }
    
    ,
    amount_paid: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    payment_date: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    created_at: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_at: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: '0000-00-00 00:00:00'
    }
  }, 
  
  {
    timestamps: false,
    freezeTableName:true
    
},
  {
    tableName: 'subscription'
  });
};
