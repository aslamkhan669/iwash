/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('subscribe_package', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    package_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    pickup_option: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    duration: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },

    payment_status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
      
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
      
    },
    isExpired: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
      
    },
    isLimitUsed: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
      
    }
    
    ,
    payment_amount: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    payment_date: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    }
  }, {
    tableName: 'subscribe_package'
  });
};
