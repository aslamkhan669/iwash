/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('order', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    service_cat_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
	subscription_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
	  },
	
    qr: {
      type: DataTypes.STRING(255),
      allowNull: true,
	  
    },
	 order_type: {
      type: DataTypes.STRING(20),
      allowNull: true,
	  
    },
	
	
    totalamount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    qty: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
	  defaultValue:0

    },
    branch: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
	added_by: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    flatandstreet: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    landmark: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    addressType: {
      type: DataTypes.ENUM('home','office','other'),
      allowNull: false
    },
    pickupDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    deliveryDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    timeOfDelivery: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    timeOfPickup: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('placed','accepted','picked up','processing','ready to deliver','delivered','cancelled'),
      allowNull: false,
	      defaultValue: 'placed'
    },
    payment_status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    payment_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    }
  }, {
    tableName: 'order'
  });
};
