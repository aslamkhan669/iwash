/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('services', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    category_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    price_wash: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    price_dryclean: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    price_iron: {
      type: DataTypes.FLOAT,
      allowNull: true
    },

    per: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    },
    image_website: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    image_app: {
      type: DataTypes.STRING(255),
      allowNull: true
	  },
    image_banner: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    
  }, {
    tableName: 'services'
  });
};
