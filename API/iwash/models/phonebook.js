module.exports = function (sequalize, DataTypes) {
    return sequalize.define('phonebook', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

        number: {
            type: DataTypes.INTEGER(11),
            allowNull: false    


        },


  },
  {
    timestamps: false,
    freezeTableName:true
},
  {
     tableName: 'phonebook'
  });
};
