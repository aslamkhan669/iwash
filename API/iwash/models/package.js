/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('package', {
      id: {
        type: DataTypes.INTEGER(20),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(20),
        allowNull: false
      },
      qty_clothes: {
        type: DataTypes.INTEGER(20),
        allowNull: false
      },
      price: {
        type: DataTypes.INTEGER(20),
        allowNull: false
      },
      description: {
        type: DataTypes.STRING(11),
        allowNull: false
        
      },
     
    
    },
    {
        timestamps: false,
        freezeTableName:true
    },
    
    {
      tableName: 'package'
    });
  };
  