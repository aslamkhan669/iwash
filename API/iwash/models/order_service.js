/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('order_service', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    order_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    service_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    qty_w: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    qty_dc: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    qty_ir: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    price_dc: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    price_ir: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    price_w: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    }
  },
 
  
  {
    tableName: 'order_service'
  });
};
