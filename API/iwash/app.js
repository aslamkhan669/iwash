var express = require('express');
var rootPath = require('./routes/index');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');


var userRoute = require("./routes/user");
var testimonialsRoute = require("./routes/testimonials");
var faqsRoute = require("./routes/faqs");
var categoriesRoute = require("./routes/category");
var servicesRoute = require("./routes/services");
var bannerRoute = require('./routes/banner');
var orderRoute = require('./routes/order');
var packageRoute= require('./routes/package');
var https=require('https');
var fs = require('fs');
var testRoute=require('./routes/Test');
var crudRoute=require('./routes/crud');
var pbookRoute=require('./routes/phonebook');
var newpackageRoute=require('./routes/newpackage');

var app = express();
app.use(cors());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Origin, Authorization, Content-Length, Accept, X-Requested-With, xaccesstoken');
  if (req.method === 'OPTIONS') {
      return res.end();
  }
  next();

});
app.disable('etag')
app.use('/api/uploads', express.static(__dirname + '/uploads'));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use('/api/uploads', express.static(__dirname + '/uploads'));


app.use('/', rootPath);
app.use('/api/user', userRoute);
app.use('/api/testimonials', testimonialsRoute);
app.use('/api/faqs', faqsRoute);
app.use('/api/categories', categoriesRoute);
app.use('/api/services', servicesRoute);
app.use('/api/banner', bannerRoute);
app.use('/api/package', packageRoute);
app.use('/api/Test',testRoute);
app.use('/api/order', orderRoute);
app.use('/api/crud',crudRoute);
app.use('/api/phonebook',pbookRoute);
app.use('/api/newpackage',newpackageRoute);



app.use(express.static(path.join(__dirname, 'uploads')));

// app.listen(3000, function () {
  // console.log('Example app listening on port 3000!')
// });

 var newapp=https.createServer({
  key: fs.readFileSync('/etc/letsencrypt/live/iwashqatar.com/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/iwashqatar.com/cert.pem')
  },app).listen(3000, function () {
     console.log('express server listening on port ' + 3000);
});