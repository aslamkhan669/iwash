var models = require('../models');

var sequelize = require('sequelize');

var crypto = require('crypto');

var config    = require('../config/config.json')

 var userRepository = (function (){
                    function userRepository() {
                       // var _dbConfig = new dbConfig.Config();
                    }
                    
                    //get all users 
                    userRepository.prototype.getAll = function () {
                        return models.users.findAll();
                    }
                    userRepository.prototype.getuserCategory = function (id) {
                        var findOptions = {};
                        findOptions.where = { user_id: id };
                        findOptions.attributes=['category_id']
                        return models.user_profiles.find(findOptions);
                    }

                    //get user details by id
                    userRepository.prototype.findById = function (id) {
                        var findOptions = {};
                        findOptions.where = { id: id};

                        return models.users.find(findOptions);
                    }

                     userRepository.prototype.findByuserIdEmail = function (id,emai) {
                        var findOptions = {};
                        findOptions.where = { id: id,emai: emai};

                        return models.users.find(findOptions);
                    }

                 userRepository.prototype.findavailabilityById = function (id) {

                        var findOptions = {};
                 
                        findOptions.where = { user_id: id};
                        findOptions.attributes=['service_at']
                        return models.user_profiles.find(findOptions);
                    }

                 userRepository.prototype.getusersByLocationForFeeds = function (location) {
                 var queryJson=[];
                        for(var i =0;i<location.length;i++){
                            queryJson.push({location:{$like:'%'+location[i]+'%'}});
                        }
                        var findOptions = {};
                 
                        findOptions.where ={
                                $or:queryJson
                              };
                        findOptions.attributes=['user_id']
                        return models.user_profiles.findAll(findOptions);
                    }



                    //get user details by email 
                    userRepository.prototype.findByEmail = function (email) {
                        var findOptions = {};
                        findOptions.where= { Email: email };
                        return models.users.find(findOptions);
                    }
                     userRepository.prototype.findByEmailAndConfirmed = function (email) {
                        var findOptions = {};
                        findOptions.where= { Email: email,confirmed:'1'};
                        return models.users.find(findOptions);
                    }  
                    userRepository.prototype.findbyMobile = function (mobile) {
                        var findOptions = {};
                        findOptions.where= { mobile: mobile };
                        return models.users.find(findOptions);

                    }
                userRepository.prototype.findbyUserId= function (user_id) {
                        var findOptions = {};
                        findOptions.where= { user_id: user_id };
                        return models.user_profiles.find(findOptions);

                    }
                 userRepository.prototype.findByEmailOrName = function (email) {
                     models.users.hasOne(models.user_profiles,{foreignKey:'user_id'});
                      models.users.hasOne(models.follow,{foreignKey:'follower_id'});
                 //models.user_profiles.hasOne(models.category,{foreignKey:'category_id'});
                        var findOptions = {};
                        findOptions.where= { Email:email };
                          findOptions.include = 
                        [
                            {model:models.follow, attributes:['id','follower_id','following_id']},
                            {model:models.user_profiles, attributes:['first_name','last_name','about_me','mobile','location','avatar','category_id','enlisted']},
                          
                           //{model:models.category,attributes:['id','name']}
                        ];
                        return models.users.find(findOptions);
                    }


                 //add new user / register a user
                    userRepository.prototype.addSocialUser = function (user) {
                        //setting default values for the user

                      
                        
                        //register user as a seeker initially
                        user.role_id = 2;
                          user.confirmed ='0';
                        user.mobile_confirmed = 0;
                        user.active ='1';
                        user.createdAt = new Date();
                        user.updatedAt = new Date();
                        
                       
                        //user confirmation code 
                       
                        //send welcome email to the user for registation confirmation 
                        //mailerDao.testmail();
                        var buildOptions = {};
                        buildOptions.isNewRecord = true;
                        buildOptions.raw = true;
                        var newUser = models.users.build(user, buildOptions);
                        return newUser.save();
                    }

                    //add new user / register a user
                    userRepository.prototype.addUser = function (user) {
                        //setting default values for the user

                      
                        
                        //register user as a seeker initially
                        user.role_id = 2;
                        user.confirmed ='0';
                        user.mobile_confirmed = 0;
                        user.active ='1';
                        user.password = crypto.createHmac("md5",config.salt).update(user.password).digest('hex');
                        user.confirmation_code = crypto.createHmac("md5",config.confirmation_salt).update(user.password).digest('hex');
                        user.createdAt = new Date();
                        user.updatedAt = new Date();
                        
                       
                        //user confirmation code 
                       
                        //send welcome email to the user for registation confirmation 
                        //mailerDao.testmail();
                        var buildOptions = {};
                        buildOptions.isNewRecord = true;
                        buildOptions.raw = true;
                        var newUser = models.users.build(user, buildOptions);
                        return newUser.save();
                    }

                    //update user details
                    userRepository.prototype.updateUser = function (user) {

                        user.password=crypto.createHmac("md5",config.salt).update(user.password).digest('hex');
                        user.updated_at = new Date();
                        var updatedOptions = {};
                        updatedOptions.where = { id: user.user_id };
                        var newUser = models.users.update(user, updatedOptions);
                        return newUser;
                    }
					
					userRepository.prototype.confirmemail = function (user,user_id) {

                         var user={};
                             user.confirmed='1';  
                        user.updated_at = new Date();
                        var updatedOptions = {};
                        updatedOptions.where = { id:user_id };
                        var newUser = models.users.update(user, updatedOptions);
                        return newUser;
                    }
          userRepository.prototype.updateUserNewemailAndResetcode = function (userobj,confirmation_code) {
var user={};
                      user.confirmed =0;
                      user.email =userobj.email;
                     user.confirmation_code =confirmation_code;


                        user.updated_at = new Date();
                        var updatedOptions = {};
                        updatedOptions.where = { id:userobj.user_id };
                        var newUser = models.users.update(user, updatedOptions);
                        return newUser;
                    }

                     userRepository.prototype.updateUserRole = function (user) {
                        user.id = user.user_id;
                        
                        user.updated_at = new Date();
                        var updatedOptions = {};
                        updatedOptions.where = { id: user.id };
                        var newUser = models.users.update(user, updatedOptions);
                        return newUser;
                    }
                    
                    //update profile
                    userRepository.prototype.updateProfile = function (user,userid) {
                        user.updated_at = new Date();
                        var updatedOptions = {};
                        updatedOptions.where = { user_id:userid};
                        var updateUser = models.user_profiles.update(user, updatedOptions);
                        return updateUser;
                    }
                userRepository.prototype.updateProfileBymobile = function (user,mobile) {
                        user.updated_at = new Date();
                        var updatedOptions = {};
                        updatedOptions.where = { mobile:mobile,mobile_confirmed:'1'};
                        var updateUser = models.user_profiles.update(user, updatedOptions);
                        return updateUser;
                    }
                    //add user profile details used only after registration 
                    userRepository.prototype.addProfile = function(user,data)
                    {   
                        var profile = {};
                        profile.first_name = user.first_name;
                    
                        profile.mobile = user.mobile
                       if(user.last_name!=null){

                           profile.last_name =user.last_name;
                        }
                         if(user.location!=null){

                           profile.location =user.location;
                        }
                        if(user.avatar!=null){

                        profile.avatar =user.avatar;
                        }
                  
                        profile.user_id =data.dataValues.id;
                        profile.created_at = data.dataValues.created_at;
                        profile.updated_at = data.dataValues.updated_at;
                        var buildOptions = {};
                        buildOptions.isNewRecord = true;
                        buildOptions.raw = true;
                        var newProfile = models.user_profiles.build(profile, buildOptions);
                        return newProfile.save();
                    }

                    //ger user follower count 
                    userRepository.prototype.getFollowerCount = function(id){
                        var findOptions = {}; 
                        findOptions.where = { follower_id : id };
                        findOptions.attributes = [[sequelize.fn('COUNT', sequelize.col('following_id')), 'Followers']];
                        return models.follow.find(findOptions); 
                    }
                    //get user following count
                    userRepository.prototype.getFollowingCount = function(id){
                        var findOptions = {}; 
                        findOptions.where = { following_id : id };
                        findOptions.attributes = [[sequelize.fn('COUNT', sequelize.col('follower_id')), 'Following']];
                        return models.follow.find(findOptions); 
                    }
                    //get user reviews count 
                    userRepository.prototype.getReviewsCount = function(id){
                        var findOptions = {}; 
                        findOptions.where = { to_id : id };
                        findOptions.attributes = [[sequelize.fn('COUNT', sequelize.col('reviews.to_id')), 'Reviews']];
                        return models.reviews.find(findOptions);
                    }
                     userRepository.prototype.getReviewedByCount = function(id){
                        var findOptions = {}; 
                        findOptions.where = { to_id : id };
                        findOptions.attributes = [[sequelize.fn('COUNT', sequelize.col('reviews.from_id')), 'ReviewedBy']];
                        var data = new Get(models.reviews.find(findOptions)).then(function(result){
                                data = result.dataValues;
                        });
                        console.log(data);
                        return data;
                    }
                    //get user role
                    userRepository.prototype.getUserRole = function(id){
                        models.users.belongsTo(models.roles, {foreignKey:'role_id',targetKey:'id'});
                        var findOptions = {};
                        findOptions.where = {id:id};
                        findOptions.include = [{model:models.roles, attributes:['id','title','slug']}];
                        findOptions.attributes = ['id'];
                        return models.users.find(findOptions);     


                    }
                    //get user details 
                    userRepository.prototype.getUserDetails = function(id)
                    {
                        models.users.belongsTo(models.roles, {foreignKey: 'role_id',targetKey: 'id'});
                     
                        models.users.hasOne(models.user_profiles,{foreignKey:'user_id'});
                  
                     
                        
                        
                        var findOptions = {}; 
                        findOptions.where = { id : id };
                        findOptions.include = 
                        [
                             {model:models.roles, attributes:['id','title','slug']},
                            {model:models.user_profiles},
                        
                        ];
                                
                       return models.users.find(findOptions);      
                  
                    }
                    //Get user details from two tables by tahseen
                    userRepository.prototype.getUserDetailsByTahseen = function (id) {
                        models.users.hasOne(models.user_profiles,{foreignKey:'user_id'});
                        
                    
                           var findOptions = {};
                           //findOptions.where = { id: };
                             findOptions.include = 
                           [
                             
                              {model:models.user_profiles, attributes:['first_name','last_name']},
                             
                              
                           ];
                           findOptions.attributes = ["id","email"];
                           return models.users.findAll(findOptions);
                       }
   
                   
                    userRepository.prototype.verifyEmail = function (id,code) {
                        var updatedOptions = {};
                        var user = {};
                        user.confirmed =1; //enum field takes index of the enum 
                        user.updated_at = new Date();
                        updatedOptions.where = { id: id,confirmation_code:code };
                        return models.users.update(user, updatedOptions);
                    }
    
    return userRepository;
})();
exports.userRepository = userRepository;
