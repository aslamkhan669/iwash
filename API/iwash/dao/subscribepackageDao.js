var models = require('../models');


   
var subscribepackageRepository = (function () {
    function subscribepackageRepository() {
	

    }
   
   
    subscribepackageRepository.prototype.addSubscribeServices = function (data) {
     
        var buildOptions = {};
        buildOptions.isNewRecord = true;
        buildOptions.raw = true;
        return models.subscribe_services.bulkCreate(data, buildOptions);
       
    }
    subscribepackageRepository.prototype.unsubscribe = function (authuser,id) {
        
        var updatedOptions = {};
        updatedOptions.where = { user_id:authuser,id:id};

        var options={};
        options.status =0;
        options.active=0;
        options.updatedAt = new Date();
     
      
      return models.subscription.update(options, updatedOptions);



    }
    subscribepackageRepository.prototype.create = function (subscribe) {

            var buildOptions = {};
            buildOptions.isNewRecord = true;
            buildOptions.raw = true;
            return  models.subscription.build(subscribe, buildOptions).save();
    }

    subscribepackageRepository.prototype.findByPackageId = function (authuser,id) {
        var findOptions = {};  
        findOptions.where = {package_id: id,user_id:authuser};

        return models.subscribe_package.find(findOptions);
    };

    subscribepackageRepository.prototype.findPackageUserId = function (authuser,id) {
        models.subscribe_package.belongsTo(models.monthly_packages, {foreignKey:'package_id',targetKey:'id'});   
        models.subscribe_package.hasMany(models.subscribe_services, {foreignKey:'subscribe_id',targetKey:'id'});   
         models.subscribe_services.belongsTo(models.categories, {foreignKey:'category_id',targetKey:'id'});   
            models.subscribe_services.belongsTo(models.services, {foreignKey:'service_id',targetKey:'id'});    


        var findOptions = {};
        findOptions.where = {id: id,user_id:authuser,status:1};
        
        //findOptions.having='CURRENT_DATE() < DATE(createdAt) + INTERVAL duration MONTH';
        //SELECT subscribe_package.*, CURRENT_DATE() as currentdate,(DATE(createdAt) + INTERVAL duration MONTH) as expdate FROM subscribe_package where status=1 and user_id="+authuser+" HAVING currentdate < expdate 
        findOptions.include = [
            { model: models.monthly_packages},
            {model:models.subscribe_services,include:[{model:models.categories},{model:models.services}]}

        ];
        return models.subscribe_package.find(findOptions);
    };

    subscribepackageRepository.prototype.findIsSubscribed = function (authuser,package_id) {
      


        var findOptions = {};
        findOptions.where = {user_id:authuser,status:1,package_id:package_id};
     
        return models.subscribe_package.find(findOptions);
    };
    
    return subscribepackageRepository;
})();
exports.subscribepackageRepository = subscribepackageRepository;
//# sourceMappingURL=userDao.js.map,



