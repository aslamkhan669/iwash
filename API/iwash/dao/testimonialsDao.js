var models = require('../models');


   
var testimonialsRepository = (function () {
    function testimonialsRepository() {
	

    }
   
   
   
    testimonialsRepository.prototype.getAll= function () {
        return models.testimonials.findAll();
    };



    testimonialsRepository.prototype.findById = function (id) {
        var findOptions = {};
       
        findOptions.where = {id: id};
        return models.testimonials.find(findOptions);
    };
    
   
    
    return testimonialsRepository;
})();
exports.testimonialsRepository = testimonialsRepository;
//# sourceMappingURL=userDao.js.map,