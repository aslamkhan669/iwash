var models = require('../models');

var sequelize = require('sequelize');
var social_account_sourcesRepository = (function () {
    function social_account_sourcesRepository() {

    }
   
    
   

    social_account_sourcesRepository.prototype.findBySource = function (source) {
        var findOptions = {};
       
        findOptions.where = {source: source};
        return models.social_account_sources.find(findOptions);
    };
    
    

    
    return social_account_sourcesRepository;
})();
exports.social_account_sourcesRepository = social_account_sourcesRepository;
//# sourceMappingURL=userDao.js.map,