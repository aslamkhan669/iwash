var models = require('../models');


   
var packagesRepository = (function () {
    function packagesRepository() {
	

    }
   
     packagesRepository.prototype.getById= function (id) {

      models.monthly_packages.hasMany(models.package_services, {foreignKey:'package_id',targetKey:'id'});   
            models.package_services.belongsTo(models.categories, {foreignKey:'category_id',targetKey:'id'});   
            models.package_services.belongsTo(models.services, {foreignKey:'service_id',targetKey:'id'});   

        var findOptions = {};
        findOptions.include = [
                {model:models.package_services,include:[{model:models.categories},{model:models.services}]}

        ];
        findOptions.where = { id:id };


        return models.monthly_packages.find(findOptions);
    };

     packagesRepository.prototype.getByIdBasic= function (id) {
        var findOptions = {};
        models.monthly_packages.hasMany(models.package_services, {foreignKey:'package_id',targetKey:'id'});   
        findOptions.include = [
            {model:models.package_services}

    ];
        findOptions.where = { id:id };


        return models.monthly_packages.find(findOptions);
     };
   
    // packagesRepository.prototype.getAll= function () {

    //   models.monthly_packages.hasMany(models.package_services, {foreignKey:'package_id',targetKey:'id'});   
    //         models.package_services.belongsTo(models.categories, {foreignKey:'category_id',targetKey:'id'});   
    //         models.package_services.belongsTo(models.services, {foreignKey:'service_id',targetKey:'id'});   

    //     var findOptions = {};
    //     findOptions.include = [
    //             {model:models.package_services,include:[{model:models.categories},{model:models.services}]}

    //     ];


    //     return models.monthly_packages.findAll(findOptions);
    // };
   
    packagesRepository.prototype.getAll= function (uid) {

       
        models.subscription.belongsTo(models.package,{foreignKey:'package_id'});
        var findOptions ={};
        findOptions.include = [{model:models.package}];
        findOptions.where={user_id:uid,status:1}
        return models.subscription.find(findOptions);

        };
        


    packagesRepository.prototype.Create = function (addpackage) {
        var buildOptions = {};
        buildOptions.isNewRecord = true;
        var newpackage = models.package.build(addpackage, buildOptions);
        return newpackage.save();
    };
  

    return packagesRepository;
})();
exports.packagesRepository = packagesRepository;
//# sourceMappingURL=userDao.js.map,