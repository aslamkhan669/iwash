var models = require('../models');

var pbookRepository = (function () {
    function pbookRepository() {


    }
    pbookRepository.prototype.getAll = function () {
        return models.phonebook.findAll();
    };

    pbookRepository.prototype.findById = function (id) {
        var findOptions = {};
        findOptions.where = { id: id };
        return models.phonebook.find(findOptions);
    };

    pbookRepository.prototype.Create = function (phonebook) {
        var buildOptions = {};
        buildOptions.isNewRecord = true;
        var newpbook = models.phonebook.build(phonebook, buildOptions);
        return newpbook.save();
    };

    pbookRepository.prototype.Update = function (phonebook) {
        var updatedOptions = {};
        var obj = {number:parseInt(phonebook.number)};
        updatedOptions.where = { id: parseInt(phonebook.id) };
        var newpbook = models.phonebook.update(obj, updatedOptions);
        return newpbook;
    };

    pbookRepository.prototype.Delete = function (id) {
        var findOptions = {};
        findOptions.where = { id: id };
        return models.phonebook.destroy(findOptions);
    };


    return pbookRepository;


})();

exports.pbookRepository = pbookRepository;