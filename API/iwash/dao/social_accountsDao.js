var models = require('../models');

var sequelize = require('sequelize');
var social_accountsRepository = (function () {
    function social_accountsRepository() {

    }
   
    
   

    social_accountsRepository.prototype.findUserSocialAccount = function (user_id,souce_account_source_id) {
        var findOptions = {};
       
        findOptions.where = {user_id: user_id,souce_account_source_id:souce_account_source_id};
        return models.social_accounts.find(findOptions);
    };
	
    social_accountsRepository.prototype.findUserSocialAccountByProvider = function (provider_id,souce_account_source_id) {
        var findOptions = {};
       
        findOptions.where = {provider_id: provider_id,souce_account_source_id:souce_account_source_id};
        return models.social_accounts.find(findOptions);
    };
	
	

     social_accountsRepository.prototype.addSocial = function (social_accounts) {
        var buildOptions = {};
        buildOptions.isNewRecord = true;
        buildOptions.raw = true;
        social_accounts.createdAt = social_accounts.updatedAt = new Date();
        var newsocial_accounts = models.social_accounts.build(social_accounts, buildOptions);
        return newsocial_accounts.save();
    };
    
    
    

    
    return social_accountsRepository;
})();
exports.social_accountsRepository = social_accountsRepository;
//# sourceMappingURL=userDao.js.map,