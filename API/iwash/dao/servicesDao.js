var models = require('../models');


   
var servicesRepository = (function () {
    function servicesRepository() {
	

    }
   
   
   
    servicesRepository.prototype.getAll= function () {
        return models.services.findAll();
    };



    servicesRepository.prototype.findById = function (id) {
        var findOptions = {};
       
        findOptions.where = {id: id};
        return models.services.find(findOptions);
    };

    servicesRepository.prototype.findWithFeatures = function () {
         models.categories.hasMany(models.service_features,{foreignKey:'service_cat_id',targetKey: 'id'});
        var findOptions = {};
       

       findOptions.include = [
            {model:models.service_features, required:true},
        ];
        return models.categories.findAll(findOptions);
    };


   servicesRepository.prototype.findByCatId = function (catid) {
        var findOptions = {};
       
        findOptions.where = {category_id: catid};
        return models.services.findAll(findOptions);
    };
    
    return servicesRepository;
})();
exports.servicesRepository = servicesRepository;
//# sourceMappingURL=userDao.js.map,