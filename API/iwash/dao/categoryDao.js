var models = require('../models');


   
var categoryRepository = (function () {
    function categoryRepository() {
	

    }
   
   
    categoryRepository.prototype.getAll= function () {
        return models.categories.findAll();
    };

 

    categoryRepository.prototype.findById = function (id) {
        var findOptions = {};
       
        findOptions.where = {id: id};
        return models.categories.find(findOptions);
    };
    
  

    //
    categoryRepository.prototype.getParentById = function (id) {
       models.categories.hasOne(models.categories,{onDelete: 'cascade',foreignKey:'id',as:'pcat'});
       //models.categories.hasMany(models.categories,{foreignKey:'id',as:'scat',where:{parent_id:id}});
        var findOptions = {};
        findOptions.where = {id: id};
        findOptions.include = [
            {model:models.categories, required:true,as:'pcat', attributes:[['name','Parent']] },
           // {model:models.categories,as:'scat', attributes:[['name','Sub']] },
        ];
        findOptions.attributes = ['name','parent_id'];
        return models.categories.find(findOptions);
    };
    

    categoryRepository.prototype.getAllParentcats = function () {
		

        var findOptions = {};
        findOptions.where = {parent_id:0};
        findOptions.attributes = ['id','name','description','image'];
           //  findOptions.limit = limit;
      //  findOptions.offset =offset;
        return models.categories.findAll(findOptions);
    };
     categoryRepository.prototype.getSubcatByParent = function (parent_id,page,limit) {
        var offset = parseInt((page - 1) * parseInt(limit)) ;
        var findOptions = {};
        findOptions.where = {parent_id:parent_id};
        findOptions.attributes = ['id','name','description','image'];
        
       // findOptions.limit = limit;
        //findOptions.offset =offset;
        return models.categories.findAll(findOptions);
    };
    categoryRepository.prototype.getSubcatByParentId = function (parent_id) {
         
        var findOptions = {};
        findOptions.where = {parent_id:parent_id};
        findOptions.attributes = ['id','name','description','image'];
        return models.categories.findAll(findOptions);
    };
    
    categoryRepository.prototype.getAllParent = function () {
         
        var findOptions = {};
        findOptions.where = {parent_id:0};
        findOptions.attributes = ['id','name','description','image'];
        return models.categories.findAll(findOptions);
    };
    


  
 
   

   
  
    
    return categoryRepository;
})();
exports.categoryRepository = categoryRepository;
//# sourceMappingURL=userDao.js.map,