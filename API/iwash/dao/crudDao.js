var models = require('../models');

var crudRepository = (function () {
    function crudRepository() {


    }

    crudRepository.prototype.getAll = function () {
        return models.crud.findAll();
    };

    crudRepository.prototype.findById = function (id) {
        var findOptions = {};
        findOptions.where = { id: id };
        return models.crud.find(findOptions);
    };

    crudRepository.prototype.Create = function (crud) {
        var buildOptions = {};
        buildOptions.isNewRecord = true;
        var newcrud = models.crud.build(crud, buildOptions);
        return newcrud.save();
    };

    crudRepository.prototype.update = function (crud) {
        var updatedOptions = {};
        updatedOptions.where = { id: crud.id };
        var newcrud = models.crud.update(crud, updatedOptions);
        return newcrud;
    };

    crudRepository.prototype.Delete = function (id) {
        var findOptions = {};
        findOptions.where = { id: id };
        return models.crud.destroy(findOptions);
    };

    return crudRepository;


})();

exports.crudRepository = crudRepository;