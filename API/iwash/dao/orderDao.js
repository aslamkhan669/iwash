var models = require('../models');


   
var orderRepository = (function () {
    function orderRepository() {
	

    }
	
	orderRepository.prototype.getBranches = function () {
      
        return models.branches.findAll();
    };
   orderRepository.prototype.findCartByuserId = function (user_id) {
        var findOptions = {};
       
        findOptions.where = {user_id: user_id};
        return models.cart.find(findOptions);
    };
    // orderRepository.prototype.updateCartByuserId = function (user_id,service_cat_id,quantity) {
    //     var cart={};
    //      cart.updatedAt = new Date();
    //      cart.quantity=quantity;
    //      var updatedOptions = {};
    //     updatedOptions.where = {user_id: user_id,service_cat_id:service_cat_id};
    //     return models.feeds.update(feeds, updatedOptions);

    // };
    orderRepository.prototype.UpdateCartByuserIdService = function (user_id,service_cat_id) {
        var findOptions = {};
       
        findOptions.where = {user_id: user_id};
        return models.cart.find(findOptions);
    };
     orderRepository.prototype.deleteCartService = function (user_id,service_cat_id) {
        var findOptions = {};
       
        findOptions.where = {user_id: user_id,service_cat_id:service_cat_id};
        return models.cart.destroy(findOptions);
    };



    orderRepository.prototype.getAll= function (user_id,data) {
       models.order.belongsTo(models.deliverytime, {foreignKey:'timeOfDelivery',targetKey:'id'});
       models.order.belongsTo(models.pickuptime, {foreignKey:'timeOfPickup',targetKey:'id'});
       models.order.belongsTo(models.categories, {foreignKey:'service_cat_id',targetKey:'id'});
	   
	   
      models.order.hasMany(models.order_service, {foreignKey:'order_id',targetKey:'id'});
	   
      models.order_service.belongsTo(models.services, {foreignKey:'service_id',targetKey:'id'});
       var page=data.page;
       var limit=data.limit;
                var offset = parseInt((page - 1) * parseInt(limit)) ;

          var findOptions = {};
       
        findOptions.where = {user_id: user_id};
        findOptions.include = [
            { model: models.deliverytime },
            { model: models.pickuptime },
           // { model: models.order_service,include:[ { model: models.services }]},
            { model: models.categories}
        ];
      findOptions.order=[['createdAt', 'DESC']];
        findOptions.limit = limit;
        findOptions.offset =offset;


        return models.order.findAll(findOptions);
    };



    orderRepository.prototype.findById = function (authuser,id) {
             models.order.belongsTo(models.deliverytime, {foreignKey:'timeOfDelivery',targetKey:'id'});
       models.order.belongsTo(models.pickuptime, {foreignKey:'timeOfPickup',targetKey:'id'});
       models.order.belongsTo(models.categories, {foreignKey:'service_cat_id',targetKey:'id'});
        
      models.order.hasMany(models.order_service, {foreignKey:'order_id',targetKey:'id'});
	   
      models.order_service.belongsTo(models.services, {foreignKey:'service_id',targetKey:'id'});
	  
      models.services.belongsTo(models.categories, {foreignKey:'category_id',targetKey:'id'});
	  
        var findOptions = {};
       
        findOptions.where = {id: id,user_id:authuser};
          findOptions.include = [
            { model: models.deliverytime },
            { model: models.pickuptime },
			 { model: models.order_service,include:[ { model: models.services ,include:[ { model: models.categories }]}]},

            { model: models.categories}
            
        ];
        return models.order.find(findOptions);
    };


    orderRepository.prototype.placeorder = function (order) {
     
                        var buildOptions = {};
                        buildOptions.isNewRecord = true;
                        buildOptions.raw = true;
                  return  models.order.build(order, buildOptions).save();
                    }
     orderRepository.prototype.addOrderService = function (order) {
     
                        var buildOptions = {};
                        buildOptions.isNewRecord = true;
                        buildOptions.raw = true;
                  return  models.order_service.build(order, buildOptions).save();
                    }

                    orderRepository.prototype.addOrderServices = function (data) {
                           //var arr=[];
                           var buildOptions = {};
                           buildOptions.isNewRecord = true;
                           buildOptions.raw = true;
                      
                       
                
                          
                           return models.order_service.bulkCreate(data, buildOptions);
                          
                       }

    orderRepository.prototype.cancelorder = function (order) {
        var orderobj={};
         orderobj.updatedAt = new Date();
         orderobj.status=order.status;
         var updatedOptions = {};
        updatedOptions.where = {user_id:order.user_id,id:order.orderid};
        return models.order.update(orderobj, updatedOptions);

    };
   orderRepository.prototype.getDeliveryTime= function () {
       
        return models.deliverytime.findAll();
    };
     orderRepository.prototype.getPickupTime= function () {
       
        return models.pickuptime.findAll();
    };
    return orderRepository;
})();
exports.orderRepository = orderRepository;
//# sourceMappingURL=userDao.js.map,