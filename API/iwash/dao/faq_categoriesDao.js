var models = require('../models');


   
var faqscategoriesRepository = (function () {
    function faqscategoriesRepository() {
	

    }
   
   
   
    faqscategoriesRepository.prototype.getAll= function () {
        return models.faq_categories.findAll();
    };

   faqscategoriesRepository.prototype.getAllWithQuestions= function () {

    models.faq_categories.hasMany(models.faqs, {foreignKey:'faq_category',targetKey:'id'});


          var findOptions = {};
       
        findOptions.include = [{ model: models.faqs }];

        return models.faq_categories.findAll(findOptions);
    };

    faqscategoriesRepository.prototype.findById = function (id) {
        var findOptions = {};
       
        findOptions.where = {id: id};
        return models.faq_categories.find(findOptions);
    };
    
   
    
    return faqscategoriesRepository;
})();
exports.faqscategoriesRepository = faqscategoriesRepository;
//# sourceMappingURL=userDao.js.map,