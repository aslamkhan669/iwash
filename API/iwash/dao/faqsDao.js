var models = require('../models');


   
var faqsRepository = (function () {
    function faqsRepository() {
	

    }
   
   
   
    faqsRepository.prototype.getAll= function () {
        return models.faqs.findAll();
    };



    faqsRepository.prototype.findById = function (id) {
        var findOptions = {};
       
        findOptions.where = {id: id};
        return models.faqs.find(findOptions);
    };
    
   faqsRepository.prototype.findByCatId = function (catid) {
        var findOptions = {};
       
        findOptions.where = {faq_category: catid};
        return models.faqs.findAll(findOptions);
    };
    
    return faqsRepository;
})();
exports.faqsRepository = faqsRepository;
//# sourceMappingURL=userDao.js.map,