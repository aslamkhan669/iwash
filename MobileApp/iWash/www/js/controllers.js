var api_url='https://api.iwashqatar.com/';
var siteurl='https://iwashqatar.com/';
var googleadd='https://maps.googleapis.com/maps/api/geocode/json?address=';

var currentlocation='';
//var api_url='http://localhost:3000/api/';
// var siteurl='http://localhost/';
var items_added = [], quantity_added=0, total=0, tax=0,category_selected ='';

angular.module('starter.controllers', [])
.directive('datepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: new Date(),
                onSelect: function (date) {
                    ngModelCtrl.$setViewValue(date);
                    ngModelCtrl.$render();
                    scope.$apply();
                }
            });
        }
    };
})
.directive('changegoogleplace', function($rootScope) {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {country: "IN" }
            };
            scope.gPlacen = new google.maps.places.Autocomplete(element[0], options);
 
            google.maps.event.addListener(scope.gPlacen, 'place_changed', function() {                
                scope.$apply(function() {
                 

                    model.$setViewValue(element.val()); 
                   // console.log(this);
                     $rootScope.location=element.val();
                   $rootScope.autolocation(element.val()); 
                    $rootScope.locationmodal.newlocation='';

                     $('#newlocation').val('');
                });
            });
        }
    };
})
.service('LoginService', function($q, $http, $window) {
    var serviceToken, serviceHost, tokenKey;
    tokenKey = 'xaccesstoken';
    if (localStorage.getItem(tokenKey)) {
        serviceToken = $window.localStorage.getItem(tokenKey);
    }
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var credentials = {};
            credentials.username = name;
            credentials.password = pw;
            var credentials = JSON.stringify(credentials);
            $http.post(api_url + 'user/authenticate', credentials).then(function(resp) {

                if (resp.data.success === true) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject(resp.data);
                }

            }, function(err) {
                deferred.reject(resp.data);

            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;

        },
        signup: function(fname, lname, email,mobile,password) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var credentials = {};
            credentials.email = email;
            credentials.mobile = mobile;
            credentials.password = password;
            credentials.fname = fname;
            credentials.lname = lname;
            var credentials = JSON.stringify(credentials);
            $http.post(api_url + 'user/register', credentials).then(function(resp) {
                if (resp.data.success === true) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject(resp.data);
                }

            }, function(err) {
                deferred.reject(resp.data);

            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;

        },
        setAuth: function(authuser) {
            $window.localStorage.setItem('authuser', JSON.stringify(authuser));
        },

        getAuth: function() {
            if (localStorage.getItem('authuser')) {
                var retrievedObject = $window.localStorage.getItem('authuser');
                return JSON.parse(retrievedObject);
            }
        },

        setToken: function(token) {
            serviceToken = token;
            $window.localStorage.setItem(tokenKey, token);
        },

        getToken: function() {
            return serviceToken;
        },

        removeToken: function() {
            serviceToken = undefined;
            $window.localStorage.removeItem(tokenKey);
            $window.localStorage.removeItem('authuser');
        },

        get: function(uri, params) {
            return $http({
                url: uri,
                method: "GET",
                params: {
                    xaccesstoken: serviceToken
                }
            });
        },

        post: function(uri, params) {
            if (params) {
                params = JSON.parse(params);

            } else {
                params = params || {};

            }
            params['xaccesstoken'] = serviceToken;
            params = JSON.stringify(params);
            return $http.post(uri, params);
        }
    };

})

.controller('AppCtrl', function($ionicNavBarDelegate,$ionicHistory,$ionicPlatform,$stateParams,$cordovaToast,$ionicLoading,$window,$ionicHistory,$http,$rootScope,$ionicPopup,$scope,$ionicPopover,$ionicModal,$timeout, LoginService, $state,$cordovaGeolocation, $location,$cordovaBadge,$cordovaSocialSharing,$ionicScrollDelegate) {
	console.log($state.current.name);
    $rootScope.currentorder={};
    $scope.currentservice='';
    $scope.api_url=api_url;
    $rootScope.currentorder.timslot={};
    $rootScope.currentorder.address={};
    $rootScope.orderstep =1;
    if(!LoginService.getAuth()){
      $state.go('login');

    }
    $rootScope.gotoStep=function(stepno){
        $rootScope.orderstep =stepno;
        $state.go('app.placeorderStep'+stepno);
        
    }
    $scope.disableTap = function(){
        container = document.getElementsByClassName('pac-container');
        // disable ionic data tab
        angular.element(container).attr('data-tap-disabled', 'true');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function(){
            document.getElementById('searchBar').blur();
        });
      };
      $scope.getauthuserlanglatbyAddress=function(address){
         $http.get(googleadd+address).then(function(resp) {
            if(resp.data.status=='OK'){
                console.log(resp.data.results[0]);
                var lanlat=resp.data.results[0].geometry.location;
                $rootScope.authlng=lanlat.lng;
                $rootScope.authlat=lanlat.lat;
                $rootScope.$broadcast('locationchange');
                //$rootScope.$apply();
            }

        }, function(err) {
             console.error('ERR', err);

        });
    }

    $rootScope.autolocation=function(address){

        $scope.getauthuserlanglatbyAddress(address);
        $scope.locationmodal.hide();

    }
    $ionicModal.fromTemplateUrl('templates/locationmodal.html', {
        scope: $scope,
        animation: 'slide-in-up',
    }).then(function(modal) {
        $rootScope.locationmodal = modal;
        
    });

    $rootScope.openModal=function($event) {

        $rootScope.locationmodal.show($event);
    };

    $rootScope.closeModallocation = function() {
            //jQuery('#newlocation').val('');
        $rootScope.locationmodal.hide();
    };

    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $rootScope.locationmodal.remove();
    });

    // Execute action on hide modal
    $rootScope.$on('locationmodal.hidden', function() {
        // Execute action
    });

    // Execute action on remove modal
    $rootScope.$on('locationmodal.removed', function() {
        // Execute action
    });
    $rootScope.getgeolocation=function(){
        var posOptions = {timeout: 5000, enableHighAccuracy: false};
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            $rootScope.authlng=long;
            $rootScope.authlat=lat;
            ReverseGeocode(lat,long);
       }, function(err) {
                                if(err.PERMISSION_DENIED){
                        var locationpopup= $ionicPopup.confirm({
                                        title: "Location Services",
                                        content: "Whoops, we're having trouble finding your location. Please verify you've switched Location to On and you've given Location consent. Also, set your Location Mode to High Accuracy ",
                                scope: $scope,
                buttons: [
                    {
                        text: 'Cancel' ,
        
                     }
                ]
        
                                        })
                                        .then(function(res) {
                                            console.log(res);
         if(res) {
                console.log('sure');
                // $rootScope.switchToLocationSettings();
                 } else {
        
                    // console.log('You are not sure');
                 }
        
        
                                        //ionic.Platform.exitApp();
                                        });
                                }
                        });
                }
                $scope.clearlocation=function(){
                    $rootScope.authlng='';
                    $rootScope.authlat='';
                    $rootScope.location='';
                    $rootScope.locationmodal.newlocation='';
                                        
                }
                function ReverseGeocode(latitude, longitude){
                    var reverseGeocoder = new google.maps.Geocoder();
                    var currentPosition = new google.maps.LatLng(latitude, longitude);
                    reverseGeocoder.geocode({'latLng': currentPosition}, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                             console.log(results);
                                     if (results[0]) {
                                 var arrAddress = results[0].address_components;
                               //  console.log(arrAddress);
                                                currentlocation='';
                                    /*
                                                $.each(arrAddress, function (i, address_component) {
        
                                                if (address_component.types[0] == "locality") {
                                                        currentlocation+=address_component.long_name+',';
                                                       // console.log(currentlocation);
                                                }
                                                if (address_component.types[0] == "country") {
                                                    currentlocation+=address_component.long_name; //location
        
                                                }
                                                     if (address_component.types[0] == "administrative_area_level_1") {
                                                    currentlocation+=address_component.long_name+','; //location
        
                                                }
        
                                    });
                                    */
        console.log(currentlocation);
        console.log('test');
                    $rootScope.location=results[0].formatted_address;
                   // $rootScope.$apply();
                    $rootScope.$broadcast('locationchange');
                         // $rootScope.location=currentlocation;
        
        
        
                        }else {
                                navigator.notification.alert('Unable to detect your address.');
                        }
                            } else {
                                    navigator.notification.alert('Unable to detect your address.');
                            }
                    });
            }
  $scope.groups = [];
  for (var i=0; i<3; i++) {
    if(i==0){
    $scope.groups[i] = {
      name: "Pickup & Delivery",
      items: ["test 1"]
    };
  }
  if(i==1){
    $scope.groups[i] = {
      name: "Washing",
      items: ["test 2"]
    };
  }
  if(i==2){
    $scope.groups[i] = {
      name: "Payment",
      items: ["test 3"]
    };
  }
    for (var j=0; j<1; j++) {
      $scope.groups[i].items.push();
    }
  }

  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };

  $rootScope.calluser=function(mobile){
    if(mobile!=undefined){
        window.plugins.CallNumber.callNumber(onSuccess, onError, mobile, true);
    }
  //window.open('tel:'+mobile, '_system')
  }

  function onSuccess(result){
    console.log("Success:"+result);
  }

  function onError(result) {
    console.log("Error:"+result);
  }
 	$scope.logOut = function () {
	LoginService.removeToken();
	$scope.myinfo=undefined;
	$scope.isAuthenticated = false;
	 $ionicLoading.show({
			template: '<ion-spinner icon="android"></ion-spinner>'
			});
			$timeout(function() {
			$state.go('login');
		  $ionicLoading.hide();
			}, 1000);
			 gapi.auth.signOut();

    };
     
 

    $scope.getauthuser=function(){
		authuser=LoginService.getAuth();
		$scope.authuserid=authuser.userId;
		LoginService.post(api_url+'user/details/'+$scope.authuserid).then(function(resp) {
			$scope.authrole=resp.data.role.id;
			$scope.myinfo = resp.data;
		
			if(resp.data.social_avatar!=undefined && resp.data.social_avatar!='' && resp.data.social_avatar!=null ){
		   				$scope.profileimage=resp.data.social_avatar;
		   				$scope.noprofile=0;

		   }
			else if(resp.data.avatar!='' && resp.data.avatar!=null){
				$scope.profileimage=api_url+''+resp.data.avatar;
				$scope.profileimagemainurl=resp.data.avatar;
				$scope.noprofile=0;
			}else{
				$scope.noprofile=1;

				$scope.profileimage='img/avatar.jpg';
			}
			console.log(resp.data);
		}, function(err) {
            if(err.success==false && err.type=='token_expired'){
                $scope.logOut();
        
        }

		})
    }	
    if($rootScope.myinfo==undefined && LoginService.getToken()){
        
                   $scope.getauthuser();
            }else{
        
            $rootScope.$on( '$stateChangeStart', function(e, toState  , toParams, fromState, fromParams) {
        
                //alert(444444444444);
                if(fromState.url=='/'){
                      $scope.getauthuser();
                }
            //alert(fromState.url);
            
                });	
            }
})
.controller('FaqCtrl', function($scope, $http, $state,$stateParams) {

		$scope.getFaqcat=function(){
				$http.get(api_url+'faqs/categories').then(function(resp) {
          
						$scope.faqscat=resp.data;
					}, function(err) {
              console.error('ERR', err);
              console.log(err);

					});
		 }
    $scope.getFaqcat();
    
		$scope.getFaq=function(catid){
            var currelement=this.cat;
            if(currelement.items==undefined){
                $http.get(api_url+'faqs/'+catid).then(function(resp) {
                    currelement.items=resp.data;
                    console.log(currelement);
                }, function(err) {
                    console.error('ERR', err);
                    console.log(err);

                });


            }
		 }


})
 .controller('LoginCtrl', function($cordovaToast,$scope,$http,$scope, LoginService,$state,$cordovaOauth,$timeout,$ionicLoading) {

        $scope.data={};
         $scope.register = {};
          
	 $scope.sociallogin = function(social) {
		 $ionicLoading.show({
								template: '<ion-spinner icon="android"></ion-spinner>'
			});
			var social=JSON.stringify(social);
			//alert(social);
			$http.post(api_url+'user/loginsocial',social).then(function(resp) {
						//alert(JSON.stringify(social));
						if(resp.data.success==true){
								LoginService.setAuth(resp.data);
								LoginService.setToken(resp.data.token);
								$scope.signedIn = true;
							    $ionicLoading.hide();
								//$scope.getauthuser();
								//$scope.$apply();
								$state.go('app.home');

						}else{
							 $cordovaToast.showLongCenter('Registration failed!').then(function(success) {

								}, function (error) {

								});
						}
			}, function(err) {
					$cordovaToast.showLongCenter('Registration failed!').then(function(success) {

					}, function (error) {

					});

			});
		}
 $scope.googleLogin = function() {
			
			$cordovaOauth.google("177774509713-n7t6m213a64lkmope4c3plc63v6t6cf3.apps.googleusercontent.com", ['email',"https://www.googleapis.com/auth/urlshortener", "https://www.googleapis.com/auth/userinfo.email",'https://www.googleapis.com/auth/userinfo.profile']).then(function(result) {
					var obj={};
					obj.network='gplus';
					obj.socialToken=result.access_token;
					$scope.sociallogin(obj);
			}, function(error) {
					console.log(error);
			});
		}

		$scope.facebookLogin = function() {
				$cordovaOauth.facebook("707986062733495", ["email","public_profile","user_website", "user_location", "user_relationships"]).then(function(result) {
					var obj={};
					obj.network='facebook';
					obj.socialToken=result.access_token;
					$scope.sociallogin(obj);
				}, function(error) {

				});
		}
	$scope.login = function(){
			if($scope.data.username!=null && $scope.data.password!=null){
					$ionicLoading.show({
								template: '<ion-spinner icon="android"></ion-spinner>'
								});
					var mobile=$scope.data.username;
					var password=$scope.data.password;
					LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
							$ionicLoading.hide();							
						  if(data.success==true && data.message=='mobile_verify'){
								$ionicLoading.hide();
								sessionkey=data.session_key;
								verifymob=mobile;
								afterresetuser.username=mobile;
								afterresetuser.password=password;
								console.log(afterresetuser);						  
								$state.go('resetmobile');

							}else if(data.success==true){
								LoginService.setAuth(data);
								LoginService.setToken(data.token);
								$scope.signedIn = true;
								$timeout(function() {
								$state.go('app.home');
								}, 2000);

							}else{
							 
								$cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});

							 }
							
									

						}).error(function(data) {
							$ionicLoading.hide();
								$cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});

						});
			}else{
					
					$cordovaToast.showLongCenter('Username/password required').then(function(success) {

					}, function (error) {

					});
			}
	};
		$scope.signup = function() {
                     var  registermob=$scope.register.mobile;
                      var  registeremail=$scope.register.email;
				 var  registerpassword=$scope.register.password;
				if($scope.register.fname!=null && $scope.register.email!=null && $scope.register.mobile!=null && $scope.register.lname!=null && $scope.register.password!=null ){
						LoginService.signup($scope.register.fname, $scope.register.lname,$scope.register.email,$scope.register.mobile,$scope.register.password).success(function(data) {
								
						    if(data.success==true && data.message=='mobile_verify'){
								verifymob=registermob;
								$ionicLoading.hide();
								sessionkey=data.session_key;
								$scope.register={};							 
								afterresetuser.username=registermob;
								afterresetuser.password=registerpassword;
								console.log(afterresetuser);
								$state.go('resetmobile');
                            }else if(data.success==true && data.message!='mobile_verify'){
    $scope.register={};
    $scope.data.username=registeremail;
    $scope.data.password=registerpassword;
    
    

    
    
    	$scope.login(); 
								//$state.go('app.home');

                            }else{
								 
						         $cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});

							 }
						}).error(function(data) {
							   $cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});
						});
				}else{
						$cordovaToast.showLongCenter('All fields required').then(function(success) {

						}, function (error) {

						});
				}

		};

    })
    .controller('HomeCtrl', function($ionicSlideBoxDelegate,$ionicScrollDelegate,$ionicSideMenuDelegate,$ionicHistory,$rootScope,$ionicNavBarDelegate,$location,$rootScope,$scope,$http, LoginService, $state, $timeout,$ionicLoading,$window) {
	
		
    
		$scope.getbanner = function() {
            $http.get(api_url + "banner").then(function(resp) {
                $scope.banners = resp.data;
                $ionicSlideBoxDelegate.update();
                
            }, function(err) {
                console.error("ERR", err);
                console.log(err);
            });
        };

		$scope.getTestimonial = function() {
            $http.get(api_url + "testimonials").then(function(resp) {
                $scope.testimonials = resp.data.result;
            }, function(err) {
                console.error("ERR", err);
                console.log(err);
            });
        };

        
        $rootScope.$on('locationchange',function(event,args){
    
        
        });

       $scope.getservicesCatHome=function(catid){
        $http.get(api_url+'categories').then(function(resp) {
            $scope.categorieshome=resp.data;
            
            }, function(err) {
            
            });
		}

       $scope.$on('$ionicView.beforeEnter', function(){
		          $rootScope.getgeolocation();
				  $scope.getbanner();
				  $scope.getTestimonial();
		        $scope.getservicesCatHome();


		});


}).controller('ServiceFeaturesCtrl', function($scope, $http, $state,$stateParams,$ionicSlideBoxDelegate,$ionicSideMenuDelegate) {
  $ionicSideMenuDelegate.canDragContent(false); 
	$scope.getserviceFeatures=function(catid){
		$http.get(api_url+'services/features/list').then(function(resp) { 
            $scope.servicesfeatures=resp.data;
            $ionicSlideBoxDelegate.update();
			}, function(err) {
              console.error('ERR', err);
              console.log(err);
        });
	}


	$scope.getserviceFeatures();

})
.controller('PlaceOrderStep2Ctrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$stateParams,$location) {
    $rootScope.currentorder.address={};
    $scope.$on('$ionicView.beforeEnter', function(ev) {
        if(ev.targetScope !== $scope)
            return;
        if(! LoginService.getToken() || $rootScope.orderstep!=2) {
            $state.go('app.home');
  
   }
      
    });
    if(! LoginService.getToken() || $rootScope.orderstep!=2) {
                 $state.go('app.home');
       
        }
      
/*  if(!$rootScope.currentorder.service){
          $state.go('app.home');
                
    
    } */

    $rootScope.currentorder.address.landmark=$rootScope.location;
        console.log($rootScope.orderstep);
          $scope.getBranches=function(){
						LoginService.get(api_url+'order/branches').then(function(resp) {      
						$scope.branches=resp.data;

						}, function(err) {
						if(err.success==false && err.type=='token_expired'){
						$scope.logOut();

						}


						});
              }
			  $scope.getBranches();

    })
      .controller('PlaceOrderStep3Ctrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$stateParams,$location) {
        $scope.$on('$ionicView.beforeEnter', function(ev) {
            if(ev.targetScope !== $scope)
                return;
            if(! LoginService.getToken() || $rootScope.orderstep!=3) {
                $state.go('app.home');
                
                }
          
        });
        if(! LoginService.getToken() || $rootScope.orderstep!=3) {
            $state.go('app.home');
            
            }
		if(!$rootScope.currentorder.address||!$rootScope.currentorder.address.street || !$rootScope.currentorder.address.landmark || !$rootScope.currentorder.address.addresstype  || !$rootScope.currentorder.branch){
				$rootScope.gotoStep(2);
		}
		$scope.getservicesCatforOrder=function(){
                $http.get(api_url+'categories').then(function(resp) {
                    
                        $scope.servicescat=resp.data;
                    }, function(err) {
                        		if(err.success==false && err.type=='token_expired'){
					$scope.logOut();

					}

        
                });
            }

            $scope.getMyCurrentSubscription=function(){
                LoginService.get(api_url+'package/current/subscription').then(function(resp) {
                          if(resp.data.status==true){
							  $rootScope.currentorder.currentSubscription=resp.data.result;
                              $rootScope.currentorder.useSubscription=0;
							  }else{
							 $rootScope.currentorder.currentSubscription=null;
							  }
                        
                    }, function(err) {
                        		if(err.success==false && err.type=='token_expired'){
					               $scope.logOut();

					                }

        
                });
            }
				
         
        })   

.controller('PlaceOrderStep4Ctrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$stateParams,$location) {
       $rootScope.currentorder.cart={};
$rootScope.currentorder.cartPrice=0;
   $scope.$on('$ionicView.beforeEnter', function(ev) {
        if(ev.targetScope !== $scope)
            return;
        if(! LoginService.getToken() || $rootScope.orderstep!=4) {
            $state.go('app.home');
                    
                    }
      
    });
    if(! LoginService.getToken() || $rootScope.orderstep!=4) {
            $state.go('app.home');
                    
    }
	if((!$rootScope.currentorder.service && $rootScope.currentorder.useSubscription==0)){
              $rootScope.gotoStep(3);
                
	}
	
    function setCartData(){
  
        $("#quantity").text(quantity_added);
        $("#total").text(total);
        var tax = (total * 14.5)/100;
        $("#tax").text(tax);
    }
    function vibrateCart(){
        var i=0;
        var myInterval = setInterval(function(){
            if(i++>=10)
                clearInterval(myInterval);
            if(i%2==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(15deg)",
                    "-moz-transform": "rotate(15deg)",
                    "transform": "rotate(15deg)",
                    "-o-transform": "rotate(15deg)",
                    "-ms-transform": "rotate(15deg)"
                });        
            }
            else if(i%3==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(-15deg)",
                    "-moz-transform": "rotate(-15deg)",
                    "transform": "rotate(-15deg)" ,
                    "-o-transform": "rotate(-15deg)",
                    "-ms-transform": "rotate(-15deg)"
                });   
            }
            else{
                $(".estimator img").css({
                    "-webkit-transform": "rotate(0deg)",
                    "-moz-transform": "rotate(0deg)",
                    "transform": "rotate(0deg)",
                    "-o-transform": "rotate(0deg)",
                    "-ms-transform": "rotate(0deg)"
                });   
            }
        }, 100);
		console.log($rootScope.currentorder.cart);
		console.log($rootScope.currentorder.cartPrice);
     }  
     
	   $scope.increment=function(serviceselectd,categoryid,itemid,price){

          var item_label =itemid;

          if(items_added[item_label]){
                 items_added[item_label]++;
		            this.service.quantity=items_added[item_label];

          }else{
          items_added[item_label]=1;
		            this.service.quantity=items_added[item_label];


          }    
          if(quantity_added==0){
             $(".estimator").fadeIn(1500);

          }
             
          quantity_added++;
          total +=  Number(price);
		  serviceselectd['cart_qty']=items_added[item_label];
		  $rootScope.currentorder.cart[item_label]=serviceselectd;
		  $rootScope.currentorder.cartPrice=total;
          setCartData();
          vibrateCart();
        //  $ionicSlideBoxDelegate.update();

       }


$scope.decrement=function(serviceselectd,categoryid,itemid,price){

 var item_label =itemid;

                if(items_added[item_label]>0){
                    items_added[item_label]--;
                    quantity_added--;
                    total -= Number(price);
                    setCartData();
                    if(quantity_added==0)
                        $(".estimator").fadeOut(1000);
				    this.service.quantity=items_added[item_label];		
                }
				           
							  if(items_added[item_label]==0){
								delete $rootScope.currentorder.cart[item_label];

							   }else{
							   		  serviceselectd['cart_qty']=items_added[item_label];

							    $rootScope.currentorder.cart[item_label]=serviceselectd;
							   }
		
		  $rootScope.currentorder.cartPrice=total;

                vibrateCart();

}
     
    $scope.getservicesByCatid=function(catid){
        
            $http.get(api_url+'services/'+catid).then(function(resp) {      
                $rootScope.currentorder.catservices=resp.data;
				
				for (i=0; i< $scope.currentorder.catservices.length;i++){

                     items_added[$scope.currentorder.catservices[i]['id']] = new Array();   
                 }
             
            }, function(err) {
                    console.error('ERR', err);
                    console.log(err);

            });

     }
	 if($rootScope.currentorder.useSubscription==0 || !$rootScope.currentorder.useSubscription){
		 $scope.getservicesByCatid($rootScope.currentorder.service.id);
		 
	}

            
                })  
.controller('PlaceOrderStep5Ctrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$stateParams,$location) {
	console.log($rootScope.currentorder.cart);
		console.log($rootScope.currentorder.cartPrice);
    
   $scope.$on('$ionicView.beforeEnter', function(ev) {
        if(ev.targetScope !== $scope)
            return;
        if(! LoginService.getToken() || $rootScope.orderstep!=5) {
            $rootScope.gotoStep(2);
            
            }
      
    });
            console.log($rootScope.currentorder.service);
              if((!$rootScope.currentorder.service && !$rootScope.currentorder.useSubscription)||(!$rootScope.currentorder.cart && $rootScope.currentorder.useSubscription==1)){
              $rootScope.gotoStep(4);
                
    
               }
            
                
            $scope.getdeliverytime=function(){
                LoginService.get(api_url+'order/deliverytime').then(function(resp) {      
                $scope.deliverytime=resp.data;
                }, function(err) {
                    if(err.success==false && err.type=='token_expired'){
                    $scope.logOut();

                    }


                });
            }
        $scope.getdeliverytime();
            $scope.getpickuptime=function(){
                    LoginService.get(api_url+'order/pickuptime').then(function(resp) {      
                    $scope.pickuptime=resp.data;

                    }, function(err) {
						if(err.success==false && err.type=='token_expired'){
						$scope.logOut();

						}


                    });
              }
			  
			  
  $scope.getpickuptime();
    }) 

 .controller('PlaceOrderStep6Ctrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$stateParams,$location) {
 	console.log($rootScope.currentorder.cart);
		console.log($rootScope.currentorder.cartPrice);
		
		$scope.$on('$ionicView.beforeEnter', function(ev) {
        if(ev.targetScope !== $scope)
            return;
        if(! LoginService.getToken() || $rootScope.orderstep!=6) {
            $rootScope.gotoStep(2);
            
            }
      
    });
        if(! LoginService.getToken() || $rootScope.orderstep!=6) {
                    $rootScope.gotoStep(2);
                    
                    }

	
					
                    if(!$rootScope.currentorder.timslot|| !$rootScope.currentorder.timslot.pickupdate || !$rootScope.currentorder.timslot.deliverydate || !$rootScope.currentorder.timslot.pickuptime || !$rootScope.currentorder.timslot.deliverytime || !$rootScope.currentorder.cart){
                        $rootScope.gotoStep(5);
                        
            
                       }
                    $scope.placeOrder=function(order){
					console.log(order);
					console.log('order');
                        
                                    var obj={};
                                    obj.flatandstreet=order.address.street;
                                    obj.landmark=order.address.landmark;
                                    obj.addressType=order.address.addresstype;
                                    obj.pickupDate=order.timslot.pickupdate;
                                    obj.deliveryDate=order.timslot.deliverydate;
                                    obj.timeOfDelivery=order.timslot.deliverytime.id;
                                    obj.timeOfPickup=order.timslot.pickuptime.id;
									if(order.service){
										 obj.service_cat_id=order.service.id;
									}
                                   
                                    obj.branch=order.branch.id;

									var services=[];
									for (var key in order.cart) {
										if (order.cart.hasOwnProperty(key)) {
										services.push(order.cart[key]);

										}
									}
                                    obj.services=services;
									if(order.useSubscription){
									     obj.subscription_id=order.currentSubscription.id;
										 obj.useSubscription=order.useSubscription;
									}else{
									
									 obj.useSubscription=0;
									}
                                   console.log('obj');
                                   console.log(obj);
                                    
                                      var data=JSON.stringify(obj);
                                      LoginService.post(api_url+'order/place',data).then(function(resp) {
                                        $rootScope.currentorder={};
                                        $scope.currentservice='';
                                        $rootScope.currentorder.timslot={};
                                        $rootScope.currentorder.address={};
                                        
                                        $rootScope.orderstep =1;
                                        if(resp.data){
                                        $location.path('/app/order/success/'+resp.data.id);

                                                       
                        
                                        }
                                
                        
                                      }, function(err) {
                                                if(err.success==false && err.type=='token_expired'){
                                                $scope.logOut();

                                                }

                        
                                      }); 
                        
                          }
      
        }) 

.controller('orderSuccessCtrl', function($ionicPlatform,$scope, $http, $state,$stateParams,$ionicNavBarDelegate,$rootScope , $ionicHistory,$ionicSideMenuDelegate) {
    
     
            $scope.$on('$ionicView.beforeEnter', function (e,config) {
            config.enableBack = false;
            });

        $rootScope.$ionicGoBack = function(backCount) {
            //  $ionicSideMenuDelegate.toggleLeft();

            //$state.go('app.order');
        };    
    $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
            // $state.go('app.order');
                
        });
     
$scope.placedorder=$stateParams.orderid;
$rootScope.currentorder={};

})



.controller('OrderDetailsCtrl', function($scope, $http, $state,$stateParams,LoginService,$cordovaToast,$ionicPopup) {


  $scope.getOrderDetails=function(){
                  

     LoginService.get(api_url+'order/get/'+$stateParams.orderid).then(function(resp) {      
        
               $scope.order=resp.data

          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
  }

if($stateParams.orderid){
$scope.getOrderDetails();

}


$scope.confirmcancel = function(orderid) {
	var orderid=orderid;
      var confirmPopup = $ionicPopup.confirm({
         title: 'Cancel Order',
         template: 'Are you sure?'
      });

      confirmPopup.then(function(res) {
         if(res) {
            console.log('Sure!');
            $scope.orderCancel(orderid);
         } else {
            console.log('Not sure!');
         }
      });
		
   };

 $scope.orderCancel=function(orderid){
  
                  

     LoginService.get(api_url+'order/cancelled/'+orderid).then(function(resp) {      
          $cordovaToast.showLongCenter('Order cancelled').then(function(success) {
                   

              }, function (error) {

              });
              $scope.order.status='cancelled';
              $state.go("app.order");

          }, function(err) {
            if(err.success==false && err.type=='token_expired'){
                $scope.logOut();
        
        }

          });


          
  }
})
.controller('MysubscriptionCtrl', function($window,$ionicNavBarDelegate,$ionicHistory,$ionicPlatform,$stateParams,$cordovaToast,$ionicLoading,$window,$ionicHistory,$http,$rootScope,$ionicPopup,$scope,$ionicPopover,$ionicModal,$timeout, LoginService, $state,$cordovaGeolocation, $location,$cordovaBadge,$cordovaSocialSharing,$ionicScrollDelegate) {

  $scope.getSubscription=function(){
        LoginService.get(api_url+'package/subscriptions').then(function(resp) {     
            if(resp.data.status==true){
                $scope.subscriptions=resp.data.result;
                $scope.subcribepackage=resp.data.subscription;
                if(resp.data.subscription!=false){
                    $scope.isLimitUsed=resp.data.isLimitUsed;
                    
                }

            }else{
                    $scope.subscriptions=null;


            }
            }, function(err) {
                console.error('ERR', err);
                console.log(err);

        });
    }
    $scope.$on('$ionicView.beforeEnter', function () {
        $scope.getSubscription();
      });
    //$scope.getSubscription();
    $scope.unsubscribe=function(subscribeid){

        LoginService.get(api_url+'package/unsubscribe/'+subscribeid).then(function(resp) {
                        $cordovaToast.showLongCenter('Package Unsubscribed').then(function(success) {

                            $scope.getSubscription();
                            

                        }, function (error) {

                        });
            
            
        }, function(err) {
            if(err.success==false && err.type=='token_expired'){
            $scope.logOut();

            }


  }); 



    }


    $scope.confirmcancel = function(subscribeid) {
        var orderid=orderid;
        var index=index;
          var confirmPopup = $ionicPopup.confirm({
             title: 'Unsubscribe',
             template: 'Are you sure?'
          });
    
          confirmPopup.then(function(res) {
             if(res) {
                console.log('Sure!');
                $scope.unsubscribe(subscribeid);
             } else {
                console.log('Not sure!');
             }
          });
            
       };
    $scope.subscription={}; 
    $scope.btntxt='Subscribe';
        $scope.subscribe=function(pack,package_id){
            if( !LoginService.getToken()) {                
                $state.go("app.home");
                    
            }else{
    
                var obj={};
                 obj.package_id=package_id;
       var data=JSON.stringify(obj);
                    LoginService.post(api_url+'package/subscribe',data).then(function(resp) {
                      if(resp.data.status==true){
                          pack.btntxt='Subscribed';
                          
                      }else{
                        pack.btntxt='Already Subscribed!';
                      }

                      $window.location.reload();
              
      
                    }, function(err) {
                              if(err.success==false && err.type=='token_expired'){
                              $scope.logOut();
    
                              }
    
      
                    }); 
            }
            
                    
            
              }
})

.controller('PackagesCtrl', function($window,$scope, $http, $state,$stateParams,LoginService,$cordovaToast,$ionicPopup,$ionicModal,$rootScope ) {

 $scope.getMonthlyPackages=function(){
        LoginService.get(api_url+'package/list').then(function(resp) {     
            if(resp.data.status==true){
                $scope.monthlypackages=resp.data.result;
                $scope.subcribepackage=resp.data.subscription;
                if(resp.data.subscription!=false){
                    $scope.isLimitUsed=resp.data.isLimitUsed;
                    
                }

            }else{

                $scope.monthlypackages={};
                
            }  
                
            }, function(err) {
                console.error('ERR', err);
                console.log(err);

        });
    }
    $scope.$on('$ionicView.beforeEnter', function () {
        $scope.getMonthlyPackages();
      }); 
    


    $ionicModal.fromTemplateUrl('templates/subscribemodal.html', {
        scope: $scope,
        animation: 'slide-in-up',
    }).then(function(modal) {
        $rootScope.subscribebox = modal;
        
    });

    $rootScope.openSubscribeModal=function(package) {
        $scope.curentpackage=package;
        $scope.subppack={};
        $scope.subppack.package_id=package.id;
        $scope.subppack.pickup_option=package.pickup_options.split(',')[0].trim();
        $scope.subppack.duration=package.available_packages.split(',')[0].trim();
        $rootScope.subscribebox.show();
    };

    $rootScope.closeSubscribeModal = function() {
            //jQuery('#newlocation').val('');
        $rootScope.subscribebox.hide();
    };

    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $rootScope.subscribebox.remove();
    });

    // Execute action on hide modal
    $rootScope.$on('locationmodal.hidden', function() {
        // Execute action
    });

    // Execute action on remove modal
    $rootScope.$on('locationmodal.removed', function() {
        // Execute action
    });


    $scope.subscribePack=function(currpack){
        if( !LoginService.getToken()) {                
            $state.go("app.home");
                
        }else{

            var obj={};
             obj.package_id=currpack.package_id;
             obj.pickup_option=currpack.pickup_option;
             obj.duration=currpack.duration;
   var data=JSON.stringify(obj);
   console.log(data);
                LoginService.post(api_url+'package/subscribe',data).then(function(resp) {
                  $rootScope.closeSubscribeModal();
                  // $state.go($state.current, {}, {reload: true});
                    $cordovaToast.showLongCenter(resp.data.message).then(function(success) {
                                                                    
                        $scope.getMonthlyPackages();
                        
                                      }, function (error) {
                        
                                      });
              
          
  
                }, function(err) {
                          if(err.success==false && err.type=='token_expired'){
                          $scope.logOut();

                          }

  
                }); 
        }
        
                
        
          }
})

  .controller('AllOrderCtrl', function($window,$scope, $http, $state,$stateParams,LoginService,$cordovaToast,$ionicPopup, $rootScope,$ionicSideMenuDelegate) {
        $scope.$on('$ionicView.beforeEnter', function (e,config) {
        config.enableBack = false;
        });
        $rootScope.$ionicGoBack = function(backCount) {
        $ionicSideMenuDelegate.toggleLeft();

        //$state.go('app.order');
        }; 
    var page=1;
    $scope.moreresults =true;

$scope.notfound=false;
$scope.getAllorders=function(pageno){
            var obj={};
            obj.page=pageno;
            obj.limit=3;
          

                   var data=JSON.stringify(obj);

        LoginService.post(api_url+'order/getAll',data).then(function(resp) {   
          
          if(resp.data.length==0 && page==1 ){
                $scope.notfound=true;
            }
          if(resp.data.length){
               $scope.notfound=false;
              if(resp.data.length < obj.limit){
                  $scope.moreresults =true;
              }else{
                  $scope.moreresults =false;
              }
          if(page!=1 ){
                  for (var i=0; i<resp.data.length; i++){
                      $scope.allorders.push(resp.data[i]);
                  }
                  $scope.$broadcast('scroll.infiniteScrollComplete');
              }else{
                  $scope.allorders=resp.data;
              }

          }else{
             $scope.moreresults =true;
          }
          




          }, function(err) {
            if(err.success==false && err.type=='token_expired'){
                $scope.logOut();
        
        }

          });
     }
$scope.getAllorders(page);
 $scope.loadMoreOrders= function() {
        page++;
        $scope.getAllorders(page);

    };
    $scope.confirmcancel = function(index,orderid) {
        var orderid=orderid;
        var index=index;
          var confirmPopup = $ionicPopup.confirm({
             title: 'Cancel Order',
             template: 'Are you sure?'
          });
    
          confirmPopup.then(function(res) {
             if(res) {
                console.log('Sure!');
                $scope.orderCancel(index,orderid);
             } else {
                console.log('Not sure!');
             }
          });
            
       };

  $scope.orderCancel=function($index,orderid){
  
                  

     LoginService.get(api_url+'order/cancelled/'+orderid).then(function(resp) {    
        $scope.allorders[$index]['status']='cancelled';  
          $cordovaToast.showLongCenter('Order cancelled').then(function(success) {

                     


              }, function (error) {

              });
    

          }, function(err) {
            if(err.success==false && err.type=='token_expired'){
                $scope.logOut();
        
        }

          });
  }
  //  $scope.getAllorders();

})

.controller('RateCtrl', function($scope, $http, $state,$stateParams,$ionicSlideBoxDelegate,$rootScope) {
    $scope.basequantity=0;
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        $('.iw-qty').val(0);
        $scope.categories=[];
        $scope.getcats();
        quantity_added=0;
        total=0; 
        tax=0;
        setCartData();
        $(".estimator").hide();

    });
    function setCartData(){
  
        $("#quantity").text(quantity_added);
        $("#total").text(total);
        var tax = (total * 14.5)/100;
        $("#tax").text(tax);
    }
    function vibrateCart(){
        var i=0;
        var myInterval = setInterval(function(){
            if(i++>=10)
                clearInterval(myInterval);
            if(i%2==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(15deg)",
                    "-moz-transform": "rotate(15deg)",
                    "transform": "rotate(15deg)",
                    "-o-transform": "rotate(15deg)",
                    "-ms-transform": "rotate(15deg)"
                });        
            }
            else if(i%3==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(-15deg)",
                    "-moz-transform": "rotate(-15deg)",
                    "transform": "rotate(-15deg)" ,
                    "-o-transform": "rotate(-15deg)",
                    "-ms-transform": "rotate(-15deg)"
                });   
            }
            else{
                $(".estimator img").css({
                    "-webkit-transform": "rotate(0deg)",
                    "-moz-transform": "rotate(0deg)",
                    "transform": "rotate(0deg)",
                    "-o-transform": "rotate(0deg)",
                    "-ms-transform": "rotate(0deg)"
                });   
            }
        }, 100);
     }  
        $scope.getservices=function(catid){
        $http.get(api_url+'services/'+catid).then(function(resp) {      
          console.log($scope.categories);
                        $scope.services=resp.data;
                        $scope.categories[0].service=$scope.services;
                        $ionicSlideBoxDelegate.update();
          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }
    $scope.getcats=function(){
        $http.get(api_url+'categories').then(function(resp) {
            $scope.categories=resp.data;
            for (i=0; i< $scope.categories.length;i++){

            console.log($scope.categories);
                    items_added[$scope.categories[i]['id']] = new Array();   
            }
            console.log('items_added');
            console.log(items_added);
            $scope.getservices($scope.categories[0]['id']);
            $ionicSlideBoxDelegate.update();
            console.log('items_added');
          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }


    $scope.getcats();

      $scope.increment=function(categoryid,itemid,price){

          var item_label =itemid;

          if(items_added[categoryid][item_label]){
          items_added[categoryid][item_label]++;
          this.service.quantity=items_added[categoryid][item_label];
          }else{
          items_added[categoryid][item_label]=1;
          this.service.quantity=items_added[categoryid][item_label];

          }    
          if(quantity_added==0){
             $(".estimator").fadeIn(1500);

          }
             
          quantity_added++;
          total +=  Number(price);
          setCartData();
          vibrateCart();
          $ionicSlideBoxDelegate.update();

       }


$scope.decrement=function(categoryid,itemid,price){

 var item_label =itemid;

                if(items_added[categoryid][item_label]>0){
                    items_added[categoryid][item_label]--;
                    quantity_added--;
                    total -= Number(price);
                    setCartData();
                    if(quantity_added==0)
                        $(".estimator").fadeOut(1000);
                }
              this.service.quantity=items_added[categoryid][item_label];
                vibrateCart();
                $ionicSlideBoxDelegate.update();

}

  $scope.getservicesbycat=function($index,catid){
    console.log($index);
        var curr= $scope.categories[$index];

   if(curr.service==undefined){
       $http.get(api_url+'services/'+catid).then(function(resp) {      
                            $scope.services=resp.data;
                            if(resp.data.length){
                             curr.service=resp.data;
                              
                            }
                           $ionicSlideBoxDelegate.update();
              }, function(err) {
                  console.error('ERR', err);
                  console.log(err);

              });

    }

       
      
        
     }

   

          
 
        $ionicSlideBoxDelegate.update();


}); 