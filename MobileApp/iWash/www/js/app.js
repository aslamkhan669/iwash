// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngCordovaOauth', 'ngCordova'])

.run(function($ionicHistory,$rootScope,$timeout,$window,$state,$ionicPopup,$location, LoginService,$ionicPlatform,$cordovaNetwork,$ionicLoading) {



    $ionicPlatform.registerBackButtonAction(function (event) {
        event.preventDefault();
      if ($state.current.name=='app.home') {
				$ionicPopup.confirm({
				title: 'Confirm Exit',
				template: 'Are you sure you want to exit?'
				}).then(function(res) {
				if (res) {
				//ionic.Platform.exitApp();
				navigator.app.exitApp();

				}
				})
	  }
	   else if ($state.current.name=='login') {
		   				navigator.app.exitApp();

		   
       }else if ($state.current.name=='app.orderSuccess') {
		     $state.go('app.home');
          //navigator.app.exitApp();
        } else {
          $ionicHistory.goBack();
        }
     }, 100);
 $rootScope.$on( '$stateChangeStart', function(e, toState  , toParams
                                                   , fromState, fromParams) {


 if(fromState.name=='intro' || fromState.name==''){
  
  /*  $ionicLoading.show({
                template: '<ion-spinner class="spinner-energized"></ion-spinner>'
                });*/
  }
        var isintro = toState.name=='login';
        var logcheck=LoginService.getToken();
        if(isintro && logcheck){
              
            e.preventDefault();

            $state.go('app.home'); // go to home


        }
         else if(!isintro && (logcheck ==null || logcheck==undefined ) ){

                      e.preventDefault();


           $state.go('login'); // go to home
        
        }


 
               });

               $rootScope.$on('$ionicView.beforeEnter', function (event, toState, toParams, fromState, fromParams) {
                
                        
                      
                                   if(window.Connection) {
                                      if(navigator.connection.type == Connection.NONE) {
                                          $ionicPopup.alert({
                                              title: "Internet Disconnected",
                                              content: "The internet is disconnected on your device."
                                          })
                                          .then(function(result) {
                                            
                                                  ionic.Platform.exitApp();
                                            
                                          });
                                      }
                                  }            
                             
                          });
               $ionicPlatform.ready(function() {
                
               
                             if(window.Connection) {
                                       if(navigator.connection.type == Connection.NONE) {
                                           $ionicPopup.alert({
                                               title: "Internet Disconnected",
                                               content: "The internet is disconnected on your device."
                                           })
                                           .then(function(result) {
                                             
                                                   ionic.Platform.exitApp();
                                             
                                           });
                                       }
                                   }
               
                               if (window.cordova && window.cordova.plugins.Keyboard) {
                                 cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                                 cordova.plugins.Keyboard.disableScroll(true);
               
                               }
                               if (window.StatusBar) {
                                 StatusBar.styleDefault();
                                }
                         });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.backButton.text('');
    
    $ionicConfigProvider.backButton.previousTitleText(false);
    
    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })
        .state('app.home', {
            url: '/home',
            views: {
                'menuContent': {
                    templateUrl: 'templates/home.html',
                    controller:'HomeCtrl'
                }
            }
        })
        .state('app.order', {
            url: '/order',
            views: {
                'menuContent': {
                    templateUrl: 'templates/order.html',
                     controller:'AllOrderCtrl'
                }
            }
        })

  


  .state('app.order-details', {
            url: '/order-details/:orderid',
            views: {
                'menuContent': {
                    templateUrl: 'templates/order-details.html',
                     controller:'OrderDetailsCtrl'
                }
            }
        })
    .state('app.rate', {
        url: '/rate',
        views: {
            'menuContent': {
                templateUrl: 'templates/rate.html',
                 controller: 'RateCtrl'
            }
        }
    })
    .state('app.packages', {
        url: '/packages',
        views: {
            'menuContent': {
                templateUrl: 'templates/packages.html',
                 controller: 'PackagesCtrl'
            }
        }
    })
    .state('app.mysubscription', {
        url: '/mysubscription',
        views: {
            'menuContent': {
                templateUrl: 'templates/mysubscription.html',
                 controller: 'MysubscriptionCtrl'
            }
        }
    })
    .state('app.services', {
        url: '/services',
        views: {
            'menuContent': {
                templateUrl: 'templates/services.html',
                controller:'ServiceFeaturesCtrl'
            }
        }
    })
  .state('app.placeorderStep2', {
            url: '/placeorder/step2',
             views: {
            'menuContent': {
          templateUrl: 'templates/order-step2.html',
                     controller:'PlaceOrderStep2Ctrl'
            }
        }

           
                 
            
        })
        .state('app.placeorderStep3', {
            url: '/placeorder/step3',
           
                   views: {
            'menuContent': {
          templateUrl: 'templates/order-step3.html',
                     controller:'PlaceOrderStep3Ctrl'
            }
        }
            
        })
        .state('app.placeorderStep4', {
            url: '/placeorder/step4',
           
                    views: {
            'menuContent': {
          templateUrl: 'templates/order-step4.html',
                     controller:'PlaceOrderStep4Ctrl'
            }
        }
            
        })
        .state('app.placeorderStep5', {
            url: '/placeorder/step5',
           
                    views: {
            'menuContent': {
          templateUrl: 'templates/order-step5.html',
                     controller:'PlaceOrderStep5Ctrl'
            }
        }
            
        })
        .state('app.placeorderStep6', {
            url: '/placeorder/step6',
           
                    views: {
            'menuContent': {
          templateUrl: 'templates/order-step6.html',
                     controller:'PlaceOrderStep6Ctrl'
            }
        }
            
        })
        .state('app.orderSuccess', {
            url: '/order/success/:orderid',
            views: {
                'menuContent': {
                    templateUrl: 'templates/ordersuccessful.html',
                     controller:'orderSuccessCtrl'
                }
            }
        })

   
.state('app.support', {
            url: '/support',
            views: {
                'menuContent': {
                    templateUrl: 'templates/support.html'
                }
            }
        })
    .state('app.faq', {
            url: '/faq',
            views: {
                'menuContent': {
                    templateUrl: 'templates/faq.html',
                    controller: 'FaqCtrl'

                }
            }
        })
        
   .state('login', {
        url: '/login',
    
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    
    });
  $urlRouterProvider.otherwise('/login');

});
