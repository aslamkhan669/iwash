-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: iwash
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (5,'uploads/banners/banner_2017-08-02 10:57:44.jpg','1','2017-08-02 10:57:44','2017-08-02 10:57:44'),(6,'uploads/banners/banner_2017-08-02 10:57:48.jpg','1','2017-08-02 10:57:48','2017-08-02 10:57:48'),(14,'uploads/banners/banner_67947abb05bc9cab6895332e8ec48f03.jpg','1','2017-08-18 11:06:43','2017-08-18 11:06:43'),(15,'uploads/banners/banner_3e18ba2ebb4b13f57a455abc7cafbf3b.jpg','1','2017-08-18 11:07:06','2017-08-18 11:07:06');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'branch1','location1','2017-08-17 10:43:44','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `per` varchar(100) NOT NULL,
  `rank_weight` int(10) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Laundry',0,'Laundry','uploads/categories/cat_df37d0533e764b1eed6e0e324a9afb58.png',0.00,'',0,'2017-06-29 07:31:49','0000-00-00 00:00:00'),(2,'Men',0,'Men','uploads/categories/cat_499efbe825d993c93abdbd588aa1ad18.png',0.00,'',0,'2017-06-29 07:33:17','0000-00-00 00:00:00'),(3,'Women',0,'Women','uploads/categories/cat_ed89033621d3fce0fdded313720514fc.png',0.00,'',0,'2017-06-29 07:33:32','0000-00-00 00:00:00'),(4,'Household',0,'Household','uploads/categories/cat_adf8cd29610f232ac5620c1fa9ed9f81.png',0.00,'',0,'2017-06-29 07:33:55','0000-00-00 00:00:00'),(5,'Woolen',0,'Woolen','uploads/categories/cat_5c3fb6d37b83aaa48d6084b633e1004a.png',0.00,'',0,'2017-06-29 07:34:17','0000-00-00 00:00:00'),(6,'Accessories',0,'Accessories','uploads/categories/cat_816deda9d7abbb71f037787a2f3bf697.png',0.00,'',0,'2017-07-03 07:41:31','0000-00-00 00:00:00'),(7,'Kids',0,'Kids','uploads/categories/cat_b73c4f83273f45d667f809163b818b9f.png',0.00,'',0,'2017-07-03 07:41:55','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Rakesh Bisht','9650719414','raks.bisht@gmail.com',0,'hello testing','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverytime`
--

DROP TABLE IF EXISTS `deliverytime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverytime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timebetween` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverytime`
--

LOCK TABLES `deliverytime` WRITE;
/*!40000 ALTER TABLE `deliverytime` DISABLE KEYS */;
INSERT INTO `deliverytime` VALUES (1,'12:00 PM to 03:00 PM',1,'2017-07-27 11:10:22','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `deliverytime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_categories`
--

DROP TABLE IF EXISTS `faq_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_categories`
--

LOCK TABLES `faq_categories` WRITE;
/*!40000 ALTER TABLE `faq_categories` DISABLE KEYS */;
INSERT INTO `faq_categories` VALUES (1,'faq category 1',0,'faq category 1','uploads/faqcat/cat_2017-08-02 11:36:26.jpg','2017-07-03 09:28:25','0000-00-00 00:00:00'),(3,'testfaq',0,'testfaq','uploads/faqcat/faqcat_2017-08-02 11:37:42.jpg','2017-08-02 11:37:42','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `faq_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` VALUES (2,1,'question1','a','2017-06-30 11:02:47','0000-00-00 00:00:00'),(3,2,'question2','aa','2017-06-30 11:02:56','0000-00-00 00:00:00'),(4,1,'question1_2','a','2017-07-08 10:34:57','0000-00-00 00:00:00'),(5,1,'aa','aa','2017-07-20 07:44:59','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `featured_services`
--

DROP TABLE IF EXISTS `featured_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `featured_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `per` varchar(100) NOT NULL,
  `rank_weight` int(10) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `featured_services`
--

LOCK TABLES `featured_services` WRITE;
/*!40000 ALTER TABLE `featured_services` DISABLE KEYS */;
INSERT INTO `featured_services` VALUES (1,'Wash & Fold','Wash & Fold','',59.00,'',0,'2017-06-29 07:31:49','0000-00-00 00:00:00'),(2,'Wash & Iron','Wash & Iron','',89.00,'',0,'2017-06-29 07:33:17','0000-00-00 00:00:00'),(3,'Premium Laundry','Premium Laundry','',149.00,'',0,'2017-06-29 07:33:32','0000-00-00 00:00:00'),(4,'Dry Cleaning','Dry Cleaning','',79.00,'',0,'2017-06-29 07:33:55','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `featured_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `totalamount` float(10,2) DEFAULT NULL,
  `flatandstreet` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `addressType` enum('home','office','other') NOT NULL,
  `pickupDate` date NOT NULL,
  `deliveryDate` date NOT NULL,
  `timeOfDelivery` tinyint(1) NOT NULL,
  `timeOfPickup` tinyint(1) NOT NULL,
  `status` enum('cancelled','pending','placed','paid','completed') NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,6,1,22.00,'demoaddress','demolandmark','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-07-26 06:37:34','0000-00-00 00:00:00'),(2,6,1,22.00,'demoaddress','demolandmark','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-07-26 01:07:34','0000-00-00 00:00:00'),(3,6,2,NULL,'dgdfgdfg','dgdf','home','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:00:30','0000-00-00 00:00:00'),(4,6,2,NULL,'dgdfgdfg','dgdf','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-01 12:03:15','0000-00-00 00:00:00'),(5,6,2,NULL,'dgdfgdfg','dgdf','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-01 12:06:16','0000-00-00 00:00:00'),(6,6,3,NULL,'sfsdf','afafaf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:07:43','0000-00-00 00:00:00'),(7,6,2,NULL,'afafa','sdfsdf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:17:41','0000-00-00 00:00:00'),(8,6,3,NULL,'sdfsdf','sdfsdf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:18:50','0000-00-00 00:00:00'),(9,6,2,NULL,'sdfsd','sdfs','other','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:21:21','0000-00-00 00:00:00'),(10,6,2,NULL,'fa','af','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:24:29','0000-00-00 00:00:00'),(11,6,3,NULL,'fds','fsdfsdf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:28:40','0000-00-00 00:00:00'),(12,6,1,NULL,'sfs','fsdfsd','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:29:51','0000-00-00 00:00:00'),(13,6,4,NULL,'sdfsd','fsdfsdf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-01 12:30:45','0000-00-00 00:00:00'),(14,13,2,NULL,'305 badar bagh','Aligarh','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-05 14:49:40','0000-00-00 00:00:00'),(15,6,1,NULL,'Uquwuwuw','Hajajaj','home','0000-00-00','0000-00-00',1,2,'cancelled','2017-08-07 13:54:08','0000-00-00 00:00:00'),(16,15,1,NULL,'Ddddd','Fffff','home','0000-00-00','0000-00-00',1,2,'cancelled','2017-08-07 14:01:52','0000-00-00 00:00:00'),(17,6,2,NULL,'fs','fsdf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-07 16:44:15','0000-00-00 00:00:00'),(18,6,4,NULL,'a','a','office','0000-00-00','0000-00-00',1,2,'placed','2017-08-07 17:33:12','0000-00-00 00:00:00'),(19,6,3,NULL,'gg','vv','office','0000-00-00','0000-00-00',1,2,'placed','2017-08-07 17:54:06','0000-00-00 00:00:00'),(20,6,2,NULL,'sfsdf','sdfsdfsdf','other','0000-00-00','0000-00-00',1,1,'placed','2017-08-08 06:20:06','0000-00-00 00:00:00'),(21,6,2,NULL,'sdf','sdf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-08 07:08:32','0000-00-00 00:00:00'),(22,6,2,NULL,'sdfsd','fsdf','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-08 07:10:59','0000-00-00 00:00:00'),(23,6,1,NULL,'Hello','Hello','home','0000-00-00','0000-00-00',1,1,'placed','2017-08-08 08:32:58','0000-00-00 00:00:00'),(24,15,1,NULL,'Hahahah','Hahahjjjwjw','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 12:10:35','0000-00-00 00:00:00'),(25,15,1,NULL,'Hsueususuu','Ususususuus','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 12:31:57','0000-00-00 00:00:00'),(26,15,1,NULL,'Ffff','Ffrt','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 12:52:23','0000-00-00 00:00:00'),(27,6,1,NULL,'Hello','Hello','other','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 13:23:42','0000-00-00 00:00:00'),(28,15,1,NULL,'hahahah','hshzhshbs','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 13:35:43','0000-00-00 00:00:00'),(29,6,3,NULL,'Aslam','Near','office','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 13:36:52','0000-00-00 00:00:00'),(30,16,4,NULL,'Dhorra','Aligarh','home','0000-00-00','0000-00-00',1,2,'cancelled','2017-08-08 13:40:29','0000-00-00 00:00:00'),(31,17,1,NULL,'305 badar bhag','shamsad','office','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 13:41:52','0000-00-00 00:00:00'),(32,16,4,NULL,'Abc','Hfh','other','0000-00-00','0000-00-00',1,2,'cancelled','2017-08-08 13:45:00','0000-00-00 00:00:00'),(33,13,1,NULL,'Bzbs','Hzhsh','office','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-08 14:56:10','0000-00-00 00:00:00'),(34,13,1,NULL,'Add','Jdjdj','other','0000-00-00','0000-00-00',1,1,'placed','2017-08-08 15:03:02','0000-00-00 00:00:00'),(35,6,2,NULL,'vxvxcvxv','xcvxcvxc','home','0000-00-00','0000-00-00',1,1,'placed','2017-08-08 15:54:10','0000-00-00 00:00:00'),(36,6,2,NULL,'t','t','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-08 16:04:03','0000-00-00 00:00:00'),(37,6,4,NULL,'sdf','sdfs','home','2017-08-19','2017-08-25',1,1,'placed','2017-08-08 16:15:33','0000-00-00 00:00:00'),(38,6,2,NULL,'sdf','ffffffffffffff','other','2017-08-18','2017-08-24',1,1,'cancelled','2017-08-11 10:05:32','0000-00-00 00:00:00'),(39,15,1,NULL,'najaj','hahaja','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-13 17:50:21','0000-00-00 00:00:00'),(40,15,1,NULL,'najaj','hahaja','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-13 17:50:22','0000-00-00 00:00:00'),(41,15,1,NULL,'najaj','hahaja','home','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-13 17:50:27','0000-00-00 00:00:00'),(42,6,2,NULL,'305 badar bhag aligarh','dena bank','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-17 09:59:18','0000-00-00 00:00:00'),(43,6,2,NULL,'flat11','aligarh','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-17 10:19:19','0000-00-00 00:00:00'),(44,6,2,NULL,'222','delhi','home','0000-00-00','0000-00-00',1,1,'placed','2017-08-17 10:50:43','0000-00-00 00:00:00'),(45,6,1,NULL,'fsdfs','fsdf','office','0000-00-00','0000-00-00',1,1,'cancelled','2017-08-17 11:08:08','0000-00-00 00:00:00'),(46,6,3,NULL,'111','aligarh','office','0000-00-00','0000-00-00',1,1,'placed','2017-08-17 11:12:19','0000-00-00 00:00:00'),(47,6,2,NULL,'sdf','sfsdf','office','2017-08-18','2017-08-31',1,1,'placed','2017-08-17 11:13:27','0000-00-00 00:00:00'),(48,6,3,NULL,'11','abc','other','0000-00-00','0000-00-00',1,2,'placed','2017-08-17 11:15:21','0000-00-00 00:00:00'),(49,6,4,NULL,'11','delhi','other','0000-00-00','0000-00-00',1,1,'placed','2017-08-17 11:17:14','0000-00-00 00:00:00'),(50,6,2,NULL,'address','landmark','home','2017-08-26','2017-08-30',1,2,'placed','2017-08-17 11:23:50','0000-00-00 00:00:00'),(51,6,3,NULL,'111','landmark','home','0000-00-00','0000-00-00',1,1,'placed','2017-08-17 11:32:11','0000-00-00 00:00:00'),(52,6,2,NULL,'1102 civil line','landmark','home','2017-08-19','2017-08-24',1,1,'placed','2017-08-17 11:36:17','0000-00-00 00:00:00'),(53,6,3,NULL,'123','dena babk','home','2017-08-18','2017-08-30',1,2,'placed','2017-08-17 11:40:55','0000-00-00 00:00:00'),(54,6,4,NULL,'12','bank','office','2017-08-18','2017-08-30',1,1,'placed','2017-08-17 13:14:02','0000-00-00 00:00:00'),(55,6,1,NULL,'faf','afa','office','2017-08-19','2017-08-31',1,1,'placed','2017-08-17 18:44:09','0000-00-00 00:00:00'),(56,6,1,NULL,'fsdf','fsdf','office','2017-08-19','2017-08-22',1,1,'placed','2017-08-17 18:46:51','0000-00-00 00:00:00'),(57,6,2,NULL,'sdf','sdf','office','2017-08-25','2017-08-23',1,1,'placed','2017-08-17 18:48:25','0000-00-00 00:00:00'),(58,6,2,NULL,'sdf','sdfsd','office','2017-08-19','2017-08-29',1,2,'placed','2017-08-17 18:49:17','0000-00-00 00:00:00'),(59,6,2,NULL,'dfg','gdgd','other','2017-08-25','2017-08-30',1,2,'placed','2017-08-17 18:51:13','0000-00-00 00:00:00'),(60,6,2,NULL,'sdf','sdfsd','office','2017-08-30','2017-08-26',1,1,'placed','2017-08-18 09:14:30','0000-00-00 00:00:00'),(61,6,4,NULL,'12','landmarks','other','2017-08-25','2017-08-21',1,1,'placed','2017-08-18 09:36:10','0000-00-00 00:00:00'),(62,6,2,NULL,'11','landmark','home','2017-08-19','2017-08-23',1,2,'placed','2017-08-18 09:49:02','0000-00-00 00:00:00'),(63,6,2,NULL,'13','landmark','office','2017-08-22','2017-08-31',1,1,'placed','2017-08-18 10:03:40','0000-00-00 00:00:00'),(64,6,4,NULL,'14','aligarh','home','2017-08-19','2017-08-31',1,1,'placed','2017-08-18 10:09:23','0000-00-00 00:00:00'),(65,6,2,NULL,'0099','landmark','office','2017-08-25','2017-08-31',1,1,'placed','2017-08-18 10:16:33','0000-00-00 00:00:00'),(66,6,2,NULL,'11','landmark','office','2017-08-18','2017-08-21',1,1,'placed','2017-08-18 11:01:37','0000-00-00 00:00:00'),(67,6,2,NULL,'1100001 line','landmark','other','2017-08-19','2017-08-31',1,1,'placed','2017-08-18 11:44:01','0000-00-00 00:00:00'),(68,6,30,NULL,'111','delhi','office','2017-08-19','2017-08-30',1,1,'cancelled','2017-08-18 12:17:31','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_service`
--

DROP TABLE IF EXISTS `order_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_service`
--

LOCK TABLES `order_service` WRITE;
/*!40000 ALTER TABLE `order_service` DISABLE KEYS */;
INSERT INTO `order_service` VALUES (1,1,1,1,0.00,'2017-07-26 08:39:49','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `order_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pickuptime`
--

DROP TABLE IF EXISTS `pickuptime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pickuptime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timebetween` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pickuptime`
--

LOCK TABLES `pickuptime` WRITE;
/*!40000 ALTER TABLE `pickuptime` DISABLE KEYS */;
INSERT INTO `pickuptime` VALUES (1,'12:00 PM to 02:00 PM',1,'2017-07-27 11:10:14','0000-00-00 00:00:00'),(2,'12:00 PM to 03:00 PM',1,'2017-08-07 11:38:48','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `pickuptime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','admin','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'User','user','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Employee','employee','2017-08-16 12:24:01','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_features`
--

DROP TABLE IF EXISTS `service_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_features`
--

LOCK TABLES `service_features` WRITE;
/*!40000 ALTER TABLE `service_features` DISABLE KEYS */;
INSERT INTO `service_features` VALUES (1,1,'Garments','uploads/servicefeatures/service_42827cfda6958c733873b13255aed4ec.png','For Household Wear','2017-07-20 10:25:12','0000-00-00 00:00:00'),(2,2,'Garments','uploads/servicefeatures/service_8d034ab9c5eb5da880ee1bb95196d250.png','For Household Wear','2017-07-22 06:55:14','0000-00-00 00:00:00'),(3,3,'Garments','uploads/servicefeatures/service_630359e397c21d9984362775201a8edf.png','For Household Wears','2017-07-22 06:55:17','0000-00-00 00:00:00'),(5,28,'Garments','uploads/servicefeatures/','For Household wears','2017-08-02 13:40:59','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `service_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `per` varchar(100) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image_website` varchar(255) DEFAULT NULL,
  `image_app` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (8,'Pants',2,'some ','uploads/services/service_7c9f77d931c13b2dec39ca1066050f23.jpg',5.00,'item','2017-08-18 10:15:41','0000-00-00 00:00:00',NULL,NULL),(9,'Jeans',2,'some','uploads/services/service_76825781ae82b4a0bbb2cf5cebb5e00f.jpg',5.00,'item','2017-08-18 10:18:22','0000-00-00 00:00:00',NULL,NULL),(10,'Formal Pants',2,'some','uploads/services/service_8bf18fa5f855f64700432972986342fd.jpg',5.00,'item','2017-08-18 10:19:19','0000-00-00 00:00:00',NULL,NULL),(11,'Formal Pants',2,'some','uploads/services/service_09c75fc19475689dfcf8d3fef03c0db6.jpg',5.00,'item','2017-08-18 10:26:44','0000-00-00 00:00:00',NULL,NULL),(12,'Abayas',3,'some','uploads/services/service_595ed86c9476970912fe3a13233c6bb6.jpg',5.00,'item','2017-08-18 10:28:14','0000-00-00 00:00:00',NULL,NULL),(13,'Towels',4,'some','uploads/services/service_e491974c4e283f54a422fcb6746931c7.jpg',3.00,'item','2017-08-18 10:29:10','0000-00-00 00:00:00',NULL,NULL),(14,'Tops',3,'some','uploads/services/service_43368bc0ce18998606367e2cfd5bda56.jpg',5.00,'item','2017-08-18 10:30:08','0000-00-00 00:00:00',NULL,NULL),(15,'Shorts',7,'some','uploads/services/service_79445eff3413f7d548d2066fc970b69d.jpg',5.00,'item','2017-08-18 10:32:13','0000-00-00 00:00:00',NULL,NULL),(16,'Suit Jackets',3,'some','uploads/services/service_c8f2dc7d0e0baca63f012eeb21cca322.jpg',15.00,'item','2017-08-18 10:37:44','0000-00-00 00:00:00',NULL,NULL),(17,'Pillow Case',4,'some','uploads/services/service_68db50e7b2f6c3e2e22da49eca05e484.jpg',3.00,'item','2017-08-18 10:41:59','0000-00-00 00:00:00',NULL,NULL),(18,'Bed Cover',4,'some','uploads/services/service_3b02f69aa707796530a293c5775b0c54.jpg',5.00,'item','2017-08-18 10:42:48','0000-00-00 00:00:00',NULL,NULL),(19,'Comforter',4,'some','uploads/services/service_bc631807d8f0fc5873b9fb58dfe2dba3.jpg',10.00,'i','2017-08-18 10:43:31','0000-00-00 00:00:00',NULL,NULL),(20,'Bedsheets',4,'some','uploads/services/service_a9f16222440888c7b0104ddf99e44edf.jpg',6.00,'item','2017-08-18 10:44:20','0000-00-00 00:00:00',NULL,NULL),(21,'Formal Shirts',2,'some','uploads/services/service_3f5b4d20bcf2f7f9aad3eb3536d34c7c.jpg',5.00,'item','2017-08-18 10:45:15','0000-00-00 00:00:00',NULL,NULL),(22,'Formal Shirts',2,'some','uploads/services/service_be87188b12a2122740159638dd7021b1.jpg',5.00,'item','2017-08-18 10:45:21','0000-00-00 00:00:00',NULL,NULL),(23,'Thobes',2,'some','uploads/services/service_8956916c2ca9173eca3ac79724455c1c.jpg',5.00,'item','2017-08-18 10:47:39','0000-00-00 00:00:00',NULL,NULL),(24,'Qtrah',5,'some','uploads/services/service_47022791d60b677cd68f2e2666a26095.jpg',3.00,'item','2017-08-18 10:50:01','0000-00-00 00:00:00',NULL,NULL),(25,'Fnyla',6,'some','uploads/services/service_62cf50a6ed4be3f4540f878e7de9f90e.jpg',3.00,'item','2017-08-18 10:50:58','0000-00-00 00:00:00',NULL,NULL),(26,'Srwal',3,'some','uploads/services/service_76b249a8039af5641c47be0e83af46ba.jpg',3.00,'item','2017-08-18 10:51:32','0000-00-00 00:00:00',NULL,NULL),(28,'Wash & Fold',1,'some','uploads/services/service_7d3f8039cfd3107d7f95449ef6ef3358.png',59.00,'kg','2017-08-18 11:45:57','0000-00-00 00:00:00',NULL,NULL),(29,'Wash & Iron',1,'some','uploads/services/service_ce4dba069e60b8adb6a0666f22e3394c.png',89.00,'kg','2017-08-18 11:49:25','0000-00-00 00:00:00',NULL,NULL),(30,'Premium Laundry',1,'some','uploads/services/service_eef2c6d316776b7ddce6118297f3a113.png',60.00,'kg','2017-08-18 11:50:51','0000-00-00 00:00:00',NULL,NULL),(31,'Dry Cleaning',1,'some','uploads/services/service_1598b34caf473d43c4a07cd20182a9d3.png',50.00,'kg','2017-08-18 11:52:59','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_account_sources`
--

DROP TABLE IF EXISTS `social_account_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_account_sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_account_sources`
--

LOCK TABLES `social_account_sources` WRITE;
/*!40000 ALTER TABLE `social_account_sources` DISABLE KEYS */;
INSERT INTO `social_account_sources` VALUES (1,'facebook','2016-04-15 02:08:58','0000-00-00 00:00:00'),(2,'gplus','2016-04-15 02:09:05','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `social_account_sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_accounts`
--

DROP TABLE IF EXISTS `social_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `souce_account_source_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_accounts_provider_id_unique` (`provider_id`),
  KEY `user_id` (`user_id`),
  KEY `souce_account_source_id` (`souce_account_source_id`),
  CONSTRAINT `social_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `social_accounts_ibfk_2` FOREIGN KEY (`souce_account_source_id`) REFERENCES `social_account_sources` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_accounts`
--

LOCK TABLES `social_accounts` WRITE;
/*!40000 ALTER TABLE `social_accounts` DISABLE KEYS */;
INSERT INTO `social_accounts` VALUES (1,'1679446768753998','aslamkhan669@gmail.com',NULL,1,13,'2017-08-05 11:56:57','0000-00-00 00:00:00'),(2,'1341651595947567','sid.allthebest91@gmail.com',NULL,1,16,'2017-08-08 13:34:11','0000-00-00 00:00:00'),(3,'853362528154534',NULL,NULL,1,17,'2017-08-08 13:39:16','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `social_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `profile` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (2,'Waseem','Software Engineer','Iwash doing a great job!','uploads/testimonials/testimonial_2017-08-02 11:28:51.png','2017-08-02 11:28:21','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profiles`
--

DROP TABLE IF EXISTS `user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_me` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_profiles_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profiles`
--

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;
INSERT INTO `user_profiles` VALUES (1,'Rakesh','Bisht','','',1,'2017-06-30 09:12:26','0000-00-00 00:00:00'),(2,'Rakesh','Bisht','','',2,'2017-07-12 10:52:57','0000-00-00 00:00:00'),(3,'ra','faf','','',5,'2017-07-24 10:48:08','0000-00-00 00:00:00'),(4,'RA','RA','','',6,'2017-07-24 10:50:35','0000-00-00 00:00:00'),(5,'Rakesh','Bisht','','',7,'2017-08-02 13:51:44','0000-00-00 00:00:00'),(6,'aslam','khan','','',8,'2017-08-03 09:04:51','0000-00-00 00:00:00'),(7,'junaid','siddiqui','','',9,'2017-08-03 09:05:26','0000-00-00 00:00:00'),(8,'fsdfsd','fsdf','','',12,'2017-08-05 08:26:34','0000-00-00 00:00:00'),(9,'Aslam','Khan','','',13,'2017-08-05 11:56:57','0000-00-00 00:00:00'),(10,'test','test','','',14,'2017-08-06 16:55:10','0000-00-00 00:00:00'),(11,'Waseem','Ahmad','','',15,'2017-08-07 13:57:21','0000-00-00 00:00:00'),(12,'Junaid','Sid','','',16,'2017-08-08 13:34:11','0000-00-00 00:00:00'),(13,'Amir','Ansari','','',17,'2017-08-08 13:39:16','0000-00-00 00:00:00'),(14,'Alia','Khan','','',18,'2017-08-08 19:21:10','0000-00-00 00:00:00'),(15,'Wasif','Gilani','','',19,'2017-08-14 06:55:54','0000-00-00 00:00:00'),(16,'md','amir','','',20,'2017-08-17 10:01:54','0000-00-00 00:00:00'),(17,'newemp1','newemp1','','',21,'2017-08-17 10:44:09','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `role_id` int(10) unsigned NOT NULL,
  `confirmed` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `mobile_confirmed` tinyint(1) NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'raks.bisht@gmail.com','','$2y$10$VAbzPbeTcGKp5OspvuezlOXaAhPQxPj1O0PPtsA/.AAOTo.ka57wK',NULL,'0',1,'1',1,NULL,'GEXqBGBkRisuVXV1ZaJDkPANmHP833YkLcZhtQ5xbAhOVVXI4CIm1CEI9eIl','2017-06-30 09:10:52','0000-00-00 00:00:00'),(2,'raks@test.com','','db59555ae86e3ca1626305eb1b8b8d7a',NULL,'1',2,'1',1,NULL,NULL,'2017-07-12 10:52:57','0000-00-00 00:00:00'),(5,'bisht.raksA@gmail.com',NULL,'cd9d48e9f242ee364e39de55a811dcf1',NULL,'1',2,'',0,'35ce6f72f4b5e11a0b8ed35f685132ca',NULL,'2017-07-24 10:48:08','0000-00-00 00:00:00'),(6,'bisht.raks@gmail.com',NULL,'cd9d48e9f242ee364e39de55a811dcf1',NULL,'0',2,'',0,'35ce6f72f4b5e11a0b8ed35f685132ca',NULL,'2017-07-24 10:50:35','0000-00-00 00:00:00'),(7,'raks.bishtsw@gmail.com',NULL,'db59555ae86e3ca1626305eb1b8b8d7a',NULL,'1',2,'1',1,NULL,NULL,'2017-08-02 13:51:44','0000-00-00 00:00:00'),(8,'aslam@gmail.com',NULL,'bc0595a6c1a1d7246c23333b0201eea6',NULL,'1',2,'1',1,NULL,NULL,'2017-08-03 09:04:51','0000-00-00 00:00:00'),(9,'junaid@sofyrus.com',NULL,'bc0595a6c1a1d7246c23333b0201eea6',NULL,'1',2,'1',1,NULL,NULL,'2017-08-03 09:05:26','0000-00-00 00:00:00'),(10,'rakesh@sofyrus.com',NULL,'0fccae940ba99190da5507470fca915b',NULL,'0',2,'0',0,'17bf03043247afaf4d8283a1515fe5e2',NULL,'2017-08-05 08:14:41','2017-08-05 08:14:41'),(11,'raks.bisht@outlook.com',NULL,'89f2868773bff8b9669c7784da388098',NULL,'0',2,'0',0,'0955706dd7d85b6f3706b6708a27a7aa',NULL,'2017-08-05 08:17:12','2017-08-05 08:17:12'),(12,'test@test.com',NULL,'7303ac0b9d74e6b23f8db16ab760aa26',NULL,'0',2,'0',0,'2b276d70000f20362c75bce9e601527e',NULL,'2017-08-05 08:26:34','2017-08-05 08:26:34'),(13,'aslamkhan669@gmail.com',NULL,NULL,NULL,'0',2,'0',0,NULL,NULL,'2017-08-05 11:56:57','2017-08-05 11:56:57'),(14,'test@tester.com',NULL,'acae54c95331fe242aaf9a4a733ba506',NULL,'1',2,'1',1,NULL,NULL,'2017-08-06 16:55:10','0000-00-00 00:00:00'),(15,'waseemzaidi85@gmail.com',NULL,'3c1ef886bbf276b2bafb68225b1beca8',NULL,'0',2,'0',0,'67ecc812693e190de811a77d4a31d35f',NULL,'2017-08-07 13:57:21','2017-08-07 13:57:21'),(16,'sid.allthebest91@gmail.com',NULL,NULL,NULL,'0',2,'0',0,NULL,NULL,'2017-08-08 13:34:11','2017-08-08 13:34:11'),(17,NULL,NULL,NULL,NULL,'0',2,'0',0,NULL,NULL,'2017-08-08 13:39:16','2017-08-08 13:39:16'),(18,'aliawasif1983@gmail.com',NULL,'d00f763cf69a1c093d962d63e2df23fe',NULL,'0',2,'0',0,'1722cb732e3269cdce24dd63a6d0f1f4',NULL,'2017-08-08 19:21:10','2017-08-08 19:21:10'),(19,'gilaniwasif@gmail.com',NULL,'e4eb11936dd8a70cb6c01cd6b002a16f',NULL,'0',2,'0',0,'c44b55e28b5afabde033bed8e4df3ca8',NULL,'2017-08-14 06:55:54','2017-08-14 06:55:54'),(20,'aamir@iul.ac.in',NULL,'ac9f8befe1aa0844b2ba26fd9cf503d8',NULL,'0',2,'0',0,'65d854107216faf8e7373c8267722d80',NULL,'2017-08-17 10:01:54','2017-08-17 10:01:54'),(21,'employee1@test.com','9643896566','$2y$10$ZbYSIfS/KNVwoc0DY0srz.9DTJaXzo1SvEEH.Sh66XT7M4U1ApZCq',1,'1',3,'1',1,NULL,'vbo9eMVS6AT2IKFRdiBq3c0Cm5AIfBofyRv6CxN3Xn2e2h8m5OentxjdRxbx','2017-08-17 10:44:09','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-18 13:05:01
