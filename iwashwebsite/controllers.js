//var api_url='http://54.202.108.46:3000/api/';
//var siteurl='http://54.202.108.46/site/';

//var api_url = 'http://localhost:3000/api/';
//var siteurl = 'http://localhost/';

var api_url='https://api.iwashqatar.com/';
var siteurl='https://iwashqatar.com/';
var items_added = [], dryClean_items_added = [], iron_items_added = [], quantity_added = 0, total = 0, tax = 0, category_selected = '';
var googleadd = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

angular.module('iwashApp.controllers', [])
    .directive('datepicker', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                $(element).datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: new Date(),

                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        ngModelCtrl.$render();
                        scope.$apply();
                    }
                });
            }
        };
    })
    .directive('changegoogleplace', function ($rootScope) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, model) {
                var options = {
                    types: [],
                    componentRestrictions: { country: "IN" }
                };
                scope.gPlacen = new google.maps.places.Autocomplete(element[0], options);

                google.maps.event.addListener(scope.gPlacen, 'place_changed', function () {
                    scope.$apply(function () {


                        model.$setViewValue(element.val());
                        // console.log(this);
                        $rootScope.location = element.val();
                        $rootScope.autolocation(element.val());
                        $rootScope.locationmodal.newlocation = '';

                        $('#newlocation').val('');
                    });
                });
            }
        };
    })
    .service('LoginService', function ($q, $http, $window) {
        var serviceToken, serviceHost, tokenKey;
        tokenKey = 'xaccesstoken';
        if (localStorage.getItem(tokenKey)) {
            serviceToken = $window.localStorage.getItem(tokenKey);
        }
        //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        return {
            loginUser: function (name, pw) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                var credentials = {};
                credentials.username = name;
                credentials.password = pw;
                var credentials = JSON.stringify(credentials);
                $http.post(api_url + 'user/authenticate', credentials).then(function (resp) {

                    if (resp.data.success === true) {
                        deferred.resolve(resp.data);
                    } else {
                        deferred.reject(resp.data);
                    }

                }, function (err) {
                    deferred.reject(resp.data);

                });
                promise.success = function (fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function (fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;

            },
            signup: function (fname, lname, email, mobile, password) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                var credentials = {};
                credentials.email = email;
                credentials.mobile = mobile;
                credentials.password = password;
                credentials.fname = fname;
                credentials.lname = lname;
                var credentials = JSON.stringify(credentials);
                $http.post(api_url + 'user/register', credentials).then(function (resp) {
                    if (resp.data.success === true) {
                        deferred.resolve(resp.data);
                    } else {
                        deferred.reject(resp.data);
                    }

                }, function (err) {
                    deferred.reject(resp.data);

                });
                promise.success = function (fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function (fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;

            },
            setAuth: function (authuser) {
                $window.localStorage.setItem('authuser', JSON.stringify(authuser));
            },

            getAuth: function () {
                if (localStorage.getItem('authuser')) {
                    var retrievedObject = $window.localStorage.getItem('authuser');
                    return JSON.parse(retrievedObject);
                }
            },

            setToken: function (token) {
                serviceToken = token;
                $window.localStorage.setItem(tokenKey, token);
            },

            getToken: function () {
                return serviceToken;
            },

            removeToken: function () {
                serviceToken = undefined;
                $window.localStorage.removeItem(tokenKey);
                $window.localStorage.removeItem('authuser');
            },

            get: function (uri, params) {
                return $http({
                    url: uri,
                    method: "GET",
                    params: {
                        xaccesstoken: serviceToken,
                        user_id: params
                    }
                });
            },

            post: function (uri, params) {
                if (params) {
                    params = JSON.parse(params);

                } else {
                    params = params || {};

                }
                params['xaccesstoken'] = serviceToken;
                params = JSON.stringify(params);
                return $http.post(uri, params);
            }
        };

    })

    .controller('AppCtrl', function ($state, $scope, $rootScope, LoginService, $timeout, GoogleSignin, $http, $anchorScroll, $location, toastr) {
        $rootScope.currentorder = {};
        $scope.currentservice = "";
        $scope.api_url = api_url;
        $rootScope.currentorder.timslot = {};
        $rootScope.isLoggedIn = false;
        $rootScope.orderstep = 1;

        $rootScope.gotoStep = function (stepno) {

            $rootScope.orderstep = stepno;
            $state.go('placeorderStep' + stepno);

        }

        $scope.getauthuserlanglatbyAddress = function (address) {
            $http.get(googleadd + address).then(function (resp) {
                if (resp.data.status == 'OK') {
                    console.log(resp.data.results[0]);
                    var lanlat = resp.data.results[0].geometry.location;
                    $rootScope.authlng = lanlat.lng;
                    $rootScope.authlat = lanlat.lat;
                    $rootScope.$broadcast('locationchange');
                    //$rootScope.$apply();
                }

            }, function (err) {
                console.error('ERR', err);

            });
        }

        $rootScope.autolocation = function (address) {

            $scope.getauthuserlanglatbyAddress(address);

        }
        $rootScope.getgeolocation = function () {

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                //alert(44444444);
            }

            function showPosition(position) {
                //console.log(position);
                //alert(position);

                var lat = position.coords.latitude;
                var long = position.coords.longitude;
                $rootScope.authlng = long;
                $rootScope.authlat = lat;
                ReverseGeocode(lat, long);
            }

        }
        $scope.clearlocation = function () {
            $rootScope.authlng = '';
            $rootScope.authlat = '';
            $rootScope.location = '';
            $rootScope.locationmodal.newlocation = '';

        }
        function ReverseGeocode(latitude, longitude) {
            var reverseGeocoder = new google.maps.Geocoder();
            var currentPosition = new google.maps.LatLng(latitude, longitude);
            reverseGeocoder.geocode({ 'latLng': currentPosition }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    if (results[0]) {
                        var arrAddress = results[0].address_components;
                        currentlocation = '';
                        console.log(currentlocation);
                        $rootScope.location = results[0].formatted_address;
                        $rootScope.$broadcast('locationchange');



                    } else {
                        navigator.notification.alert('Unable to detect your address.');
                    }
                } else {
                    navigator.notification.alert('Unable to detect your address.');
                }
            });
        }


        $scope.settype = function (type) {

            if (type == 1) {
                $('.login').removeClass('hidden');
                $('.signup').addClass('hidden');
            } else {
                $('.signup').removeClass('hidden');
                $('.login').addClass('hidden');
            }

        }

        $scope.goto = function (id) {
            // set the location.hash to the id of
            // the element you wish to scroll to.
            $location.path(siteurl + '#/#howitworks');


            // call $anchorScroll()
            //  $anchorScroll();
        };
        if (LoginService.getAuth()) {
            $rootScope.isLoggedIn = true;

        }

        $scope.getauthuser = function () {
            authuser = LoginService.getAuth();
            $scope.authuserid = authuser.userId;
            LoginService.post(api_url + 'user/details/' + $scope.authuserid).then(function (resp) {
                $scope.authrole = resp.data.role.id;
                $scope.myinfo = resp.data;
                if (resp.data.social_avatar != undefined && resp.data.social_avatar != '' && resp.data.social_avatar != null) {
                    $scope.profileimage = api_url + '' + resp.data.social_avatar;

                    $scope.noprofile = 0;

                }
                else if (resp.data.avatar != '' && resp.data.avatar != null) {
                    $scope.profileimage = api_url + '' + resp.data.avatar;
                    $scope.profileimagemainurl = resp.data.avatar;
                    $scope.noprofile = 0;
                } else {
                    $scope.noprofile = 1;

                    $scope.profileimage = 'img/avatar.jpg';
                }
               
            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }

            })
        }
        if ($rootScope.myinfo == undefined && LoginService.getToken()) {

            $scope.getauthuser();
            $rootScope.isLoggedIn = true;


        }
        $scope.logOut = function () {
            LoginService.removeToken();
            $scope.myinfo = undefined;
            $rootScope.isLoggedIn = false;

            $scope.isAuthenticated = false;
            // $timeout(function() {
            $state.go('home');
            // }, 1000);
            gapi.auth.signOut();

        };



        $scope.data = {};
        $scope.register = {};

        $scope.sociallogin = function (social) {
            debugger;
            var social = JSON.stringify(social);

            LoginService.post(api_url + 'user/loginsocial', social).then(function (resp) {
                //alert(JSON.stringify(social));
                if (resp.data.success == true) {
                    LoginService.setAuth(resp.data);
                    LoginService.setToken(resp.data.token);
                    $scope.signedIn = true;
                    $rootScope.isLoggedIn = true;
                    $scope.getauthuser();
                    $rootScope.gotoStep(2);

                    //$scope.$apply();


                } else {

                    toastr.error(resp.data.message, 'Error');


                }
            }, function (err) {


            });
        }



        $rootScope.facebookLogin = function () {
            debugger;
            //alert(333333333333333);
            FB.login(function (response) {
                debugger;
                if (response.authResponse) {
                    //console.log('Welcome!  Fetching your information.... ');
                    FB.api('/me', function (response) {
                        //console.log(response);
                        //console.log('Good to see you, ' + response.name + '.');
                        var accessToken = FB.getAuthResponse().accessToken;
                        ////console.log(accessToken);
                        var obj = {};
                        obj.network = 'facebook';
                        obj.socialToken = accessToken;
                        $scope.sociallogin(obj);

                    });
                } else {
                    //console.log('User cancelled login or did not fully authorize.');
                }
            });

        }


        $rootScope.googleLogin = function () {
            debugger;
            GoogleSignin.signIn().then(function (result) {
                debugger;

                var obj = {};
                obj.network = 'gplus';
                obj.socialToken = result.Zi.access_token;
                $scope.sociallogin(obj);
            }, function (error) {
                //console.log(error);
            });
        }




        $scope.login = function () {
            if ($scope.data.username != null && $scope.data.password != null) {

                var mobile = $scope.data.username;
                var password = $scope.data.password;
                LoginService.loginUser($scope.data.username, $scope.data.password).success(function (data) {
                    if (data.success == true && data.message == 'mobile_verify') {
                        sessionkey = data.session_key;
                        verifymob = mobile;
                        afterresetuser.username = mobile;
                        afterresetuser.password = password;
                        console.log(afterresetuser);
                        $state.go('resetmobile');

                    } else if (data.success == true) {
                        LoginService.setAuth(data);
                        LoginService.setToken(data.token);
                        $rootScope.isLoggedIn = true;
                        $scope.getauthuser();

                        $scope.data.username = '';
                        $scope.data.password = '';
                        $scope.signedIn = true;

                        $rootScope.gotoStep(2);


                    } else {

                        if (data.success == false) {

                            toastr.error(data.message, 'Error');



                        }

                    }



                }).error(function (data) {

                    toastr.error(data.message, 'Error');
                });
            } else {

                toastr.error('All fields required', 'Error');

            }
        };
        $scope.signup = function () {
            var registermob = $scope.register.mobile;
            var registeremail = $scope.register.email;
            var registerpassword = $scope.register.password;
            if ($scope.register.fname != null && $scope.register.email != null && $scope.register.mobile != null && $scope.register.lname != null && $scope.register.password != null) {
                LoginService.signup($scope.register.fname, $scope.register.lname, $scope.register.email, $scope.register.mobile, $scope.register.password).success(function (data) {

                    if (data.success == true && data.message == 'mobile_verify') {
                        verifymob = registermob;
                        sessionkey = data.session_key;
                        $scope.register = {};
                        afterresetuser.username = registermob;
                        afterresetuser.password = registerpassword;
                        console.log(afterresetuser);
                        $state.go('resetmobile');
                    } else if (data.success == true && data.message != 'mobile_verify') {
                        $scope.register = {};
                        $scope.data.username = registeremail;
                        $scope.data.password = registerpassword;
                        $scope.login();
                        //$state.go('app.home');

                    } else {
                        if (data.success == false) {

                            toastr.error(data.message, 'Error');



                        }


                    }
                }).error(function (data) {
                    toastr.error(data.message, 'Error');

                });
            } else {
                toastr.error('All fields required', 'Error');

            }

        };





    })

    .controller('MysubscriptionCtrl', function ($scope, $http, $state, $stateParams, LoginService, toastr) {
        if (!LoginService.getToken()) {
            $state.go('home');

        }
        var obj = {};
            obj.userId = JSON.parse(window.localStorage.getItem('authuser')).userId;
            debugger;

        $scope.getSubscription = function () {
            LoginService.get(api_url + 'package/getsub',obj.userId).then(function (resp) {
                debugger;
                
                if (resp.data.status == true) {
                    $scope.subscriptions = resp.data;
                    $scope.subcribepackage = resp.data.subscription;
                    if (resp.data.subscription != false) {
                        $scope.isLimitUsed = resp.data.isLimitUsed;

                    }

                } else {
                    $scope.subscriptions = null;

                }
            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }

        $scope.getSubscription();
        $scope.unsubscribe = function (subscribeid) {

            LoginService.get(api_url + 'package/unsubscribe/' + subscribeid).then(function (resp) {
                if (resp.data != null) {
                    toastr.success('Package Unsubscribed', 'Success');
                    $scope.getSubscription();
                    window.location.reload();

                }





            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }


            });



        }


        $scope.confirmcancel = function (subscribeid) {
            var subscribeid = subscribeid;

            var result = window.confirm('Are you sure?');
            if (result == false) {
                //    e.preventDefault();
            } else {

                $scope.unsubscribe(subscribeid);
            }


        };

    })

    .controller('SubscribeCtrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location, $q) {
       debugger;
        if (!LoginService.getToken()) {
            $state.go('home');

        }
        $scope.getPackages = function () {
            LoginService.get(api_url + 'newpackage/packages').then(function (resp) {
                debugger;
                $scope.packages = resp.data;
                //window.location.reload();

            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }


            });
        }
        $scope.getPackages();

        var selectedvalue = {};
        var xyz = "";
        var abc = "";

        $scope.changed = function (item) {
debugger;
            //console.log(item);
            selectedvalue.id = item.id;
            selectedvalue.quantity_used = item.qty_clothes;


        }



        $scope.changemonth = function (hello) {

            xyz = hello;


        }
        $scope.subscription = {};
        $scope.btntxt = 'Subscribe';
        $scope.subscribe = function (pack, package_id) {
            debugger;
            if (!LoginService.getToken()) {
                $state.go("home");

            }
            else {
                debugger;

                var obj = {};
                obj.pid = selectedvalue.id;
                obj.quantity_used = selectedvalue.quantity_used;
                obj.userId = JSON.parse(window.localStorage.getItem('authuser')).userId;
                obj.noofmonths = xyz;
                debugger;

                var data = JSON.stringify(obj);

                LoginService.get(api_url + 'package/getsub', obj.userId).then(function (resp) {

                    debugger;
                    if (resp.data) {

                        if (resp.data.status == 0&&resp.data.active==0) {
                            LoginService.post(api_url + 'newpackage/packagesadd', data).then(function (resp) {
                               
                                $scope.message = "You have been successfully subscribed!"
                                if(resp.data)
                                {
                                    $location.path('/mysubscription');
                                }

                            }, function (err) {
                                if (err.success == false && err.type == 'token_expired') {
                                    $scope.logOut();

                                }


                            });
                        }
                        else {
                            $scope.message = "You are already subscribed!"
                        }

                    }
                    else {
                        LoginService.post(api_url + 'newpackage/packagesadd', data).then(function (resp) {
                           
                            $scope.message = "You have been successfully subscribed!"
                            if(resp.data)
                            {
                                $location.path('/mysubscription');
                            }
                           
                           



                        }, function (err) {
                            if (err.success == false && err.type == 'token_expired') {
                                $scope.logOut();

                            }


                        });
                    }
                })

            }
        }
        
        $scope.getSubscription = function () {
            
            var obj = {};
            obj.userId = JSON.parse(window.localStorage.getItem('authuser')).userId;
            LoginService.get(api_url + 'package/getsub',obj.userId).then(function (resp) {
                debugger;
                
               $rootScope.currentorder.subscriptions=resp.data;
            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }

        $scope.getSubscription();

        

    })



    .controller('FaqCtrl', function ($scope, $http, $state, $stateParams) {

        $scope.getFaq = function () {
            $http.get(api_url + 'faqs/catwithQuestion').then(function (resp) {
                $scope.faqs = resp.data;
            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }
        $scope.getFaq();
    })

    .controller('LoginCtrl', function ($scope, $http, $scope, LoginService, $state, $cordovaOauth, $timeout) {


    })
    .controller('HomeCtrl', function ($scope, $http, $state, $stateParams, LoginService, $rootScope) {
      
        $scope.getbanner = function () {
            debugger;
            LoginService.get(api_url + 'banner').then(function (resp) {
                debugger;
                console.log(resp);
                $scope.banners = resp.data;
            }, function (err) {
                console.error("ERR", err);
                console.log(err);
            });
        };

        //  $scope.getbanner();
        $scope.getTestimonial = function () {
            LoginService.get(api_url + 'testimonials').then(function (resp) {
                // console.log(resp);
                $scope.testimonials = resp.data.result;
            }, function (err) {
                console.error("ERR", err);
                console.log(err);
            });
        };

        $scope.getservicesCatHome = function (catid) {
            debugger;
            LoginService.get(api_url + 'categories').then(function (resp) {
                debugger;
                $scope.categorieshome = resp.data;

            }, function (err) {

            });
        }

        $scope.getMonthlyPackages = function () {
            debugger;
            LoginService.get(api_url + 'newpackage/packages').then(function (resp) {
                debugger;
                $scope.monthlypackages = resp.data;
            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }

        $rootScope.getgeolocation();
        $scope.getTestimonial();
        $scope.getservicesCatHome();
        $scope.getMonthlyPackages();
    })
    .controller('PackagesCtrl', function ($scope, $http, $state, $stateParams, LoginService, toastr, $rootScope) {

        $scope.getMonthlyPackages = function () {
            LoginService.get(api_url + 'package/list').then(function (resp) {
                if (resp.data.status == true) {
                    $scope.monthlypackages = resp.data.result;
                    $scope.subcribepackage = resp.data.subscription;
                    if (resp.data.subscription != false) {
                        $scope.isLimitUsed = resp.data.isLimitUsed;

                    }

                } else {
                    $scope.monthlypackages = {};

                }

            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }


        $scope.getMonthlyPackages();

        $rootScope.openSubscribeModal = function (package) {



            $scope.curentpackage = package;
            $scope.subppack = {};
            $scope.subppack.package_id = package.id;
            $scope.subppack.pickup_option = package.pickup_options.split(',')[0].trim();
            $scope.subppack.duration = package.available_packages.split(',')[0].trim();

            console.log($scope.curentpackage);
            console.log($scope.subppack);
            $('#subscribe').modal('show');
        }
        $scope.subscribePack = function (currpack) {
            if (!LoginService.getToken()) {
                $state.go("home");

            } else {

                var obj = {};
                obj.package_id = currpack.package_id;
                obj.pickup_option = currpack.pickup_option;
                obj.duration = currpack.duration;
                var data = JSON.stringify(obj);
                console.log(data);
                LoginService.post(api_url + 'package/subscribe', data).then(function (resp) {
                    $('#subscribe').modal('hide');
                    // $state.go($state.current, {}, {reload: true});
                    if (resp.data.status = true) {
                        toastr.success(resp.data.message, 'Success');

                    } else {

                        toastr.error(resp.data.message, 'Error');

                    }


                    $scope.getMonthlyPackages();




                }, function (err) {
                    if (err.success == false && err.type == 'token_expired') {
                        $scope.logOut();

                    }


                });
            }



        }

    })

    .controller('ServiceFeaturesCtrl', function ($scope, $http, $state, $stateParams) {

        $scope.getserviceFeatures = function (catid) {
            $http.get(api_url + 'services/features/list').then(function (resp) {
                $scope.servicesfeatures = resp.data;
            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }


        $scope.getserviceFeatures();

    })
    .controller('PlaceorderCtrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location) {
    })
    .controller('PlaceOrderStep1Ctrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location) {
      
        if (LoginService.getToken()) {
            $rootScope.gotoStep(2);

        }


        console.log($rootScope.orderstep);




    })
    .controller('PlaceOrderStep2Ctrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location) {
       
        if (!LoginService.getToken() || $rootScope.orderstep != 2) {
            $rootScope.gotoStep(1);
           

        }
        
      
       //$rootScope.currentorder={};

        ///placeorder/step2
        
        $scope.getBranches = function () {
            LoginService.get(api_url + 'order/branches').then(function (resp) {
                $scope.branches = resp.data;

            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }


            });
        }
        $scope.getBranches();
        console.log($rootScope.orderstep);
        


    })
    //.controller('PlaceOrderStep3Ctrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$stateParams,$location) {
    //    if(! LoginService.getToken() || $rootScope.orderstep!=3) {
    //        $rootScope.gotoStep(1);

    //        }
    //       if(!$rootScope.currentorder.address||!$rootScope.currentorder.address.street || !$rootScope.currentorder.address.landmark || !$rootScope.currentorder.address.addresstype  || !$rootScope.currentorder.branch){
    //        $rootScope.gotoStep(2);


    //       }

    //        $scope.getservicesCatforOrder=function(){
    //            LoginService.get(api_url+'categories').then(function(resp) {

    //                    $scope.servicescat=resp.data;
    //                }, function(err) {
    //                    		if(err.success==false && err.type=='token_expired'){
    //	$scope.logOut();

    //	}


    //            });
    //        }

    //        $scope.getMyCurrentSubscription=function(){
    //            LoginService.get(api_url+'package/current/subscription').then(function(resp) {
    //                      if(resp.data.status==true){
    //			  $rootScope.currentorder.currentSubscription=resp.data.result;
    //                          $rootScope.currentorder.useSubscription=0;
    //			  }else{
    //			 $rootScope.currentorder.currentSubscription=null;
    //			  }

    //                }, function(err) {
    //                    		if(err.success==false && err.type=='token_expired'){
    //	               $scope.logOut();

    //	                }


    //            });
    //        }




    //    }) 

    .controller('PlaceOrderStep3Ctrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location) {
        $rootScope.currentorder.cart = {};
        $rootScope.currentorder.cartPrice = 0;

        if(! LoginService.getToken() || $rootScope.orderstep!=3) {
            $rootScope.gotoStep(1);
            
            }
           if(!$rootScope.currentorder.address||!$rootScope.currentorder.address.street || !$rootScope.currentorder.address.landmark || !$rootScope.currentorder.address.addresstype  || !$rootScope.currentorder.branch){
            $rootScope.gotoStep(2);
            

           }
    

        $scope.getservicesCatforOrder = function () {
            LoginService.get(api_url + 'services/getAllItems').then(function (resp) {
              debugger;
                $scope.servicescat = resp.data;
                
            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }


            });
        }
        debugger;

        function setCartData() {
          
            $("#quantity").text(quantity_added);
            $("#total").text(total);
            var tax = (total * 14.5) / 100;
            $("#tax").text(tax);
        }
        function vibrateCart() {
            var i = 0;
            var myInterval = setInterval(function () {
                if (i++ >= 10)
                    clearInterval(myInterval);
                if (i % 2 == 0) {
                    $(".estimator img").css({
                        "-webkit-transform": "rotate(15deg)",
                        "-moz-transform": "rotate(15deg)",
                        "transform": "rotate(15deg)",
                        "-o-transform": "rotate(15deg)",
                        "-ms-transform": "rotate(15deg)"
                    });
                }
                else if (i % 3 == 0) {
                    $(".estimator img").css({
                        "-webkit-transform": "rotate(-15deg)",
                        "-moz-transform": "rotate(-15deg)",
                        "transform": "rotate(-15deg)",
                        "-o-transform": "rotate(-15deg)",
                        "-ms-transform": "rotate(-15deg)"
                    });
                }
                else {
                    $(".estimator img").css({
                        "-webkit-transform": "rotate(0deg)",
                        "-moz-transform": "rotate(0deg)",
                        "transform": "rotate(0deg)",
                        "-o-transform": "rotate(0deg)",
                        "-ms-transform": "rotate(0deg)"
                    });
                }
            }, 100);
            console.log($rootScope.currentorder.cart);
            console.log($rootScope.currentorder.cartPrice);
        }

        $scope.increment = function (serviceselectd, categoryid, itemid, washprice, drycleanprice, ironprice, type) {

            var item_label = itemid;

            switch (type) {

                case 'wash':
                    debugger;
                    if (items_added[item_label]) {
                        items_added[item_label]++;

                    } else {
                        items_added[item_label] = 1;
                    }
                    this.service.qty_w = items_added[item_label];
                    serviceselectd['cart_qty'] = items_added[item_label];
                    total += Number(washprice);
                    break;
                case 'dryclean':
                    if (dryClean_items_added[item_label]) {
                        dryClean_items_added[item_label]++;

                    } else {
                        dryClean_items_added[item_label] = 1;
                    }
                    this.service.qty_dc = dryClean_items_added[item_label];
                    serviceselectd['cart_qty'] = dryClean_items_added[item_label];
                    total += Number(drycleanprice);
                    break;
                case 'iron':
                    if (iron_items_added[item_label]) {
                        iron_items_added[item_label]++;

                    } else {
                        iron_items_added[item_label] = 1;
                    }
                    this.service.qty_ir = iron_items_added[item_label];
                    serviceselectd['cart_qty'] = iron_items_added[item_label];
                    total += Number(ironprice);
                    break;
                default:
                    null;
                    break;
            }
            CalculateRowTotal(this.service, item_label, washprice, drycleanprice, ironprice);

            if (quantity_added == 0) {
                $(".estimator").fadeIn(1500);

            }

            quantity_added++;
            $rootScope.currentorder.qty = quantity_added;
            debugger;
            
            $rootScope.currentorder.cart[item_label] = serviceselectd;
            debugger;
            $rootScope.currentorder.cartPrice = total;
            debugger;
            setCartData();
            vibrateCart();
            //  $ionicSlideBoxDelegate.update();

        }

        $scope.decrement = function (serviceselectd, categoryid, itemid, washprice, drycleanprice, ironprice, type) {

            var item_label = itemid;

            switch (type) {
                case 'wash':
                    if (items_added[item_label] > 0) {
                        items_added[item_label]--;
                        quantity_added--;
                        total -= Number(washprice);
                        setCartData();
                        if (quantity_added == 0)
                            $(".estimator").fadeOut(1000);
                        this.service.qty_w = items_added[item_label];
                    }

                    if (items_added[item_label] == 0) {
                        delete $rootScope.currentorder.cart[item_label];

                    } else {
                        serviceselectd['cart_qty'] = items_added[item_label];

                        $rootScope.currentorder.cart[item_label] = serviceselectd;
                    }
                    break;
                case 'dryclean':
                    if (dryClean_items_added[item_label] > 0) {
                        dryClean_items_added[item_label]--;
                        quantity_added--;
                        total -= Number(drycleanprice);
                        setCartData();
                        if (quantity_added == 0)
                            $(".estimator").fadeOut(1000);
                        this.service.qty_dc = dryClean_items_added[item_label];
                    }

                    if (dryClean_items_added[item_label] == 0) {
                        delete $rootScope.currentorder.cart[item_label];

                    } else {
                        serviceselectd['cart_qty'] = dryClean_items_added[item_label];

                        $rootScope.currentorder.cart[item_label] = serviceselectd;
                    }
                    break;
                case 'iron':
                    if (iron_items_added[item_label] > 0) {
                        iron_items_added[item_label]--;
                        quantity_added--;
                        total -= Number(ironprice);
                        setCartData();
                        if (quantity_added == 0)
                            $(".estimator").fadeOut(1000);
                        this.service.qty_ir = iron_items_added[item_label];
                    }

                    if (iron_items_added[item_label] == 0) {
                        delete $rootScope.currentorder.cart[item_label];

                    } else {
                        serviceselectd['cart_qty'] = iron_items_added[item_label];

                        $rootScope.currentorder.cart[item_label] = serviceselectd;
                    }
                    break;
                default:
                    null;
                    break;
            }
            CalculateRowTotal(this.service, item_label, washprice, drycleanprice, ironprice);
            debugger;
            // quantity_added--;
            $rootScope.currentorder.qty= quantity_added;
            debugger;
            $rootScope.currentorder.cartPrice = total;

            vibrateCart();

        }

        function CalculateRowTotal(service, item_label, washprice, drycleanprice, ironprice) {
            var washQty = items_added[item_label], dryCleanQty = dryClean_items_added[item_label], ironQty = iron_items_added[item_label];
            if (typeof (washQty) == "undefined")
                washQty = 0;
            if (typeof (dryCleanQty) == "undefined")
                dryCleanQty = 0;
            if (typeof (ironQty) == "undefined")
                ironQty = 0;
            debugger;
            service.rowtotal = washprice * (washQty) + drycleanprice * (dryCleanQty) + ironprice * (ironQty);

        }

        $scope.getservicesByCatid = function (catid) {

            $http.get(api_url + 'services/' + catid).then(function (resp) {
                $rootScope.currentorder.catservices = resp.data;

                for (i = 0; i < $scope.currentorder.catservices.length; i++) {

                    items_added[$scope.currentorder.catservices[i]['id']] = new Array();
                }

            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });

        }
        if ($rootScope.currentorder.useSubscription == 0 || !$rootScope.currentorder.useSubscription) {
            $scope.getservicesCatforOrder();

        }

        $scope.previous=function()
        {
            debugger;
          window.location.reload();
          $location.path('/placeorder/step2');
        }

        
    })
    // .controller('PlaceOrderStep5Ctrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location) {
    //     console.log($rootScope.currentorder.cart);
    //     console.log($rootScope.currentorder.cartPrice);
    //     if (!LoginService.getToken() || $rootScope.orderstep != 5) {
    //         $rootScope.gotoStep(1);

    //     }

    //     //console.log($rootScope.currentorder.service);
    //     //if ((!$rootScope.currentorder.service && !$rootScope.currentorder.useSubscription) || (!$rootScope.currentorder.cart && $rootScope.currentorder.useSubscription == 1)) {
    //     //    $rootScope.gotoStep(4);


    //     //}


    //     $scope.getdeliverytime = function () {
    //         LoginService.get(api_url + 'order/deliverytime').then(function (resp) {
    //             $scope.deliverytime = resp.data;
    //         }, function (err) {
    //             if (err.success == false && err.type == 'token_expired') {
    //                 $scope.logOut();

    //             }


    //         });
    //     }
    //     $scope.getdeliverytime();
    //     $scope.getpickuptime = function () {
    //         LoginService.get(api_url + 'order/pickuptime').then(function (resp) {
    //             $scope.pickuptime = resp.data;

    //         }, function (err) {
    //             if (err.success == false && err.type == 'token_expired') {
    //                 $scope.logOut();

    //             }


    //         });
    //     }
    //     $scope.getpickuptime();
    // })

    // .controller('PlaceOrderStep4Ctrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location) {
    //     console.log($rootScope.currentorder.cart);
    //     console.log($rootScope.currentorder.cartPrice);
    //     if (!LoginService.getToken() || $rootScope.orderstep != 4) {
    //         $rootScope.gotoStep(1);

    //     }
    //     // if (!$rootScope.currentorder.timslot || !$rootScope.currentorder.timslot.pickupdate || !$rootScope.currentorder.timslot.deliverydate || !$rootScope.currentorder.timslot.pickuptime || !$rootScope.currentorder.timslot.deliverytime || !$rootScope.currentorder.cart) {
    //     //     $rootScope.gotoStep(3);


    //     // }
    //     $scope.placeOrder = function (order) {

    //         var obj = {};
    //         var address = {};
    //         address.cart_quantity = 0;
    //         address.totalPrice = 0;
    //         address.street = order.address.street;
    //         address.landmark = order.address.landmark;
    //         address.addressType = order.address.addresstype;
    //         address.branchId = order.branch.id;


    //         //obj.flatandstreet = order.address.street;
    //         //obj.landmark = order.address.landmark;
    //         //obj.addressType = order.address.addresstype;
    //         obj.pickupDate = order.timslot.pickupdate;
    //         obj.deliveryDate = order.timslot.deliverydate;
    //         obj.timeOfDelivery = order.timslot.deliverytime.id;
    //         obj.timeOfPickup = order.timslot.pickuptime.id;
    //         obj.address = address;
    //         if (order.service) {
    //             obj.service_cat_id = order.service.id;
    //         }

    //         //obj.branch = order.branch.id;

    //         var services = [];
    //         for (var key in order.cart) {
    //             if (order.cart.hasOwnProperty(key)) {
    //                 services.push(order.cart[key]);

    //             }
    //         }
    //         obj.services = services;
    //         if (order.useSubscription) {
    //             obj.subscription_id = order.currentSubscription.id;
    //             obj.useSubscription = order.useSubscription;
    //         } else {

    //             obj.useSubscription = 0;
    //         }


    //         var data = JSON.stringify(obj);
    //         LoginService.post(api_url + 'order/place', data).then(function (resp) {
    //             $rootScope.currentorder = {};
    //             $scope.currentservice = '';
    //             $rootScope.currentorder.timslot = {};
    //             $rootScope.currentorder.address = {};

    //             $rootScope.orderstep = 1;
    //             if (resp.data) {
    //                 $location.path('/order/success/' + resp.data.id);


    //             }


    //         }, function (err) {
    //             if (err.success == false && err.type == 'token_expired') {
    //                 $scope.logOut();

    //             }


    //         });

    //     }

    // })


    .controller('PlaceOrderStep4Ctrl', function ($scope, $http, $state, $stateParams, $rootScope, LoginService, $stateParams, $location) {


        if(! LoginService.getToken() || $rootScope.orderstep!=4) {
            $rootScope.gotoStep(1);
            
            }

            console.log($rootScope.currentorder.service);
              if(!$rootScope.currentorder.cart){
              $rootScope.gotoStep(3);
                
    
               }
            
        debugger;

        $scope.getSubscription = function () {

            var obj = {};
            obj.userId = JSON.parse(window.localStorage.getItem('authuser')).userId;
            debugger;

            LoginService.get(api_url + 'package/getsub', obj.userId).then(function (resp) {
                debugger;

                $rootScope.currentorder.mysubscription = resp.data;
                if ($rootScope.currentorder.mysubscription) {

                    $scope.x = JSON.parse($rootScope.currentorder.mysubscription.quantity_used);
                    $scope.y = JSON.parse($rootScope.currentorder.qty);
                    $rootScope.currentorder.mysubscription.quantity_used = $scope.x - $scope.y;

                    if ($rootScope.currentorder.mysubscription.quantity_used <=0) {
                        $rootScope.currentorder.mysubscription.isExpired = 1;
                        $rootScope.currentorder.mysubscription.isLimitUsed = 1;
                        $rootScope.currentorder.mysubscription.status = 0;
                        $rootScope.currentorder.mysubscription.active = 0;

                        $rootScope.currentorder.mysubscription.message = "Limit Used or Exceeded! Confirm Order with or without Subscription";

                    }



                }




            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }
        $scope.getSubscription();




        $scope.placeOrder = function () {
            debugger;



            if ($rootScope.currentorder.mysubscription != null) {
                var currDate = new Date();

                var myDate = new Date($rootScope.currentorder.mysubscription.end_date);
                var end = myDate.getTime();
                var start = currDate.getTime();
debugger;
                if ($rootScope.currentorder.mysubscription.quantity_used >= 0 && end > start) {
                      
                    
                       
                    var x=[];
                     var myobj = $rootScope.currentorder.cart;
                     $rootScope.currentorder.array = $.map(myobj, function(value, index) {
                        return [value];
                    });
                    var obj = $rootScope.currentorder;
                     

                    var data = JSON.stringify(obj);



                    LoginService.post(api_url + 'order/placeWithSubscription', data).then(function (resp) {
                        debugger;

                       

                        $rootScope.currentorder={};
                       
                        $scope.currentservice='';
                        $rootScope.currentorder.timslot={};
                        $rootScope.currentorder.address={};
                        $rootScope.currentorder.qty=null;
                        window.location.reload();
                        
                        $rootScope.orderstep =1;
                        if(resp.data){
                           $location.path('/order/success/'+resp.data.id);
                                       
        
                        }


                    }, function (err) {
                        if (err.success == false && err.type == 'token_expired') {
                            $scope.logOut();

                        }


                    });
                }

                else {

                       
                    var x=[];
                     var myobj = $rootScope.currentorder.cart;
                     $rootScope.currentorder.array = $.map(myobj, function(value, index) {
                        return [value];
                    });
                    var obj = $rootScope.currentorder;

                    var data = JSON.stringify(obj);


                    debugger;
                    LoginService.post(api_url + 'order/place', data).then(function (resp) {
                        debugger;

                        

                        $rootScope.currentorder={};
                        $scope.currentservice='';
                        $rootScope.currentorder.timslot={};
                        $rootScope.currentorder.address={};
                        window.location.reload();
                        $rootScope.orderstep =1;
                        if(resp.data){
                           $location.path('/order/success/'+resp.data.id);
                                       
        
                        }

                    }, function (err) {
                        if (err.success == false && err.type == 'token_expired') {
                            $scope.logOut();

                        }


                    });

                }
            }
            else 
            {
                var x=[];
                var myobj = $rootScope.currentorder.cart;
                $rootScope.currentorder.array = $.map(myobj, function(value, index) {
                   return [value];
               });
                var obj = $rootScope.currentorder;

                var data = JSON.stringify(obj);
                debugger;

                LoginService.post(api_url + 'order/placeWithoutSubscription', data).then(function (resp) {
                    debugger;

                  
                    if (resp.data) {

                        
                        $rootScope.currentorder={};
                        $scope.currentservice='';
                        $rootScope.currentorder.timslot={};
                        $rootScope.currentorder.address={};
                        window.location.reload();
                        
                        $rootScope.orderstep =1;
                        if(resp.data){
                           $location.path('/order/success/'+resp.data.id);
                                       
        
                        }


                    }

                }, function (err) {
                    if (err.success == false && err.type == 'token_expired') {
                        $scope.logOut();

                    }


                });
            }



        }

        $scope.previous=function()
        {
            debugger;
          window.location.reload();
          $location.path('/placeorder/step2');
        }

    })


    .controller('orderSuccessCtrl', function ($scope, $http, $state, $stateParams, $rootScope) {
        debugger;
        // $ionicNavBarDelegate.showBackButton(false);
        $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
            debugger;
            if ($rootScope.currentorder.length) {
                $rootScope.gotoStep(1);


            }

        });

        $scope.placedorder = $stateParams.orderid;
        debugger;
        $rootScope.currentorder=null;
        $rootScope.currentorder = {};

    })

    .controller('OrderDetailsCtrl', function ($scope, $http, $state, $stateParams, LoginService, toastr) {
        debugger;
        if (!LoginService.getToken()) {
            $state.go('home');

        }
        debugger;


        $scope.getOrderDetails = function () {
            debugger;

            LoginService.get(api_url + 'order/get/' + $stateParams.orderid).then(function (resp) {
debugger;
                $scope.order = resp.data

            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }

            });
        }

        if ($stateParams.orderid) {
            $scope.getOrderDetails();

        }

       

        $scope.orderCancel = function (orderid) {
            LoginService.get(api_url + 'order/cancelled/' + orderid).then(function (resp) {


                $scope.order.status = 'cancelled';

                toastr.success('Order Cancelled', 'Cancelled');


            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }


            });
        }

        $scope.confirmordercancel = function (orderid) {
            var orderid = orderid;

            var result = window.confirm('Are you sure?');
            if (result == false) {
                // e.preventDefault();
            } else {

                $scope.orderCancel(orderid);
            }


        };
    })


    .controller('AllOrderCtrl', function ($scope, $http, $state, $stateParams, LoginService, toastr) {
        if (!LoginService.getToken()) {
            $state.go('home');

        }
        var page = 1;
        $scope.moreresults = true;


        $scope.getAllorders = function (pageno) {
            debugger;
            var obj = {};
            obj.page = pageno;
            obj.limit = 3;
            var data = JSON.stringify(obj);

            LoginService.post(api_url + 'order/getAll', data).then(function (resp) {
                debugger;

                if (resp.data.length == 0 && page == 1) {
                    $scope.notfound = true;
                }
                if (resp.data.length) {
                    $scope.notfound = false;
                    if (resp.data.length < obj.limit) {
                        $scope.moreresults = true;
                    } else {
                        $scope.moreresults = false;
                    }
                    if (page != 1) {
                        for (var i = 0; i < resp.data.length; i++) {
                            $scope.allorders.push(resp.data[i]);
                        }
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    } else {
                        $scope.allorders = resp.data;
                    }

                } else {
                    $scope.moreresults = true;
                }
            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }


            });
        }
        $scope.getAllorders(page);
        $scope.loadMoreOrders = function () {
            page++;
            $scope.getAllorders(page);

        };

        $scope.orderCancel = function ($index, orderid) {
            LoginService.get(api_url + 'order/cancelled/' + orderid).then(function (resp) {

                $scope.allorders[$index]['status'] = 'cancelled';

                toastr.success('Order Cancelled', 'Cancelled');

            }, function (err) {
                if (err.success == false && err.type == 'token_expired') {
                    $scope.logOut();

                }


            });
        }

        $scope.confirmordercancel = function (index, orderid) {
            var orderid = orderid;
            var index = index;

            var result = window.confirm('Are you sure?');
            if (result == false) {
                //e.preventDefault();
            } else {

                $scope.orderCancel(index, orderid);
            }


        };
        //  $scope.getAllorders();

    })

    .controller('RateCtrl', function ($scope, $http, $state, $stateParams, $rootScope) {

debugger;
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            $('.iw-qty').val(0);
            $scope.categories = [];
            $scope.getcats();
            quantity_added = 0;
            total = 0;
            tax = 0;
            setCartData();
            $(".estimator").hide();

        });
        function setCartData() {
            debugger;
            $("#quantity").text(quantity_added);
            $rootScope.currentorder.
            $("#total").text(total);
            var tax = (total * 14.5) / 100;
            $("#tax").text(tax);
        }
        function vibrateCart() {
            var i = 0;
            var myInterval = setInterval(function () {
                if (i++ >= 10)
                    clearInterval(myInterval);
                if (i % 2 == 0) {
                    $(".estimator img").css({
                        "-webkit-transform": "rotate(15deg)",
                        "-moz-transform": "rotate(15deg)",
                        "transform": "rotate(15deg)",
                        "-o-transform": "rotate(15deg)",
                        "-ms-transform": "rotate(15deg)"
                    });
                }
                else if (i % 3 == 0) {
                    $(".estimator img").css({
                        "-webkit-transform": "rotate(-15deg)",
                        "-moz-transform": "rotate(-15deg)",
                        "transform": "rotate(-15deg)",
                        "-o-transform": "rotate(-15deg)",
                        "-ms-transform": "rotate(-15deg)"
                    });
                }
                else {
                    $(".estimator img").css({
                        "-webkit-transform": "rotate(0deg)",
                        "-moz-transform": "rotate(0deg)",
                        "transform": "rotate(0deg)",
                        "-o-transform": "rotate(0deg)",
                        "-ms-transform": "rotate(0deg)"
                    });
                }
            }, 100);
        }
debugger;
        $scope.getservices = function (catid) {
            LoginService.get(api_url + 'services/' + catid).then(function (resp) {
                console.log($scope.categories);
                debugger
                $scope.services = resp.data;
                $scope.categories[0].service = $scope.services;
                //  $ionicSlideBoxDelegate.update();
            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }
        $scope.getcats = function () {
            debugger;
            LoginService.get(api_url + 'categories').then(function (resp) {

                $scope.categories = resp.data;


                for (i = 0; i < $scope.categories.length; i++) {

                    console.log($scope.categories);
                    items_added[$scope.categories[i]['id']] = new Array();
                }
                console.log('items_added');
                console.log(items_added);
                $scope.getservices($scope.categories[0]['id']);
                //$ionicSlideBoxDelegate.update();
                console.log('items_added');
            }, function (err) {
                console.error('ERR', err);
                console.log(err);

            });
        }


        $scope.getcats();

        $scope.increment = function (categoryid, itemid, price) {

            var item_label = itemid;

            if (items_added[categoryid][item_label]) {
                items_added[categoryid][item_label]++;
                this.service.quantity = items_added[categoryid][item_label];
            } else {
                items_added[categoryid][item_label] = 1;
                this.service.quantity = items_added[categoryid][item_label];

            }
            if (quantity_added == 0) {
                $(".estimator").fadeIn(1500);

            }

            quantity_added++;
            total += Number(price);
            setCartData();
            vibrateCart();
            //  $ionicSlideBoxDelegate.update();

        }


        $scope.decrement = function (categoryid, itemid, price) {

            var item_label = itemid;

            if (items_added[categoryid][item_label] > 0) {
                items_added[categoryid][item_label]--;
                quantity_added--;
                total -= Number(price);
                setCartData();
                if (quantity_added == 0)
                    $(".estimator").fadeOut(1000);
            }
            this.service.quantity = items_added[categoryid][item_label];
            vibrateCart();
            //  $ionicSlideBoxDelegate.update();

        }

        $scope.getservicesbycat = function ($index, catid) {
            console.log($index);
            var curr = $scope.categories[$index];
            if (curr.service == undefined) {
                $http.get(api_url + 'services/' + catid).then(function (resp) {
                    $scope.services = resp.data;
                    if (resp.data.length) {
                        curr.service = resp.data;

                    }
                    //  $ionicSlideBoxDelegate.update();
                }, function (err) {
                    console.error('ERR', err);
                    console.log(err);

                });

            }
        }


    }); 