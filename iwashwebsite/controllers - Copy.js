var api_url='http://54.202.108.46:3000/api/';
var siteurl='http://54.202.108.46/';


//var api_url='http://localhost:3000/api/';
//var siteurl='http://localhost/';
var items_added = [], quantity_added=0, total=0, tax=0,category_selected ='';

angular.module('iwashApp.controllers', [])
.directive('datepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                dateFormat: 'dd-mm-yy',
                onSelect: function (date) {
                    ngModelCtrl.$setViewValue(date);
                    ngModelCtrl.$render();
                    scope.$apply();
                }
            });
        }
    };
})
.service('LoginService', function($q, $http, $window) {
    var serviceToken, serviceHost, tokenKey;
    tokenKey = 'xaccesstoken';
    if (localStorage.getItem(tokenKey)) {
        serviceToken = $window.localStorage.getItem(tokenKey);
    }
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var credentials = {};
            credentials.username = name;
            credentials.password = pw;
            var credentials = JSON.stringify(credentials);
            $http.post(api_url + 'user/authenticate', credentials).then(function(resp) {

                if (resp.data.success === true) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject(resp.data);
                }

            }, function(err) {
                deferred.reject(resp.data);

            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;

        },
        signup: function(fname, lname, email,password) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var credentials = {};
            credentials.email = email;
            credentials.password = password;
            credentials.fname = fname;
            credentials.lname = lname;
            var credentials = JSON.stringify(credentials);
            $http.post(api_url + 'user/register', credentials).then(function(resp) {
                if (resp.data.success === true) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject(resp.data);
                }

            }, function(err) {
                deferred.reject(resp.data);

            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;

        },
        setAuth: function(authuser) {
            $window.localStorage.setItem('authuser', JSON.stringify(authuser));
        },

        getAuth: function() {
            if (localStorage.getItem('authuser')) {
                var retrievedObject = $window.localStorage.getItem('authuser');
                return JSON.parse(retrievedObject);
            }
        },

        setToken: function(token) {
            serviceToken = token;
            $window.localStorage.setItem(tokenKey, token);
        },

        getToken: function() {
            return serviceToken;
        },

        removeToken: function() {
            serviceToken = undefined;
            $window.localStorage.removeItem(tokenKey);
            $window.localStorage.removeItem('authuser');
        },

        get: function(uri, params) {
            return $http({
                url: uri,
                method: "GET",
                params: {
                    xaccesstoken: serviceToken
                }
            });
        },

        post: function(uri, params) {
            if (params) {
                params = JSON.parse(params);

            } else {
                params = params || {};

            }
            params['xaccesstoken'] = serviceToken;
            params = JSON.stringify(params);
            return $http.post(uri, params);
        }
    };

})

.controller('AppCtrl', function($state,$scope,$rootScope,LoginService,$ionicLoading,$timeout) {
     $rootScope.currentorder={};
       $scope.currentservice='';
      $scope.api_url=api_url;

       $rootScope.currentorder.timslot={};
  $scope.groups = [];
  for (var i=0; i<3; i++) {
    if(i==0){
    $scope.groups[i] = {
      name: "Pickup & Delivery",
      items: ["test 1"]
    };
  }
  if(i==1){
    $scope.groups[i] = {
      name: "Washing",
      items: ["test 2"]
    };
  }
  if(i==2){
    $scope.groups[i] = {
      name: "Payment",
      items: ["test 3"]
    };
  }
    for (var j=0; j<1; j++) {
      $scope.groups[i].items.push();
    }
  }

  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };

  $rootScope.calluser=function(mobile){
    if(mobile!=undefined){
        window.plugins.CallNumber.callNumber(onSuccess, onError, mobile, true);
    }
  //window.open('tel:'+mobile, '_system')
  }

  function onSuccess(result){
    console.log("Success:"+result);
  }

  function onError(result) {
    console.log("Error:"+result);
  }
 	$scope.logOut = function () {
	LoginService.removeToken();
	$scope.myinfo=undefined;
	$scope.isAuthenticated = false;
	 $ionicLoading.show({
			template: '<ion-spinner icon="android"></ion-spinner>'
			});
			$timeout(function() {
			$state.go('login');
		  $ionicLoading.hide();
			}, 1000);
			 gapi.auth.signOut();

    };
     


    $rootScope.setOrderService=function(){

//$rootScope.currentorder.service=service;      

console.log($rootScope.currentorder);
   if($rootScope.currentorder.service  !=undefined && $rootScope.currentorder.service  !=null && $rootScope.currentorder.service  !='' ){
    $state.go('app.orderservice');

      }
    }

    $rootScope.setOrderAddress=function(){
console.log($rootScope.currentorder.address);
   if($rootScope.currentorder.address  !=undefined && $rootScope.currentorder.address  !=null && $rootScope.currentorder.address  !='' ){
    // $state.go('app.orderservice');

      }
    }



    $scope.getauthuser=function(){
		authuser=LoginService.getAuth();
		$scope.authuserid=authuser.userId;
		LoginService.post(api_url+'user/details/'+$scope.authuserid).then(function(resp) {
			$scope.authrole=resp.data.role.id;
			$scope.myinfo = resp.data;
		
			if(resp.data.avatar!='' && resp.data.avatar!=null){
				$scope.profileimage=api_url+''+resp.data.avatar;
				$scope.profileimagemainurl=resp.data.avatar;
				$scope.noprofile=0;
			}else{
				$scope.noprofile=1;

				$scope.profileimage='img/avatar.jpg';
			}
			console.log(resp.data);
		}, function(err) {
						console.error('ERR', err);

		})
    }	
     $scope.getauthuser();
})
.controller('FaqCtrl', function($scope, $http, $state,$stateParams) {

		$scope.getFaqcat=function(){
				$http.get(api_url+'faqs/categories').then(function(resp) {
          
						$scope.faqscat=resp.data;
					}, function(err) {
              console.error('ERR', err);
              console.log(err);

					});
		 }
    $scope.getFaqcat();
    
		$scope.getFaq=function(catid){
      var currelement=this.cat;
 if(currelement.items==undefined){
				$http.get(api_url+'faqs/'+catid).then(function(resp) {
          
            currelement.items=resp.data;
            console.log(currelement);
					}, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
            

    }
		 }


})

        .controller('LoginCtrl', function($cordovaToast,$scope,$http,$scope, LoginService,$state,$cordovaOauth,$timeout,$ionicLoading) {

        $scope.data={};
         $scope.register = {};
          
	 $scope.sociallogin = function(social) {
			var social=JSON.stringify(social);
			//alert(social);
			$http.post(api_url+'user/loginsocial',social).then(function(resp) {
						//alert(JSON.stringify(social));
						if(resp.data.success==true){
								LoginService.setAuth(resp.data);
								LoginService.setToken(resp.data.token);
								$scope.signedIn = true;
							
								//$scope.getauthuser();
								//$scope.$apply();
								$ionicLoading.show({
								template: '<ion-spinner icon="android"></ion-spinner>'
								});
								$timeout(function() {
								
								$state.go('app.home');
								$ionicLoading.hide();
								}, 3000);
						}else{
							 $cordovaToast.showLongCenter('Registration failed!').then(function(success) {

								}, function (error) {

								});
						}
			}, function(err) {
					$cordovaToast.showLongCenter('Registration failed!').then(function(success) {

					}, function (error) {

					});

			});
		}
 $scope.googleLogin = function() {
			
			$cordovaOauth.google("177774509713-n7t6m213a64lkmope4c3plc63v6t6cf3.apps.googleusercontent.com", ['email',"https://www.googleapis.com/auth/urlshortener", "https://www.googleapis.com/auth/userinfo.email",'https://www.googleapis.com/auth/userinfo.profile']).then(function(result) {
					var obj={};
					obj.network='gplus';
					obj.socialToken=result.access_token;
					$scope.sociallogin(obj);
			}, function(error) {
					console.log(error);
			});
		}

		$scope.facebookLogin = function() {
				$cordovaOauth.facebook("707986062733495", ["email","public_profile","user_website", "user_location", "user_relationships"]).then(function(result) {
					var obj={};
					obj.network='facebook';
					obj.socialToken=result.access_token;
					$scope.sociallogin(obj);
				}, function(error) {

				});
		}
	$scope.login = function(){
			if($scope.data.username!=null && $scope.data.password!=null){
					$ionicLoading.show({
								template: '<ion-spinner icon="android"></ion-spinner>'
								});
					var mobile=$scope.data.username;
					var password=$scope.data.password;
					LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
							$ionicLoading.hide();							
						  if(data.success==true && data.message=='mobile_verify'){
								$ionicLoading.hide();
								sessionkey=data.session_key;
								verifymob=mobile;
								afterresetuser.username=mobile;
								afterresetuser.password=password;
								console.log(afterresetuser);						  
								$state.go('resetmobile');

							}else if(data.success==true){
								LoginService.setAuth(data);
								LoginService.setToken(data.token);
								$scope.signedIn = true;
								$timeout(function() {
								$state.go('app.home');
								}, 2000);

							}else{
							 
								$cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});

							 }
							
									

						}).error(function(data) {
							$ionicLoading.hide();
								$cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});

						});
			}else{
					
					$cordovaToast.showLongCenter('Username/password required').then(function(success) {

					}, function (error) {

					});
			}
	};
		$scope.signup = function() {
                     var  registermob=$scope.register.mobile;
                      var  registeremail=$scope.register.email;
				 var  registerpassword=$scope.register.password;
				if($scope.register.fname!=null && $scope.register.lname!=null && $scope.register.password!=null ){
						LoginService.signup($scope.register.fname, $scope.register.lname,$scope.register.email,$scope.register.password).success(function(data) {
								
						    if(data.success==true && data.message=='mobile_verify'){
								verifymob=registermob;
								$ionicLoading.hide();
								sessionkey=data.session_key;
								$scope.register={};							 
								afterresetuser.username=registermob;
								afterresetuser.password=registerpassword;
								console.log(afterresetuser);
								$state.go('resetmobile');
                            }else if(data.success==true && data.message!='mobile_verify'){
    $scope.register={};
    $scope.data.username=registeremail;
    $scope.data.password=registerpassword;
    
    

    
    
    	$scope.login(); 
								//$state.go('app.home');

                            }else{
								 
						         $cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});

							 }
						}).error(function(data) {
							   $cordovaToast.showLongCenter(data.message).then(function(success) {

								}, function (error) {

								});
						});
				}else{
						$cordovaToast.showLongCenter('All fields required').then(function(success) {

						}, function (error) {

						});
				}

		};

    })
.controller('HomeCtrl', function($scope, $http, $state,$stateParams) {

		$scope.getbanner = function() {
      $http.get(api_url + "banner").then(function(resp) {
          console.log(resp);

          $scope.banners = resp.data;
        }, function(err) {
          console.error("ERR", err);
          console.log(err);
        });
    };

  //  $scope.getbanner();


		$scope.getTestimonial = function() {
      $http.get(api_url + "testimonials").then(function(resp) {
          console.log(resp);

          $scope.testimonials = resp.data.result;
        }, function(err) {
          console.error("ERR", err);
          console.log(err);
        });
    };

    $scope.getTestimonial();
    


     	$scope.getserviceshome=function(catid){
				$http.get(api_url+'services/'+catid).then(function(resp) {      
          
						$scope.serviceshome=resp.data;
					}, function(err) {
              console.error('ERR', err);
              console.log(err);

					});
		 }


		$scope.getserviceshome(1);


}).controller('ServiceFeaturesCtrl', function($scope, $http, $state,$stateParams,$ionicSlideBoxDelegate,$ionicSideMenuDelegate) {
  $ionicSideMenuDelegate.canDragContent(false); 
	$scope.getserviceFeatures=function(catid){
				$http.get(api_url+'services/features/list').then(function(resp) {      
          
						$scope.servicesfeatures=resp.data;
            $ionicSlideBoxDelegate.update();
					}, function(err) {
              console.error('ERR', err);
              console.log(err);

					});
		 }


		$scope.getserviceFeatures();

})
    
.controller('OrderCtrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$cordovaToast,$stateParams,$location,$ionicHistory) {

$scope.$on('$ionicView.enter', function(ev) {
    if(ev.targetScope !== $scope)
        return;
    if($rootScope.currentorder.service==undefined || $rootScope.currentorder.service=='' || $rootScope.currentorder.service==null){
    
               $location.path('/app/home');

            }
});
 
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
  if(animating) return false;
  animating = true;
  
  current_fs = $(this).parent().parent();
  next_fs = $(this).parent().parent().next();
  
  //activate next step on progressbar using the index of next_fs
  $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
  
  //show the next fieldset
  next_fs.show(); 
  //hide the current fieldset with style
  current_fs.animate({opacity: 0}, {
    step: function(now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale current_fs down to 80%
      scale = 1 - (1 - now) * 0.2;
      //2. bring next_fs from the right(50%)
      left = (now * 50)+"%";
      //3. increase opacity of next_fs to 1 as it moves in
      opacity = 1 - now;
      current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
      next_fs.css({'left': left, 'opacity': opacity});
    }, 
    duration: 800, 
    complete: function(){
      current_fs.hide();
      animating = false;
    }, 
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  });
});

$(".previous").click(function(){
  if(animating) return false;
  animating = true;
  
  current_fs = $(this).parent().parent();
  previous_fs = $(this).parent().parent().prev();
  
  //de-activate current step on progressbar
  $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
  
  //show the previous fieldset
  previous_fs.show(); 
  //hide the current fieldset with style
  current_fs.animate({opacity: 0}, {
    step: function(now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale previous_fs from 80% to 100%
      scale = 0.8 + (1 - now) * 0.2;
      //2. take current_fs to the right(50%) - from 0%
      left = ((1-now) * 50)+"%";
      //3. increase opacity of previous_fs to 1 as it moves in
      opacity = 1 - now;
      current_fs.css({'left': left});
      previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
    }, 
    duration: 800, 
    complete: function(){
      current_fs.hide();
      animating = false;
    }, 
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  });
});


$('.addressinput,.pickuptime,.to,.deliverytime,.from').change(function(){
  console.log($('.pickuptime').val());
console.log($('.deliverytime').val());
 if($('.from').val() =='' || $('.to').val() =='' ||  $('.pickuptime').val() =='' || $('.deliverytime').val() ==''  ){

 //$('.revieworder').attr('disabled',false);

 }else{

   //$('.revieworder').attr('disabled',true);

 }

});
$scope.deliverytime=function(){
        LoginService.get(api_url+'order/deliverytime').then(function(resp) {      
           $scope.deliverytime=resp.data;

          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }
$scope.deliverytime();
$scope.pickuptime=function(){
        LoginService.get(api_url+'order/pickuptime').then(function(resp) {      
          $scope.pickuptime=resp.data;

          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }
$scope.pickuptime();

  $scope.placeOrder=function(order){

            var obj={};
            obj.flatandstreet=order.address.street;
            obj.landmark=order.address.landmark;
            obj.addressType=order.address.addresstype;
            obj.pickupDate=order.timslot.pickupdate;
            obj.deliveryDate=order.timslot.deliverydate;
            obj.timeOfDelivery=order.timslot.deliverytime.id;
            obj.timeOfPickup=order.timslot.pickuptime.id;
             obj.service_id=order.service.id;

                   var data=JSON.stringify(obj);
              LoginService.post(api_url+'order/place',data).then(function(resp) {
                if(resp.data){

                   $rootScope.currentorder={};
                               $location.path('/app/order/success/'+resp.data.id);

                }
        /*    $cordovaToast.showLongCenter('Order placed successfully!').then(function(success) {
                  



                  }, function (error) {

                  });*/

              }, function(err) {
                  console.error('ERR', err);

              });

  }
            

})



.controller('orderSuccessCtrl', function($scope, $http, $state,$stateParams,$ionicNavBarDelegate) {
     // $ionicNavBarDelegate.showBackButton(false);

$scope.placedorder=$stateParams.orderid;

})
.controller('OrderDetailsCtrl', function($scope, $http, $state,$stateParams,LoginService,$cordovaToast) {


  $scope.getOrderDetails=function(){
                  

     LoginService.get(api_url+'order/get/'+$stateParams.orderid).then(function(resp) {      
        
               $scope.order=resp.data

          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
  }

if($stateParams.orderid){
$scope.getOrderDetails();

}


 $scope.orderCancel=function(orderid){
  
                  

     LoginService.get(api_url+'order/cancelled/'+orderid).then(function(resp) {      
          $cordovaToast.showLongCenter('Order cancelled').then(function(success) {



                      $scope.order.status='cancelled';


              }, function (error) {

              });
    

          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
  }
})


.controller('AllOrderCtrl', function($scope, $http, $state,$stateParams,LoginService,$cordovaToast) {

var page=1;
                  $scope.moreresults =true;


$scope.getAllorders=function(pageno){
            var obj={};
            obj.page=pageno;
            obj.limit=3;
          

                   var data=JSON.stringify(obj);

        LoginService.post(api_url+'order/getAll',data).then(function(resp) {   
          
          if(resp.data.length==0 && page==1 ){
                $scope.notfound=true;
            }
          if(resp.data.length){
               $scope.notfound=false;
              if(resp.data.length < obj.limit){
                  $scope.moreresults =true;
              }else{
                  $scope.moreresults =false;
              }
          if(page!=1 ){
                  for (var i=0; i<resp.data.length; i++){
                      $scope.allorders.push(resp.data[i]);
                  }
                  $scope.$broadcast('scroll.infiniteScrollComplete');
              }else{
                  $scope.allorders=resp.data;
              }

          }else{
             $scope.moreresults =true;
          }
          




          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }
$scope.getAllorders(page);
 $scope.loadMoreOrders= function() {
        page++;
        $scope.getAllorders(page);

    };

  $scope.orderCancel=function($index,orderid){
  
                  

     LoginService.get(api_url+'order/cancelled/'+orderid).then(function(resp) {      
          $cordovaToast.showLongCenter('Order cancelled').then(function(success) {

                      $scope.allorders[$index]['status']='cancelled';


              }, function (error) {

              });
    

          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
  }
  //  $scope.getAllorders();

})

.controller('RateCtrl', function($scope, $http, $state,$stateParams,$ionicSlideBoxDelegate) {

    function setCartData(){
  
        $("#quantity").text(quantity_added);
        $("#total").text(total);
        var tax = (total * 14.5)/100;
        $("#tax").text(tax);
    }
    function vibrateCart(){
        var i=0;
        var myInterval = setInterval(function(){
            if(i++>=10)
                clearInterval(myInterval);
            if(i%2==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(15deg)",
                    "-moz-transform": "rotate(15deg)",
                    "transform": "rotate(15deg)",
                    "-o-transform": "rotate(15deg)",
                    "-ms-transform": "rotate(15deg)"
                });        
            }
            else if(i%3==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(-15deg)",
                    "-moz-transform": "rotate(-15deg)",
                    "transform": "rotate(-15deg)" ,
                    "-o-transform": "rotate(-15deg)",
                    "-ms-transform": "rotate(-15deg)"
                });   
            }
            else{
                $(".estimator img").css({
                    "-webkit-transform": "rotate(0deg)",
                    "-moz-transform": "rotate(0deg)",
                    "transform": "rotate(0deg)",
                    "-o-transform": "rotate(0deg)",
                    "-ms-transform": "rotate(0deg)"
                });   
            }
        }, 100);
     }  
     
    $scope.getcats=function(){
        $http.get(api_url+'categories/parent').then(function(resp) {      
          
             $scope.categories=resp.data.result;
            for (i=0; i< $scope.categories.length;i++){

              console.log($scope.categories);
                     items_added[$scope.categories[i]['id']] = new Array();   
            }
            console.log('items_added');
console.log(items_added);
$ionicSlideBoxDelegate.update();
     console.log('items_added');
          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }


    $scope.getcats();

      $scope.increment=function(categoryid,itemid,price){

          var item_label =itemid;

          if(items_added[categoryid][item_label]){
          items_added[categoryid][item_label]++;
          this.service.quantity=items_added[categoryid][item_label];
          }else{
          items_added[categoryid][item_label]=1;
          this.service.quantity=items_added[categoryid][item_label];

          }    
          if(quantity_added==0){
             $(".estimator").fadeIn(1500);

          }
             
          quantity_added++;
          total +=  Number(price);
          setCartData();
          vibrateCart();
          $ionicSlideBoxDelegate.update();

       }


$scope.decrement=function(categoryid,itemid,price){

 var item_label =itemid;

                if(items_added[categoryid][item_label]>0){
                    items_added[categoryid][item_label]--;
                    quantity_added--;
                    total -= Number(price);
                    setCartData();
                    if(quantity_added==0)
                        $(".estimator").fadeOut(1000);
                }
              this.service.quantity=items_added[categoryid][item_label];
                vibrateCart();
                $ionicSlideBoxDelegate.update();

}

  $scope.getservicesbycat=function($index,catid){
    console.log($index);
        var curr= $scope.categories[$index];

     if(curr.service==undefined){
       $http.get(api_url+'services/'+catid).then(function(resp) {      
                            $scope.services=resp.data;
                            if(resp.data.length){
                             curr.service=resp.data;
                              
                            }
                           $ionicSlideBoxDelegate.update();
              }, function(err) {
                  console.error('ERR', err);
                  console.log(err);

              });

     }

       
      
        
     }

      $scope.getservices=function(catid){
        $http.get(api_url+'services/'+catid).then(function(resp) {      
          console.log($scope.categories);
                        $scope.services=resp.data;
                        $scope.categories[0].service=$scope.services;
                        $ionicSlideBoxDelegate.update();
          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }

          
    $scope.getservices(1);
        $ionicSlideBoxDelegate.update();


}); 