angular.module('iwashApp', [ 'iwashApp.controllers','ui.router', 'google-signin','toastr'])

.run(function($rootScope,$timeout,$window,$state,$location, LoginService) {
   window.fbAsyncInit = function() {
    FB.init({
        appId: "707986062733495",
        xfbml: true,
        version: "v2.8"
    });
    FB.AppEvents.logPageView();
    };

    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
})

.config(function($stateProvider, $urlRouterProvider,$locationProvider,GoogleSigninProvider,$compileProvider) {
        //    $locationProvider.html5Mode(true);
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|geo):/);

    GoogleSigninProvider.init({
        client_id: '177774509713-n7t6m213a64lkmope4c3plc63v6t6cf3.apps.googleusercontent.com',
    });

    $stateProvider
       
        .state('home', {
                url: '/',
                templateUrl: 'templates/home.html',
                controller:'HomeCtrl'
             
        })
          .state('placeorderStep1', {
            url: '/placeorder/step1',
           
                    templateUrl: 'templates/placeorder_step1.html',
                     controller:'PlaceOrderStep1Ctrl',
                     cache:false,
                     
            
        })
        .state('placeorderStep2', {
            url: '/placeorder/step2',
           
                    templateUrl: 'templates/placeorder_step2.html',
                     controller:'PlaceOrderStep2Ctrl',
                     cache:false,
                   
            
        })
        .state('placeorderStep3', {
            url: '/placeorder/step3',
           
                    templateUrl: 'templates/placeorder_step3.html',
                     controller:'PlaceOrderStep3Ctrl',
                     cache:false,
            
        })

        
        .state('placeorderStep4', {
            url: '/placeorder/step4',
           
                    templateUrl: 'templates/placeorder_step6.html',
                     controller:'PlaceOrderStep4Ctrl',
                     cache:false,
                   
            
        })
        // .state('placeorderStep4', {
        //     url: '/placeorder/step4',
           
        //             templateUrl: 'templates/placeorder_step4.html',
        //              controller:'PlaceOrderStep4Ctrl'
            
        // })
        // .state('placeorderStep5', {
        //     url: '/placeorder/step5',
           
        //             templateUrl: 'templates/placeorder_step5.html',
        //              controller:'PlaceOrderStep5Ctrl'
            
        // })
         .state('features', {
            url: '/features',
           
                    templateUrl: 'templates/features.html',
                    controller:'ServiceFeaturesCtrl'
           
        })
        .state('myorders', {
            url: '/myorders',
           
                    templateUrl: 'templates/orderhistory.html',
                     controller:'AllOrderCtrl',
                     cache:false,
                     
            
        })

        .state('orderSuccess', {
                    url: '/order/success/:orderid',
           
                            templateUrl: 'templates/ordersuccessfull.html',
                            controller:'orderSuccessCtrl',
                            cache:false,
                            
                   
        })
        .state('order-details', {
            url: '/order-details/:orderid',
          
                    templateUrl: 'templates/orderdetail.html',
                     controller:'OrderDetailsCtrl',
                     cache:false,
                     
          
        })
        .state('pricing', {
            url: '/pricing',
           
                    templateUrl: 'templates/price.html',
                    controller: 'RateCtrl'
             
        })
     
        .state('faq', {
            url: '/faq',
           
                    templateUrl: 'templates/faq.html',
                    controller: 'FaqCtrl'

            
        })
        .state('packages', {
            url: '/packages',
           
                    templateUrl: 'templates/packages.html',
                    controller: 'PackagesCtrl'

            
        })
        .state('mysubscription', {
            url: '/mysubscription',
           
                    templateUrl: 'templates/mysubscription.html',
                     controller:'MysubscriptionCtrl',
                     cache:false,
                    
        })
        .state('subscribe', {
            url: '/subscribe',
           
                    templateUrl: 'templates/subscribe.html',
                     controller:'SubscribeCtrl',
                    
                     cache:false,
            
        }
        
       
    )
        
        .state('login', {
                url: '/login',
            
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            
            });
  $urlRouterProvider.otherwise('/');

});
