 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          
              <div class="box">
                <div class="box-header">
                <button onclick="goBack()">Go Back</button>

                </div><!-- /.box-header -->
                <div class="box-body">
					      <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Branch</th>
                  
                               <th>Source</th>

                        
                          <th>Registration Date </th>
                         
                          <th>Status</th>
                      <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						<?php foreach($employees as $employee): ?>

   
   



                      <tr>
                      <td><?php echo e($employee->userProfile->first_name); ?></td>
                      <td><?php echo e($employee->userProfile->last_name); ?></td>
                      <td><?php echo e($employee->email); ?></td>
                       <td><?php echo e($employee->mobile); ?></td>
                       <td><?php echo e(@$employee->userlocation->branch); ?>(<?php echo e(@$employee->userlocation->location); ?>)</td>

                               <td>
                                <?php 
                              if($employee->added_by!=NULL && $employee->location!=NULL){

                                     if($employee->addedby->role->slug=='admin'){
                                       echo 'Added by Super Admin';

                                     }else{
                                       echo $employee->addedby->email.'(' .$employee->addedby->userlocation->branch.'-'.$employee->addedby->userlocation->location.')<br>';
                                      echo 'Branch Admin';
                                     }
                               }else{

                                  echo 'NA';

                               }

                               
                              ?>

                                </td>
                                 
                            
                        <td><?php echo e($employee->createdAt); ?></td>
                         
                           
                            <td>
                              
                                                        <span class="label label-<?php echo $employee->active==1?'success':'danger' ?>"><?php echo $employee->active==1?'Active':'Inactive' ?></span>

                       </td>
                     <td>


                     <a class="btn btn-success" href="/branch/employee/<?php echo e($employee->id); ?>/edit">Edit</a>
                            <a class="btn btn-danger" href="/branch/employee/<?php echo e($employee->id); ?>/delete">Delete</a>

                     </td>
                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
                  <?php echo $employees->render(); ?>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
			    $('.getsubcat').click(function(){
			    $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
		});
     
        
        </script>
        
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>