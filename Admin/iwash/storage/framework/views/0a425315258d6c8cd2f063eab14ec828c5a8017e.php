 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          
              <div class="box">
                <div class="box-header">
                <button onclick="goBack()">Go Back</button>

                </div><!-- /.box-header -->
                <div class="box-body">
					      <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Branch</th>
                          <th>Registration Date </th>
                         
                          <th>Status</th>
                      <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						<?php foreach($branchadmins as $admin): ?>

   
   



                      <tr>
                      <td><?php echo e($admin->userProfile->first_name); ?></td>
                      <td><?php echo e($admin->userProfile->last_name); ?></td>
                      <td><?php echo e($admin->email); ?></td>
                       <td><?php echo e($admin->mobile); ?></td>
                       <td><?php echo e(@$admin->userlocation->branch); ?>(<?php echo e(@$admin->userlocation->location); ?>)</td>
                        <td><?php echo e($admin->createdAt); ?></td>
                         
                           
                            <td>
                              
                                                        <span class="label label-<?php echo $admin->active==1?'success':'danger' ?>"><?php echo $admin->active==1?'Active':'Inactive' ?></span>

                       </td>
                     <td>


                     <a class="btn btn-success" href="/branch/admin/<?php echo e($admin->id); ?>/edit">Edit</a>
                            <a class="btn btn-danger" href="/branch/admin/<?php echo e($admin->id); ?>/delete">Delete</a>

                     </td>
                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
                  <?php echo $branchadmins->render(); ?>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
			    $('.getsubcat').click(function(){
			    $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
		});
     
        
        </script>
        
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>