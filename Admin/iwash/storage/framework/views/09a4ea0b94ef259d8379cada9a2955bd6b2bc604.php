 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                <button onclick="goBack()">Go Back</button>

    <script>
function goBack() {
    window.history.back();
}
</script>
           
                <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" action=''>
								<?php echo e(csrf_field()); ?>


                  <div class="box-body">

        <div class="col-lg-12">

        <div class="col-lg-6">
        <label >Service Type:</label>
        <span> <?php echo e(@$order->servicecat->name); ?>     </span>  
        </div>
        <div class="col-lg-6">
        <label >Customer</label>

        <span><?php echo e(@$order->userProfile->first_name); ?> <?php echo e(@$order->userProfile->last_name); ?>    </span>  

        </div>

        </div>


        <div class="col-lg-12">
                
                <div class="col-lg-6">
                <label >Order Date: </label>
                    <span><?php echo e($order->createdAt); ?></span>
                </div>
                <div class="col-lg-6">
                    <label >Address Type</label>
  <select class="form-control"  id="sel1" name="addressType" placeholder="Address type">
    <option value="">Select Address type</option>
    <option value="home" <?php if($order->addressType=='home'){ echo 'selected';} ?>>Home</option>
    <option value="office" <?php if($order->addressType=='office'){ echo 'selected';} ?>>Office</option>
    <option value="other" <?php if($order->addressType=='other'){ echo 'selected';} ?>>Other</option>
  </select>
                </div>
             
        </div> 
        <div class="col-lg-12">
        <div class="col-lg-6">
                    <label >Flat and Street: </label>
                    <input type="text"  class="form-control" value="<?php echo e($order->flatandstreet); ?>" name="flatandstreet" placeholder="Flat and Street" value="<?php echo @$_GET['flatandstreet']; ?>">

                </div>
                <div class="col-lg-6">
                <label >Landmark: </label>
                <input type="text" value="<?php echo e($order->landmark); ?>"  class="form-control"  name="landmark"  placeholder="Landmark" >

                </div>
              
        </div> 
        <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Pickup Date: </label>

    <input type="date"  class="form-control" name="pickupDate" value="<?php echo e($order->pickupDate); ?>"  placeholder="Pickup Date"  >
                </div>
                <div class="col-lg-6">
                <label >Pickup  Time: </label>
    <select name='timeOfPickup'  class="form-control" >
    <option value="">Select</option>
    <?php

    foreach($pickup as $pick){ ?>
    <option  value="<?= $pick->id;?>"  <?php if($order->timeOfPickup==$pick->id){echo 'selected';} ?> ><?= $pick->timebetween;?></option>

    <?php } ?>
    </select>  
                </div>
        </div> 
        <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Delivery Date: </label>

    <input type="date"   class="form-control" name="deliveryDate"  value="<?php echo e($order->deliveryDate); ?>"  placeholder="Delivery Date"  >
                </div>
                <div class="col-lg-6">
                <label >Delivery  Time: </label>
    <select name='timeOfDelivery'  class="form-control" >
    <option value="">Select</option>
    <?php

    foreach($delivery as $del){ ?>
    <option  value="<?= $del->id;?>"  <?php if($order->timeOfDelivery==$del->id){echo 'selected';} ?>><?= $del->timebetween;?></option>

    <?php } ?>
    </select>     
                </div>
        </div> 
        <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Total Amount: </label>

    <input type="text"   class="form-control" name="totalamount"  value="<?php echo e($order->totalamount); ?>"   placeholder="Total Amount"  >
                </div>
                <div class="col-lg-6">
                <label >Quantity: </label>

    <input type="number"   class="form-control" name="qty" value="<?php echo e($order->qty); ?>"  placeholder="Quantity"  >
                </div>
        </div> 

        <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Order Status: </label>
    <select class="form-control"  id="sel1" name="status" >
    <option value="">Select Order Status</option>
    <option value="placed" <?php if($order->status=='placed'){echo 'selected';} ?>>Placed</option>
    <option value="accepted" <?php if($order->status=='accepted'){echo 'selected';} ?>>Accepted</option>
    <option value="picked up" <?php if($order->status=='picked up'){echo 'selected';} ?> >Picked Up</option>
    <option value="processing" <?php if($order->status=='processing'){echo 'selected';} ?>>Processing</option>
    <option value="ready to deliver" <?php if($order->status=='ready to deliver'){echo 'selected';} ?> >Ready to delivery</option>
    <option value="delivered" <?php if($order->status=='delivered'){echo 'selected';} ?>>Delivered</option>
    <option value="cancelled" <?php if($order->status=='cancelled'){echo 'selected';} ?>>Cancelled</option>
  </select>
                </div>
                <div class="col-lg-6">

                     <?php   if(Session::get('status')=='admin'){ ?>
                 
                      <label >Branch </label>
                      <select name='branch' class="form-control">
                          <?php foreach($branches as $parent){ ?>
                               <option <?php if($parent->id==$order->branch){echo 'selected';} ?> value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>
                  
                             <?php } ?>
                       </select>
                 
         <?php } ?>
                </div>
        </div> 
        <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Payment Date: </label>

    <input type="date"   class="form-control" value=" <?php echo  date('Y-m-d', strtotime($order->payment_date));?>"  name="payment_date"  placeholder="Payment Date"  >
                </div>
                <div class="col-lg-6">
                <label >Payment Status: </label>
    <select class="form-control"  id="sel1" name="payment_status" >
    <option value="">Select Payment Status</option>
    <option value="0" <?php if($order->payment_status==0){echo 'selected';} ?>>Pending</option>
    <option value="1" <?php if($order->payment_status==1){echo 'selected';} ?>>Paid</option>
  

  </select>
                </div>
        </div> 
    


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   </div>
                  </div>
                </form>
     
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <style>

.col-lg-12 {
    padding-bottom: 1%;

}

</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>