 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				
		
					      <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Branch Name</th>
                        <th>Location</th>

                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>

						
						<?php foreach($branches as $branch): ?>

                      <tr>
                        <td>    <?php echo e($branch->branch); ?> </td>
                        <td>    <?php echo e($branch->location); ?> </td>
             
                        <td>
                      

                            <a class="btn btn-success btn-sm" href="/branch/<?php echo e($branch->id); ?>/edit">Edit</a>
                            <a class="btn btn-danger btn-sm" href="/branch/<?php echo e($branch->id); ?>/delete">Delete</a>
                            <a class="btn btn-info btn-sm"  href="/branch/add-admin"> Add Branch Admin</a>
                            <a  class="btn btn-info btn-sm"  href="/branch/admins"> View Branch Admins </a>
                            <a class="btn btn-info btn-sm"  href="/branch/add-employee"> Add Employee</a>
                            <a  class="btn btn-info btn-sm"  href="/branch/employees"> View Employees </a>
                        </td>
                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
<div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	<?php echo $branches->render(); ?> </div>         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
     
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>