 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          
            <button onclick="goBack()">Go Back</button>
   
            <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i>
           <span class="text-center">Package: <?php echo e(@$subscription->Package->package); ?></span>
          <small class="pull-right"><strong>Subscription Date: <?php echo e(@$subscription->createdAt); ?></strong></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
 
      <div class="col-sm-4 invoice-col">
  <h4> Customer Details</h4>
        <address>
          <strong><?php echo e(@$subscription->userProfile->first_name); ?> <?php echo e(@$subscription->userProfile->last_name); ?> </strong><br>
       
          <b> Email: </b> <?php echo e(@$subscription->user->email); ?> <br>
          <b>Mobile: </b>  <?php echo e(@$subscription->user->mobile); ?> <br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
      <h4> Subscription Details</h4>
        <b>Pickup Option: </b> <?php echo e($subscription->pickup_option); ?><br>
        <b>Duration: </b> <?php echo e($subscription->duration); ?> Month<br>
        <b>Amount: </b> <?php echo e('QRS '.$subscription->payment_amount); ?><br>

                  </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
      
      <table class="table table-striped">
          <thead>
          <tr>
            <th>Serial No.</th>
            <th>Service</th>
            <th>Total Quantity</th>
         
            <th>Remaining Quantity</th>
          </tr>
          </thead>
          <tbody>
        
        <?php $totalservice=$subscription->Package->PackageServices->toArray();
        

        $i=1; foreach( $subscription->SubscribeServices as $services){?>
          <tr>
            <td><?php echo e($i); ?></td>
            <td><?php echo '<p><a>&raquo; '.$services->Service->name.'('.$services->category->name.')</a></p>'; ?></td>
            <td>
              <?php 
       ;

           if($totalservice[$i-1]['service_id']==$services->service_id && $totalservice[$i-1]['package_id']==$services->package_id){
            echo $totalservice[$i-1]['quantity'];
           }
            
            ?>
            </td>
            <td><?php echo e($services->quantity); ?></td>
          </tr>

          <?php $i++; }?>
         
          </tbody>
        </table>
           
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
    
      <!-- /.col -->
      <div class="col-xs-6">
        
      <?php 
          $status='';
          if($subscription->status===1 && $subscription->payment_status==1){
          $status='label-success"';
          $subsStatus="Active";
            
          }else  if($subscription->status==1 && $subscription->payment_status==0){
            $status='label-warning"';
            $subsStatus="Pending";
            }else {
          $status='label-danger"';
          $subsStatus="Inactive";
            
          }
					   
					   ?>
                   <b>Subscription Status:</b>                     <span class="label <?php echo $status;?>"><?php echo e($subsStatus); ?></span> 
                   <?php 
 $time = strtotime($subscription->createdAt);
$final = date("Y-m-d", strtotime("+".$subscription->duration." month", $time));
if(date("Y-m-d")>=$final && $subscription->payment_status==1 ){
  $isExpired=1;
}else{

  $isExpired=0;
}


if($subscription->payment_status==1){
  
    $remaining=$subscription->SubscribeServices->sum('quantity');
  }
?>					 
     
     <p>        <b>Subscription Expired: </b> <span class="label  <?php if($isExpired==1){ echo 'label-danger';}else{echo 'label-default' ;} ?>"><?php if($isExpired==1){ echo 'Yes';}else if($subscription->payment_status==1){ echo 'No';}else{echo 'NA' ;} ?></span> <br></p>
     <p>        <b>Subscription Limit Used: </b> <span class="label  <?php if($subscription->payment_status==1 &&  $remaining<=0){ echo 'label-danger';}else{echo 'label-default' ;} ?>"><?php if($subscription->payment_status==1 &&  $remaining<=0){ echo 'Yes';}else if($subscription->payment_status==1 ){ echo 'No';}else{echo 'NA' ;} ?></span><br></p>

        <p class=""><b>Payment Date: </b> <?php if($subscription->payment_status==0){echo '<span class="label label-default">NA</span>';}else{ echo $subscription->payment_date;} ?></p>
        <p class=""><b>Payment Status:</b>  <span class="label label-<?php if($subscription->payment_status==1){echo 'success';}else{ echo 'danger';} ?>"><?php if($subscription->payment_status==1){echo 'Completed';}else{ echo 'Pending';} ?></span></p>
        <div class="table-responsive">
          <table class="table">
            <tbody>
           
            <tr>
              <th>Total: <?php if($subscription->payment_status==0){echo '<span class="label label-default">NA</span>';}else{  echo 'QRS '. $subscription->payment_amount;} ?></th>
              <td></td>
            </tr>
          </tbody></table>
        </div>
      </div>
      <div class="col-xs-6">
     
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
          

            </div><!-- /.col -->
          </div><!-- /.row -->


        </section><!-- /.content -->
    <script>
function goBack() {
    window.history.back();
}
</script>
        
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>