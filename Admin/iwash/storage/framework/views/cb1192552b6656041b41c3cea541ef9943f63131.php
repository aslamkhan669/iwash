 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
     
                <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" action=''>
								<?php echo e(csrf_field()); ?>


                  <div class="box-body">
                    <div class="form-group">
                      <label >Package Name</label>
                      <input type="text" required value="<?php echo e($pack->package); ?>"   class="form-control" placeholder="For eg.(Traditional)" name='package' id="exampleInputEmail1" required >
                    </div>
                    <!-- <div class="form-group">
                      <label >Usage Limit</label>
                      <input type="text" required value="<?php echo e($pack->usage_limit); ?>"  class="form-control"  placeholder="For eg.(50 Thobs,50 Srwal)"  name='usage_limit' id="exampleInputEmail1" required >
                    </div> -->
                    <div class="form-group">
                      <label >Pickup Options(Enter value as Comma Seperated)</label>
                      <input type="text" required value="<?php echo e($pack->pickup_options); ?>"  class="form-control" placeholder="For eg.(Alternative,Weekly)"  name='pickup_options' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Available Packages(Enter value as Comma Seperated)</label>
                      <input type="text" required value="<?php echo e($pack->available_packages); ?>"   class="form-control" placeholder="For eg.(1,3,6,12)"  name='available_packages' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Pricings(For each month)</label>
                      <input type="text" required value="<?php echo e($pack->pricing_for_each_month); ?>"  class="form-control" placeholder="For eg.(700)"   name='pricing_for_each_month' id="exampleInputEmail1" required >
                    </div>
                    <?php 
//print_r($pack->PackageServices->toArray());
$subsripcats = array_column($pack->PackageServices->toArray(), 'category_id');
$srvs = array_column($pack->PackageServices->toArray(), 'service_id');
////print_r($srvs);
?>
                    <div class="form-group">
                      <label >Category</label>
                      <?php foreach($categories as $category){ ?>
                        
                      <div class="checkbox packagecategory"> 
                      <label for="<?= $category->id;?>"><input <?php if (in_array($category->id, $subsripcats)){echo 'checked';} ?>  id="<?= $category->id;?>" type="checkbox" class="category" name='category_id[]'  value="<?= $category->id;?>"><?= $category->name;?></label>
                    </div>
                    <div class="services">
            <?php
           if (in_array($category->id, $subsripcats)){
             echo '<b>Choose Service </b> <br>';
            foreach($category->services as $serv){

?>
<div class="checkbox"> <label for="<?php echo $serv->id; ?>"><input <?php if (in_array($serv->id, $srvs)){echo 'checked';} ?>   id="<?php echo $serv->id; ?>"  type="checkbox" name="service_id<?php echo $category->id; ?>[]" class="servicechek" value="<?php echo $serv->id; ?>"><?php echo $serv->name; ?></label> <input type="number"  class="quantity" name="quantity_<?php echo $category->id; ?>_<?php echo $serv->id; ?>" value="<?php               if (in_array($serv->id, $srvs)){$requiredkey=array_search($serv->id,$srvs);echo    $pack->PackageServices->toArray()[$requiredkey]['quantity'];  }
 ?>"></div>
<?php 
            

            }
            }
            
            ?>

                    </div>
                    <?php
                  
                  } ?>
                    
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
           	
    <style>
    .services .quantity {


      width:45px;
    }
    .services{

      margin-left: 2%;
    }
    </style>
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>