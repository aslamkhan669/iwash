<?php $__env->startSection('container'); ?>
<style>
   .dashboard img{
           width: 130px;
   height: 130px;
   border: 1px solid black;
   }
   .dash span{
       float: right;
   }
   .dash span .iw-qtyminus{
       color: white;
   background: #5380f2!important;
   border: 1px solid #5380f2!important;
   width: 30px;
   height: 30px;
   }
   .dash span .iw-qtyplus{
       color: white;
   background: #5380f2!important;
   border: 1px solid #5380f2!important;
   width: 30px;
   height: 30px;
   }
   .dash span .iw-qty{
           text-align: center;
   width: 50px;
   height: 30px;
   }
   .iw-admin .col-md-12{
       border-bottom: 1px solid;
       margin-top: 5px;
   }
   .estimator_menu {
    display: none;
    float: right;
    margin-top: 33px;
}

.decrement {
    padding: 3px 9px;
    border: 1px solid #E0DFDF;
    border-radius: 3px;
    cursor: pointer;
}

.quantity {
    margin: 5px 6px 0 6px;
}

.increment {
    padding: 3px 8px;
    border: 1px solid #E0DFDF;
    border-radius: 3px;
    cursor: pointer;
}

.estimator_menu p {
    float: left;
}

p.decrement:hover,
p.increment:hover {
    background: #f7f7f7;
}

.estimator {
    position: fixed;
    bottom: 17px;
    right: 50px;
    padding: 10px;
    background: #f57a1d;
    color: white;
    border-radius: 12px;
    box-shadow: #CECECE 2px 2px 5px 2px;
    display: none;
}

.estimator img {
    width: 32px;
    float: left;
}

.estimator p {
    color: #053169;
    margin-left: -35px;
    font-weight: bolder;
    width: 38px;
    float: left;
    text-align: center;
    margin-top: 11px;
    z-index: 99999;
    transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    /* IE 9 */
    -moz-transform: rotate(0deg);
    /* Firefox */
    -webkit-transform: rotate(0deg);
    /* Safari and Chrome */
    -o-transform: rotate(0deg);
}

.estimator span {
    margin-top: 7px;
    margin-left: 4px;
    float: left;
}

#price_list ul {
    overflow: auto;
    margin-bottom: 100px;
}
</style>
   
<div class="container">
<div id="exTab2"> 
<ul class="nav nav-tabs header_bar">
<?php foreach ($categories as $cat) {
?>
           <li category="<?php echo $cat->id; ?>" class="<?php if($cat->name=='Laundry'){echo 'active';} ?>" onclick="getServices(<?php echo $cat->id; ?>)">
       <a  href="#<?php echo $cat->id; ?>" data-toggle="tab"><?php echo $cat->name; ?></a>
           </li>
    <?php } ?>
     
       </ul>

           <div class="tab-content iw-admin" style="    margin-top: 20px;">
             <?php foreach ($categories as $cat) { ?>
             <div class="tab-pane active" id="<?php echo $cat->id; ?>">
        <div class="row"></div>

               </div>
           <?php } ?>
            
           </div>
 </div>
   
</div>
       </section><!-- /.content -->
       <div class="estimator">
                <img src="basket.png">
                <p id="quantity">0</p>
                <span style="margin-left:15px;">Estimated Price(QRS): </span>
                <span id="total">0</span>
                
            </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>