 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
			  				  <div class="box-header with-border">
<a href="/orders"><h3>Orders </h3></a>

<form class="form-inline" name="branchchoose" id="branchchoose" method="get" action="">
   <?php   if(Session::get('status')=='admin'){ ?>
              
                      <select name='branch' id="selectlocation" class="form-control">
                        <option value="" >All Branches</option>
              <?php foreach($branches as $parent){ ?>
                 <option <?php if(isset($_GET['branch']) && ($_GET['branch']==$parent->id)){echo 'selected';} ?> value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>
                  
               <?php } ?>
                       </select>
                 
         <?php } ?>
</form>
                </div><!-- /.box-header -->
                <div class="box-header">


                <form class="form-inline" method="get" action="">
                <div class="form-group">
                <select class="form-control" required name="year" id="year">
    
 
  </select>
              </div>
              <div class="form-group">
    <input type="text" class="form-control" name="from" required  placeholder="Order From Date" onfocus="(this.type='date')" value="<?php echo @$_GET['from']; ?>">
    <input type="hidden" name="filtertype" value="simple">

  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="to"  required placeholder="Order To Date" onfocus="(this.type='date')" value="<?php echo @$_GET['to']; ?>">
  </div>
  <div class="form-group">
        <select name='service_type' class="form-control" placeholder="Service Type">
            <option value="">Select Service Type</option>
            <?php

            foreach($categories as $category){ ?>
            <option <?php if(isset($_GET['service_type'])&&$_GET['service_type']==$category->id){ echo 'selected';} ?> value="<?= $category->id;?>" ><?= $category->name;?></option>

            <?php } ?>
        </select>
   </div>  



            <div class="form-group">
        
            <select class="form-control" id="sel1" name="status" >
            <option value="">Select Order Status</option>
            <option value="placed" <?php if(isset($_GET['status']) && $_GET['status']=='placed'){ echo 'selected';} ?>>Placed</option>
            <option value="accepted" <?php if(isset($_GET['status']) && $_GET['status']=='accepted'){ echo 'selected';} ?>>Accepted</option>
            <option value="picked up" <?php if(isset($_GET['status']) && $_GET['status']=='picked up'){ echo 'selected';} ?>>Picked Up</option>
            <option value="processing" <?php if(isset($_GET['status']) && $_GET['status']=='processing'){ echo 'selected';} ?>>Processing</option>
            <option value="ready to deliver" <?php if(isset($_GET['status']) && $_GET['status']=='ready to deliver'){ echo 'selected';} ?>>Readt to delivery</option>
            <option value="delivered" <?php if(isset($_GET['status']) && $_GET['status']=='delivered'){ echo 'selected';} ?>>Delivered</option>
            <option value="cancelled" <?php if(isset($_GET['status']) && $_GET['status']=='cancelled'){ echo 'selected';} ?>>Cancelled</option>
          </select>
            </div>
         
                 
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="payment_status" >
            <option value="">Select Payment Status</option>
            <option value="1" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==1){ echo 'selected';} ?>>Paid</option>
            <option value="0" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==0){ echo 'selected';} ?>>Pending</option>
          
          </select>
            </div>
  <button type="submit" class="btn btn-default">Submit</button>
  <a  data-toggle="modal" data-target="#advance">Open Advance Filter</a>

</form>
<script>
var start = 2017;
var end = new Date().getFullYear();
var options = "<option value=''>Order Year</option>";
for(var year = start ; year <=end; year++){
  options += "<option>"+ year +"</option>";
}
document.getElementById("year").innerHTML = options;

</script>
  <!-- Modal -->
  <div class="modal fade" id="advance" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Advance Filter</h4>
        </div>
        <div class="modal-body">
            <form  method="get" action="<?php echo e(URL::to('orders')); ?>">
            <input type="hidden" name="filtertype" value="advance">

<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Order Id" name='order_id' value="<?php echo @$_GET['order_id']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="First Name" name='first_name' value="<?php echo @$_GET['first_name']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Last Name" name='last_name' value="<?php echo @$_GET['last_name']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Mobile" name='mobile' value="<?php echo @$_GET['mobile']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
    
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Email" name='email' value="<?php echo @$_GET['email']; ?>">
          </div>
          
          
            </div>
            <div class="col-lg-6">
            <div class="form-group">
                              <select name='service_type' class="form-control" placeholder="Service Type">
                    <option value="">Select</option>
                      <?php
                      
                       foreach($categories as $category){ ?>
                         <option <?php if(isset($_GET['service_type'])&&$_GET['service_type']==$category->id){ echo 'selected';} ?> value="<?= $category->id;?>" ><?= $category->name;?></option>
                          
                       <?php } ?>
                               </select>
                            </div>  
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
        
          <select class="form-control" id="sel1" name="addressType" placeholder="Address type">
            <option value="">Select Address type</option>
            <option value="home" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='home'){ echo 'selected';} ?>>Home</option>
            <option value="office" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='office'){ echo 'selected';} ?>>Office</option>
            <option value="other" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='other'){ echo 'selected';} ?>>Other</option>
          </select>
          </div>
            </div>
            <div class="col-lg-6">
               
          <div class="form-group">
            <input type="text" class="form-control" name="flatandstreet" placeholder="Flat and Street" value="<?php echo @$_GET['flatandstreet']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control"  name="landmark"  placeholder="Landmark" value="<?php echo @$_GET['landmark']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" name="pickupDate"  placeholder="Pickup Date" onfocus="(this.type='date')" value="<?php echo @$_GET['pickupDate']; ?>" >
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" name="deliveryDate"  placeholder="Delivery Date" onfocus="(this.type='date')" value="<?php echo @$_GET['deliveryDate']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" name="createdAt"  placeholder="Order Date" onfocus="(this.type='date')" value="<?php echo @$_GET['createdAt']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="status" >
            <option value="">Select Order Status</option>
            <option value="placed" <?php if(isset($_GET['status']) && $_GET['status']=='placed'){ echo 'selected';} ?>>Placed</option>
            <option value="accepted" <?php if(isset($_GET['status']) && $_GET['status']=='accepted'){ echo 'selected';} ?>>Accepted</option>
            <option value="picked up" <?php if(isset($_GET['status']) && $_GET['status']=='picked up'){ echo 'selected';} ?>>Picked Up</option>
            <option value="processing" <?php if(isset($_GET['status']) && $_GET['status']=='processing'){ echo 'selected';} ?>>Processing</option>
            <option value="ready to deliver" <?php if(isset($_GET['status']) && $_GET['status']=='ready to deliver'){ echo 'selected';} ?>>Readt to delivery</option>
            <option value="delivered" <?php if(isset($_GET['status']) && $_GET['status']=='delivered'){ echo 'selected';} ?>>Delivered</option>
            <option value="cancelled" <?php if(isset($_GET['status']) && $_GET['status']=='cancelled'){ echo 'selected';} ?>>Cancelled</option>
          </select>
            </div>
            </div>
            <div class="col-lg-6">
                 
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="payment_status" >
            <option value="">Select Payment Status</option>
            <option value="1" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==1){ echo 'selected';} ?>>Paid</option>
            <option value="0" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==0){ echo 'selected';} ?>>Pending</option>
          
          </select>
            </div>
            </div>


</div>

<div class="col-lg-12">
     
            <div class="form-group">
            <input type="text" class="form-control" name="payment_date"  placeholder="Payment Date" onfocus="(this.type='date')" >
          </div>
        </div>



           
         
         
       
        
        
        
        
       
        
       
        
       
        
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
</form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


























                <form class="form-inline" method="get">
                
                </div><!-- /.box-header -->
                <div class="box-body">

                <?php if(session()->has('ok')): ?>
      <?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>  
    <?php if(isset($info)): ?>
      <?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <?php if(session()->has('error')): ?>
      <?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>  
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					    <th>Order ID</th>
                       <th>User</th>
                        <th>Service</th>
                          <th>Flat and Street</th>
                  <!--
                          <th>Landmark</th>
                            <th>Address Type</th>
               -->
                             <th>Pickup Date</th>
                              <th>Delivery Date</th>
                              <th>Order Placed Date</th>
                              <th>Order Status</th>
                              <th>Quantity</th>
                              <th>Total Amount</th>
                              <th>Payment Status</th>

                              <th>Payment Date</th>


							                          
							                           <th>Action</th>


                      </tr>
                    </thead>
                    <tbody>
            
            <?php foreach($orders as $order): ?>



                      <tr>
					                          <td> IW<?php echo e(@$order->id); ?></td>

					
                      <?php 
                     $str='Name: '.$order->userProfile->first_name.' '.$order->userProfile->last_name."<br />";
                     $str.='Email: '.$order->user->email."<br />";
                      $str.='Mobile : '.$order->user->mobile."<br />";
                       $str.='Register Date : '.$order->user->createdAt."<br />";


                      ?>
                   <td ><?php echo e(@$order->userProfile->first_name); ?></td>


                        <td> <?php echo e(@$order->servicecat->name); ?></td>
                        <td> <?php echo e(@$order->flatandstreet); ?></td>
                  <!--      <td> <?php echo e(@$order->landmark); ?></td>
                        <td> <?php echo e(@$order->addressType); ?></td>
               -->
                        <td> <?php echo e(@$order->pickupDate); ?> (<?php echo e(@$order->pickup->timebetween); ?>)</td>
                        <td> <?php echo e(@$order->deliveryDate); ?> (<?php echo e(@$order->delivery->timebetween); ?>)</td>
                     
                         <td> <?php echo e(@$order->createdAt); ?></td>
                       
					   
                                    <?php 
                                      $status='';
                                    if($order->status=='delivered'){
                                    $status='label-success"';
                                      
                                    }else  if($order->status=='cancelled'){
                                    $status='label-danger"';
                                      
                                    }else  if($order->status=='placed'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='accepted'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='picked up'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='processing'){
                                    $status='label-info"';
                                      
                                    }
                                          else  if($order->status=='ready to deliver'){
                                    $status='label-info"';
                                      
                                    }
                                    else  if($order->status=='delivered'){
                                    $status='label-success"';
                                      
                                    }
                                    
                                    ?>
                                             <td><span class="label <?php echo $status;?>"><?php echo e(@ucfirst($order->status)); ?></span> </td>
                                             <td> <?php echo e(@$order->qty); ?></td>
                                             <?php 
                                      $status='';
                                    if($order->payment_status==1){
                                         $status='label-success"';
                                      
                                    }else{
                                        $status='label-danger"';
                                      
                                    }
                                    
                                    ?>
                                             <td> <?php echo e(@$order->totalamount); ?></td>
                                             <td><span class="label <?php echo $status;?>"><?php if($order->payment_status==1){echo 'Paid';}else{echo 'Pending';} ?></span> </td>

                                             <td> <?php if($order->payment_status==0){echo '<span class="label label-default">NA</span>';}else{ echo $order->payment_date;} ?></td>

											 
                                             <td>
                                             <a class="btn btn-default btn-sm" href="<?php echo e(URL::to('order/'.$order->id.'/view')); ?>">View</a>
                            <a class="btn btn-default btn-sm" href="<?php echo e(URL::to('order/'.$order->id.'/edit')); ?>">Edit</a>
                            <a class="btn btn-danger btn-sm" href="<?php echo e(URL::to('order/'.$order->id.'/delete')); ?>">Delete</a>
                          
                        
                        </td>

                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
                  <?php echo $orders->render(); ?>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>