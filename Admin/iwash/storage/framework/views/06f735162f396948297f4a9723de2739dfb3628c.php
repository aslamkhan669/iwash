 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				
		
					      <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Package Name</th>
                        <th>Usage Limit</th>
                        <th>Pickup Options</th>
                        <th>Available Packages</th>
                        <th>Pricings(For each month)</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>

						
						<?php foreach($packages as $val): ?>

                      <tr>
                        <td>  <?php echo e($val->package); ?></td>
                        <td>
                          <?php
                      foreach($val->PackageServices as $servicepack){

                       
                          echo '<p>'.$servicepack->quantity.' '. $servicepack->Service->name .'('.$servicepack->Category->name.')'.'</p>';


                      }
                      ?>
                      

                      </td>

                        <td>  <?php echo e($val->pickup_options); ?></td>

                        <td>  <?php echo e($val->available_packages); ?></td>

                        <td>  <?php echo e($val->pricing_for_each_month); ?></td>

             
                        <td>
                  

                            <a class="btn btn-success" href="/package/<?php echo e($val->id); ?>/edit">Edit</a>
                            <a class="btn btn-danger" href="/package/<?php echo e($val->id); ?>/delete">Delete</a>
                          
                        
                        </td>
                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
<div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	<?php echo $packages->render(); ?> </div>         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
     
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>