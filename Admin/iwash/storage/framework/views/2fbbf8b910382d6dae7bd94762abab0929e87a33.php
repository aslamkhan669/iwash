 <?php $__env->startSection('container'); ?>
<!-- Small boxes (Stat box) -->

<div class="row">
  <div class="col-md-12">

    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <?php if(session()->has('ok')): ?> <?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php endif; ?> <?php if(isset($info)): ?>
        <?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php endif; ?> <?php if(session()->has('error')): ?> <?php echo $__env->make('partials/error',
        ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php endif; ?>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" method='post'  enctype='multipart/form-data'>
        <?php echo e(csrf_field()); ?>

        <div class="box-body">

          <div class="form-group">
            <div class="col-lg-12">
              <label>First Name</label>
              <input type="text" class="form-control" name='first_name' required>
            </div>

            <div class="col-lg-12">
              <label>Last Names</label>
              <input type="text" class="form-control" name='last_name' required>
            </div>


          </div>

          <div class="form-group">
              <div class="col-lg-12">
           
   <label>Email</label>
              <input type="email" class="form-control" name='email'></div>

            </div>
            <div class="col-lg-12">
              <label>Mobile</label>
              <input type="tel" class="form-control" name='mobile' required>
            </div>

          



          <div class="form-group ">
            <div class="col-lg-12">
                 <label>Password</label>
              <input type="password" class="form-control" name='password' required>
            
            </div>
             </div>
    



       




        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>



    </div>
    <!-- /.box -->


  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>