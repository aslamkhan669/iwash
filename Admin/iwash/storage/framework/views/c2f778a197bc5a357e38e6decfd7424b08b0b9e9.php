
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo e(URL::asset('AdminLTE/dist/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo e(Auth::user()->role->title); ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
         
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="<?php echo e(URL::to('dashboard')); ?>">
                <i class="fa fa-dashboard"></i> <span>Price Estimator</span>
              </a>
            
            </li>
          <?php   if(Session::get('status')=='admin' || Session::get('status')=='employee'  || Session::get('status')=='branch_admin'){ ?>

              <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Orders</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('create-order')); ?>"><i class="fa fa-circle-o"></i> Create Order</a></li>
                <li><a href="<?php echo e(URL::to('orders')); ?>"><i class="fa fa-circle-o"></i> View All </a></li>
                <li><a href="<?php echo e(URL::to('/order/graphs')); ?>"><i class="fa fa-circle-o"></i> Orders Graph </a></li>

                

             </ul>
            </li>

				<li class="treeview">
					<a href="#">
					  <i class="fa fa-files-o"></i>
					  <span>Customers</span> <i class="fa fa-angle-left pull-right"></i>
					 
					</a>
					<ul class="treeview-menu">
					  <!--<li><a href="<?php echo e(URL::to('/users/pending')); ?>"><i class="fa fa-circle-o"></i> New User(Unverified) </a></li>
					 -->
					  <li><a href="<?php echo e(URL::to('/add-user')); ?>"><i class="fa fa-circle-o"></i> Add new</a></li>
					  <li><a href="<?php echo e(URL::to('/users')); ?>"><i class="fa fa-circle-o"></i> View All</a></li>

				   </ul>
				</li>

			<?php } ?>
			    <?php if( Session::get('status')=='branch_admin'){ ?>
				
             <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Employees</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="/branch/add-employee"><i class="fa fa-circle-o"></i> Add Employee</a></li>
                <li><a href="/branch/employees"><i class="fa fa-circle-o"></i> View Employees </a></li>

             </ul>
            </li>
				<?php } ?>

				
             <?php if( Session::get('status')=='admin'){ ?>
      
        <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Monthly Packages</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="/add-package"><i class="fa fa-circle-o"></i> Add Package</a></li>
                <li><a href="/packages"><i class="fa fa-circle-o"></i> View Packages </a></li>

             </ul>
            </li>
        <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Subscriptions</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="/add-subscription"><i class="fa fa-circle-o"></i> Add Subscription</a></li>
                <li><a href="/subscriptions"><i class="fa fa-circle-o"></i> View Subscriptions </a></li>

             </ul>
            </li>
           <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Branch Management</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-branch')); ?>"><i class="fa fa-circle-o"></i> Add Branch</a></li>
                <li><a href="<?php echo e(URL::to('branches')); ?>"><i class="fa fa-circle-o"></i> View Branches </a></li>

             </ul>
            </li>

         
         
      
         
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Categories</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-category')); ?>"><i class="fa fa-circle-o"></i> Add Category</a></li>
                 <li><a href="<?php echo e(URL::to('categories')); ?>"><i class="fa fa-circle-o"></i> View Categories </a></li>
              <!-- <li><a href="<?php echo e(URL::to('add-sub-category')); ?>"><i class="fa fa-circle-o"></i> Add Sub Category </a></li>
 -->
             </ul>
            </li>
          <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Services</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-service')); ?>"><i class="fa fa-circle-o"></i> Add Service</a></li>
                 <li><a href="<?php echo e(URL::to('services')); ?>"><i class="fa fa-circle-o"></i> View Services </a></li>
       
             </ul>
            </li>
               <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Service Features</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-servicefeature')); ?>"><i class="fa fa-circle-o"></i> Add Service Feature</a></li>
                 <li><a href="<?php echo e(URL::to('servicefeatures')); ?>"><i class="fa fa-circle-o"></i> View Service Features </a></li>
       
             </ul>
            </li>
     
<!--
        <li class="treeview"><a href="<?php echo e(URL::to('contacts')); ?>"><i class="fa fa-files-o"></i> <span>Contacts</span></a></li>

             -->
             <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Banners</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-banner')); ?>"><i class="fa fa-circle-o"></i> Add Banner</a></li>
                <li><a href="<?php echo e(URL::to('banners')); ?>"><i class="fa fa-circle-o"></i> View Banners </a></li>

             </ul>
            </li>
      

             <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Delivery Time Slot</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-deliverytime')); ?>"><i class="fa fa-circle-o"></i> Add </a></li>
                <li><a href="<?php echo e(URL::to('deliverytime')); ?>"><i class="fa fa-circle-o"></i> View  </a></li>

             </ul>
            </li>

             <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Pickup Time Slot</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-pickuptime')); ?>"><i class="fa fa-circle-o"></i> Add </a></li>
                <li><a href="<?php echo e(URL::to('pickuptime')); ?>"><i class="fa fa-circle-o"></i> View  </a></li>

             </ul>
            </li>
            <li class="treeview">
            <a href="#">
              <i class="fa fa-files-o"></i>
              <span>FAQS</span> <i class="fa fa-angle-left pull-right"></i>
             
            </a>
            <ul class="treeview-menu">
             <li><a href="<?php echo e(URL::to('add-faq-category')); ?>"><i class="fa fa-circle-o"></i> Add Faq Category</a></li>
              <li><a href="<?php echo e(URL::to('faq-categories')); ?>"><i class="fa fa-circle-o"></i> View Faq Categories</a></li>
              <li><a href="<?php echo e(URL::to('add-faq')); ?>"><i class="fa fa-circle-o"></i> Add faq</a></li>
              <li><a href="<?php echo e(URL::to('faqs')); ?>"><i class="fa fa-circle-o"></i> View Faqs</a></li>

           </ul>
        </li>
  <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Testimonials</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo e(URL::to('add-testimonial')); ?>"><i class="fa fa-circle-o"></i> Add Testimonial</a></li>
                <li><a href="<?php echo e(URL::to('testimonials')); ?>"><i class="fa fa-circle-o"></i> View Testimonial </a></li>

             </ul>
            </li>
             <?php }?>

        </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
