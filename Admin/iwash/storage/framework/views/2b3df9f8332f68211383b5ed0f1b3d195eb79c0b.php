 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
           
                <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" action=''>
								<?php echo e(csrf_field()); ?>


                  <div class="box-body">
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Service Type</label>
    <select name='service_cat_id' class="form-control" required placeholder="Service Type">
    <option value="">Select</option>
    <?php

    foreach($categories as $category){ ?>
    <option  value="<?= $category->id;?>" ><?= $category->name;?></option>

    <?php } ?>
    </select>   
                </div>
                <div class="col-lg-6">
                <label >Customer Email </label>

    <input type="text" class="form-control" required name="email" placeholder="">
                </div>
            </div> 
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Address Type</label>
  <select class="form-control" required id="sel1" name="addressType" placeholder="Address type">
    <option value="">Select Address type</option>
    <option value="home" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='home'){ echo 'selected';} ?>>Home</option>
    <option value="office" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='office'){ echo 'selected';} ?>>Office</option>
    <option value="other" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='other'){ echo 'selected';} ?>>Other</option>
  </select>
                </div>
                <div class="col-lg-6">
                <label >Flat and Street</label>
    <input type="text" required class="form-control" name="flatandstreet" placeholder="Flat and Street" value="<?php echo @$_GET['flatandstreet']; ?>">
  
                </div>
            </div> 
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Landmark</label>

    <input type="text" required class="form-control"  name="landmark"  placeholder="Landmark" >
                </div>
                <div class="col-lg-6">
                <label >Pickup Date</label>

    <input type="date" required class="form-control" name="pickupDate"  placeholder="Pickup Date"  >
                </div>
            </div> 
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Pickup  Time</label>
    <select name='timeOfPickup' required class="form-control" >
    <option value="">Select</option>
    <?php

    foreach($pickup as $pick){ ?>
    <option  value="<?= $pick->id;?>" ><?= $pick->timebetween;?></option>

    <?php } ?>
    </select>    
                </div>
                <div class="col-lg-6">
                <label >Delivery Date</label>

    <input type="date"  required class="form-control" name="deliveryDate"  placeholder="Delivery Date"  >
                </div>
            </div> 
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Delivery  Time</label>
    <select name='timeOfDelivery' required class="form-control" >
    <option value="">Select</option>
    <?php

    foreach($delivery as $del){ ?>
    <option  value="<?= $del->id;?>" ><?= $del->timebetween;?></option>

    <?php } ?>
    </select>   
                </div>
                <div class="col-lg-6">
                <label >Total Amount</label>

    <input type="text"   class="form-control" name="totalamount"  placeholder="Total Amount"  >
                </div>
            </div> 
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Quantity </label>

    <input type="number"  required class="form-control" name="qty"  placeholder="Quantity"  >
                </div>
                <div class="col-lg-6">
                <label >Order Status</label>
    <select class="form-control" required id="sel1" name="status" >
    <option value="">Select Order Status</option>
    <option value="placed" >Placed</option>
    <option value="accepted" >Accepted</option>
    <option value="picked up" >Picked Up</option>
    <option value="processing" >Processing</option>
    <option value="ready to deliver" >Ready to delivery</option>
    <option value="delivered" >Delivered</option>
    <option value="cancelled">Cancelled</option>
  </select>
                </div>
            </div> 
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Payment Status</label>
    <select class="form-control"  id="sel1"  name="payment_status"  required>
    <option value="">Select Payment Status</option>
    <option value="0" >Pending</option>
    <option value="1" >Paid</option>
  

  </select>
                </div>
                <div class="col-lg-6">
                <label >Payment Date: </label>

    <input type="date"   class="form-control"  name="payment_date"   placeholder="Payment Date"  >
                </div>
            </div> 





                  </div><!-- /.box-body -->

                  <div class="box-footer">
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   </div>
                  </div>
                </form>
     
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <style>

.col-lg-12 {
    padding-bottom: 1%;

}

</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>