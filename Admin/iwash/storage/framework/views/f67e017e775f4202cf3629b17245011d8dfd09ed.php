 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">FAQS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <?php if(session()->has('ok')): ?>
      <?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>  
    <?php if(isset($info)): ?>
      <?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <?php if(session()->has('error')): ?>
      <?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>  
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>Faq Category</th>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Date</th>

                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
            
            <?php foreach($faqs as $message): ?>



                      <tr>
                   <td><?php echo e(@$message->FaqCategory->name); ?></td>
                          <td><?php echo e(@$message->question); ?></td>
                        <td><?php echo e(@$message->answer); ?></td>
                     
                         <td> 
                    <?php echo e($message->created_at); ?>

                        
                        </td>
                        <td>
                          
                            <a class="btn btn-danger" href="<?php echo e(URL::to('faqs/'.$message->id.'/edit')); ?>">Edit</a>
                                <a class="btn btn-danger" href="<?php echo e(URL::to('faqs/'.$message->id.'/delete')); ?>">Delete</a>
                        
                        </td>
                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
                  <?php echo $faqs->render(); ?>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>