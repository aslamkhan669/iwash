 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Categories</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
	        
					      <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>Image</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						
						<?php foreach($categories as $category): ?>

     <tr>
      <td>
      <img src=" <?php echo e($url. $category->image); ?>"  onClick="swipe(this);"class="img-thumbnail" style="width:150px;height:150px">
             
                        </td>
                        <td><i class="fa fa-cog"></i>  <?php echo e($category->name); ?>

                        
                       
                        </td>
                        <td> 
							    <?php echo e($category->description); ?>

                        <td>
                            <a class="btn btn-default" href="<?php echo e(URL::to('faq-category/'.$category->id.'/edit')); ?>">Edit</a>
                            <a class="btn btn-danger" href="<?php echo e(URL::to('faq-category/'.$category->id.'/delete')); ?>">Delete</a>
                          
                        
                        </td>
                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
                    <?php echo $categories->render(); ?>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
			    $('.getsubcat').click(function(){
			    $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
		});
     
        
        </script>
        
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>