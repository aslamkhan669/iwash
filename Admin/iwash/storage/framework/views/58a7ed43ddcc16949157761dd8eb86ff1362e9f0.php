 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Banners</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				
		
					      <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Banner Image</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>

						
						<?php foreach($banners as $banner): ?>

                      <tr>
                        <td>
							<img src=" <?php echo e($url.$banner->url); ?>"  onClick="swipe(this);"sclass="img-thumbnail" style="width:150px;height:150px">
                    
                        
                        </td>
                   
							<!--    <td>
								    <div class="checkbox icheck">
                <label>
                 								   <input type='checkbox'  name="active" <?php if($banner->status==1){ echo 'checked';} ?> >

                </label>
              </div>
								   
                    
                        
                        </td> -->
                        <td>
                            <a class="btn btn-danger" href="/banners/<?php echo e($banner->id); ?>/delete">Delete</a>
                          
                        
                        </td>
                      </tr>
          <?php endforeach; ?>
                    </tbody>
                   
                  </table>
<div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	<?php echo $banners->render(); ?> </div>         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
     
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>