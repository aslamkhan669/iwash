
      <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0 Iwash
      </div>
      <strong>Copyright &copy; 2017 <a href="">Iwash</a>.</strong> All rights reserved.
    </footer>

 
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

  </div><!-- ./wrapper -->

  <!-- jQuery 2.1.4 -->
  <script src="<?php echo e(URL::asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')); ?>"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="<?php echo e(URL::asset('bootstrap/js/bootstrap.min.js')); ?>"></script>
  <!-- FastClick -->
  <script src="<?php echo e(URL::asset('AdminLTE/plugins/fastclick/fastclick.min.js')); ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo e(URL::asset('AdminLTE/dist/js/app.min.js')); ?>"></script>
  <!-- Sparkline -->
  <script src="<?php echo e(URL::asset('AdminLTE/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
  <!-- jvectormap -->
  <script src="<?php echo e(URL::asset('AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
  <script src="<?php echo e(URL::asset('AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="<?php echo e(URL::asset('AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')); ?>"></script>
  <!-- ChartJS 1.0.1 -->
  <script src="<?php echo e(URL::asset('AdminLTE/plugins/chartjs/Chart.min.js')); ?>"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo e(URL::asset('AdminLTE/dist/js/demo.js')); ?>"></script>
  
  
  

  <script src="<?php echo e(URL::asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
          <script src="<?php echo e(URL::asset('AdminLTE/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
     
       <script>
    $(function () {
      $("#example1").DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": false
      });
   
    });
  </script>
  <script type="text/javascript">
$.ajaxSetup({
// headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});


   function goBack() {
  window.history.back();
}
</script>
</body>
</html>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();   
});
</script>


<script>


function swipe(largeImage) {

 largeImage.style.display = 'block';
 largeImage.style.width=200+"px";
 largeImage.style.height=200+"px";
 var url=largeImage.getAttribute('src');
 window.open(url,'Image','width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
}



function addmoreservice(){
   var str=' <div class="form-group row col-lg-12 servicebox"><div class="col-lg-6"> <label >Services</label><input type="text" class="form-control"  name="service_name[]" placeholder="Servie Name"  > </div> <div class="col-lg-5"><label ></label>  <input type="text" class="form-control"  name="service_price[]" placeholder="Servie Price" ></div>';
str+='<div <div class="col-lg-1"><label ></label><button type="button" class="btn btn-danger closeme " ><i class="fa fa-trash-o fa-lg"></i> Delete</button></div></div>';
       jQuery(".servicebox:last").after(str);
  }

  jQuery("body").on('click','.closeme',function(){
     jQuery(this).parents('.servicebox').remove();
  });

  var subcaturl="<?php echo URL::to('/api/getsubcat'); ?>";

     $('#prod_cat').change(function(){ 
    var parent_id=$(this).val();     
    if(parent_id!=''){     
      $.ajax({
        url:subcaturl+'/'+parent_id,
        type: "get",
        success: function(data){
          var str='<option value="">Select</option>';
         $.each(data, function(i, item) {
             str+='<option value="'+item.id+'" >'+item.name +'</option>';
          })
        $('#prod_sub_cat').html(str);
       
        }
      });     


      } 
    });






      var providerupdate="<?php echo URL::to('/api/update-provider'); ?>";
$('#updateprofile').submit(function(event){ 
  
  var userid=$(this).find('.userid').val();


var token="<?php echo csrf_token();  ?>";
  var fields = $(this).serializeArray();
    event.preventDefault();    
      $.ajax({
        url:providerupdate+'/'+userid,
        type: "post",
        data:fields,
        success: function(data){
     
        if(data.success){
         window.location.reload();

        }else{

                     alert(data.message);

        }
        }
      });     

    });  


   var verify="<?php echo URL::to('/api/verify-user'); ?>";
$('#setstatus').click(function(event){ 
  var status=$(this).attr('status');
var verificationids=$('.verification:checked').map(function(){return $(this).val();}).get();
console.log(verificationids);
var token="<?php echo csrf_token();  ?>";
    event.preventDefault();    
      $.ajax({
        url:verify+'?_token='+token,
        type: "post",
        data:{verification:verificationids,status:status},
        success: function(data){
     
        if(data.success){
      window.location.reload();

        }else{

          alert('Error occured');
        }
        }
      });     

    });  
</script>


  <script type="text/javascript">

$("#selectlocation").change(function(){
$("#branchchoose").submit();
});


$(".packagecategory .category").change(function(){

  if($(this).is(":checked")){
    var service=$(this).parents('.packagecategory').next('.services');
          var catid=$(this).val();

          var servicesbycaturl="<?php echo URL::to('/api/services/'); ?>/";
          var api_url="<?php echo Config::get('app.api_url'); ?>";
          console.log(api_url);
            $.ajax({
                    url:servicesbycaturl+catid,
                    type: "get",
                    success: function(data){
                    console.log(data);
                    if(data.success==true){
                      var str='<b>Choose Service </b> <br>';
                      $.each(data.data, function(i, item) {
                        str+='<div class="checkbox"> <label for="'+item.id+'"><input id="'+item.id+'"  type="checkbox" name="service_id'+catid+'[]" class="servicechek" value="'+item.id+'">'+item.name+'</label> <input type="number"  class="quantity hidden" name="quantity_'+catid+'_'+item.id+'"></div>';
                      })
                     
                      service.html(str);
                      $(".servicechek").change(function(){
                          var qty=$(this).parent().next('.quantity');

                          if($(this).is(":checked")){
                                qty.removeClass('hidden');

                          }else{
                            qty.addClass('hidden');


                          }

                          });

                    }

                    
                  
                    }
                  });
  }else{
    $(this).parents('.packagecategory').next('.services').empty();


  }


 

});





function getServices(catid){
console.log($('#'+catid).find('.row').is(':empty'));
if($('#'+catid).find('.row').is(':empty')){

var servicesbycaturl="<?php echo URL::to('/api/services/'); ?>/";
var api_url="<?php echo Config::get('app.api_url'); ?>";
console.log(api_url);
$.ajax({
        url:servicesbycaturl+catid,
        type: "get",
        success: function(data){
         console.log(data);
         if(data.success==true){
           var str='';
          $.each(data.data, function(i, item) {
             str+=" <div class='col-md-12'> <div class='dash'> <div class='col-md-2'> <div class='dashboard'> <a href=''> <img src='"+api_url+ item.image+"'> </a> </div> </div> <div class='col-md-8'> <div> <h4>"+item.name+" (Per "+item.per+" )</h4> <p> QRS "+item.price+"</p> <span class='iw-rate' method='POST' action='#'> <input type='button' value='-' class='iw-qtyminus' field='quantity'  onclick='decrement($(this),"+item.category_id+","+item.id+","+item.price+")'  /> <input type='text' readonly name='quantity'value='0' class='iw-qty' /> <input type='button' value='+' class='iw-qtyplus' field='quantity' onclick='increment($(this),"+item.category_id+","+item.id+","+item.price+")' /></span> </div> </div> </div> <hr> </div>";
          })
        $('#'+catid).find('.row').html(str);

         }

         
       
        }
      });




}
}
getServices(1);

var items_added = [], quantity_added=0, total=0, tax=0,category_selected ='';

function initialize_data(){
      $(".header_bar li").each(function(){
          var category = $(this).attr("category");
          items_added[category] = new Array();
      });
  }

  initialize_data();
function setCartData(){

      $("#quantity").text(quantity_added);
      $("#total").text(total);
      var tax = (total * 14.5)/100;
      $("#tax").text(tax);
  }
  function vibrateCart(){
      var i=0;
      var myInterval = setInterval(function(){
          if(i++>=10)
              clearInterval(myInterval);
          if(i%2==0){
              $(".estimator img").css({
                  "-webkit-transform": "rotate(15deg)",
                  "-moz-transform": "rotate(15deg)",
                  "transform": "rotate(15deg)",
                  "-o-transform": "rotate(15deg)",
                  "-ms-transform": "rotate(15deg)"
              });        
          }
          else if(i%3==0){
              $(".estimator img").css({
                  "-webkit-transform": "rotate(-15deg)",
                  "-moz-transform": "rotate(-15deg)",
                  "transform": "rotate(-15deg)" ,
                  "-o-transform": "rotate(-15deg)",
                  "-ms-transform": "rotate(-15deg)"
              });   
          }
          else{
              $(".estimator img").css({
                  "-webkit-transform": "rotate(0deg)",
                  "-moz-transform": "rotate(0deg)",
                  "transform": "rotate(0deg)",
                  "-o-transform": "rotate(0deg)",
                  "-ms-transform": "rotate(0deg)"
              });   
          }
      }, 100);
   }  
   
function increment(curr,categoryid,itemid,price){
console.log(curr.prev('.iw-qty').val());
        var item_label =itemid;

        if(items_added[categoryid][item_label]){
        items_added[categoryid][item_label]++;
        curr.prev('.iw-qty').val(items_added[categoryid][item_label]);

        }else{
        items_added[categoryid][item_label]=1;
        curr.prev('.iw-qty').val(items_added[categoryid][item_label]);
        

        }    
        if(quantity_added==0){
           $(".estimator").fadeIn(1500);

        }
           
        quantity_added++;
        total +=  Number(price);
        setCartData();
        vibrateCart();

     }


function decrement(curr,categoryid,itemid,price){

var item_label =itemid;

              if(items_added[categoryid][item_label]>0){
                  items_added[categoryid][item_label]--;
                  quantity_added--;
                  total -= Number(price);
                  setCartData();
                  if(quantity_added==0)
                      $(".estimator").fadeOut(1000);
              }
              curr.next('.iw-qty').val(items_added[categoryid][item_label]);

              vibrateCart();

}






</script>
 <style>
 #chartdiv {
   width		: 100%;
   height		: 500px;
   font-size	: 11px;
 }					
 </style>
 
 <!-- Resources -->
 <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
 <script src="https://www.amcharts.com/lib/3/serial.js"></script>
 <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
 <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
 <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<?php 
if(Request::is('order/graphs')){

?>
<script>

var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "light",
                "marginRight": 70,
                "dataProvider":<?php echo json_encode($orders);; ?>,
                "valueAxes": [{
                  "axisAlpha": 0,
                  "position": "left",
                  "title": "Total Orders"
                }],
                "startDuration": 1,
                "graphs": [{
                  "balloonText": "<b>[[category]]: [[value]]</b>",
                  "fillColorsField": "color",
                  "fillAlphas": 0.9,
                  "lineAlpha": 0.2,
                  "type": "column",
                  "valueField": "totalrecord"
                }],
                "chartCursor": {
                  "categoryBalloonEnabled": false,
                  "cursorAlpha": 0,
                  "zoomable": false
                },
                "categoryField": "category",
                "categoryAxis": {
                  "gridPosition": "start",
                  "labelRotation": 45
                },
                "export": {
                  "enabled": true
                }

              });
</script>
<?php 

            }
?>
<style>
.pac-container{

    z-index: 1077!important;

}

</style>