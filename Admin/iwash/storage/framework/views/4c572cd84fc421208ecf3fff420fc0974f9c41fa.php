 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-bsubscription">
                <button onclick="goBack()">Go Back</button>

    <script>
function goBack() {
    window.history.back();
}
</script>
           
                <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" action=''>
								<?php echo e(csrf_field()); ?>


                  <div class="box-body">

        <div class="col-lg-12">

        <div class="col-lg-6">
        <label >Package:</label>
        <span> <?php echo e(@$subscription->Package->package); ?>     </span>  
        </div>

        <div class="col-lg-6">
        <label >Customer</label>

        <span><?php echo e(@$subscription->userProfile->first_name); ?> <?php echo e(@$subscription->userProfile->last_name); ?>    </span>  

        </div>

        </div>


        <div class="col-lg-12">
                
                <div class="col-lg-6">
                <label >Subscription Date: </label>
                    <span><?php echo e($subscription->createdAt); ?></span>
                </div>
                <div class="col-lg-6">
                    <label >Pickup Option</label>
                    <span><?php echo e($subscription->pickup_option); ?></span>

                </div>
             
        </div> 
        <div class="col-lg-12">
        <div class="col-lg-6">
        <label >Services: </label>

        <?php foreach( $subscription->SubscribeServices as $services){?>
                <div>
                <input type="hidden" name="services[]" value="<?php echo $services->service_id;?>"> 
                <input type="number" name="quantity_<?php echo $services->service_id;?>"  class="quantity" value="<?php echo $services->quantity; ?>" required>
                
                <?php 
                  echo $services->Service->name.'('.$services->category->name.')';

?>
      </div>  <?php } ?>

        </div> 
    
        </div> 


        <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Subscription Status: </label>
    <select class="form-control"  id="sel1" name="status"  required>
    <option value="">Select Subscription Status</option>
    <option value="0" <?php if($subscription->status==0){ echo 'selected';} ?>>Inactive</option>
    <option value="1" <?php if($subscription->status==1){ echo 'selected';} ?>>Active</option>
  </select>
                </div>
                <div class="col-lg-6">
                <label>Payment Amount: </label>

                <input type="text"   class="form-control" name="payment_amount"  value="<?php echo e($subscription->payment_amount); ?>" required   placeholder="Payment Amount"  >

                </div>
        </div> 
        <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Payment Date: </label>
               
    <input type="date"   class="form-control" value="<?php echo  date('Y-m-d', strtotime($subscription->payment_date));?>"  name="payment_date"  placeholder="Payment Date"  >
                </div>
                <div class="col-lg-6">
                <label >Payment Status: </label>
    <select class="form-control"  id="sel1" name="payment_status" >
    <option value="">Select Payment Status</option>
    <option value="0" <?php if($subscription->payment_status==0){echo 'selected';} ?>>Pending</option>
    <option value="1" <?php if($subscription->payment_status==1){echo 'selected';} ?>>Paid</option>
  

  </select>
                </div>
        </div> 
    


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   </div>
                  </div>
                </form>
     
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <style>

.col-lg-12 {
    padding-bottom: 1%;

}
.quantity {


      width:45px;
    }
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>