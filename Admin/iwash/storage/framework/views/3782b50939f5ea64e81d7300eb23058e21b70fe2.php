 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
      
                <?php if(session()->has('ok')): ?>
      <?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>  
    <?php if(isset($info)): ?>
      <?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <?php if(session()->has('error')): ?>
      <?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>  
    
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" >
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1"> Feature  Name</label>
                      <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Enter Service name">
                    </div>
                        <div class="form-group">
                      <label for="exampleInputEmail1">Service Type </label>
                     <select class="form-control" name="service_cat_id">
                        <?php
              
               foreach($categories as $category){ ?>
                 <option value="<?= $category->id;?>" ><?= $category->name;?></option>
                  
               <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1"> Feature Description</label>
                    <textarea class="form-control"  name='description' required></textarea>
                    </div>

                     <div class="form-group">
                      <label >Feature Image</label>
                      <input type="file" class="form-control"  name='image' id="exampleInputEmail1" required >
                    </div>
                   
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>