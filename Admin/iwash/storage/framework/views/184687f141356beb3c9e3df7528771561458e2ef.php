 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          
            <button onclick="goBack()">Go Back</button>
   
            <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i>
           <span class="text-center"> <?php echo e(@$order->servicecat->name); ?></span>&nbsp;&nbsp;&nbsp;
           <span>Branch: <?php echo e(@$order->orderbranch->branch); ?>-<?php echo e(@$order->orderbranch->location); ?></span>
           &nbsp;&nbsp;&nbsp;
           <span>Order Source: 
<?php 
 if($order->added_by!=NULL && $order->branch!=NULL){

                                     if($order->addedby->role->slug=='admin'){
                                       echo 'Added by Super Admin';

                                     }else{
                                       echo $order->addedby->email.'(' .$order->addedby->userlocation->branch.'-'.$order->addedby->userlocation->location.')<br>';
                                      echo 'Branch Admin';
                                     }
                               }else{

                                  echo 'Website/App';

                               }
?>
           </span>
          <small class="pull-right"><strong>ORDER Date: <?php echo e(@$order->createdAt); ?></strong></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
      <h3>ORDER ID : IW<?php echo e(@$order->id); ?></h3>
      <div>

      <img src=" <?php echo e($url. $order->qr); ?>"  onClick="swipe(this);"class="img-thumbnail" style="width:150px;height:150px"></td>

      </div>

       
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
  <h4> Customer Details</h4>
        <address>
          <strong><?php echo e(@$order->userProfile->first_name); ?> <?php echo e(@$order->userProfile->last_name); ?> </strong><br>
          <b>Address Type: </b> <?php echo e($order->addressType); ?><br>
          <b>flatandstreet: </b> <?php echo e($order->flatandstreet); ?><br>
          <b>landmark: </b> <?php echo e($order->landmark); ?><br>
          <b> Email: </b> <?php echo e(@$order->user->email); ?> <br>
          <b>Mobile: </b>  <?php echo e(@$order->user->mobile); ?> <br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
      <h4> Order Details</h4>
        <b>Pickup Date:</b>  <?php echo e(@$order->pickupDate); ?><br>
        <b>Pickup Time:</b>  <?php echo e(@$order->pickup->timebetween); ?><br>
        <b>Delivery Date:</b> <?php echo e(@$order->deliveryDate); ?><br>
        <b>Delivery Time:</b> <?php echo e(@$order->delivery->timebetween); ?><br>

        <?php 
         $status="";
					   if($order->status=='delivered'){
					   $status='label-success"';
						   
						 }else  if($order->status=='cancelled'){
					   $status='label-danger"';
						   
						 }else  if($order->status=='placed'){
					   $status='label-warning"';
						   
						 }else  if($order->status=='accepted'){
					   $status='label-warning"';
						   
						 }else  if($order->status=='picked up'){
					   $status='label-warning"';
						   
						 }else  if($order->status=='processing'){
					   $status='label-info"';
						   
						 }
                  else  if($order->status=='ready to deliver'){
					   $status='label-info"';
						   
						 }
						 else  if($order->status=='delivered'){
					   $status='label-success"';
						   
						 }
					   
					   ?>
                   <b>Order Status:</b>                     <span class="label <?php echo $status;?>"><?php echo e(@ucfirst($order->status)); ?></span> 
											 
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
      <!-- 
      <table class="table table-striped">
          <thead>
          <tr>
            <th>Qty</th>
            <th>Product</th>
         
            <th>Subtotal</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>1</td>
            <td>Call of Duty</td>
           
            <td>$64.50</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Call of Duty</td>
           
            <td>$64.50</td>
          </tr>
         
          <tr>
            <td>1</td>
            <td>Call of Duty</td>
           
            <td>$64.50</td>
          </tr>
         
          <tr>
            <td>1</td>
            <td>Call of Duty</td>
           
            <td>$64.50</td>
          </tr>
         
          </tbody>
        </table>

            -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
    
      <!-- /.col -->
      <div class="col-xs-6">
        <p class="lead">Payment Date: <?php echo e($order->payment_date); ?></p>
        <p class="lead"><span class="label label-<?php if($order->payment_status==1){echo 'success';}else{ echo 'danger';} ?>"><?php if($order->payment_status==1){echo 'Payment Completed';}else{ echo 'Payment Pending';} ?></span></p>
        <div class="table-responsive">
          <table class="table">
            <tbody>
            <tr>
              <th>Quantity: <?php echo e($order->qty); ?></th>
              <td></td>
            </tr>
            <tr>
              <th>Total:<?php echo e($order->totalamount); ?></th>
              <td></td>
            </tr>
          </tbody></table>
        </div>
      </div>
      <div class="col-xs-6">
     
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
          

            </div><!-- /.col -->
          </div><!-- /.row -->


        </section><!-- /.content -->
    <script>
function goBack() {
    window.history.back();
}
</script>
        
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>