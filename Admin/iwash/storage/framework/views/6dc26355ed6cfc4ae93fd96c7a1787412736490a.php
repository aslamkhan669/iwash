 <?php $__env->startSection('container'); ?>
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" method='post' action=''>
								<?php echo e(csrf_field()); ?>


                  <div class="box-body">
                    <div class="form-group">
                      <label >Category Name</label>
                      <input type="text" class="form-control"  value="<?php echo e(@$category->name); ?>" name='name' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Category Description</label>
                    <textarea class="form-control"  name='description' required><?php echo e(@$category->description); ?></textarea>
                    </div>


                <div class="form-group">
                      <label >Category Image</label>
                      <input type="file" class="form-control"  name='cat_image' id="exampleInputEmail1" required >
                       <img src=" <?php echo e($url. $category->image); ?>"  onClick="swipe(this);"class="img-thumbnail" style="width:150px;height:150px">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
                
                <?php if(session()->has('ok')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'success', 'message' => session('ok')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		<?php if(isset($info)): ?>
			<?php echo $__env->make('partials/error', ['type' => 'info', 'message' => $info], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		<?php if(session()->has('error')): ?>
			<?php echo $__env->make('partials/error', ['type' => 'danger', 'message' => session('error')], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>	
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>