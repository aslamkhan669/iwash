<?php

namespace App\Listeners;
use Config;
use App\Events\ContactPageContact;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
class ContactEventHandler implements ShouldQueue
{
    
       public $mailer;
       public $contact_mail;
       public $email;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer){
   $this->mailer = $mailer;
   $this->contact_mail = Config::get('hunggerstrike.contact_email');
}
    /**
     * Handle the event.
     *
     * @param  ContactPageContact  $event
     * @return void
     */
    public function handle(ContactPageContact $event)
    {
      
        
       
    }
    
    
    
    public function contact_us(ContactPageContact $event){
  
    $this->email=    $event->contactRequest['email'];
       
       $this->mailer->send('emails.contact_us', [], function($message) {
           
        $message->to($this->email,'Hunggerstrike')
                    ->subject('Contact us');
        });
        
       
    }
}
