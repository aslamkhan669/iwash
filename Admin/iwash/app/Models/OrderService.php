<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderService extends Model  {
	protected $table = 'order_service';

    const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';
	 public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

   public function service()
    {
        return $this->belongsTo('App\Models\Services','service_id','id');
    }
	
	
}
