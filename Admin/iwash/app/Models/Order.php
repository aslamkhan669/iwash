<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model  {
	protected $table = 'order';

	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	 public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
   public function orderbranch() 
	{
		return $this->belongsTo('App\Models\Branches','branch','id');
	}
	
	 public function orderservices() 
	{
		return $this->hasMany('App\Models\OrderService','order_id','id');
	}
	
	public function addedby() 
	{
		return $this->belongsTo('App\Models\User','added_by','id');
	}
   public function userProfile()
    {
        return $this->hasOne('App\Models\UserProfile','user_id','user_id');
    }

    	public function servicecat() 
	{
		return $this->belongsTo('App\Models\Category','service_cat_id');
	}
	public function subscription() 
	{
		return $this->belongsTo('App\Models\SubscribePackage','subscription_id');
	}
		public function delivery() 
	{
		return $this->belongsTo('App\Models\Deliverytime','timeOfDelivery');
	}
		public function pickup() 
	{
		return $this->belongsTo('App\Models\Pickuptime','timeOfPickup');
	}


}
