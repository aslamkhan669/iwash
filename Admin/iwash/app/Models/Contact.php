<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;

class Contact extends Model  {

	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contacts';

		public function user() 
	{
		return $this->belongsTo('App\Models\User','user_id');
	}

	 public function userProfile()
    {
        return $this->belongsTo('App\Models\UserProfile','user_id');
    }

}