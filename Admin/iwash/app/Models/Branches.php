<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;

class Branches extends Model  {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'branches';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

}