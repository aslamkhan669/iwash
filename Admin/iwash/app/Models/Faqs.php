<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;

class Faqs extends Model  {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'faqs';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

		public function FaqCategory() 
	{
		return $this->belongsTo('App\Models\FaqCategory','faq_category');
	}

}