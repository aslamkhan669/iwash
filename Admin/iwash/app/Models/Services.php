<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model  {
	protected $table = 'services';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

		public function Category() 
	{
		return $this->belongsTo('App\Models\Category','category_id');
	}

}
