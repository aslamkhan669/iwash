<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;

class Monthlypackages extends Model  {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'monthly_packages';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	public function PackageServices() 
	{
	  return $this->hasMany('App\Models\PackageServices','package_id','id');
	}
}