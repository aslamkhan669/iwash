<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;

class SubscribeServices extends Model  {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'subscribe_services';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	public function subscribe() 
	{
	  return $this->belongsTo('App\Models\SubscribePackage','subscribe_id','id');
	}
	public function category() 
	{
		return $this->belongsTo('App\Models\Category','category_id');
	}
	public function Package()
    {
        return $this->belongsTo('App\Models\Monthlypackages','package_id','id');
	}
	public function Service()
    {
        return $this->belongsTo('App\Models\Services','service_id','id');
	}
	
	
}