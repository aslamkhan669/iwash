<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicefeatures extends Model  {
	protected $table = 'service_features';

	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

		public function servicecat() 
	{
		return $this->belongsTo('App\Models\Category','service_cat_id','id');
	}




}
