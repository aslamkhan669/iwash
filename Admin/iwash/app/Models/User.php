<?php namespace App\Models;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract{

    use Authenticatable, Authorizable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';
	  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  //  protected $fillable = ['name', 'email', 'password'];
    protected $fillable = ['email','mobile','location','added_by', 'password','active','role_id','confirmed','mobile_confirmed'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */

	public function role() 
	{
		return $this->belongsTo('App\Models\Role','role_id');
	}
	public function userlocation() 
	{
		return $this->belongsTo('App\Models\Branches','location','id');
	}
	public function addedby() 
	{
		return $this->belongsTo('App\Models\User','added_by','id');
	}
	public function category() 
	{
		return $this->belongsTo('App\Models\Category');
	}
     public function userProfile()
    {
        return $this->hasOne('App\Models\UserProfile');
    }
	
	public function socialAccounts() 
	{
	  return $this->hasMany('App\Models\SocialAccounts');
	}
	
	public function photo() 
	{
	  return $this->hasMany('App\Models\Photo');
	}

		public function contact() 
	{
	  return $this->hasMany('App\Models\Contact');
	}
	public function feedback() 
	{
	  return $this->hasMany('App\Models\Feedback');
	}
	public function banner() 
	{
	  return $this->hasMany('App\Models\Banner');
	}
public function message() 
	{
	  return $this->hasMany('App\Models\Message');
	}

    
    
	public function bookmark() 
	{
	  return $this->hasMany('App\Models\Bookmark');
	}
	public function favourite() 
	{
	  return $this->hasMany('App\Models\Favourite');
	}
	
	public function suggest() 
	{
	  return $this->hasMany('App\Models\Suggest','from_id');
	}


	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function posts() 
	{
	  return $this->hasMany('App\Models\Post');
	}

	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function comments() 
	{
	  return $this->hasMany('App\Models\Comment');
	}

	/**
	 * Check admin role
	 *
	 * @return bool
	 */
	public function isAdmin()
	{
		return $this->role->slug == 'admin';
	}

	/**
	 * Check not user role
	 *
	 * @return bool
	 */
	public function isUser()
	{
		return $this->role->slug == 'user';
	}
    public function isEmployee()
	{
		return $this->role->slug == 'employee';
	}


    public function isBranchAdmin()
	{
		return $this->role->slug == 'branch_admin';
	}

 public function isAdminOrBranchAdmin()
	{
		return $this->role->slug == 'admin' || $this->role->slug == 'branch_admin';
	}

	/**
	 * Check media all access
	 *
	 * @return bool
	 */
	public function accessMediasAll()
	{
	    return $this->isAdmin();
	}

	/**
	 * Check media access one folder
	 *
	 * @return bool
	 */
	public function accessMediasFolder()
	{
	    return $this->isNotUser();
	}

}
