<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;

class SubscribePackage extends Model  {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'subscribe_package';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	public function SubscribeServices() 
	{
	  return $this->hasMany('App\Models\SubscribeServices','subscribe_id','id');
	}

	public function user()
    {
        return $this->belongsTo('App\Models\User');
	}
	
	public function Package()
    {
        return $this->belongsTo('App\Models\Monthlypackages','package_id','id');
	}
	
	

   public function userProfile()
    {
        return $this->hasOne('App\Models\UserProfile','user_id','user_id');
    }
}