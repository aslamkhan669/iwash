<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'faq_categories';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';
	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	
    
}
