<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model  {


    protected $table = 'subscription';
    protected $fillable=['id','user_id','package_id','amount_paid','quantity_used','duration','active','status','payment_status','isExpired','isLimitUsed','start_date','end_date','created_at','updated_at'];	
	
}