<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SocialAccounts extends Model
{
   	protected $table = 'social_accounts';
   	public $timestamps = false;

	 protected $fillable = array('username', 'provider_id', 'avatar', 'souce_account_source_id	', 'user_id');
   public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
	
	 public function socialAccountSources()
    {       
	   return $this->belongsTo('App\Models\SocialAccountSources');
    }
	
	
	
}
