<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialAccountSources extends Model
{
   	protected $table = 'social_account_sources';
   	public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
	public function socialAccounts() 
	{
	  return $this->hasMany('App\Models\SocialAccounts');
	}
	
}
