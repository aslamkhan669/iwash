<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Suggest extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public $timestamps = false;

protected $table='suggest_service_providers';
	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function users() 
	{
	  return $this->belongsTo('App\Models\User','from_id');
	}
public function category() 
	{
	  return $this->belongsTo('App\Models\Category','category_id');
	}
}
