<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';
	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function users() 
	{
	  return $this->hasMany('App\Models\User');
	}

	public function services()
	{
			return $this->hasMany('App\Models\Services','category_id','id');   
	}
    public function subCategory()
    {
        return $this->hasMany('App\Models\Category','parent_id');   
    }
    public function suggest() 
	{
	  return $this->hasMany('App\Models\Suggest');
	}
	 public function requestquote() 
	{
	  return $this->hasMany('App\Models\RequestQuote');
	}
}
