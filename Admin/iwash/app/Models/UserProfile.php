<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model  {
	protected $table = 'user_profiles';
	protected $fillable = ['first_name', 'last_name','mobile','user_id','category_id','enlisted','skills','experience','about_me','dob','gender','language','service_at','visiting_charges','visiting_charges_adjusted_infinal','address','pincode','avatar','location','full_location','lng','lat','document','mobile_confirmed'];
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	 public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

	public function contact(){
			  return $this->hasMany('App\Models\Contact');
		 }
   public function category() 
	{
		return $this->belongsTo('App\Models\Category','category_id');
	}
	public function enlistedcategory() 
	{
		return $this->hasMany('App\Models\EnlistSubCat','user_id','user_id');
	}
}
