<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;

class PackageServices extends Model  {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'package_services';
	protected $fillable = ['category_id', 'service_id', 'package_id', 'quantity'];
	
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	public function Category() 
	{
		return $this->belongsTo('App\Models\Category','category_id');
    }
    public function Service() 
	{
		return $this->belongsTo('App\Models\Services','service_id');
    }
    public function Package() 
	{
		return $this->belongsTo('App\Models\Monthlypackages','package_id');
	}
}