<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'auth.login' => ['App\Services\Status@setLoginstatus'],
		'auth.logout' => ['App\Services\Status@setVisitorStatus'],
	   	'user.access' => ['App\Services\Status@setStatus'],
       'App\Events\ContactPageContact' => ['App\Listeners\ContactEventHandler@contact_us']
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param \Illuminate\Contracts\Events\Dispatcher $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);
	}

}
