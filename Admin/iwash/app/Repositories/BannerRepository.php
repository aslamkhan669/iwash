<?php

namespace App\Repositories;

use App\Models\Banner;

class BannerRepository extends BaseRepository
{

	
	protected $model;


	public function __construct(
		Banner $banner
		
        )
	{
		$this->model = $banner;
	
	}
	public function getBanners()	{
 
        
   return  $this->model->paginate(20);
        
   
	}
   
   public function deleteBanner($id){
		$this->model->destroy($id);
	}
	
	
	public function saveBanner($url)
	{
		$user = new $this->model;

		$user->url =$url;
		
        $user->save();
	}
	
	
	
	public function editBanner($id){
		return $this->model->find($id);
	}
	
	
	
	public function updateBanner($inputs, $banner){
           /*
        $admin->password = $inputs['name'];
		$admin->active = $inputs['active'];
		$admin->save();
		*/
	}
	
	
	

    
}
