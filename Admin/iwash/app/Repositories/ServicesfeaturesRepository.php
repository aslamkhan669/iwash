<?php namespace App\Repositories;

use App\Models\Servicefeatures;

class ServicesfeaturesRepository  extends BaseRepository {

	/**
	 * Create a new ContactRepository instance.
	 *
	 * @param  App\Models\Contact $contact
	 * @return void
	 */
	public function __construct(Servicefeatures $servicefeature)
	{
		$this->model = $servicefeature;
	}

	public function getServicefeatures()
	{
		return $this->model->paginate(20);
	}

	
	public function deleteServicefeature($id){
		$this->model->destroy($id);
	}
	
	public function editServicefeature($id){
		return $this->model->find($id);
	}
	
	public function updateServicefeature($inputs, $testobj,$image){

        $testobj->title =$inputs['title'];
         $testobj->service_cat_id =$inputs['service_cat_id'];
		$testobj->description =$inputs['description'];
		
       $testobj->image =$image;
        $testobj->save();
	}

	public function store($inputs,$image)	{
		
        $testobj = new $this->model;
		  $testobj->title =$inputs['title'];
         $testobj->service_cat_id =$inputs['service_cat_id'];
		$testobj->description =$inputs['description'];
	
       $testobj->image =$image;
        $testobj->save();

	   
	}
	
}