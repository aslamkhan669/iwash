<?php namespace App\Repositories;

use App\Models\Faqs;

class FaqsRepository extends BaseRepository {

	/**
	 * Create a new ContactRepository instance.
	 *
	 * @param  App\Models\Contact $contact
	 * @return void
	 */
	public function __construct(Faqs $faqs)
	{
		$this->model = $faqs;
	}

	public function getFaqs()
	{
		return $this->model->paginate(20);
	}

	
	public function deleteFaqs($id){
		$this->model->destroy($id);
	}
	
	public function editFaqs($id){
		return $this->model->find($id);
	}
	
	public function updateFaqs($inputs, $faq){

        $faq->question = $inputs['question'];
		$faq->faq_category =$inputs['faq_category'];
		$faq->answer = $inputs['answer'];
		$faq->save();
	}

		public function store($inputs)	{


				$faq = new $this->model;
				$faq->faq_category =$inputs['faq_category'];
					$faq->question =$inputs['question'];
				$faq->answer =$inputs['answer'];
				$faq->save();

	   
	}
	
}