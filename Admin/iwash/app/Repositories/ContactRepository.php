<?php namespace App\Repositories;

use App\Models\Contact;

class ContactRepository extends BaseRepository {

	/**
	 * Create a new ContactRepository instance.
	 *
	 * @param  App\Models\Contact $contact
	 * @return void
	 */
	public function __construct(Contact $contact)
	{
		$this->model = $contact;
	}

	public function getContacts()
	{
		return $this->model->paginate(20);
	}

	
	public function deleteContacts($id){
		$this->model->destroy($id);
	}
	
	public function editContacts($id){
		return $this->model->find($id);
	}
	
	public function updateContacts($inputs, $contact){

        $contact->password = $inputs['name'];
		$contact->active = $inputs['active'];
		$contact->save();
	}
	
}