<?php namespace App\Repositories;

use App\Models\Category;
use GuzzleHttp\Client;
use Config;

class CategoryRepository {

	 
	protected $model;
    protected $client;
    protected $api_url;
	
	public function __construct(Category $category,Client $client)
	{
		$this->model = $category;
		$this->client = $client;
		$this->api_url =Config::get('barrioConfig.api_url');
	}

	public function getmainCat(){

		return $this->model->where('parent_id', '=', 0)->paginate(10);
		
		
	}


	public function getsubcatbyparent($id){
		return $this->model->where('parent_id', '=', $id)->get();
		
	
	}


	public function catbytype($parent=0)
	{
		return $this->model->where('parent_id', '=',$parent)->get();
		
		
	}
   public function getbyId($id){
		return $this->model->findOrFail($id);
		
			}
			public function store($inputs,$image,$image_banner,$image_website,$image_app)	{
				
		if(isset($inputs['parent'])){
			$parent_id=$inputs['parent'];
		}else{
				$parent_id=0;
		}
	

        $category = new $this->model;
		$category->name =$inputs['name'];
		$category->description =$inputs['description'];
		$category->parent_id =$parent_id;
$category->image =$image;
$category->image_banner =$image_banner;
$category->image_website =$image_website;
$category->image_app =$image_app;
        $category->save();

		
	   
	}
	public function delete($id){
	     $this->model->destroy($id);
		
	}
	
	public function edit($id){
	      return $this->model->find($id);
		
		
		 
	}
	public function parentCats(){
		return $this->model->where('parent_id', '=',0)->get();
	}
	public function update($inputs, $category,$image,$image_banner,$image_website,$image_app){

        $category->name = $inputs['name'];
        $category->description = $inputs['description'];
        if(isset($inputs['parent'])){
			$parent_id=$inputs['parent'];
		}else{
				$parent_id=0;
		}

		if($image!=''){

			$category->image ='uploads/categories/'.$image;
		}	
		if($image_banner!=''){
			
			$category->image_banner ='uploads/categories/'.$image_banner;
		}	
		if($image_website!=''){

			$category->image_website ='uploads/categories/'.$image_website;
		}	
		if($image_app!=''){

			$category->image_app ='uploads/categories/'.$image_app;
		}	

		$category->parent_id =$parent_id;
		$category->save();
	}
	public function totalCategory()
	{
		return $this->model->count();
		
		
	}
}
