<?php namespace App\Repositories;

use App\Models\Testimonials;

class TestimonialsRepository  extends BaseRepository {

	/**
	 * Create a new ContactRepository instance.
	 *
	 * @param  App\Models\Contact $contact
	 * @return void
	 */
	public function __construct(Testimonials $testimonials)
	{
		$this->model = $testimonials;
	}

	public function getTestimonials()
	{
		return $this->model->paginate(20);
	}

	
	public function deleteTestimonial($id){
		$this->model->destroy($id);
	}
	
	public function editTestimonial($id){
		return $this->model->find($id);
	}
	
	public function updateTestimonial($inputs, $testobj,$image){

        $testobj->name =$inputs['name'];
		$testobj->designation =$inputs['designation'];
		$testobj->message =$inputs['message'];
       $testobj->profile =$image;
        $testobj->save();
	}

	public function store($inputs,$image)	{
		
        $testobj = new $this->model;
		$testobj->name =$inputs['name'];
		$testobj->designation =$inputs['designation'];
		$testobj->message =$inputs['message'];
       $testobj->profile =$image;
        $testobj->save();

	   
	}
	
}