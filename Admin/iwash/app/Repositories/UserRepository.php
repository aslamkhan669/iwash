<?php

namespace App\Repositories;

use App\Models\User, App\Models\Role, App\Models\UserProfile;
use App\Models\SocialAccounts, App\Models\SocialAccountSources;
use Illuminate\Http\Request;

use Auth;
use Response;
use Redirect;
use Hash;
class UserRepository extends BaseRepository
{


	protected $model;
	protected $role;
	protected $userProfile;
	protected $socialaccounts;
	protected $socialaccountsources;
    protected $userprofilepath='/var/www/api/uploads/profiles/';
        protected $documentspath='/var/www/api/uploads/documents/';
        protected $gallerypath='/var/www/api/uploads/gallery/';

	public function __construct(
		User $user,
		Role $role,
		UserProfile $userProfile,
		SocialAccounts $socialaccounts,
		SocialAccountSources $socialaccountsources

         )
	{
		$this->model = $user;

		$this->role = $role;
		$this->userProfile = $userProfile;
		$this->socialaccounts = $socialaccounts;
		$this->socialaccountsources = $socialaccountsources;
		$this->secret='725c179052e4f0f048f59622d49f9a81c8c8900d5c95a25494027bd9c3ae56d8';
	}


	public function getCustomers($email)	{


			 if(Auth::user()->isAdmin()){
					return  $this->model->where('email', 'LIKE', '%'.$email.'%')->whereHas(
									'role', function($q){
										$q->where('slug', 'user');
									}
							)->get(['email as value' ,'email as label','id'])->toJson();
			 }else{

				return  $this->model->where('email', 'LIKE', '%'.$email.'%')->whereHas(
							'role', function($q){
								$q->where('slug', 'user');
							}
					)->where('location','=',Auth::user()->location)->get(['email as value' ,'email as label','id'])->toJson();

			 }

		}
	public function getEmployees()	{


		 if(Auth::user()->isAdmin()){
				return  $this->model-> whereHas(
								'role', function($q){
									$q->where('slug', 'employee');
								}
						)->paginate(20);
		 }else{

		return  $this->model-> whereHas(
						'role', function($q){
							$q->where('slug', 'employee');
						}
				)->where('location','=',Auth::user()->location)->paginate(20);

		 }

	}
	public function getBranchAdmins()	{
		return  $this->model-> whereHas(
					'role', function($q){
						$q->where('slug', 'branch_admin');
					}
			)->paginate(20);

	}
    public function getUser($userid)	{
        return  $this->model->find($userid);
    }

	public function getVerifiedUser($request)	{
		$first_name=$request->input('first_name');
		$last_name=$request->input('last_name');
		$email=$request->input('email');
		$mobile=$request->input('mobile');
		$mainorderobj=$this->model;

		if($email!=''){
			$mainorderobj=$mainorderobj->where('email', 'LIKE', '%'.$email.'%');

	   }
		   if($mobile!=''){
			$mainorderobj=$mainorderobj->where('mobile', 'LIKE', '%'.$mobile.'%');

	   }

	   if($first_name!=''){
			$mainorderobj=$mainorderobj->whereHas(
				  'userProfile', function($q) use($first_name){
					  $q->where('first_name', 'LIKE', '%'.$first_name.'%');
				  }
		  );

	   }
	   if($last_name!=''){
			$mainorderobj=$mainorderobj->whereHas(
				  'userProfile', function($q) use($last_name){
					  $q->where('last_name', 'LIKE', '%'.$last_name.'%');
				  }
		  );

	   }
			 if(Auth::user()->isAdmin()){
			   return  $mainorderobj->where('active', '=', 1)-> whereHas(
			            'role', function($q){

			                $q->where('slug','user');
			            }
			    )->paginate(20);
			}else{

  return  $mainorderobj->where('active', '=', 1)-> whereHas(
			            'role', function($q){

			                $q->where('slug','user');
			            }
			    )->where('location','=',Auth::user()->location)->paginate(20);


			}

	}

	public function usersPending()	{

			return  $this->model->where('active', '=', 0)-> whereHas(
						'role', function($q){

							$q->where('slug','user');
						}
				)->paginate(20);


	}

   public function deleteEmployee($id){
		$this->model->destroy($id);
	}
  public function deleteBranchAdmin($id){
		$this->model->destroy($id);
	}


	public function saveEmployee($data)
	{


			$userData=(object)$data->input();
			 if(Auth::user()->isAdmin()){
	             $location=$userData->location;
	             $added_by=Auth::user()->id;
	        }else{

	        	$location=Auth::user()->location;
	        	$added_by=Auth::user()->id;

	        }
		 			$role_user = $this->role->where('slug', 'employee')->first();
        $user = $this->model->where('mobile', '=', $userData->mobile)->first();
        if(empty($user)){
				if(isset($userData->email)){
				$user = $this->model->where('email', '=', $userData->email)->first();
				if(empty($user)){
						$user=$this->model->create([
						'email' => $userData->email,
						'location' =>$location,
						'added_by' =>$added_by,
						'mobile' => $userData->mobile,
						'password'=>Hash::make($userData->password),
						'active' =>'1',
						'role_id' =>$role_user->id,
						'confirmed' =>'1',
						'mobile_confirmed' =>'1',

						]);

						}
						else{
		                        return  'Email already registered';
						}
				}else{


				}
                    //$profile= $this->uploadprofile($data);
 				$userprofile=$this->userProfile->create([
						'first_name' => $userData->first_name,
						'last_name' => $userData->last_name,
						'user_id' => $user->id,

						]);

	}else{
		return  'Mobile number already registered';

        }
	}

	public function saveBranchAdmin($data)
	{




			$userData=(object)$data->input();
			 if(Auth::user()->isAdmin()){
	             $location=$userData->location;
	             $added_by=Auth::user()->id;
	        }else{

	        	$location=Auth::user()->location;
	        	$added_by=Auth::user()->id;

	        }
		 			$role_user = $this->role->where('slug', 'branch_admin')->first();
        $user = $this->model->where('mobile', '=', $userData->mobile)->first();
        if(empty($user)){
				if(isset($userData->email)){
				$user = $this->model->where('email', '=', $userData->email)->first();
				if(empty($user)){
						$user=$this->model->create([
						'email' => $userData->email,
						'location' =>$location,
						'added_by' =>$added_by,
						'mobile' => $userData->mobile,
						'password'=>Hash::make($userData->password),
						'active' =>'1',
						'role_id' =>$role_user->id,
						'confirmed' =>'1',
						'mobile_confirmed' =>'1',

						]);

						}
						else{
		                        return  'Email already registered';
						}
				}else{


				}
                    //$profile= $this->uploadprofile($data);
 				$userprofile=$this->userProfile->create([
						'first_name' => $userData->first_name,
						'last_name' => $userData->last_name,
						'user_id' => $user->id,

						]);

	}else{
		return  'Mobile number already registered';

        }
	}


	public function editEmployee($id){
		return $this->model->find($id);
	}

   public function editBranchAdmin($id){
		return $this->model->find($id);
	}

	public function updateEmployee($inputs, $admin){

		if($inputs['password']!=''){
	     		$admin->password =Hash::make($inputs['password']) ;
		}

		if(Auth::user()->isAdmin()){
	        $admin->location = $inputs['location'];
	     }

		//$admin->mobile = $inputs['mobile'];
		$admin->active = $inputs['active'];
		$admin->save();

		$userprofile=$this->userProfile->where('user_id', $admin->id)->update([
			'first_name' =>$inputs['first_name'],
			'last_name' => $inputs['last_name'],

			]);
	}

	public function updateBranchAdmin($inputs, $admin){

		 if($inputs['password']!=''){
     		$admin->password = Hash::make($inputs['password']) ;
		 }
		 if($inputs['location']!=''){
     		$admin->location = $inputs['location'];
		 }

		//$admin->mobile = $inputs['mobile'];
		$admin->active = $inputs['active'];
		$admin->save();

		$userprofile=$this->userProfile->where('user_id', $admin->id)->update([
			'first_name' =>$inputs['first_name'],
			'last_name' => $inputs['last_name'],

			]);
	}


  	private function save($user, $inputs,$role)
	{
		if(isset($inputs['seen']))
		{
			$user->seen = $inputs['seen'] == 'true';
		} else {

			$user->email = $inputs['email'];

			if(isset($inputs['role'])) {
				$user->role_id = $inputs['role'];
			} else {
				$role_user = $this->role->where('slug', $role)->first();
				$user->role_id = $role_user->id;
			}
		}

		$registereduser=$user->save();

		if($registereduser){
			$this->saveProfile($user->id,$inputs);

		}

	}
	public function saveProfile($userid,$input){
		$this->userProfile->first_name=$input['first_name'];
		$this->userProfile->last_name=$input['last_name'];
		$this->userProfile->mobile=$input['mobile'];
		$this->userProfile->user_id=$userid;
		$this->userProfile->save();

	}

	public function index($n, $role)
	{
		if($role != 'total')
		{
			return $this->model
			->with('role')
			->whereHas('role', function($q) use($role) {
				$q->whereSlug($role);
			})
			->oldest('seen')
			->latest()
			->paginate($n);
		}

		return $this->model
		->with('role')
		->oldest('seen')
		->latest()
		->paginate($n);
	}

	/**
	 * Count the users.
	 *
	 * @param  string  $role
	 * @return int
	 */
	public function count($role = null)
	{
		if($role)
		{
			return $this->model
			->whereHas('role', function($q) use($role) {
				$q->whereSlug($role);
			})->count();
		}

		return $this->model->count();
	}

	/**
	 * Count the users.
	 *
	 * @param  string  $role
	 * @return int
	 */
	public function countUsers()
	{
		$counts = [
			'providers' => $this->count('provider'),
			'seekers' => $this->count('seeker')
		];

		$counts['total'] = array_sum($counts);

		return $counts;
	}

	/**
	 * Create a user.
	 *
	 * @param  array  $inputs
	 * @param  int    $confirmation_code
	 * @return App\Models\User
	 */
	public function store($inputs, $confirmation_code = null,$role='foodie')
	{
		$user = new $this->model;

		$user->password = bcrypt($inputs['password']);

		if($confirmation_code) {
			$user->confirmation_code = $confirmation_code;
		} else {
			$user->confirmed = true;
		}

		$this->save($user, $inputs,$role);

		return $user;
	}

	/**
	 * Update a user.
	 *
	 * @param  array  $inputs
	 * @param  App\Models\User $user
	 * @return void
	 */
	public function update($inputs, $user)
	{
		$user->confirmed = isset($inputs['confirmed']);

		$this->save($user, $inputs);
	}

	/**
	 * Get status of authenticated user.
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return session('status');
	}

	/**
	 * Valid user.
	 *
     * @param  bool  $valid
     * @param  int   $id
	 * @return void
	 */
	public function valid($valid, $id)
	{
		$user = $this->getById($id);

		$user->valid = $valid == 'true';

		$user->save();
	}

	/**
	 * Destroy a user.
	 *
	 * @param  App\Models\User $user
	 * @return void
	 */
	public function destroyUser(User $user)
	{
		$user->comments()->delete();

		$user->delete();
	}

	/**
	 * Confirm a user.
	 *
	 * @param  string  $confirmation_code
	 * @return App\Models\User
	 */
	public function confirm($confirmation_code)
	{
		$user = $this->model->whereConfirmationCode($confirmation_code)->firstOrFail();

		$user->confirmed = true;
		$user->confirmation_code = null;
		$user->save();
	}
	public function uploadprofile($request){

		   if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $extension = $file->getClientOriginalExtension();
            $filename ='profile_'.date('Y-m-d H:i:s') . '.' . $extension;
            $file->move($this->userprofilepath, $filename);

            return 'uploads/profiles/'.$filename;
            }


	}

	public function uploadDocument($request){

		   if ($request->hasFile('document')) {
            $file = $request->file('document');
            $extension = $file->getClientOriginalExtension();
            $filename ='document_'.date('Y-m-d H:i:s') . '.' . $extension;
            $file->move($this->documentspath, $filename);

            return 'uploads/documents/'.$filename;
            }


	}
	 public function saveUser($data) {
	 	$userData=(object)$data->input();
	 	 if(Auth::user()->isAdmin()){
	             $location=$userData->location;
	             $added_by=Auth::user()->id;
	        }else{

	        	$location=Auth::user()->location;
	        	$added_by=Auth::user()->id;

	        }
		 			$role_user = $this->role->where('slug', 'user')->first();
        $user = $this->model->where('mobile', '=', $userData->mobile)->first();
        if(empty($user)){
				if(isset($userData->email)){
				$user = $this->model->where('email', '=', $userData->email)->first();
				if(empty($user)){
						$user=$this->model->create([
						'email' => $userData->email,
						'mobile' => $userData->mobile,
						'location' =>$location,
						'added_by' =>$added_by,
						'password'=>hash_hmac ( "md5", $userData->password , $this->secret),
						'active' =>'1',
						'role_id' =>$role_user->id,
						'confirmed' =>'1',
						'mobile_confirmed' =>'1',

						]);

						}
						else{
		                        return  'Email already registered';
						}
				}else{


				}
                    //$profile= $this->uploadprofile($data);
 				$userprofile=$this->userProfile->create([
						'first_name' => $userData->first_name,
						'last_name' => $userData->last_name,
						'user_id' => $user->id,

						]);

	}else{
		return  'Mobile number already registered';

        }


    }


	 public function findByUserNameOrCreate($userData) {
		$source= $this->socialaccountsources->where('source', 'google')->first();
		$sourceid=$source->id;
			$role_user = $this->role->where('slug', 'foodie')->first();
        $socialaccount= $this->socialaccounts
		        ->where('souce_account_source_id', '=', $sourceid)
		        ->where('provider_id', '=', $userData->id)
		        ->first();

        $user = $this->model->where('email', '=', $userData->email)->first();
        if(empty($socialaccount)  and !empty($user)) {
            $socialaccount = $this->socialaccounts->create([
                'provider_id' => $userData->id,
                'user_id' => $user->id,
                'username' => $userData->nickname,
                'souce_account_source_id' =>$sourceid,
                'avatar' => $userData->avatar,
            ]);
        }else if(empty($socialaccount)  and empty($user)){
			 $user=$this->model->create([
                'email' => $userData->email,
                'active' =>1,
                'role_id' =>$role_user->id,
                'confirmed' =>1,
            ]);
			 $userprofile=$this->userProfile->create([
                'first_name' => $userData->name,
                'last_name' => $userData->name,
                'user_id' => $user->id,
             //   'mobile' => $userData->mobile,
                'avatar' => $userData->avatar,
            ]);
			   $socialaccount = $this->socialaccounts->create([
                'provider_id' => $userData->id,
                'user_id' => $user->id,
                'username' => $userData->nickname,
                'souce_account_source_id' =>$sourceid,
                'avatar' => $userData->avatar,
            ]);
		}

        $this->checkIfUserNeedsUpdating($userData, $user);
      return $user;
    }

    public function checkIfUserNeedsUpdating($userData, $user) {

        $socialData = [
            'first_name' => $userData->name,
            'last_name' => $userData->name,
            'avatar' => $userData->avatar,

        ];
        $dbData = [
            'first_name' => $user->userProfile->first_name,
            'last_name' => $user->userProfile->last_name,
            'avatar' => $user->userProfile->avatar,
        ];

        if (!empty(array_diff($socialData, $dbData))) {
            $user->userProfile->first_name = $userData->name;
            $user->userProfile->last_name = $userData->name;
            $user->userProfile->avatar = $userData->avatar;
            $user->userProfile->save();
        }
    }

}
