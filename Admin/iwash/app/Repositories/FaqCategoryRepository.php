<?php namespace App\Repositories;

use App\Models\FaqCategory;
use GuzzleHttp\Client;
use Config;

class FaqCategoryRepository {

	 
	protected $model;
    protected $client;
    protected $api_url;
	
	public function __construct(FaqCategory $category,Client $client)
	{
		$this->model = $category;
		$this->client = $client;
		$this->api_url =Config::get('barrioConfig.api_url');
	}

	public function getmainCat(){
	/*
		$request=$this->client->get($this->api_url.'category/ordered');
	  return  $result=json_decode($request->getBody());
	*/
		return $this->model->where('parent_id', '=', 0)->paginate(10);
		
		
	}


	public function getsubcatbyparent($id){
		return $this->model->where('parent_id', '=', $id)->get();
		
	
	}


	public function catbytype($parent=0)
	{
		return $this->model->where('parent_id', '=',$parent)->get();
		
		
	}
   public function getbyId($id){
		return $this->model->findOrFail($id);
		
			}
	public function store($inputs,$image)	{
		
		if(isset($inputs['parent'])){
			$parent_id=$inputs['parent'];
		}else{
				$parent_id=0;
		}
	

        $category = new $this->model;
		$category->name =$inputs['name'];
		$category->description =$inputs['description'];
		$category->parent_id =$parent_id;
$category->image =$image;
        $category->save();

			/*
				$data=array('name'=>$inputs['name'],'description'=>$inputs['description'],'parent_id'=>$parent_id);
		$request = $this->client->post($this->api_url.'category/add', [ 'body' => json_encode($data) ]);
		  print_r($request->getBody());
		  */
	   
	}
	public function delete($id){
	     $this->model->destroy($id);
		
		//$request=$this->client->delete($this->api_url.'category/delete/'.$id);
	}
	
	public function edit($id){
	      return $this->model->find($id);
		// $request=$this->client->get($this->api_url.'category/update/'.$id);
		//return  $result=json_decode($request->getBody());
		
		 
	}
	public function parentCats(){
		return $this->model->where('parent_id', '=',0)->get();
	}
	public function update($inputs, $category,$image){

        $category->name = $inputs['name'];
        $category->description = $inputs['description'];
        if(isset($inputs['parent'])){
			$parent_id=$inputs['parent'];
		}else{
				$parent_id=0;
		}
		if($image!=''){
$category->image =$image;
		}
		$category->parent_id =$parent_id;
		$category->save();
	}
	public function totalCategory()
	{
		return $this->model->count();
		
		
	}
}
