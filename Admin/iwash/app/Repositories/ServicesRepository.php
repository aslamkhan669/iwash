<?php namespace App\Repositories;

use App\Models\Services;

class ServicesRepository  extends BaseRepository {

	/**
	 * Create a new ContactRepository instance.
	 *
	 * @param  App\Models\Contact $contact
	 * @return void
	 */
	public function __construct(Services $service)
	{
		$this->model = $service;
	}

	public function getServices()
	{
		return $this->model->paginate(20);
	}
	public function getOnlyMainsCatservice(){
		return $this->model->where('category_id', '=', 1)->get();
		
	
	}
	public function getServicesByCat($catid){

		return $this->model->where('category_id', '=', $catid)->get();
		
	}
	public function deleteService($id){
		$this->model->destroy($id);
	}
	
	public function editService($id){
		return $this->model->find($id);
	}
	
	public function updateService($inputs, $testobj,$image,$image_banner,$image_website,$image_app){

        $testobj->name =$inputs['name'];
         $testobj->category_id =$inputs['category_id'];
		$testobj->description =$inputs['description'];
		$testobj->price_wash =$inputs['price_wash'];
		$testobj->price_dryclean =$inputs['price_dryclean'];
		$testobj->price_iron =$inputs['price_iron'];
		$testobj->per =$inputs['per'];
		if($image!=''){

          $testobj->image ='uploads/services/'.$image;
		}	
		if($image_banner!=''){
			
			$testobj->image_banner ='uploads/services/'.$image_banner;
	    }	
		if($image_website!=''){

          $testobj->image_website ='uploads/services/'.$image_website;
		}	
		if($image_app!=''){

           $testobj->image_app ='uploads/services/'.$image_app;
		}		
       
        $testobj->save();
	}

	public function store($inputs,$image,$image_banner,$image_website,$image_app)	{
		
        $testobj = new $this->model;
		  $testobj->name =$inputs['name'];
         $testobj->category_id =$inputs['category_id'];
		$testobj->description =$inputs['description'];
		$testobj->price_wash =$inputs['price_wash'];
		$testobj->price_dryclean =$inputs['price_dryclean'];
		$testobj->price_iron =$inputs['price_iron'];
		$testobj->per =$inputs['per'];
	   $testobj->image =$image;
	   $testobj->image_banner =$image_banner;
       $testobj->image_website =$image_website;
       $testobj->image_app =$image_app;
        $testobj->save();

	   
	}
	
}