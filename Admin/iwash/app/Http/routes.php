<?php

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/****************************Admin***************************/
 Route::get('/register','RegisterController@getAdminRegister');
 Route::post('/register','RegisterController@postAdminRegister');

Route::get('/','Auth\AuthController@getAdminLogin');
Route::post('/','Auth\AuthController@postAdminLogin');
Route::get('/dashboard', [
	'uses' => 'AdminController@admin',
	'as' => 'admin',

]);
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
/****************************Admin***************************/

/******************** Branch Admin*****************************/

Route::get('/branch/add-admin','AdminController@addBranchAdmin');
Route::post('/branch/add-admin','AdminController@saveEmployee');
Route::get('/branch/admins','AdminController@getBranchAdmins');
Route::get('/branch/admin/{id}/edit','AdminController@editBranchAdmin')
->where('id', '[0-9]+');
Route::post('/branch/admin/{id}/edit','AdminController@updateBranchAdmin')
->where('id', '[0-9]+');
Route::get('/branch/admin/{id}/delete','AdminController@deleteBranchAdmin')
->where('id', '[0-9]+');
/********************Branch Admin*****************************/



/******************** Branch Employee*****************************/

Route::get('/branch/add-employee','AdminController@addEmployee');
Route::post('/branch/add-employee','AdminController@saveEmployee');
Route::get('/branch/employees','AdminController@getEmployees');
Route::get('/branch/employee/{id}/edit','AdminController@editEmployee')
->where('id', '[0-9]+');
Route::post('/branch/employee/{id}/edit','AdminController@updateEmployee')
->where('id', '[0-9]+');
Route::get('/branch/employee/{id}/delete','AdminController@deleteEmployee')
->where('id', '[0-9]+');
/********************Branch Employee*****************************/


/********************Branch*****************************/

Route::get('/add-branch','BranchesController@add');
Route::post('/add-branch','BranchesController@save');
Route::get('/branches','BranchesController@get');
Route::get('branch/{id}/edit','BranchesController@edit')
->where('id', '[0-9]+');
Route::post('branch/{id}/edit','BranchesController@update')
->where('id', '[0-9]+');
Route::get('branch/{id}/delete','BranchesController@delete')
->where('id', '[0-9]+');

/********************Branch*****************************/


/********************Users*****************************/

Route::get('/users','UserController@users');
Route::get('/users/pending','UserController@usersPending');
Route::get('user/{userid}/view','UserController@UserDetails')
->where('userid', '[0-9]+');
Route::get('/add-user','UserController@addUser');
Route::post('/add-user','UserController@saveUser');
/********************Users*****************************/
/*************************************************** */

Route::get('/add-simplepackage','PackageController@add');
Route::post('/add-simplepackage','PackageController@save');
Route::get('/all-simplepackage', 'PackageController@showpackage')->name('all-simplepackage');
Route::get('/add-simplesubscription','SimplesubscriptionController@add');
Route::post('/add-simplesubscription','SimplesubscriptionController@save');
Route::get('/Simplesubscription/{id}/','SimplesubscriptionController@GetPackageInfo')
->where('id', '[0-9]+');
/*Route::get('/Simplesubscription/{id}/','SimplesubscriptionController@GetDurationInfo')
->where('id', '[0-9]+');*/

Route::get('/all-subscription','SimplesubscriptionController@get');
Route::get('/all-subscription', 'SimplesubscriptionController@showsubscription')->name('all-subscription');

Route::get('all-subscription/{id}/edit','SimplesubscriptionController@edit')
->where('id', '[0-9]+');
Route::post('all-subscription/{id}/edit','SimplesubscriptionController@update')
->where('id', '[0-9]+');
Route::get('all-simplepackage/{id}/delete','PackageController@delete')
->where('id', '[0-9]+');
Route::get('/GetCustomerInfo/{email}/','SimplesubscriptionController@GetCustomerInfoFromEmailId');
Route::get('/GetPackageInfo/{email}/','SimplesubscriptionController@GetPackageInfoFromEmailId');
/********************Package*****************************/

Route::get('/add-package','MonthlypackagesController@add');
Route::post('/add-package','MonthlypackagesController@save');
Route::get('/packages','MonthlypackagesController@get');
Route::get('package/{id}/edit','MonthlypackagesController@edit')
->where('id', '[0-9]+');
Route::post('package/{id}/edit','MonthlypackagesController@update')
->where('id', '[0-9]+');
Route::get('package/{id}/delete','MonthlypackagesController@delete')
->where('id', '[0-9]+');
/********************Package*****************************/


/********************Subscription*****************************/

Route::get('/subscriptions','SubscriptionController@get');
Route::get('subscription/{id}/view','SubscriptionController@getDetails')
->where('id', '[0-9]+');
Route::get('/create-subscription','SubscriptionController@create');
Route::post('/create-subscription','SubscriptionController@save');
Route::get('subscription/{id}/edit','SubscriptionController@edit')
->where('id', '[0-9]+');
Route::post('subscription/{id}/edit','SubscriptionController@update')
->where('id', '[0-9]+');
Route::get('subscription/{id}/delete','SubscriptionController@delete')
->where('id', '[0-9]+');

//Route::get('/subscriptionPackageGet/{id}/','SubscriptionController@GetPackageInfo')
//->where('id', '[0-9]+');
/********************Subscription*****************************/



/********************category*****************************/


Route::get('/add-category','CategoryController@addCategory');
Route::post('/add-category','CategoryController@saveCategory');
Route::get('/categories','CategoryController@getCategories');
Route::get('category/{id}/edit','CategoryController@editCategory')
->where('id', '[0-9]+');
Route::post('category/{id}/edit','CategoryController@updateCategory')
->where('id', '[0-9]+');
Route::get('category/{id}/delete','CategoryController@deleteCategory')
->where('id', '[0-9]+');

Route::get('/add-sub-category/{id?}','CategoryController@addSubCategory');
Route::post('/add-sub-category','CategoryController@saveSubCategory');
Route::get('sub-category/{id}/edit','CategoryController@editSubCategory')
->where('id', '[0-9]+');
Route::get('sub-category/{id}/delete','CategoryController@deleteSubCategory')
->where('id', '[0-9]+');
Route::post('sub-category/{id}/edit','CategoryController@updateSubCategory')
->where('id', '[0-9]+');
/********************category*****************************/



/********************FAQ*****************************/


Route::get('/add-faq-category','FaqCategoryController@addCategory');
Route::post('/add-faq-category','FaqCategoryController@saveCategory');
Route::get('/faq-categories','FaqCategoryController@getCategories');
Route::get('faq-category/{id}/edit','FaqCategoryController@editCategory')->where('id', '[0-9]+');
Route::post('faq-category/{id}/edit','FaqCategoryController@updateCategory')->where('id', '[0-9]+');
Route::get('faq-category/{id}/delete','FaqCategoryController@deleteCategory')->where('id', '[0-9]+');



Route::get('/add-faq','FaqsController@addFaq');
Route::post('/add-faq','FaqsController@saveFaq');
Route::get('/faqs','FaqsController@getFaqs');
Route::get('faqs/{id}/edit','FaqsController@editFaqs')
->where('id', '[0-9]+');
Route::post('faqs/{id}/edit','FaqsController@updateFaqs')
->where('id', '[0-9]+');
Route::get('faqs/{id}/delete','FaqsController@deleteFaqs')
->where('id', '[0-9]+');

/********************FAQ*****************************/


/********************Contact*****************************/


Route::get('/contacts','ContactController@getContacts');
Route::get('contacts/{id}/edit','ContactController@editContacts')
->where('id', '[0-9]+');
Route::post('contacts/{id}/edit','ContactController@updateContacts')
->where('id', '[0-9]+');
Route::get('contacts/{id}/delete','ContactController@deleteContacts')
->where('id', '[0-9]+');
/********************Contact*****************************/




/********************Delivery time*****************************/


Route::get('/add-deliverytime','DeliverytimeController@add');
Route::post('/add-deliverytime','DeliverytimeController@save');
Route::get('/deliverytime','DeliverytimeController@get');
Route::get('deliverytime/{id}/edit','DeliverytimeController@edit')
->where('id', '[0-9]+');
Route::post('deliverytime/{id}/edit','DeliverytimeController@update')
->where('id', '[0-9]+');
Route::get('deliverytime/{id}/delete','DeliverytimeController@delete')
->where('id', '[0-9]+');


Route::get('/add-pickuptime','PickuptimeController@add');
Route::post('/add-pickuptime','PickuptimeController@save');
Route::get('/pickuptime','PickuptimeController@get');
Route::get('pickuptime/{id}/edit','PickuptimeController@edit')
->where('id', '[0-9]+');
Route::post('pickuptime/{id}/edit','PickuptimeController@update')
->where('id', '[0-9]+');
Route::get('pickuptime/{id}/delete','PickuptimeController@delete')
->where('id', '[0-9]+');
/********************Delivery time*****************************/


/********************Orders*****************************/
Route::get('/ordersrequest','OrderRequestController@getOrders');
Route::get('/scanorders','OrderController@scanOrder');
Route::get('/create-orderrequest','OrderRequestController@create');
Route::post('/create-orderrequest','OrderRequestController@save');
Route::get('getuserinfo/{mob}','OrderController@getUserDetails')
->where('mob', '[0-9]+');
Route::get('/orders','OrderController@getOrders');
Route::get('/completedorders','OrderController@getCompletedOrders');
Route::get('/revenue','OrderController@revenue');
Route::get('/order/graphs','OrderController@getOrderGraphs');
Route::get('order/{orderid}/view','OrderController@getOrderDetails')
->where('orderid', '[0-9]+');
Route::get('/create-order','OrderController@create');
Route::post('/create-order','OrderController@save');
Route::get('order/{id}/edit','OrderController@edit')
->where('id', '[0-9]+');
Route::post('order/{id}/edit','OrderController@update')
->where('id', '[0-9]+');
Route::get('order/{id}/delete','OrderController@delete')
->where('id', '[0-9]+');
/********************Orders*****************************/


/********************Testimonials*****************************/

Route::get('/add-testimonial','TestimonialController@addTestimonial');
Route::post('/add-testimonial','TestimonialController@saveTestimonial');
Route::get('/testimonials','TestimonialController@getTestimonials');
Route::get('testimonial/{id}/edit','TestimonialController@editTestimonial')
->where('id', '[0-9]+');
Route::post('testimonial/{id}/edit','TestimonialController@updateTestimonial')
->where('id', '[0-9]+');
Route::get('testimonial/{id}/delete','TestimonialController@deleteTestimonial')
->where('id', '[0-9]+');
/********************Testimonials*****************************/



Route::get('/add-banner','BannerController@addBanner');
Route::post('/add-banner','BannerController@saveBanner');
Route::get('banners/{id}/delete','BannerController@deleteBanner')
->where('id', '[0-9]+');
Route::get('/banners','BannerController@getBanners');


/********************Service*****************************/

Route::get('/add-service','ServiceController@addService');
Route::post('/add-service','ServiceController@saveService');
Route::get('/services','ServiceController@getServices');
Route::get('service/{id}/edit','ServiceController@editService')
->where('id', '[0-9]+');
Route::post('service/{id}/edit','ServiceController@updateService')
->where('id', '[0-9]+');
Route::get('service/{id}/delete','ServiceController@deleteService')
->where('id', '[0-9]+');

Route::get('/add-servicefeature','ServicefeatureController@addServicefeature');
Route::post('/add-servicefeature','ServicefeatureController@saveServicefeature');
Route::get('/servicefeatures','ServicefeatureController@getServicesfeature');
Route::get('servicefeature/{id}/edit','ServicefeatureController@editServicefeature')
->where('id', '[0-9]+');
Route::post('servicefeature/{id}/edit','ServicefeatureController@updateServicefeature')
->where('id', '[0-9]+');
Route::get('servicefeature/{id}/delete','ServicefeatureController@deleteServicefeature')
->where('id', '[0-9]+');
/********************Service*****************************/



Route::get('/api/services/{catid}','ServiceController@getServicesByCat');

Route::post('/api/verify-user','UserController@verifyUser');
Route::post('/api/update-provider/{id}','UserController@updateProvider')
->where('id', '[0-9]+');
Route::get('/api/getCustomers','UserController@getCustomers');
