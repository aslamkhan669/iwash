<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SubscribePackage;
use App\Models\SubscribeServices;
use Auth;
use Config;
use App\Repositories\CategoryRepository;
use DB;
use App\Models\User;
use App\Models\Monthlypackages;

class SubscriptionController extends Controller {


	public function __construct(SubscribePackage $sub,SubscribeServices $subserv,Monthlypackages $pack)
	{
		//$this->middleware('admin', ['except' => ['create', 'store']]);
		
		$this->model = $sub;
		$this->submodel = $subserv;
		$this->packageobj = $pack;
		$this->middleware('auth');
		
		
	}

	
	public function get(CategoryRepository $category,Request $request)	{
		$mainobj=$this->model;
		
		if($request->input('filtertype')=='simple'){
			$mainobj=$mainobj->whereYear('createdAt', '=', $request->input('year'))->whereBetween('createdAt', [ $request->input('from'),  $request->input('to')]);

			}else if($request->input('filtertype')=='advance'){
				$first_name=$request->input('first_name');
				$last_name=$request->input('last_name');
				$email=$request->input('email');
				$mobile=$request->input('mobile');
				$package_id=$request->input('package_id');
				$status=$request->input('status');
				$payment_status=$request->input('payment_status');
				$payment_date=$request->input('payment_date');

				if($email!=''){
					$mainobj=$mainobj->whereHas(
							'user', function($q) use($email){
								$q->where('email', 'LIKE', '%'.$email.'%');
							}
					);
				
				}
					if($mobile!=''){
					$mainobj=$mainobj->whereHas(
							'user', function($q) use($mobile){
								$q->where('mobile', 'LIKE', '%'.$mobile.'%');
							}
					);
				
				}
				
				if($first_name!=''){
					$mainobj=$mainobj->whereHas(
							'userProfile', function($q) use($first_name){
								$q->where('first_name', 'LIKE', '%'.$first_name.'%');
							}
					);
				
				}
				if($last_name!=''){
					$mainobj=$mainobj->whereHas(
							'userProfile', function($q) use($last_name){
								$q->where('last_name', 'LIKE', '%'.$last_name.'%');
							}
					);
				
				}
			   	 if($status!=''){
					 $mainobj=$mainobj->where('status', '=',$status);
					
					 }
					 if($package_id!=''){
					 $mainobj=$mainobj->where('package_id', '=',$package_id);
					
					 }
					 if($payment_status!=''){
						$mainobj=$mainobj->whereDate('payment_status', '=',$payment_status);
						
						}
						if($payment_date!=''){
							$mainobj=$mainobj->whereDate('payment_date', '=',$payment_date);
							
							}


		}

		$subscriptions =$mainobj->paginate(10);

		$url=Config::get('app.api_url');
		$packages=$this->packageobj->get();

		return view('admin.subscriptions', compact('subscriptions','url','packages'))->with('title','Subsriptions');
	}


		public function getDetails($id){

			$subscription= $this->model->find($id);
			$url=Config::get('app.api_url');
			
				return view('admin.subscriptiondetails', compact('subscription','url'))->with('title','Subscription Details');


		  }
		  
		  public function update(Request $request,$id){
			$inputs=$request->all();
			 $obj=$this->model->find($id);
	   
			$obj->status =$inputs['status'];
			$obj->payment_amount =$inputs['payment_amount'];
			if($inputs['payment_status']){
				$obj->payment_status =$inputs['payment_status'];
				
			}
			if($inputs['payment_date']){

				$obj->payment_date =$inputs['payment_date'];
				
			}
			$obj->updatedAt=date('Y-m-d H:i:s');
		   $obj->save();
		   if( $obj->id){
		

				   $services=$inputs['services'];
				   foreach($services as $val){

					   $quantity=$inputs['quantity_'.$val];
					   
						   if($quantity!=''){

							$this->submodel->where('subscribe_id','=',$id)->where('service_id','=',$val)->update(['quantity' =>$quantity,'updatedAt'=>date('Y-m-d H:i:s')]);
							

						   }
					   
				   
					   }
   
				
		  
		   }
   
		 return redirect()->back()->with('ok', 'Subscription updated');
	   }
   
   
	   public function edit($id,CategoryRepository $category){
   
			$subscription=$this->model->find($id);
			$categories=$category->parentCats();
			//$cards = DB::select("select categories.name as category,subscribe_services.* from subscribe_services join categories on categories.id=subscribe_services.category_id where subscribe_id=$id group by category_id");
			//print_r($cards);
		  return view('admin.edit-subcription', compact('subscription','categories'))->with('title','Update Subscription');
	   }
	   
   
   
	//    public function delete($id){
	// 	   $this->model->destroy($id);
	// 	   return redirect('orders');
	//    }
   
   


	

}
