<?php namespace App\Http\Controllers;

use App\Repositories\BannerRepository;
use Illuminate\Http\Request;
use Config;
class  BannerController extends Controller {


    protected $banner;
    protected $bannerpath;


    public function __construct(BannerRepository $banner)
    {
		$this->banner = $banner;
		$this->bannerpath = Config::get('app.uploadPath').'uploads/banners/';

		$this->middleware('admin');
		
    }


	 public function getBanners(){
	         $banners=$this->banner->getBanners();
           $url=Config::get('app.api_url');
	   		 return view('admin.banner', compact('banners','url'))->with('title','Banners');;


	}	 

	public function addBanner(){
	         $banners=$this->banner->getBanners();
	   		 return view('admin.add-banner', compact('banners'))->with('title','Add Banner');


	}	public function saveBanner(Request $request){
      $file = $request->file('url');
  
        $destinationPath =$this->bannerpath;
       $extension = $file->getClientOriginalExtension();
       $filename ='banner_'.md5(date('Y-m-d H:i:s')) . '.' . $extension;
        $file->move($destinationPath, $filename);
 
$banners=$this->banner->saveBanner('uploads/banners/'.$filename);
	   	 return redirect()->back()->with('ok', 'Banner Added');


	}
	
	
	public function deleteBanner($id){
	     $this->banner->deleteBanner($id);
        return redirect()->back()->with('ok', 'Banner Deleted');
	}

	
}
