<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Subscription;
use App\Models\User;
use App\Models\Package;
use App\Models\UserProfile;
use App\Http\Controllers\Controller;


class SimplesubscriptionController extends Controller
{
    public function add(){
      $items = array(
        'itemlist' =>  DB::table('package')->get()
      );
     return view('admin.add-simplesubscription', $items)->with('title','Add subscription');
}
public function GetPackageInfo( $id){
      
  return response()->json(DB::table('package')->select('id','qty_clothes','price','description')->where('id','=',$id)->first());
}
     
    public function save(Request $request){
        $email = $request->searchemail;
        $mobile = $request->mobile;
        $firstname = $request->first_name;
        $lastname = $request->last_name;
        $packageid = $request->package;
        $qty_clothes = $request->qty_clothes;
        $price = $request->price;
        $description = $request->description;
        $duration = $request->duration;
        $amount_paid= $request->amount_paid;
        $date = date('Y-m-d H:i:s'); 
        $start_date= $date;
         $end_date = date('Y-m-d H:i:s', strtotime($start_date.' + '.$duration.' months'));

        $user = DB::table('users')->select('id','email')->where('email','=',$email);
        if(!$user->first())
        {
            $usertabledata = array(
                "email" => $email,                
                "mobile" => $mobile, 
                "active" => "1",
                "password" =>"iwash1234",              
                "role_id" => "2"
                
            );
            DB::table('users')->insert($usertabledata);

            $userid = DB::table('users')->select('id','email')->where('email','=',$email)->first()->id;
            $userprofiledata = array(
                "first_name" => $firstname,
                "last_name" => $lastname,
                "user_id" =>  $userid
            );
            DB::table('user_profiles')->insert($userprofiledata);

            $data = array(
                "package_id" => $packageid,
                "user_id" => $userid,
                "duration" => $duration,
                "start_date" => $start_date,
                "end_date" => $end_date,
                "amount_paid"=> $amount_paid,
                "quantity_used"=>$qty_clothes,
                "active" => "1",
                "status" =>"1",
                "created_at" => $date,
                "updated_at" => $date
            );
            if (DB::table('subscription')->insert($data)) {
                $subsid=DB::table('subscription')->select('id','user_id')->where('user_id','=',$userid)->first();
                return redirect()->action(
                    'SimplesubscriptionController@edit', ['id' => $subsid->id]
                )->with('ok','Subscription create' );
              //  return redirect('add-simplesubscriptionn')->with('ok', 'Subscription created');
            } else {
                return redirect('add-simplesubscription')->with('error', 'Subscription error');
            }
        }
        else
        {
          $userid = DB::table('users')->select('id','email')->where('email','=',$email)->first()->id;
        $subs =DB::table('subscription')->where('user_id','=',$userid);
            if ($subs->first())
            {         
               // $subsid = $subs->orderBy('id', 'DESC')->first()->id;
            $data = array(
               "user_id" =>$userid,
            "package_id" => $packageid,
            "duration" => $duration,
            "start_date" => $start_date,
            "end_date" => $end_date,
            "amount_paid"=> $amount_paid,
            "quantity_used"=>$qty_clothes,
            "active" => "1",
            "status" =>"1",
            "created_at" => $date,
            "updated_at" => $date
           );
        
       if (DB::table('subscription')->insert($data)) {
        $subsid=DB::table('subscription')->select('id','user_id','active')->where('user_id','=',$userid)->where('active','=','1')->where('status','=','1')->first();
        return redirect()->action(
            'SimplesubscriptionController@edit', ['id' => $subsid->id]
        )->with('ok','Subscription update' );
      //  return redirect('add-simplesubscription')->with('ok', 'Subscription update');
    } else {
           return redirect('add-simplesubscription')->with('error', 'Subscription error');
       }
    }

    else
    {
        $userid = $user->first()->id;
        $data = array(
        "package_id" => $packageid,
        "user_id" => $userid,
        "duration" => $duration,
        "start_date" => $start_date,
        "end_date" => $end_date,
        "amount_paid"=> $amount_paid,
        "quantity_used"=>$qty_clothes,
        "active" => "1",
        "status" =>"1",
        "created_at" => $date,
        "updated_at" => $date
       );
    
   if (DB::table('subscription')->insert($data)) {
    $subsid=DB::table('subscription')->select('id','user_id','active')->where('user_id','=',$userid)->where('active','=','1')->first();
    return redirect()->action(
        'SimplesubscriptionController@edit', ['id' => $subsid->id]
    )->with('ok','Subscription created' );
      // return redirect('add-simplesubscriptionn')->with('ok', 'Subscription created');
   } else {
       return redirect('add-simplesubscription')->with('error', 'Subscription error');
   }
}

        }
        
    }

    public function get()	{
		return view('admin.all-subscription')->with('title','Subsription');
	}
   
    public function showsubscription()
    {
        $infosubscription = DB::table('users as u')->join('user_profiles as p', 'p.user_id','=','u.id')->join('subscription as s','s.user_id','=','u.id')->join('package as up','s.package_id','=','up.id')
        ->select('s.id','up.name','up.qty_clothes','up.price','up.description','s.start_date','s.end_date','s.amount_paid','s.duration','s.quantity_used','p.first_name','p.last_name','u.email','u.mobile');
        
        
      $subscription = $infosubscription->orderBy('start_date','DESC')
       ->paginate(10);
       
     return view('admin.all-subscription',compact('subscription' ))->with('title','Subscription');
     
    }

    public  function edit($id){

        $infosubscription=Subscription::find($id);
        $userid = $infosubscription->user_id;
        $packageid = $infosubscription->package_id;
        $selectedduration = $infosubscription->duration;
        $userprofileid = DB::table('user_profiles')->where('user_id','=',$userid)->first()->id;
        $packagesubs= Package::find($packageid);
        $userprofile= UserProfile::find($userprofileid);
        
        $usersubs= User::find($userid);
        $items = array(
            'itemlist' =>  DB::table('package')->get()
          );
         
          $selectedpackage = $packageid;
          $selectedmonth = $selectedduration;
      //  $usersubs = DB::table('users')->select('id','email','mobile')->where('id','=',$userid)->first();
          return view('admin.edit-allsubscription',$items,compact('infosubscription','selectedmonth','selectedpackage','usersubs','packagesubs','userprofile'))->with('title','Subscription');
       
    }

    public function update(Request $request,$id){ 
         $subs = Subscription::find($id);   
          $durat = $request->duration;
          $subs->duration= $durat;
         $subs->package_id = $request->input('package_id');
         $subs->amount_paid= $request->amount_paid;
         $subs->quantity_used= $request->qty_clothes;
        // $subs->quantity_used= "0";
         $date = date('Y-m-d H:i:s'); 
         $subs->start_date= $date;
         $subs->end_date = date('Y-m-d H:i:s', strtotime($date.' + '.$durat.' months'));
          $subs->save(); 
     return redirect()->back()->with('ok', 'Subscription updated');
   }

   public function GetCustomerInfoFromEmailId($id){
	
	 $data = DB::table('users as u')->join('user_profiles as p', 'p.user_id','=','u.id')
	->where('email','=',$id)->first();
	if($data == null)
	{
		$isUserExist = false;
		return response()->json($data,$isUserExist);
		
	}
	return response()->json($data);
	
}
public function GetPackageInfoFromEmailId($id){
    

	 $package = DB::table('users as u')->join('subscription as k', 'k.user_id','=','u.id')->join('package as p','p.id','=','k.package_id')->select('p.id','k.id AS subsid','u.id AS userid','p.qty_clothes','p.price','p.description','p.name','k.start_date','k.end_date','k.duration','k.amount_paid')
    ->where('email','=',$id)->orderBy('subsid', 'DESC');
    $userid = $package->first()->userid;
     
    $subs = DB::table('subscription')->select('id','quantity_used','active','end_date','user_id')->where('user_id','=',$userid)->where('active','=','1')->where('status','=','1');
    
   if($package->first())
    {
        $packageinfo = $package->first();
        if($subs->first())
        {
        $subsinfo = $subs->first();
        if($subsinfo->quantity_used > '0' AND $subsinfo->end_date > date('Y-m-d H:i:s'))
        {
            return response()->json($packageinfo);
        }
      
    }
 

        $activepackagenotexist = "No Package Exist";
        return response()->json($activepackagenotexist);
   }
   else 
    {
         $activepackagenotexist = "No Package Exist";
         return response()->json($activepackagenotexist);
    }
	return response()->json($package);
	
}

}
