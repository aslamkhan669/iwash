<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Requests\ContactRequest;
//use App\Events\ContactPageContact;
use App\Models\Pickuptime;


class  PickuptimeController  extends Controller {


	public function __construct(Pickuptime $pick)
	{
				$this->model = $pick;
				$this->middleware('admin');
	
	}

	
	public function get()	{
		$time =$this->model->paginate(20);
$timetype='pickup';
		return view('admin.time', compact('time','timetype'))->with('title','Pickup Time');
	}

	
	public function update(Request $request,$id){
		 $inputs=$request->all();
	     $timeobj=$this->model->find($id);
	
		$timeobj->timebetween= $inputs['timebetween'];
		$timeobj->save();


		  return redirect()->back()->with('ok', 'Record updated');
	}


	public function edit($id){

	     $time=$this->model->find($id);
	   return view('admin.edit-time', compact('time'))->with('title','Edit Pickup Time');
	}
	


	public function delete($id){
		$this->model->destroy($id);
		return redirect('pickuptime');
	}



	  public function add(){
	  	  

		 return view('admin.add-time')->with('title','Add Pickup Time');
	}


	 public function save(Request $request){
	
	       $inputs=$request->all();



	       	$obj = new $this->model;
			
			$obj->timebetween =$inputs['timebetween'];
			$obj->save();

       return redirect()->back()->with('ok', 'Pickup Time is saved');
	}
	

}
