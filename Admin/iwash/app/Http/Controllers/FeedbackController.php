<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Requests\FeedbackRequest;
use App\Repositories\FeedbackRepository;
//use App\Events\ContactPageContact;

class FeedbackController extends Controller {


	public function __construct()
	{
		//$this->middleware('admin', ['except' => ['create', 'store']]);
		$this->middleware('ajax', ['only' => 'update']);
		$this->middleware('admin');
	}

	
	public function getFeedbacks(
		FeedbackRepository $feedback)	{
		$feedbacks = $feedback->getFeedbacks();

		return view('admin.feedback', compact('feedbacks'))->with('title','Contacts');
	}


	
	public function deleteFeedbacks(FeedbackRepository $feedback, $id){
		$feedback->deleteFeedbacks($id);
		return redirect('feedbacks');
	}

}
