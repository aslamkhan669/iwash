<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Requests\ContactRequest;
use App\Repositories\FaqsRepository;
//use App\Events\ContactPageContact;
use App\Repositories\FaqCategoryRepository;

class FaqsController extends Controller {


	public function __construct()
	{
		//$this->middleware('admin', ['except' => ['create', 'store']]);
		$this->middleware('ajax', ['only' => 'update']);
		$this->middleware('admin');
	}

	
	public function getFaqs(
		FaqsRepository $faq)	{
		$faqs = $faq->getFaqs();

		return view('admin.faqs', compact('faqs'))->with('title','Faqs');
	}

	
	public function updateFaqs(FaqsRepository $faq,
		Request $request, 		 
		$id){
		
	     $faqobj=$faq->getById($id);
		$faq->updateFaqs($request->all(),$faqobj);
		  return redirect()->back()->with('ok', 'faq updated');
	}
	public function editFaqs(FaqsRepository $faq,FaqCategoryRepository $category,$id){
		  	         $categories=$category->getmainCat();

	     $faq=$faq->getById($id);
	   return view('admin.edit-faq', compact('faq','categories'))->with('title','Edit Faq');
	}
	
	public function deleteFaqs(FaqsRepository $faq, $id){
		$faq->deleteFaqs($id);
		return redirect('faqs');
	}
	  public function addFaq(FaqCategoryRepository $category){
	  	         $categories=$category->getmainCat();

		 return view('admin.add-faq', compact('categories'))->with('title','Add FAQ');
	}
	 public function saveFaq(FaqsRepository $faq,Request $request){
	
	       $faq->store($request->all());
       return redirect()->back()->with('ok', 'FAQ is saved');
	}
	

}
