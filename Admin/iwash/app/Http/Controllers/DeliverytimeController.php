<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Requests\ContactRequest;
//use App\Events\ContactPageContact;
use App\Models\Deliverytime;


class DeliverytimeController  extends Controller {


	public function __construct(Deliverytime $delivery)
	{
				$this->model = $delivery;

				$this->middleware('admin');
	}

	
	public function get()	{
		$time =$this->model->paginate(20);
$timetype='delivery';

		return view('admin.time', compact('time','timetype'))->with('title','Delivery Time');
	}

	
	public function update(Request $request,$id){
		 $inputs=$request->all();
	     $timeobj=$this->model->find($id);
	
		$timeobj->timebetween= $inputs['timebetween'];
		$timeobj->save();


		  return redirect()->back()->with('ok', 'Record updated');
	}


	public function edit($id){

	     $time=$this->model->find($id);
	   return view('admin.edit-time', compact('time'))->with('title','Edit Delivery Time');
	}
	


	public function delete($id){
		$this->model->destroy($id);
		return redirect('deliverytime');
	}



	  public function add(){
	  	  

		 return view('admin.add-time')->with('title','Add Delivery Time');
	}


	 public function save(Request $request){
	
	       $inputs=$request->all();



	       	$obj = new $this->model;
			
			$obj->timebetween =$inputs['timebetween'];
			$obj->save();

       return redirect()->back()->with('ok', 'Delivery Time is saved');
	}
	

}
