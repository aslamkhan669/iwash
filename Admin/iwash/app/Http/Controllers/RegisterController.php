<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Http\Controllers\Controller;

use App\Http\Requests\Auth\RegisterRequest;


use Validator;
use DB;
use Hash;
class RegisterController extends Controller
{
	public function getAdminRegister(){
		return view('admin.register')->with('title','Register');
	}
	
    public function postAdminRegister(Request $request){
       $first_name = $request->first_name;
       $last_name = $request->last_name;
       $email = $request->email;
       $password = $request->password;
       $remember_token = $request->_token;
       $date = date('Y-m-d H:i:s');

       $validator = Validator::make(
            array( 
            	"first_name" => $request->first_name,
            	"last_name" => $request->last_name,
            	"email" => $request->email,
              "password" => $request->password,
              "c_password" => $request->c_password

            	),array(
            	"first_name" => 'required',
            	"last_name" => 'required',
            	"email" => 'required | email',
            	"password" => 'required',
            	"c_password" => 'required | same:password'
            	)
       	);
       if ($validator->fails()) {
       	return redirect('register')->withErrors($validator)->withInput();
       } else {
		
		$id_email = DB::table('users')->select('email')->where('email',$email)->get();
          if( count($id_email) ==0 ){
			$user = array(
				"email" => $email,
				"location" =>"2",
				"added_by" =>"0",
				"active" =>"1",
				"role_id" =>"1",
				"confirmed" =>"1",
				"mobile_confirmed" =>"1",
				"password" =>Hash::make($password),
				"remember_token" => $remember_token,
				"createdAt" => $date,
				"updatedAt" => $date
				);
				DB::table('users')->insert($user);
				$userid = DB::table('users')->select('id','email')->where('email','=',$email)->first()->id;
				$userprofiledata = array(
				 "first_name" => $first_name,
				 "last_name" => $last_name,
				 "user_id" =>  $userid,
				 "createdAt" => $date,
				   "updatedAt" => $date
			 );
       	if (DB::table('user_profiles')->insert($userprofiledata)) {
       		return redirect('register')->with("ok","SuccessFully Sign Up");
       	} else {
       		return redirect('register')->with("error","Not Insert");
       	}
       	
       }
     
     else
     {
       return redirect('register')->with("error","Email already Exist");
      }
   }
  }





	
}
