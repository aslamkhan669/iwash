<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\AdminPostRequest;

use App\Repositories\UserRepository;
use App\Repositories\CategoryRepository;
use App\Models\Branches;
use Config;
use Auth;
class AdminController extends Controller {

	protected $user_gestion;


	public function __construct(UserRepository $user_gestion)
	{
		$this->user_gestion = $user_gestion;


		$this->middleware('adminORbranchadmin',['except' => [ 'admin','getBranchAdmins','addBranchAdmin','saveBranchAdmin','deleteBranchAdmin','editBranchAdmin','updateBranchAdmin']]);
		$this->middleware('admin',['only' => ['getBranchAdmins','addBranchAdmin','saveBranchAdmin','deleteBranchAdmin','editBranchAdmin','updateBranchAdmin']]);
	}

	public function admin(CategoryRepository $category){

		$categories=$category->parentCats();
		$api_url=Config::get('app.api_url');

		return view('admin.dashboard', compact('categories','api_url'))->with('title','Order Request');
	}

   /*************************************************/

	public function getBranchAdmins(){
		$branchadmins=$this->user_gestion->getBranchAdmins();
		return view('admin.branchadmins', compact('branchadmins'))->with('title','Branch Admins');;

	}
	public function addBranchAdmin(Branches $branch){
		$branches=$branch->get();
		return view('admin.add-employee',compact('branches'))->with('title','Add Branch Admin');
	}
	
	public function saveBranchAdmin(AdminPostRequest $request){

		$error=$this->user_gestion->saveBranchAdmin($request);
		if(isset($error) && !empty($error)){
			return redirect()->back()->with('error', $error);
		}else{
			return redirect()->back()->with('ok', 'Branch Admin added');
		}
	}



	public function deleteBranchAdmin($id){
		$this->user_gestion->deleteBranchAdmin($id);
		return redirect()->back()->with('ok', 'Branch Admin  Deleted');
	}


	public function editBranchAdmin(Branches $branch,$id){
		$admin=$this->user_gestion->editBranchAdmin($id);
		$branches=$branch->get();

		return view('admin.edit-employee', compact('admin','branches'))->with('title','Edit Branch Admin');;
	}


	public function updateBranchAdmin(Request $request,$id){
		$adminobj=$this->user_gestion->getbyId($id);
		$this->user_gestion->updateBranchAdmin($request->all(),$adminobj);
		return redirect()->back()->with('ok', 'Branch Admin updated');
	}
   /************************************************/

	public function getEmployees(){
		$employees=$this->user_gestion->getEmployees();
		return view('admin.employees', compact('employees'))->with('title','Employees');;

	}
	public function deleteEmployee($id){
		$this->user_gestion->deleteEmployee($id);
		return redirect()->back()->with('ok', 'Employee Deleted');
	}

	public function addEmployee(Branches $branch){
		$branches=$branch->get();
		return view('admin.add-employee',compact('branches'))->with('title','Add Employee');
	}


	public function saveEmployee(AdminPostRequest $request){

		$error=$this->user_gestion->saveEmployee($request);
		if(isset($error) && !empty($error)){
			return redirect()->back()->with('error', $error);
		}else{
			return redirect()->back()->with('ok', 'Employee added');
		}
	}

	public function editEmployee(Branches $branch,$id){
		$admin=$this->user_gestion->editEmployee($id);
		$branches=$branch->get();

		return view('admin.edit-employee', compact('admin','branches'))->with('title','Edit Employee');;
	}

	public function updateEmployee(Request $request,$id){
		$adminobj=$this->user_gestion->getbyId($id);
		$this->user_gestion->updateEmployee($request->all(),$adminobj);
		return redirect()->back()->with('ok', 'Employee updated');
	}



}
