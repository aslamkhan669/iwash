<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\UserRepository;
use App\Jobs\SendMail;
use App\Repositories\RoleRepository;
use App\Repositories\AuthenticateUser; 
use Validator;
use DB;
use Hash;
class AuthController extends Controller
{

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;
	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'getLogout']);
	}
   public function loginsocial(AuthenticateUser $authenticateUser, Request $request, $provider = null) {
       return $authenticateUser->execute($request->all(), $this, $provider);
    }
	
	public function getLogin(){
		
		return redirect()->back();
	}
	/**
	 * Handle a login request to the application.
	 *
	 * @param  App\Http\Requests\LoginRequest  $request
	 * @param  App\Services\MaxValueDelay  $maxValueDelay
	 * @param  Guard  $auth
	 * @return Response
	 */
	public function postLogin(
		LoginRequest $request,
		Guard $auth)
	{
		
		$logValue = $request->input('login_email');

		//$logAccess = filter_var($logValue, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $throttles = in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
			return redirect()->back()
				->with('error', trans('front/login.maxattempt'))
				->withInput($request->only('login_email'));
        }

		$credentials = [
			'email'  => $logValue, 
			'password'  => $request->input('login_password')
		];

		if(!$auth->validate($credentials)) {
			if ($throttles) {
	            $this->incrementLoginAttempts($request);
	        }

			return redirect()->back()
				->with('error', trans('front/login.credentials'))
				->withInput($request->only('login_email'));
		}
			
		$user = $auth->getLastAttempted();

		if($user->confirmed) {
			if ($throttles) {
                $this->clearLoginAttempts($request);
            }

			$auth->login($user, $request->has('memory'));

			if($request->session()->has('user_id'))	{
				$request->session()->forget('user_id');
			}

			return redirect('/');
		}
		
		$request->session()->put('user_id', $user->id);	

		return redirect()->back()->with('error', trans('front/verify.again'));			
	}


	/**
	 * Handle a registration request for the application.
	 *
	 * @param  App\Http\Requests\RegisterRequest  $request
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @return Response
	 */


	/**
	 * Handle a confirmation request.
	 *
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @param  string  $confirmation_code
	 * @return Response
	 */
	public function getConfirm(
		UserRepository $user_gestion,
		$confirmation_code)
	{
		$user = $user_gestion->confirm($confirmation_code);

        return redirect('/')->with('ok', trans('front/verify.success'));
	}

	/**
	 * Handle a resend request.
	 *
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @param  Illuminate\Http\Request $request
	 * @return Response
	 */
	public function getResend(
		UserRepository $user_gestion,
		Request $request)
	{
		if($request->session()->has('user_id'))	{
			$user = $user_gestion->getById($request->session()->get('user_id'));

			$this->dispatch(new SendMail($user));

			return redirect('/')->with('ok', trans('front/verify.resend'));
		}

		return redirect('/');        
	}


public function getAdminLogin(){
		return view('admin.login');
	}

// 	public function getAdminRegister(){
// 		return view('admin.register');
// 	}
	
//     public function postAdminRegister(Request $request){
//        $first_name = $request->first_name;
//        $last_name = $request->last_name;
//        $email = $request->email;
//        $password = $request->password;
//        $remember_token = $request->_token;
//        $date = date('Y-m-d H:i:s');

//        $validator = Validator::make(
//             array( 
//             	"first_name" => $request->first_name,
//             	"last_name" => $request->last_name,
//             	"email" => $request->email,
//               "password" => $request->password,
//               "c_password" => $request->c_password

//             	),array(
//             	"first_name" => 'required',
//             	"last_name" => 'required',
//             	"email" => 'required | email',
//             	"password" => 'required',
//             	"c_password" => 'required | same:password'
//             	)
//        	);
//        if ($validator->fails()) {
//        	return redirect('register')->withErrors($validator)->withInput();
//        } else {
		
// 		$id_email = DB::table('users')->select('email')->where('email',$email)->get();
//           if( count($id_email) ==0 ){
// 			$user = array(
// 				"email" => $email,
// 				"location" =>"2",
// 				"added_by" =>"0",
// 				"active" =>"1",
// 				"role_id" =>"1",
// 				"confirmed" =>"1",
// 				"mobile_confirmed" =>"1",
// 				"password" =>Hash::make($password),
// 				"remember_token" => $remember_token,
// 				"createdAt" => $date,
// 				"updatedAt" => $date
// 				);
// 				DB::table('users')->insert($user);
// 				$userid = DB::table('users')->select('id','email')->where('email','=',$email)->first()->id;
// 				$userprofiledata = array(
// 				 "first_name" => $first_name,
// 				 "last_name" => $last_name,
// 				 "user_id" =>  $userid,
// 				 "createdAt" => $date,
// 				   "updatedAt" => $date
// 			 );
//        	if (DB::table('user_profiles')->insert($userprofiledata)) {
//        		return redirect('register')->with("ok","SuccessFully Sign Up");
//        	} else {
//        		return redirect('register')->with("error","Not Insert");
//        	}
       	
//        }
     
//      else
//      {
//        return redirect('register')->with("error","Email already Exist");
//       }
//    }
//   }





public function postAdminLogin(
		LoginRequest $request,
		Guard $auth)
	{
		$logValue = $request->input('email');


        $throttles = in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
			return redirect()->back()
				->with('error', trans('front/login.maxattempt'))
				->withInput($request->only('email'));
        }

		$credentials = [
			'email'  => $logValue, 
			'password'  => $request->input('password')
		];

		if(!$auth->validate($credentials)) {
			if ($throttles) {
	            $this->incrementLoginAttempts($request);
	        }

			return redirect()->back()
				->with('error', trans('front/login.credentials'))
				->withInput($request->only('login_email'));
		}
			
		$user = $auth->getLastAttempted();
	

		if($user->active && ($user->isAdmin()|| $user->isEmployee() || $user->isBranchAdmin())) {
			if ($throttles) {
                $this->clearLoginAttempts($request);
            }

			$auth->login($user, $request->has('memory'));

			if($request->session()->has('user_id'))	{
				$request->session()->forget('user_id');
			}

			return redirect('/orders');
		}
		
		$request->session()->put('user_id', $user->id);	

		return redirect()->back()->with('error', 'HoneyPot ,You are tracked, :P');			
	}
	
}
