<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\SuggestRepository;
use Illuminate\Http\Request;

class  SuggestController extends Controller {


    protected $user_gestion;
    protected $suggest;


    public function __construct(UserRepository $user_gestion,SuggestRepository $suggest)
    {
		$this->user_gestion = $user_gestion;
		$this->suggest = $suggest;
		
		$this->middleware('admin');
    }


	 public function getSuggests(){
	         $suggest=$this->suggest->getSuggests();
	         	

	   		 return view('admin.suggest', compact('suggest'))->with('title','Service Provider Suggested');;


	}	
	
	
	public function deleteSuggestbyId($id){
	     $this->suggest->deleteSuggestbyId($id);
        return redirect()->back()->with('ok', 'Record Deleted');
	}
	
	
	/*
	public function deleteCategory(CategoryRepository $category,$id){
	      $category->delete($id);
        return redirect()->back()->with('ok', 'Category Deleted');
	}
	
	public function editCategory(CategoryRepository $category,$id){
	     $category=$category->edit($id);
	   return view('admin.edit-category', compact('category','parentcat'))->with('title','Edit Category');;
	}
	
	public function updateCategory(CategoryRepository $category,Request $request,$id){
		$categoryobj= $category->getbyId($id);
	    $category->update($request->all(),$categoryobj);
	  return redirect()->back()->with('ok', 'Category updated');
	}
	*/
	
}
