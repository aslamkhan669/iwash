<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\ServicesRepository;
use Illuminate\Http\Request;
use Config;
use App\Repositories\ServicesfeaturesRepository;
use App\Repositories\CategoryRepository;
class  ServicefeatureController extends Controller {

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;

    /**
     * Create a new AdminController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion
     * @return void
     */
    public function __construct(UserRepository $user_gestion)
    {
		$this->user_gestion = $user_gestion;
					$this->servicefeaturepath = Config::get('app.uploadPath').'uploads/servicefeatures/';

		$this->middleware('admin');
    }

	/**
	* Show the admin panel.
	*
	* @param  App\Repositories\ContactRepository $contact_gestion
	* @param  App\Repositories\BlogRepository $blog_gestion
	* @param  App\Repositories\CommentRepository $comment_gestion
	* @return Response
	*/
	
    public function addServicefeature(CategoryRepository $cat){
    			    $categories=$cat->parentCats();

		 return view('admin.add-servicefeatures', compact('categories'))->with('title','Add Service Feature');
	}	

	
	 public function saveServicefeature(ServicesfeaturesRepository $servicefeat,Request $request){

		  $file = $request->file('image');
  
        $destinationPath =$this->servicefeaturepath;
       $extension = $file->getClientOriginalExtension();
       $filename ='service_feature'.md5(date('Y-m-d H:i:s')) . '.' . $extension;
        $file->move($destinationPath, $filename);
	       $servicefeat->store($request->all(),'uploads/servicefeatures/'.$filename);
       return redirect()->back()->with('ok', 'Service feature is saved');
	}
	
	 public function getServicesfeature(ServicesfeaturesRepository $servicefeat){
	         $servicefeatures=$servicefeat->getServicefeatures();
	         	                    $url=Config::get('app.api_url');

	   		 return view('admin.servicefeatures', compact('servicefeatures','url'))->with('title','Service Features');;

	}	
	
		
	
	public function deleteServicefeature(ServicesfeaturesRepository $servicefeat,$id){
	      $servicefeat->deleteServicefeature($id);
        return redirect()->back()->with('ok', 'Service feature Deleted');
	}
	
	public function editServicefeature(ServicesfeaturesRepository $servicefeat,CategoryRepository $cat,$id){
	     $servicefeature=$servicefeat->editServicefeature($id);
	     	                    $url=Config::get('app.api_url');
    			      			    $categories=$cat->parentCats();


	   return view('admin.edit-servicefeatures', compact('servicefeature','url','categories'))->with('title','Edit Service Feature');;
	}
	
	public function updateServicefeature(ServicesfeaturesRepository $servicefeat,Request $request,$id){
			$filename='';
		$testobj= $servicefeat->getbyId($id);
		if ($request->hasFile('image')) {
			$file = $request->file('image');
  
        $destinationPath =$this->servicefeaturepath;
       $extension = $file->getClientOriginalExtension();
       $filename ='service_feature'.md5(date('Y-m-d H:i:s')).'.' . $extension;
        $file->move($destinationPath, $filename);
			}
		  
	    $servicefeat->updateServicefeature($request->all(),$testobj,'uploads/servicefeatures/'.$filename);
	  return redirect()->back()->with('ok', 'Service Feature updated');
	}
	
	
	
	
	
	
}
