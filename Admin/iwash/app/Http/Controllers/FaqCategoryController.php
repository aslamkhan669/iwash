<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\FaqCategoryRepository;
use Illuminate\Http\Request;
use Config;

class  FaqCategoryController extends Controller {

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;

    /**
     * Create a new AdminController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion
     * @return void
     */
    public function __construct(UserRepository $user_gestion)
    {
		$this->user_gestion = $user_gestion;
			$this->faqcatpath = Config::get('app.uploadPath').'uploads/faqcat/';

		
		$this->middleware('admin');
    }

	/**
	* Show the admin panel.
	*
	* @param  App\Repositories\ContactRepository $contact_gestion
	* @param  App\Repositories\BlogRepository $blog_gestion
	* @param  App\Repositories\CommentRepository $comment_gestion
	* @return Response
	*/
	
    public function addCategory(){
		 return view('admin.add-faq-category')->with('title','Add FAQ Category');
	}	
	 public function addSubCategory(FaqCategoryRepository $category,$id=NULL){
		   $parent=$id;
		    $categories=$category->parentCats();
	   		 return view('admin.add-sub-category', compact('categories','parent'))->with('title','Add Sub Category');;
	}	
	
	 public function saveCategory(FaqCategoryRepository $category,Request $request){
		  $file = $request->file('cat_image');
  
        $destinationPath =$this->faqcatpath;
       $extension = $file->getClientOriginalExtension();
       $filename ='faqcat_'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename);
	       $category->store($request->all(),'uploads/faqcat/'.$filename);
       return redirect()->back()->with('ok', 'Category is saved');
	}
	public function saveSubCategory(FaqCategoryRepository $category,Request $request){

		  $file = $request->file('cat_image');
  
        $destinationPath =$this->faqcatpath;
       $extension = $file->getClientOriginalExtension();
       $filename ='faqcat_'.date('Y-m-d H:i:s') . '.' . $extension;
        $file->move($destinationPath, $filename);
	       $category->store($request->all(),'uploads/faqcat/'.$filename);
        return redirect()->back()->with('ok', 'Sub Category is saved');
	}		
	
	 public function getCategories(FaqCategoryRepository $category){
	         $categories=$category->getmainCat();
	         	                    $url=Config::get('app.api_url');

	   		 return view('admin.faq-categories', compact('categories','url'))->with('title','FAQ Categories');;

	}	
	
	 public function getSubCat(FaqCategoryRepository $category){
	         $categories=$category->all();
	       return   response()->json($categories);

	}	
	
	 public function getsubcatbyparent(FaqCategoryRepository $category,$id){
	         $categories=$category->getsubcatbyparent($id);
	       return   response()->json($categories);

	}	
	
	public function deleteCategory(FaqCategoryRepository $category,$id){
	      $category->delete($id);
        return redirect()->back()->with('ok', 'Category Deleted');
	}
	
	public function editCategory(FaqCategoryRepository $category,$id){
	                    $url=Config::get('app.api_url');

	     $category=$category->edit($id);
	   return view('admin.edit-faq-category', compact('category','url'))->with('title','Edit  FAQ Category');;
	}
	
	public function updateCategory(FaqCategoryRepository $category,Request $request,$id){
			$filename='';
		$categoryobj= $category->getbyId($id);
		if ($request->hasFile('cat_image')) {
			$file = $request->file('cat_image');
  
        $destinationPath =$this->faqcatpath;
       $extension = $file->getClientOriginalExtension();
       $filename ='faqcat_'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename);
			}
		  
	    $category->update($request->all(),$categoryobj,'uploads/faqcat/'.$filename);
	  return redirect()->back()->with('ok', 'Category updated');
	}
	
	
	public function deleteSubCategory(FaqCategoryRepository $category,$id){
	     $category->delete($id);
        return redirect()->back()->with('ok', 'Sub Category Deleted');
	}
	
	public function editSubCategory(FaqCategoryRepository $category,$id){
		 $parentcategories=$category->catbytype(0);
	     $category=$category->edit($id);
	    
	   return view('admin.edit-sub-category', compact('category','parentcategories'))->with('title','Edit FAQ Category');;
	}
	
	public function updateSubCategory(FaqCategoryRepository $category,Request $request,$id){
		$filename='';
		$categoryobj= $category->getbyId($id);
		if ($request->hasFile('cat_image')) {
			$file = $request->file('cat_image');
  
        $destinationPath = public_path() .'/uploads/categories/';
       $extension = $file->getClientOriginalExtension();
       $filename ='cat_'.date('Y-m-d H:i:s') . '.' . $extension;
        $file->move($destinationPath, $filename);
			}
		  
	    $category->update($request->all(),$categoryobj,'uploads/categories/'.$filename);
	  return redirect()->back()->with('ok', 'Category updated');
	}
	
}
