<?php namespace App\Http\Controllers;
use App\Repositories\CategoryRepository;

use Illuminate\Http\Request;

use App\Models\Monthlypackages;
use App\Models\PackageServices;



class  MonthlypackagesController  extends Controller {


	public function __construct(Monthlypackages $pack,PackageServices  $packservices)
	{
				$this->model = $pack;
				$this->packservices=$packservices;
				$this->middleware('admin');
	
	}

	
	public function get()	{
		$packages =$this->model->paginate(20);
		return view('admin.packages', compact('packages'))->with('title','Monthly Packages');
	}

	
	public function update(Request $request,$id){
		 $inputs=$request->all();
		  $obj=$this->model->find($id);
	
		 $obj->package =$inputs['package'];
		// $obj->usage_limit =$inputs['usage_limit'];
		 $obj->pickup_options =$inputs['pickup_options'];
		 $obj->available_packages =$inputs['available_packages'];
		 $obj->pricing_for_each_month =$inputs['pricing_for_each_month'];
		 $obj->updatedAt=date('Y-m-d H:i:s');
		 
		$obj->save();
		//echo  $obj->id;
		if( $obj->id){
			$subsripcats = array_column($obj->PackageServices->toArray(), 'category_id');
		//	print_r($subsripcats);
		//	print_r($inputs['category_id']);
			$result=array_diff($subsripcats,$inputs['category_id']);
			

			if(count($result)){
			//	print_r($result);
				foreach($result as $oldcat){
					//echo $oldcat;
					$this->packservices->where('package_id','=',$obj->id)->where('category_id','=',$oldcat)->delete();
					

				}

			}

			$newfields=array_diff($inputs['category_id'],$subsripcats);
			//print_r( $newfields);
			$records=[];
			$cats=$inputs['category_id'];
			foreach($cats as $category_id){
				if($category_id!=''){
					if(isset($inputs['service_id'.$category_id])){
								$services=$inputs['service_id'.$category_id];
								foreach($services as $val){

									$quantity=$inputs['quantity_'.$category_id.'_'.$val];
									
										if($quantity!=''){

											$packages=$this->packservices->where('package_id','=',$obj->id)->where('category_id','=',$category_id)->where('service_id','=',$val)->get();
											if(count($packages)){

												$this->packservices->where('package_id','=',$obj->id)->where('category_id','=',$category_id)->where('service_id','=',$val)->update(['quantity' =>$quantity,'updatedAt'=>date('Y-m-d H:i:s')]);
											}else{

											$this->packservices->create(['package_id'=>$obj->id,'category_id'=>$category_id,'service_id'=>$val,'quantity'=>$quantity]);
											
											}
											
											// $records[]=array('package_id'=>$obj->id,'category_id'=>$category_id,'service_id'=>$val,'quantity'=>$quantity);


										}
									
								
									}

					}

				}
					
				
			}
	   
		}

	  return redirect()->back()->with('ok', 'Monthly Package updated');
	}


	public function edit($id,CategoryRepository $category){
		$categories=$category->parentCats();
		
	     $pack =$this->model->find($id);
	   return view('admin.edit-package', compact('pack','categories'))->with('title','Edit Monthly Package');
	}
	


	public function delete($id){
		$this->model->destroy($id);
		return redirect('packages');
	}



	  public function add(CategoryRepository $category){
	  	  
		$categories=$category->parentCats();
		
		 return view('admin.add-package', compact('categories'))->with('title','Add Monthly Package');
	}


	 public function save(Request $request){
		
		
		  $inputs=$request->all();
	
		   $obj = new $this->model;
		   $obj->package =$inputs['package'];
		   //$obj->usage_limit =$inputs['usage_limit'];
		   $obj->pickup_options =$inputs['pickup_options'];
		   $obj->available_packages =$inputs['available_packages'];
		   $obj->pricing_for_each_month =$inputs['pricing_for_each_month'];
		   $obj->save();
            if( $obj->id){
				$records=[];
				$cats=$inputs['category_id'];
				foreach($cats as $category_id){
					if($category_id!=''){
                          if(isset($inputs['service_id'.$category_id])){
						$services=$inputs['service_id'.$category_id];
						foreach($services as $val){

							$quantity=$inputs['quantity_'.$category_id.'_'.$val];
							
						if($quantity!=''){
							  $records[]=array('package_id'=>$obj->id,'category_id'=>$category_id,'service_id'=>$val,'quantity'=>$quantity);


						}
							  
						
							}

						}

					}
						
					
				}

				$this->packservices->insert($records);

			}
			

//dd($inputs);

return redirect()->back()->with('ok', 'Monthly Package is saved');
	}
	

}
