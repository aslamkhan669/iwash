<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use Validator;
use DB;

class PackageController extends Controller{

    public function showpackage()
    {
       $infopackage = DB::table('package')->select('id','name','qty_clothes','price','description');
       
        
      $package = $infopackage->orderBy('id','DESC') ->paginate(10);
       
     return view('admin.all-simplepackage',compact('package'))->with('title','package');
     
    }
    // public function delete($id){
	// 	$this->model->destroy($id);
	// 	return redirect('all-simplepackage');
	// }

    public function add(){
        
     return view('admin.add-simplepackage')->with('title','Add Package');
}

public function save(Request $request)
    
{
    
     $name = $request->name;
     $qty_clothes = $request->qty_clothes;
     $price = $request->price;
     $description = $request->description;
     $data = date('Y-m-d H:i:s'); 

          $validator = Validator::make(
         array( 
             "name" => $request->name,
             "qty_clothes" => $request->qty_clothes,
             "price" => $request->price,
             "description" => $request->description
             ),array(
             "name" => 'required',
             "qty_clothes" => 'required',
             "price" => 'required',
             "description" => 'required'
             )
     );

       if ($validator->fails()) {
     return redirect('add-simplepackage')->withErrors($validator)->withInput();
    } else {

          $data = array(
         "name" => $name,
         "qty_clothes" => $qty_clothes,
         "price" => $price,
         "description" => $description,
         "created_at" => $data,
         "updated_at" => $data
         );
       
     if (DB::table('package')->insert($data)) {
         return redirect('add-simplepackage')->with('ok', 'Package created');
     } else {
         return redirect('add-simplepackage')->with('error', 'Package error');
     }
}
  
}

}
