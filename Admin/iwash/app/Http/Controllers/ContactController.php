<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Repositories\ContactRepository;
use App\Events\ContactPageContact;

class ContactController extends Controller {


	public function __construct()
	{
		//$this->middleware('admin', ['except' => ['create', 'store']]);
		$this->middleware('ajax', ['only' => 'update']);
	}

	
	public function getContacts(
		ContactRepository $contact_gestion)	{
		$messages = $contact_gestion->getContacts();

		return view('admin.contact', compact('messages'))->with('title','Contacts');
	}

	
	public function updateContacts(ContactRepository $contact_gestion,
		Request $request, 		 
		$id){
		$contact_gestion->updateContacts($request->input('seen'), $id);
		return response()->json(['statut' => 'ok']);
	}

	
	public function deleteContacts(ContactRepository $contact_gestion, $id){
		$contact_gestion->deleteContacts($id);
		return redirect('contacts');
	}

}
