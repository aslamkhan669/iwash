<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderService;
use App\Models\Services;
use App\Models\Category;
use App\Models\Package;
use App\Models\Subscription;
use App\Models\UserProfile;
use Config;
use App\Repositories\CategoryRepository;
use App\Models\Pickuptime;
use App\Models\Deliverytime;
use App\Models\User;
use DB;
use App\Helperlibrary\QRcode;
use App\Models\Branches;
use App\Models\SubscribePackage;



use Auth;
use DNS1D;
use DNS2D;
class OrderController extends Controller {


	
	public function __construct(Order $order,User $user,UserProfile $profile,CategoryRepository $category,SubscribePackage $subscribeobj,OrderService $orderservice,Category $cat,Services $service)
	{
		//$this->middleware('admin', ['except' => ['create', 'store']]);

		$this->model = $order;
		$this->user = $user;
		$this->catobj=$category;
		$this->subscribeobj=$subscribeobj;
		$this->orderservice=$orderservice;
		$this->userprofile = $profile;
		$this->service = $service;
		$this->category = $cat;
		//$this->orderuploadpath = Config::get('app.uploadPath').'uploads/orders/';

		$this->middleware('auth');

	}

	public function getOrderGraphs(CategoryRepository $category,Order $orderobj,Request $request,Branches $branchobj)	{
		$branch=$request->input('branch');

	$service_type=$request->input('service_type');
	$status=$request->input('status');
	$payment_status=$request->input('payment_status');
	$year=$request->input('year');
	$month=$request->input('month');
		 $rawquery="SELECT categories.name as category,count(*) as totalrecord FROM `order`  join categories on order.service_cat_id=categories.id where ";
	 if($request->input('filtertype')=='simple'){

		if($year!='' && $month!=''){
 $rawquery.=" YEAR(order.createdAt) = ".$request->input('year')." AND MONTH(order.createdAt) =  ".$request->input('month')."";
		}


     if($service_type!=''){
			$rawquery.=" AND  service_cat_id=$service_type ";
		}

		if($status!=''){
			$rawquery.=" AND status='".$status."' ";


		}
		if($payment_status!=''){
			$rawquery.=" AND payment_status=$payment_status  ";

		}
	}
       $role=Auth::user()->role->slug;




		if(Auth::user()->isEmployee()|| Auth::user()->isBranchAdmin()){
              $location=Auth::user()->location;
         	$rawquery.=" AND branch =$location ";


		}

		if(Auth::user()->isAdmin()){
	       if($branch!=''){
			 $rawquery.=" AND branch =$branch ";

			}

		}

		$rawquery.=" GROUP by service_cat_id order by service_cat_id";


		if($year=='' || $month==''){
			$orders=[];

		}else{
			$orders=DB::select(DB::raw($rawquery));

		}
		$url=Config::get('app.api_url');
		$categories=$category->parentCats();
		   	         $branches=$branchobj->get();

		return view('admin.ordergraph', compact('orders','url','categories','branches'))->with('title','Orders graph');


	}
	public function getCompletedOrders(CategoryRepository $category,Order $orderobj,Request $request,Branches $branchobj)	{

	$order_id=strtolower($request->input('order_id'));
	$first_name=$request->input('first_name');
	$last_name=$request->input('last_name');
	$email=$request->input('email');
	$mobile=$request->input('mobile');
	$service_type=$request->input('service_type');
	$addressType=$request->input('addressType');
	$flatandstreet=$request->input('flatandstreet');
	$landmark=$request->input('landmark');
	$pickupDate=$request->input('pickupDate');
	$deliveryDate=$request->input('deliveryDate');
	$orderDate=$request->input('createdAt');
	$status=$request->input('status');
	$payment_status=$request->input('payment_status');
	$payment_date=$request->input('payment_date');
	$branch=$request->input('branch');

 	$mainorderobj=$orderobj;
	 if($request->input('filtertype')=='simple'){
		 if($request->input('from')!='' && $request->input('to')!=''){
			$mainorderobj=$mainorderobj->whereBetween('createdAt', [ $request->input('from'),  $request->input('to')]);
		 }
			if($service_type!=''){
				$mainorderobj=$mainorderobj->where('service_cat_id', '=',$service_type);

			}

			if($status!=''){
				$mainorderobj=$mainorderobj->where('status', '=',$status);

			}
			if($payment_status!=''){
				$mainorderobj=$mainorderobj->where('payment_status', '=',$payment_status);

			}
		}else if($request->input('filtertype')=='advance'){
	if($email!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'user', function($q) use($email){
                $q->where('email', 'LIKE', '%'.$email.'%');
            }
    );

 }
 	if($mobile!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'user', function($q) use($mobile){
                $q->where('mobile', 'LIKE', '%'.$mobile.'%');
            }
    );

 }

 if($first_name!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'userProfile', function($q) use($first_name){
                $q->where('first_name', 'LIKE', '%'.$first_name.'%');
            }
    );

 }
 if($last_name!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'userProfile', function($q) use($last_name){
                $q->where('last_name', 'LIKE', '%'.$last_name.'%');
            }
    );

 }

if($order_id!=''){
	if(strpos($order_id, 'iw') !== false){
		 $mainorderobj=$mainorderobj->where('id', '=',explode('iw',$order_id)[1]);

	}else{
	 $mainorderobj=$mainorderobj->where('id', '=',$order_id);

	}

 }

 if($service_type!=''){
 $mainorderobj=$mainorderobj->where('service_cat_id', '=',$service_type);

 }
  if($addressType!=''){
 $mainorderobj=$mainorderobj->where('addressType', '=',$addressType);

 }
  if($flatandstreet!=''){
 $mainorderobj=$mainorderobj->where('flatandstreet', '=',$flatandstreet);

 }
   if($landmark!=''){
 $mainorderobj=$mainorderobj->where('landmark', '=',$landmark);

 }
   if($pickupDate!=''){
 $mainorderobj=$mainorderobj->whereDate('pickupDate', '=',$pickupDate);

 }
   if($deliveryDate!=''){
 $mainorderobj=$mainorderobj->whereDate('deliveryDate', '=',$deliveryDate);

 }
   if($orderDate!=''){
 $mainorderobj=$mainorderobj->whereDate('createdAt', '=',$orderDate);

 }
 if($payment_status!=''){
	$mainorderobj=$mainorderobj->whereDate('payment_status', '=',$payment_status);

	}
	if($payment_date!=''){
		$mainorderobj=$mainorderobj->whereDate('payment_date', '=',$payment_date);

		}
	}
       $role=Auth::user()->role->slug;

		if(Auth::user()->isAdmin()){
	       if($branch!=''){
			  $mainorderobj=$mainorderobj->where('branch', '=',$branch);

			}
			$status = $status!=''?$status:"delivered";
			$orders =$mainorderobj->where('status', '=',$status)->orderBy('createdAt', 'DESC')->paginate(10);



		}else{
			$status = $status!=''?$status:"delivered";
            $orders =$mainorderobj->where('branch','=',Auth::user()->location)->where('status', '=',$status)->orderBy('createdAt', 'DESC')->paginate(10);
		}

		//		$orders =$orderobj->where('parent_id', '=', 0)->paginate(10);
		$url=Config::get('app.api_url');
		$categories=$category->parentCats();

		$branches=$branchobj->get();

		return view('admin.order', compact('orders','url','categories','branches'))->with('title','Completed Orders');
	}
	public function getOrders(CategoryRepository $category,Order $orderobj,Request $request,Branches $branchobj)	{
	$filter_order_id	= $request->input('orderid');
	$order_id=strtolower($request->input('order_id'));
	$first_name=$request->input('first_name');
	$last_name=$request->input('last_name');
	$email=$request->input('email');
	$mobile=$request->input('mobile');
	$service_type=$request->input('service_type');
	$addressType=$request->input('addressType');
	$flatandstreet=$request->input('flatandstreet');
	$landmark=$request->input('landmark');
	$pickupDate=$request->input('pickupDate');
	$deliveryDate=$request->input('deliveryDate');
	$orderDate=$request->input('createdAt');
	$status=$request->input('status');
	$payment_status=$request->input('payment_status');
	$payment_date=$request->input('payment_date');
	$branch=$request->input('branch');

 	$mainorderobj=$orderobj;
	 if($request->input('filtertype')=='simple'){
		 if($request->input('from')!='' && $request->input('to')!=''){
			$mainorderobj=$mainorderobj->whereBetween('createdAt', [ $request->input('from'),  $request->input('to')]);
		 }
			if($filter_order_id!=''){
				$mainorderobj=$mainorderobj->where('id', '=',$filter_order_id);

			}

			if($status!=''){
				$mainorderobj=$mainorderobj->where('status', '=',$status);

			}
			if($payment_status!=''){
				$mainorderobj=$mainorderobj->where('payment_status', '=',$payment_status);

			}
		}else if($request->input('filtertype')=='advance'){
	if($email!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'user', function($q) use($email){
                $q->where('email', 'LIKE', '%'.$email.'%');
            }
    );

 }
 	if($mobile!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'user', function($q) use($mobile){
                $q->where('mobile', 'LIKE', '%'.$mobile.'%');
            }
    );

 }

 if($first_name!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'userProfile', function($q) use($first_name){
                $q->where('first_name', 'LIKE', '%'.$first_name.'%');
            }
    );

 }
 if($last_name!=''){
      $mainorderobj=$mainorderobj->whereHas(
            'userProfile', function($q) use($last_name){
                $q->where('last_name', 'LIKE', '%'.$last_name.'%');
            }
    );

 }

if($order_id!=''){
	if(strpos($order_id, 'iw') !== false){
		 $mainorderobj=$mainorderobj->where('id', '=',explode('iw',$order_id)[1]);

	}else{
	 $mainorderobj=$mainorderobj->where('id', '=',$order_id);

	}

 }

 if($service_type!=''){
 $mainorderobj=$mainorderobj->where('service_cat_id', '=',$service_type);

 }
  if($addressType!=''){
 $mainorderobj=$mainorderobj->where('addressType', '=',$addressType);

 }
  if($flatandstreet!=''){
 $mainorderobj=$mainorderobj->where('flatandstreet', '=',$flatandstreet);

 }
   if($landmark!=''){
 $mainorderobj=$mainorderobj->where('landmark', '=',$landmark);

 }
   if($pickupDate!=''){
 $mainorderobj=$mainorderobj->whereDate('pickupDate', '=',$pickupDate);

 }
   if($deliveryDate!=''){
 $mainorderobj=$mainorderobj->whereDate('deliveryDate', '=',$deliveryDate);

 }
   if($orderDate!=''){
 $mainorderobj=$mainorderobj->whereDate('createdAt', '=',$orderDate);

 }
 if($payment_status!=''){
	$mainorderobj=$mainorderobj->whereDate('payment_status', '=',$payment_status);

	}
	if($payment_date!=''){
		$mainorderobj=$mainorderobj->whereDate('payment_date', '=',$payment_date);

		}
	}
       $role=Auth::user()->role->slug;

		if(Auth::user()->isAdmin()){
	       if($branch!=''){
			  $mainorderobj=$mainorderobj->where('branch', '=',$branch);

			}
			$status = $status!=''?$status:"accepted";
			$orders =$mainorderobj->where('status', '=',$status)->orderBy('createdAt', 'DESC')->paginate(10);



		}else{
			$status = $status!=''?$status:"accepted";
            $orders =$mainorderobj->where('branch','=',Auth::user()->location)->where('status', '=',$status)->orderBy('id', 'DESC')->paginate(10);
		}

		//		$orders =$orderobj->where('parent_id', '=', 0)->paginate(10);
		$url=Config::get('app.api_url');
		$categories=$category->parentCats();

		$branches=$branchobj->get();

		return view('admin.order', compact('orders','url','categories','branches'))->with('title','Placed Orders');
	}
	public function revenue(CategoryRepository $category,Order $orderobj,Request $request,Branches $branchobj)	{
	$total = DB::table('order');
	$order_id=strtolower($request->input('order_id'));
	$first_name=$request->input('first_name');
	$last_name=$request->input('last_name');
	$email=$request->input('email');
	$mobile=$request->input('mobile');
	$service_type=$request->input('service_type');
	$addressType=$request->input('addressType');
	$flatandstreet=$request->input('flatandstreet');
	$landmark=$request->input('landmark');
	$pickupDate=$request->input('pickupDate');
	$deliveryDate=$request->input('deliveryDate');
	$orderDate=$request->input('createdAt');
	$status=$request->input('status');
	$payment_status=$request->input('payment_status');
	$payment_date=$request->input('payment_date');
	$branch=$request->input('branch');
	$paymentMode = $request->input('payment_mode');

 	$mainorderobj=$orderobj;
	 if($request->input('filtertype')=='simple'){
		 	if($request->input('year')!=''){
			$mainorderobj=$mainorderobj->whereBetween('createdAt', [ $request->input('from'),  $request->input('to')]);
			 }
			if($service_type!=''){
				$mainorderobj=$mainorderobj->where('service_cat_id', '=',$service_type);

			}

			
			if($payment_status!=''){
				$total = $total->where('payment_status','=',$payment_status);
				$mainorderobj=$mainorderobj->where('payment_status', '=',$payment_status);

			}
			if($paymentMode!=''){
				$total = $total->where('payment_mode','=',$paymentMode);
				$mainorderobj=$mainorderobj->where('payment_mode','=',$paymentMode);
			}
		}

       $role=Auth::user()->role->slug;

		if(Auth::user()->isAdmin()){
	       if($branch!=''){
			  $mainorderobj=$mainorderobj->where('branch', '=',$branch);

			}
			
			$status = $status!=''?$status:'accepted';
			$total = $total->where('status','=',$status);
			$orders =$mainorderobj->where('status', '=',$status)->orderBy('createdAt', 'DESC')->paginate(10);



		}else{
				$status = $status!=''?$status:'accepted';
			$total = $total->where('status','=',$status);

            $orders =$mainorderobj->where('branch','=',Auth::user()->location)->orderBy('id', 'DESC')->paginate(10);
		}
		if($branch!=null || $branch!=''){
			$total->where("branch","=",$branch);
		}

		$subs = DB::table("subscription")->select('id','user_id','amount_paid');
        $subtotal = $subs->sum("amount_paid");
		$clothes = $total->sum("qty");
		$customers = $total->distinct('user_id')->count("user_id");
		$total = $total->sum('totalamount') ;
		
		//		$orders =$orderobj->where('parent_id', '=', 0)->paginate(10);
		$url=Config::get('app.api_url');
		$categories=$category->parentCats();

		$branches=$branchobj->get();

		return view('admin.revenue', compact('customers','clothes','total','subtotal','orders','url','categories','branches'))->with('title','Revenue');
	}


		public function getOrderDetails(Order $orderobj,$orderid){

			$order= $orderobj->find($orderid);
			$url=Config::get('app.api_url');

				return view('admin.orderdetails', compact('order','url'))->with('title','Order Details');


		  }

	public function update(Request $request,User $user,order $order,Branches $branchobj){

	$inputs=$request->all();
	$oid = $inputs['oid'];
	$os = $this->orderservice->where('order_id','=',$oid);
	$os->delete();

	$totalQty = 0;
	$totalPrice = 0;
	$items_w = [];
	$itemqty_w = [];
	$itemprice_w = [];
	$items_dc = [];
	$itemqty_dc = [];
	$itemprice_dc = [];
	$items_ir = [];
	$itemqty_ir = [];
	$itemprice_ir = [];

	$length = count($inputs['product_id']);
	for($i=0;$i<$length;$i++){
		if($inputs['qty_w'][$i]!="" && $inputs['qty_w'][$i]!=0){
			$totalQty +=$inputs['qty_w'][$i];
			array_push($items_w,$inputs['product_id'][$i]);
			array_push($itemqty_w,$inputs['qty_w'][$i]);
			array_push($itemprice_w,$inputs['price_w'][$i]);
		}
		if($inputs['qty_dc'][$i]!="" && $inputs['qty_dc'][$i]!=0){
			$totalQty +=$inputs['qty_dc'][$i];
			array_push($items_dc,$inputs['product_id'][$i]);
			array_push($itemqty_dc,$inputs['qty_dc'][$i]);
			array_push($itemprice_dc,$inputs['price_dc'][$i]);
		}
		if($inputs['qty_ir'][$i]!="" && $inputs['qty_ir'][$i]!=0){
			$totalQty +=$inputs['qty_ir'][$i];
			array_push($items_ir,$inputs['product_id'][$i]);
			array_push($itemqty_ir,$inputs['qty_ir'][$i]);
			array_push($itemprice_ir,$inputs['price_ir'][$i]);
		}
	
		if($inputs['price_w'][$i]!="" && $inputs['price_w'][$i]!=0 && $inputs['qty_w'][$i]!=""){
			$totalPrice+=$inputs['price_w'][$i] * $inputs['qty_w'][$i];
		}
		if($inputs['price_dc'][$i]!="" && $inputs['price_dc'][$i]!=0 && $inputs['qty_dc'][$i]!=""){
			$totalPrice +=$inputs['price_dc'][$i]* $inputs['qty_dc'][$i];
		}
		if($inputs['price_ir'][$i]!="" && $inputs['price_ir'][$i]!=0 && $inputs['qty_ir'][$i]!=""){
			$totalPrice +=$inputs['price_ir'][$i]* $inputs['qty_ir'][$i];
		}
		
	}
	$userobj=$user->where('active', '=', '1')->where('email', '=',$inputs['mobile'])->orWhere('mobile','=',$inputs['mobile'])-> whereHas(
				'role', function($q){

					$q->where('slug','user');
				}
		)->first();
//subscription edit
$userid = $userobj->id;
$order=$this->model->find($oid);
$totalqtytoupdate =$order->qty;
$subscriptionobj = DB::table('subscription')->where('user_id','=',$userid)->where('active','=','1')->where('status','=','1');

	//edit code

	if($subscriptionobj->first())
	{
		$subscription = $subscriptionobj->first();
		$packageid = $subscription->package_id;
		$packageinfo = DB::table('package')->where('id','=',$packageid);
		
		if($subscription->quantity_used > '0' AND $subscription->quantity_used > $totalQty  AND $subscription->end_date >date('Y-m-d'))

	{
		$totalPrice="0";
		$date = date('Y-m-d H:i:s');
		$subscriptionid = $subscription->id;
			$data = array(
			"user_id" => $subscription->user_id,
			"package_id" => $subscription->package_id,
			"amount_paid"=> $subscription->amount_paid,
			"quantity_used" =>  $subscription->quantity_used + $totalqtytoupdate - $totalQty,
			"updated_at" => $date
		   );
		DB::table('subscription')->where('id','=',$subscriptionid)->update($data);
	}
}


	if($userobj){
		if(Auth::user()->isAdmin()){
			$branch=$inputs['branch'];
			$added_by=Auth::user()->id;
		}
		else{
			$branch=Auth::user()->location;
			$added_by=Auth::user()->id;
		}
		$userDetails = $this->user->find($userobj->id);
		//$userDetails->email = $inputs['email']?$inputs['email']:'';
		$userDetails->mobile = $inputs['mobile'];
		$userDetails->location = $branch;
		$userDetails->added_by = $added_by;
		$userDetails->save();
		if(isset($inputs['fname'])){
			$up = $this->userprofile->where('user_id','=',$userobj->id)->first();
		
		$usp = array("first_name"=>$inputs['fname'],"last_name"=>$inputs['lname']);
		$up->update($usp);
		
		}
	}



	


              $obj=$this->model->find($oid);

				$obj->qty= $totalQty;
				
				$obj->totalamount= $totalPrice;
				$obj->flatandstreet= "";
				$obj->landmark= "";
				$obj->addressType= "Home";
				$obj->pickupDate= $inputs['pickupDate']?$inputs['pickupDate']:date('Y-m-d');
				$obj->deliveryDate= $inputs['deliveryDate']?$inputs['deliveryDate']:date('Y-m-d');
				$obj->timeOfDelivery= 1;
				$obj->timeOfPickup= 1;
				$obj->status= $inputs['status'];
				$obj->payment_mode= $inputs['payment_mode'];
				
					$obj->payment_status= 1;

				
				if(Auth::user()->isAdmin()){

				$branch=$inputs['branch'];
				$added_by=Auth::user()->id;
				}else{

				$branch=Auth::user()->location;
				$added_by=Auth::user()->id;

				}

				$obj->branch= $branch;
				$obj->added_by= $added_by;

				$obj->updatedAt=date('Y-m-d H:i:s');

				$obj->save();
				//comment out for server 
				$path=DNS1D::getBarcodePNGPath('Iw'.$obj->id, "C39");
					$filename = basename($path);
				$obj->qr='uploads/orders/'.$filename ;
				$obj->save();
				
				$message = "Order is updated. Your Order id is IW".$obj->id;
				if( $obj->id){
					$records=[];
					if($obj->order_type=='simple'){
					//saving srvices for each order
					$dc = [];
					$wa = [];
					$ir = [];
					$orderItem = [];
					$items = 0;
					for($i=0;$i<$length;$i++){
						if($inputs['qty_w'][$i]!="" || $inputs['qty_dc'][$i]!="" ||  $inputs['qty_ir'][$i]!=""){
							$w = $inputs['qty_w'][$i]!=""?$inputs['qty_w'][$i]:0;
							$d = $inputs['qty_dc'][$i]!=""?$inputs['qty_dc'][$i]:0;
							$r = $inputs['qty_ir'][$i]!=""?$inputs['qty_ir'][$i]:0;

							$orderItem[$i] = array('order_id'=>$obj->id,'service_id'=>$inputs['product_id'][$i],'qty_ir'=>$r,'qty_w'=>$w,'qty_dc'=>$d,'price_dc'=>$inputs['price_dc'][$i],'price_w'=>$inputs['price_w'][$i],'price_ir'=>$inputs['price_ir'][$i]);
							$items++;
						}
						

					}
					// for($i=0;$i<count($items_dc);$i++){
					// 	$dc[$i] = array('order_id'=>$obj->id,'service_id'=>$items_dc[$i],'qty_dc'=>$itemqty_dc[$i],'price_dc'=>$itemprice_dc[$i]);
					// }
					// for($i=0;$i<count($items_w);$i++){
					// 	$wa[$i] = array('order_id'=>$obj->id,'service_id'=>$items_w[$i],'qty_w'=>$itemqty_w[$i],'price_w'=>$itemprice_w[$i]);
					// }
					// for($i=0;$i<count($items_ir);$i++){
					// 	$ir[$i] = array('order_id'=>$obj->id,'service_id'=>$items_ir[$i],'qty_ir'=>$itemqty_ir[$i],'price_ir'=>$itemprice_ir[$i]);
					// }

					
					$this->orderservice->insert($orderItem);
					// $this->orderservice->insert($wa);
					// $this->orderservice->insert($ir);		

					}
					
				
			 return redirect()->back()->with('ok',$message);
			}
			else{
				return redirect()->back()->with('error', 'User Does Not exist!');
			}
	}




	   public function edit($id,order $order,Subscription $subs,Package $package,CategoryRepository $category,Pickuptime $pick,Deliverytime $del,Branches $branch){
   	         $branches=$branch->get();
			 $tableview = DB::table('services as a')
	 					->leftjoin('order_service as b',function($join) use($id){
							 $join->on('a.id','=','b.service_id');
							 $join->on('b.order_id','=',DB::raw("'".$id."'"));
							 
							 })
						->select('a.id','a.name','a.price_wash','a.price_iron','a.price_dryclean','b.order_id','b.service_id','b.qty_w','b.qty_dc','b.qty_ir')
						->get();	
			$order=$this->model->find($id);
			// $order = DB::table('order')->select('id','createdAt','user_id','order_type','service_cat_id','subscription_id','qr','totalamount','qty','branch','added_by','flatandstreet','landmark','addressType','pickupDate','deliveryDate','timeOfDelivery','timeOfPickup','status','updatedAt','payment_status','payment_mode','payment_date')->where('id','=',$id);
			
			// if($order->first())
			// {
			// 	$orderfirst =$order->first();
			// 	$orderfirst->createdAt = date('m-d-Y',strtotime($orderfirst->createdAt));
			// }
	
		//	$info = DB::table('users as u')->join('user_profiles as p', 'p.user_id','=','u.id')->join('subscription as s','s.user_id','=','u.id')
			
			
			
			$subs = DB::table('subscription')->select('id','quantity_used','active','package_id','end_date','user_id')->where('user_id','=',$order->user_id)->where('active','=','1')->where('status','=','1');	;		
			$end_date= null;
			$packinfo =null;
			$subscription =null;
			if($subs->first())
			{
			$subscription=$subs->first();
			$timestamp = strtotime( $subscription->end_date );  
			$end_date= date('d-F-Y', $timestamp );
		    $package =DB::table('package')->select('qty_clothes','id')->where('id','=',$subscription->package_id);
			$packinfo = $package->first();
			}
			
			
			$categories=$category->parentCats();
			$pickup=$pick->get();
			$delivery=$del->get();
			$url=Config::get('app.api_url');
		  return view('admin.edit-order', compact('tableview','packinfo','end_date','subscription','order','categories','pickup','delivery','branches','url'))->with('title','Update Order');
	
		}



	   public function delete($id){
		   $this->model->destroy($id);
		   return redirect('orders');
	   }



		public function create(CategoryRepository $category,Pickuptime $pick,Deliverytime $del,Branches $branch,Request $request){
	$subscription=false;
	if($request->input('email')){
		// $user_id=$request->input('user_id');
		// $subscription=DB::select(DB::raw("SELECT subscribe_package.*,CURRENT_DATE() as currentdate,(DATE(createdAt) + INTERVAL duration MONTH) as expdate FROM subscribe_package where status=1 and payment_status=1 and user_id=".$user_id." HAVING currentdate < expdate limit 1"));
		// if(count($subscription)){
		// 	$subscription_id=$subscription[0]->id;
		// 	$subscriptionlimit=DB::select(DB::raw("SELECT  subscribe_package.id as package, subscribe_services.*, sum(subscribe_services.quantity) as remaining FROM `subscribe_package` join subscribe_services on subscribe_package.id=subscribe_services.subscribe_id WHERE subscribe_package.payment_status=1 && subscribe_package.id=".$subscription_id." having sum(subscribe_services.quantity) <=0"));
		// 	if(count($subscriptionlimit)){
		// 		//limit used
		// 		$subscription=false;
		// 	}
		// 	else{
		// 		$subscription=$this->subscribeobj->where('id','=',$subscription_id)->first();
		// 	}
		// }
		// else{
		// 	$subscription=false;
		// }
	}
	
	$branches=$branch->get();
	
	$uid = Auth::user()->id;
	$user = $this->user->find($uid);
	$userbranch = $branch->find($user->location);
	if(!$userbranch)
	$userbranch = "";
	else
	$userbranch = $userbranch->branch;
	$categories=$category->parentCats();
	$pickup=$pick->get();
	$delivery=$del->get();
	$tableview = $users = DB::table('services')
            		    ->get(['services.*']);
	return view('admin.add-orderrequest', compact('userbranch','tableview','categories','pickup','delivery','branches','subscription'))->with('title','Create order');
}

public function scanOrder(){
	$url=Config::get('app.api_url');
	return view('admin.scanorder', compact('url'))->with('title','Scan Order');
}

public function getUserDetails($mob,User $user){
	
	$userobj=$user->where('active', '=', '1')->where('email', '=',$mob)->orWhere('mobile','=',$mob)-> whereHas(
				'role', function($q){

					$q->where('slug','user');
				}
		)->first();

	if(!$userobj){
		return response()->json("{'message':'false'}");

	}
	else{
		$res = $this->userprofile->where("user_id","=",$userobj->id)->first();
		$subs = DB::table('subscription')->select('id','quantity_used','package_id','active','status','end_date','user_id')->where('user_id','=',$userobj->id)->where('active','=','1');		
		$subsinfo = $subs->orderBy('id', 'DESC')->first();
		$timestamp = strtotime( $subsinfo->end_date);  
		$end_date= date('d-F-Y', $timestamp );
		$package =DB::table('package')->select('qty_clothes','id')->where('id','=',$subsinfo->package_id);
		$packinfo = $package->first();

		$json = '{"firstname":"'.$res->first_name.'","lastname":"'.$res->last_name.'","qty_clothes":"'.$packinfo->qty_clothes.'","quantity_used":"'.$subsinfo->quantity_used.'","end_date":"'.$end_date.'","ispackageexist":"Package Exist"}';
	
		if($package->first())
		{
			$packageinfo = $package->first();
			if($subs->first())
			{
			$subsinfo = $subs->first();
			$timestamp = strtotime( $subsinfo->end_date );  
			$end_date= date('Y-m-d', $timestamp );
			if($subsinfo->quantity_used > '0' AND $subsinfo->status ='1'  AND $end_date >date('Y-m-d'))
			{
				return response()->json($json);
			}
		  
		}
		$jsonfirst = '{"firstname":"'.$res->first_name.'","lastname":"'.$res->last_name.'","qty_clothes":"'.$packinfo->qty_clothes.'","quantity_used":"'.$subsinfo->quantity_used.'","end_date":"'.$end_date.'","ispackageexist":"NoPackageExist"}';
			
			//$activepackagenotexist = "No Package Exist";
			return response()->json($jsonfirst);
		}
		else 
		{
			$activepackagenotexist = "No Package Exist";
			return response()->json($activepackagenotexist);
		}

		//return response()->json($json);
	}

}
	public function save(Request $request,User $user,Branches $branchobj){
	$inputs=$request->all();
	//$order=$orderobj->find( $inputs['branch']);
	$totalQty = 0;
	$totalPrice = 0;
	$items_w = [];
	$itemqty_w = [];
	$itemprice_w = [];
	$items_dc = [];
	$itemqty_dc = [];
	$itemprice_dc = [];
	$items_ir = [];
	$itemqty_ir = [];
	$itemprice_ir = [];
	$date = date('Y-m-d'); 

	$length = count($inputs['product_id']);
	for($i=0;$i<$length;$i++){
		if($inputs['qty_w'][$i]!="" && $inputs['qty_w'][$i]!=0){
			$totalQty +=$inputs['qty_w'][$i];
			array_push($items_w,$inputs['product_id'][$i]);
			array_push($itemqty_w,$inputs['qty_w'][$i]);
			array_push($itemprice_w,$inputs['price_w'][$i]);
		}
		if($inputs['qty_dc'][$i]!="" && $inputs['qty_dc'][$i]!=0){
			$totalQty +=$inputs['qty_dc'][$i];
			array_push($items_dc,$inputs['product_id'][$i]);
			array_push($itemqty_dc,$inputs['qty_dc'][$i]);
			array_push($itemprice_dc,$inputs['price_dc'][$i]);
		}
		if($inputs['qty_ir'][$i]!="" && $inputs['qty_ir'][$i]!=0){
			$totalQty +=$inputs['qty_ir'][$i];
			array_push($items_ir,$inputs['product_id'][$i]);
			array_push($itemqty_ir,$inputs['qty_ir'][$i]);
			array_push($itemprice_ir,$inputs['price_ir'][$i]);
		}
	
		if($inputs['price_w'][$i]!="" && $inputs['price_w'][$i]!=0 && $inputs['qty_w'][$i]!=""){
			$totalPrice+=$inputs['price_w'][$i] * $inputs['qty_w'][$i];
		}
		if($inputs['price_dc'][$i]!="" && $inputs['price_dc'][$i]!=0 && $inputs['qty_dc'][$i]!=""){
			$totalPrice +=$inputs['price_dc'][$i]* $inputs['qty_dc'][$i];
		}
		if($inputs['price_ir'][$i]!="" && $inputs['price_ir'][$i]!=0 && $inputs['qty_ir'][$i]!=""){
			$totalPrice +=$inputs['price_ir'][$i]* $inputs['qty_ir'][$i];
		}
		
	}

	$userobj=$user->where('active', '=', '1')->where('email', '=',$inputs['email'])->orWhere('mobile','=',$inputs['email'])-> whereHas(
				'role', function($q){
					$q->where('slug','user');
				}
		)->first();
		
		
	if(!$userobj){
		if(Auth::user()->isAdmin()){
			$branch=$inputs['branch'];
			$added_by=Auth::user()->id;
		}
		else{
			$branch=Auth::user()->location;
			$added_by=Auth::user()->id;
		}
		$this->user->email = $inputs['email']?$inputs['email']:'';
		$this->user->mobile = $inputs['email'];
		$this->user->location = $branch;
		$this->user->added_by = $added_by;
		$this->user->active = 1;
		$this->user->role_id = 2;
		$this->user->confirmed = 1;
		$this->user->password = "iwash1234";
		$this->user->mobile_confirmed = 1;
		$this->user->save();
		if(isset($inputs['fname'])){
		$this->userprofile->first_name = $inputs['fname'];
		$this->userprofile->last_name = $inputs['lname'];
		$this->userprofile->user_id = $this->user->id;
		$this->userprofile->save();
		}

	}	
	$userobj1=$user->where('active', '=', '1')->where('mobile', '=',$inputs['email'])->first();
	
		$userid = $userobj1->id;
	$subscriptionobj = DB::table('subscription')->where('user_id','=',$userid)->where('active','=','1')->where('status','=','1');
	
	if($userobj || $this->user->id){
			$obj = new $this->model;
			$obj->user_id= $userobj?$userobj->id:$this->user->id;
			
			
			if(isset($inputs['use_subscription']) && $inputs['use_subscription']==1){
				$obj->order_type='subscription';
				$obj->subscription_id= $inputs['subscription_id'];
			}else{
			$obj->order_type='simple';
			$obj->service_cat_id= 2;
			}
			$obj->totalamount= $totalPrice;
			if($subscriptionobj->first())
			{
				$subscription = $subscriptionobj->first();
				$packageid = $subscription->package_id;
				$packageinfo = DB::table('package')->where('id','=',$packageid);
				
			if($subscription->quantity_used > '0' AND $subscription->quantity_used >= $totalQty  AND $subscription->end_date >date('Y-m-d'))
			{
				$obj->subscription_id = $subscription->id;
				$obj->totalamount= "0";
				//$active ='1';
				$status ='1';
				if($subscription->quantity_used - $totalQty <= '0')
				{
//$active = '0';
$status = '0';
				}
				$subscriptionid = $subscription->id;
					$data = array(
					"user_id" => $subscription->user_id,
					"package_id" => $subscription->package_id,
					"amount_paid"=> $subscription->amount_paid,
					"quantity_used" =>  $subscription->quantity_used - $totalQty,
					"updated_at" => $date,
					"status" => $status,
					//"active" => $active
				   );
				DB::table('subscription')->where('id','=',$subscriptionid)->update($data);
			}
			
		}

				$obj->qty= $totalQty;
				
				
				$obj->flatandstreet= $inputs['flatandstreet'];
				$obj->landmark= $inputs['landmark'];
				$obj->addressType= "Home";
				$obj->pickupDate= $inputs['pickupDate']?$inputs['pickupDate']:date('Y-m-d');
				$obj->deliveryDate= $inputs['deliveryDate']?$inputs['deliveryDate']:date('Y-m-d');
				$obj->timeOfDelivery= 1;
				$obj->timeOfPickup= 1;
				//$obj->totalamount = $totalPrice;
				$obj->status= $inputs['status'];
				$obj->payment_mode= $inputs['payment_mode'];
				if($inputs['payment_status']){
					$obj->payment_status= $inputs['payment_status'];

				}
				if( $inputs['payment_status'] && $inputs['payment_date']){
					$obj->payment_date=$inputs['payment_date'];
				}

				if(Auth::user()->isAdmin()){
				$branch=$inputs['branch'];
				$added_by=Auth::user()->id;
				 } 
				 else
				 {
				$branch=Auth::user()->location;
				$added_by=Auth::user()->id;
				}

				

				$obj->branch= $branch;
				$obj->added_by= $added_by;

				$obj->updatedAt=date('Y-m-d H:i:s');

				$obj->save();
				//comment out for server 
				$path=DNS1D::getBarcodePNGPath('Iw'.$obj->id, "C39");
				$filename = basename($path);
				$obj->qr='uploads/orders/'.$filename ;
				$obj->save();
				
				$message = "Order is created. Your Order id is IW".$obj->id;
				if( $obj->id){
					$records=[];
					if($obj->order_type=='simple'){
					//saving srvices for each order
					$dc = [];
					$wa = [];
					$ir = [];
					$orderItem = [];
					$items = 0;
					for($i=0;$i<$length;$i++){
						if($inputs['qty_w'][$i]!="" || $inputs['qty_dc'][$i]!="" ||  $inputs['qty_ir'][$i]!=""){
							$w = $inputs['qty_w'][$i]!=""?$inputs['qty_w'][$i]:0;
							$d = $inputs['qty_dc'][$i]!=""?$inputs['qty_dc'][$i]:0;
							$r = $inputs['qty_ir'][$i]!=""?$inputs['qty_ir'][$i]:0;

							$orderItem[$i] = array('order_id'=>$obj->id,'service_id'=>$inputs['product_id'][$i],'qty_ir'=>$r,'qty_w'=>$w,'qty_dc'=>$d,'price_dc'=>$inputs['price_dc'][$i],'price_w'=>$inputs['price_w'][$i],'price_ir'=>$inputs['price_ir'][$i]);
							$items++;
						}
						

					}
					// for($i=0;$i<count($items_dc);$i++){
					// 	$dc[$i] = array('order_id'=>$obj->id,'service_id'=>$items_dc[$i],'qty_dc'=>$itemqty_dc[$i],'price_dc'=>$itemprice_dc[$i]);
					// }
					// for($i=0;$i<count($items_w);$i++){
					// 	$wa[$i] = array('order_id'=>$obj->id,'service_id'=>$items_w[$i],'qty_w'=>$itemqty_w[$i],'price_w'=>$itemprice_w[$i]);
					// }
					// for($i=0;$i<count($items_ir);$i++){
					// 	$ir[$i] = array('order_id'=>$obj->id,'service_id'=>$items_ir[$i],'qty_ir'=>$itemqty_ir[$i],'price_ir'=>$itemprice_ir[$i]);
					// }

					
					$this->orderservice->insert($orderItem);
					// $this->orderservice->insert($wa);
					// $this->orderservice->insert($ir);		

					}
					
				}
			 return redirect()->action(
					'OrderController@edit', ['id' => $obj->id]
				)->with('ok', $message);
			}
			else{
				return redirect()->back()->with('error', 'User Does Not exist!');
			}
	}

}
