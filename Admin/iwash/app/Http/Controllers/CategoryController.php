<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Config;

class  CategoryController extends Controller {

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;

    /**
     * Create a new AdminController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion
     * @return void
     */
    public function __construct(UserRepository $user_gestion)
    {
		$this->user_gestion = $user_gestion;
		
		$this->middleware('admin');
			$this->categorypath = Config::get('app.uploadPath').'uploads/categories/';

    }

	/**
	* Show the admin panel.
	*
	* @param  App\Repositories\ContactRepository $contact_gestion
	* @param  App\Repositories\BlogRepository $blog_gestion
	* @param  App\Repositories\CommentRepository $comment_gestion
	* @return Response
	*/
	
    public function addCategory(){
		 return view('admin.add-category')->with('title','Add Category');
	}	
	 public function addSubCategory(CategoryRepository $category,$id=NULL){
		   $parent=$id;
		    $categories=$category->parentCats();
	   		 return view('admin.add-sub-category', compact('categories','parent'))->with('title','Add Sub Category');;
	}	
	
	 public function saveCategory(CategoryRepository $category,Request $request){
		  $file = $request->file('cat_image');  
        $destinationPath = $this->categorypath ;
       $extension = $file->getClientOriginalExtension();
       $filename ='cat_'.md5(date('Y-m-d H:i:s')).'.' . $extension;
		$file->move($destinationPath, $filename);
		
		$file = $request->file('image_banner');
		
			  $destinationPath =$this->categorypath;
			 $extension = $file->getClientOriginalExtension();
			 $filename_banner ='cat_banner'.md5(date('Y-m-d H:i:s')). '.' . $extension;
			  $file->move($destinationPath, $filename_banner);


        $file = $request->file('image_website');
  
        $destinationPath =$this->categorypath;
       $extension = $file->getClientOriginalExtension();
       $filename_web ='cat_web'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename_web);


        $file = $request->file('image_app');
  
        $destinationPath =$this->categorypath;
       $extension = $file->getClientOriginalExtension();
       $filename_app ='cat_app'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename_app);


	       $category->store($request->all(),'uploads/categories/'.$filename,'uploads/categories/'.$filename_banner,'uploads/categories/'.$filename_web,'uploads/categories/'.$filename_app);
	   return redirect()->back()->with('ok', 'Category is saved');
	   
	}
	public function saveSubCategory(CategoryRepository $category,Request $request){

		  $file = $request->file('cat_image');
  
        $destinationPath = public_path() .'/uploads/categories/';
       $extension = $file->getClientOriginalExtension();
       $filename ='cat_'.date('Y-m-d H:i:s') . '.' . $extension;
        $file->move($destinationPath, $filename);
	       $category->store($request->all(),'uploads/categories/'.$filename);
        return redirect()->back()->with('ok', 'Sub Category is saved');
	}		
	
	 public function getCategories(CategoryRepository $category){
	         $categories=$category->getmainCat();
	         	                    $url=Config::get('app.api_url');

	   		 return view('admin.categories', compact('categories','url'))->with('title','Categories');;

	}	
	
	 public function getSubCat(CategoryRepository $category){
	         $categories=$category->all();
	       return   response()->json($categories);

	}	
	
	 public function getsubcatbyparent(CategoryRepository $category,$id){
	         $categories=$category->getsubcatbyparent($id);
	       return   response()->json($categories);

	}	
	
	public function deleteCategory(CategoryRepository $category,$id){
	      $category->delete($id);
        return redirect()->back()->with('ok', 'Category Deleted');
	}
	
	public function editCategory(CategoryRepository $category,$id){
	     $category=$category->edit($id);
	                    $url=Config::get('app.api_url');

	   return view('admin.edit-category', compact('category','url'))->with('title','Edit Category');;
	}
	
	public function updateCategory(CategoryRepository $category,Request $request,$id){
			$filename='';
			$filename_banner='';
			$filename_web='';	
			$filename_app='';

		$categoryobj= $category->getbyId($id);
		if ($request->hasFile('cat_image')) {
			$file = $request->file('cat_image');
  
        $destinationPath = $this->categorypath ;
       $extension = $file->getClientOriginalExtension();
       $filename ='cat_'.md5(date('Y-m-d H:i:s')).'.' . $extension;
        $file->move($destinationPath, $filename);
			}
			if ($request->hasFile('image_banner')) {
				$file = $request->file('image_banner');
	  
			$destinationPath =$this->categorypath;
		   $extension = $file->getClientOriginalExtension();
		   $filename_banner ='cat_banner'.md5(date('Y-m-d H:i:s')). '.' . $extension;
			$file->move($destinationPath, $filename_banner);
				}
	
	
				if ($request->hasFile('image_website')) {
				$file = $request->file('image_website');
	  
			$destinationPath =$this->categorypath;
		   $extension = $file->getClientOriginalExtension();
		   $filename_web ='cat_web'.md5(date('Y-m-d H:i:s')). '.' . $extension;
			$file->move($destinationPath, $filename_web);
				}
			  
			  if ($request->hasFile('image_app')) {
				$file = $request->file('image_app');
	  
			$destinationPath =$this->categorypath;
		   $extension = $file->getClientOriginalExtension();
		   $filename_app ='cat_app'.md5(date('Y-m-d H:i:s')). '.' . $extension;
			$file->move($destinationPath, $filename_app);
				}
		  
	    $category->update($request->all(),$categoryobj,$filename,$filename_banner,$filename_web,$filename_app);
	  return redirect()->back()->with('ok', 'Category updated');
	}
	
	
	public function deleteSubCategory(CategoryRepository $category,$id){
	     $category->delete($id);
        return redirect()->back()->with('ok', 'Sub Category Deleted');
	}
	
	public function editSubCategory(CategoryRepository $category,$id){
		 $parentcategories=$category->catbytype(0);
	     $category=$category->edit($id);
	    
	   return view('admin.edit-sub-category', compact('category','parentcategories'))->with('title','Edit Category');;
	}
	
	public function updateSubCategory(CategoryRepository $category,Request $request,$id){
		$filename='';
		$categoryobj= $category->getbyId($id);
		if ($request->hasFile('cat_image')) {
			$file = $request->file('cat_image');
  
        $destinationPath = public_path() .'/uploads/categories/';
       $extension = $file->getClientOriginalExtension();
       $filename ='cat_'.date('Y-m-d H:i:s') . '.' . $extension;
        $file->move($destinationPath, $filename);
			}
		  
	    $category->update($request->all(),$categoryobj,'uploads/categories/'.$filename);
	  return redirect()->back()->with('ok', 'Category updated');
	}
	
}
