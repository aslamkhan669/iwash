<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\ServicesRepository;
use Illuminate\Http\Request;
use Config;
use App\Repositories\CategoryRepository;
use Response;
class  ServiceController extends Controller {

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;

    /**
     * Create a new AdminController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion
     * @return void
     */
    public function __construct(UserRepository $user_gestion)
    {
		$this->user_gestion = $user_gestion;
					$this->servicepath = Config::get('app.uploadPath').'uploads/services/';

					$this->middleware('admin',['except' => [ 'getServicesByCat']]);
}

	/**
	* Show the admin panel.
	*
	* @param  App\Repositories\ContactRepository $contact_gestion
	* @param  App\Repositories\BlogRepository $blog_gestion
	* @param  App\Repositories\CommentRepository $comment_gestion
	* @return Response
	*/
	public function getServicesByCat(ServicesRepository $service,$catid){
		$services=$service->getServicesByCat($catid);

		return Response::json(
			array(
				'success' => true,
				'data'=>$services,
				),
			 200
		 );


}	
    public function addService(CategoryRepository $category){
    			    $categories=$category->parentCats();

		 return view('admin.add-service', compact('categories'))->with('title','Add Service');
	}	

	
	 public function saveService(ServicesRepository $service,Request $request){

		  $file = $request->file('image');
  
        $destinationPath =$this->servicepath;
       $extension = $file->getClientOriginalExtension();
       $filename ='service_'.md5(date('Y-m-d H:i:s')). '.' . $extension;
		$file->move($destinationPath, $filename);
		
	// 	  $file = $request->file('image_banner');
	// 	  if($file){
	// 		  $destinationPath =$this->servicepath;
	// 		 $extension = $file->getClientOriginalExtension();
	// 		 $filename_banner ='service_banner'.md5(date('Y-m-d H:i:s')). '.' . $extension;
	// 		  $file->move($destinationPath, $filename_banner);
	// 	  }

    //     $file = $request->file('image_website');
	// 	if($file){
    //     $destinationPath =$this->servicepath;
    //    $extension = $file->getClientOriginalExtension();
    //    $filename_web ='service_web'.md5(date('Y-m-d H:i:s')). '.' . $extension;
    //     $file->move($destinationPath, $filename_web);
	// 	}

    //     $file = $request->file('image_app');
	// 	if($file){
    //     $destinationPath =$this->servicepath;
    //    $extension = $file->getClientOriginalExtension();
    //    $filename_app ='service_app'.md5(date('Y-m-d H:i:s')). '.' . $extension;
    //     $file->move($destinationPath, $filename_app);

	// 	}

	     $service->store($request->all(),'uploads/services/'.$filename,'uploads/services/null','uploads/services/null/','uploads/services/null/');
       return redirect()->back()->with('ok', 'Service is saved');
	}
	
	 public function getServices(ServicesRepository $service){
	         $services=$service->getServices();
	        $url=Config::get('app.api_url');

	   		 return view('admin.services', compact('services','url'))->with('title','Services');;

	}	
	
		
	
	public function deleteService(ServicesRepository $service,$id){
	      $service->deleteService($id);
        return redirect()->back()->with('ok', 'Service Deleted');
	}
	
	public function editService(CategoryRepository $category,ServicesRepository $service,$id){
	     $service=$service->editService($id);
	     	                    $url=Config::get('app.api_url');
    			    $categories=$category->parentCats();

	   return view('admin.edit-service', compact('service','url','categories'))->with('title','Edit Service');;
	}
	
	public function updateService(ServicesRepository $service,Request $request,$id){
		$filename='';
		$filename_banner='';
		$filename_web='';

		$filename_app='';

		$testobj= $service->getbyId($id);


		if ($request->hasFile('image')) {
			$file = $request->file('image');
  
        $destinationPath =$this->servicepath;
       $extension = $file->getClientOriginalExtension();
       $filename ='service_'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename);
			}

			
		if ($request->hasFile('image_banner')) {
			$file = $request->file('image_banner');
  
        $destinationPath =$this->servicepath;
       $extension = $file->getClientOriginalExtension();
       $filename_banner ='service_banner'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename_banner);
			}


			if ($request->hasFile('image_website')) {
			$file = $request->file('image_website');
  
        $destinationPath =$this->servicepath;
       $extension = $file->getClientOriginalExtension();
       $filename_web ='service_web'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename_web);
			}
		  
		  if ($request->hasFile('image_app')) {
			$file = $request->file('image_app');
  
        $destinationPath =$this->servicepath;
       $extension = $file->getClientOriginalExtension();
       $filename_app ='service_app'.md5(date('Y-m-d H:i:s')). '.' . $extension;
        $file->move($destinationPath, $filename_app);
			}
		  
		  
	    $service->updateService($request->all(),$testobj,$filename,$filename_banner,$filename_web,$filename_app);
	  return redirect()->back()->with('ok', 'Service updated');
	}
	
	
	
	
	
	
}
