<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Models\User, App\Models\Role;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Models\UserProfile;
use App\Models\Order;
use App\Models\OrderService;
use App\Models\Services;
use App\Models\Category;
use App\Models\Pickuptime;
use App\Models\Deliverytime;
use App\Helperlibrary\QRcode;
use App\Models\Branches;
use App\Models\SubscribePackage;



use Auth;
use DNS1D;
use DNS2D;
use Response;
use Redirect;
use Config;
use DB;

class UserController extends Controller {

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user;

    /**
     * Create a new AdminController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion
     * @return void
     */
    public function __construct(UserRepository $user)
    {
		$this->user = $user;
		$this->middleware('auth');
    }


	public function users(Request $request){
	 $users=$this->user->getVerifiedUser($request);

           return view('admin.users',compact('users'))->with('title','Verified Customers');
    }

public function  usersPending(){
    $users=$this->user->usersPending();

           return view('admin.users-pending',compact('users'))->with('title','Pending Verification');
    }

    public function addUser(Branches $branch){

        $branches=$branch->get();

           return view('admin.add-user',compact('branches'))->with('title','Add User');
    }


     public function saveUser(UserRepository $user,Request $request){
       $error= $this->user->saveUser($request);
        if(isset($error) && !empty($error)){
            return redirect()->back()->with('error', $error);
        }else{
            return redirect()->back()->with('ok', 'User added');
        }

    }


    public function UserDetails($userId,CategoryRepository $category,Order $orderobj,Request $request,Branches $branchobj) {
        $filter_order_id	= $request->input('orderid');
            $order_id=strtolower($request->input('order_id'));
            $first_name=$request->input('first_name');
            $last_name=$request->input('last_name');
            $email=$request->input('email');
            $mobile=$request->input('mobile');
            $service_type=$request->input('service_type');
            $addressType=$request->input('addressType');
            $flatandstreet=$request->input('flatandstreet');
            $landmark=$request->input('landmark');
            $pickupDate=$request->input('pickupDate');
            $deliveryDate=$request->input('deliveryDate');
            $orderDate=$request->input('createdAt');
            $status=$request->input('status');
            $payment_status=$request->input('payment_status');
            $payment_date=$request->input('payment_date');
            $branch=$request->input('branch');
        
             $mainorderobj=$orderobj;
             if($request->input('filtertype')=='simple'){
                 if($request->input('from')!=''){
                    $mainorderobj=$mainorderobj->whereBetween('createdAt', [ $request->input('from'),  $request->input('to')]);
                 }
                    if($service_type!=''){
                        $mainorderobj=$mainorderobj->where('service_cat_id', '=',$service_type);
        
                    }
        //orderid serech
                    if($filter_order_id!=''){
				$mainorderobj=$mainorderobj->where('id', '=',$filter_order_id);

			}
        
                    if($status!=''){
                        $mainorderobj=$mainorderobj->where('status', '=',$status);
        
                    }
                    if($payment_status!=''){
                        $mainorderobj=$mainorderobj->whereDate('payment_status', '=',$payment_status);
        
                    }
                }
           
               $role=Auth::user()->role->slug;
               $mainorderobj =$mainorderobj->where("user_id","=",$userId);
                if(Auth::user()->isAdmin()){
                   if($branch!=''){
                      $mainorderobj=$mainorderobj->where('branch', '=',$branch);
        
                    }
                    $orders =$mainorderobj->orderBy('createdAt','DESC')->paginate(10);
        
        
        
                }else{
                    $orders =$mainorderobj->where('branch','=',Auth::user()->location)->orderBy('createdAt', 'DESC')->paginate(10);
                }
        
                // $orders =$orderobj->where('parent_id', '=', 0)->paginate(10);
                $url=Config::get('app.api_url');
                $categories=$category->parentCats();
        
                $branches=$branchobj->get();
        
                return view('admin.user-details', compact('orders','url','categories','branches'))->with('title','User Detail');
            }

    public function getCustomers(Request $request){
        echo $this->user->getCustomers($request->input('email'));
    }
public function verifyUser(User $user,Request $request){
$Verification=$request->input('verification');
$status=$request->input('status');
foreach($Verification as $val){
$usr = $user->where('id', $val)->update(['active' =>$status]);
}
   return Response::json(
                        array(
                            'success' => true,
                            'message'=>'User verified',
                            ),
                         200
                     );
}


  public function updateProvider(User $user,UserProfile  $profile,Request $request,$id){
    $userprofile=$profile->where('user_id','!=', $id)->where('mobile',$request->input('mobile'))->first();
        $user=$user->where('id','!=', $id)->where('email',$request->input('email'))->first();

    if(!empty($userprofile)){
            return Response::json(
                                    array(
                                        'success' => false,
                                        'message'=>'Already user registered with this mobile',
                                        ),
                                     200
                                 );

    }else if(!empty($user)){
        return Response::json(
                                    array(
                                        'success' => false,
                                        'message'=>'Already user registered with this email',
                                        ),
                                     200
                                 );

}   else{


  $this->user->updateprovider($request,$id);
         return Response::json(
                        array(
                            'success' => true,
                            'message'=>'charges Updated',
                            ),
                         200
                     );
    }

  }
}
