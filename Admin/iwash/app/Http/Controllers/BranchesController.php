<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Branches;


class  BranchesController  extends Controller {


	public function __construct(Branches $branch){
				$this->model = $branch;
				$this->middleware('admin');
	
	}

	
	public function get()	{
		$branches =$this->model->paginate(20);
		return view('admin.branches', compact('branches'))->with('title','Branches');
	}

	
	public function update(Request $request,$id){
		 $inputs=$request->all();
	     $branchobj=$this->model->find($id);
	
		$branchobj->branch= $inputs['branch'];
		$branchobj->location= $inputs['location'];
		$branchobj->save();


		  return redirect()->back()->with('ok', 'Record updated');
	}


	public function edit($id){

	     $branch=$this->model->find($id);
	   return view('admin.edit-branch', compact('branch'))->with('title','Edit Branch');
	}
	


	public function delete($id){
		$this->model->destroy($id);
		return redirect('branches');
	}



	  public function add(){
	  	  

		 return view('admin.add-branch')->with('title','Add Branch');
	}


	 public function save(Request $request){
	
	       $inputs=$request->all();



	       	$obj = new $this->model;
			
			$obj->branch =$inputs['branch'];
			$obj->location =$inputs['location'];
			$obj->save();

       return redirect()->back()->with('ok', 'Branch is saved');
	}
	

}
