<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderService;
use App\Models\Services;
use App\Models\Category;
use App\Models\UserProfile;
use Config;
use App\Repositories\CategoryRepository;
use App\Models\Pickuptime;
use App\Models\Deliverytime;
use App\Models\User;
use DB;
use App\Helperlibrary\QRcode;
use App\Models\Branches;
use App\Models\SubscribePackage;



use Auth;
use DNS1D;
use DNS2D;
class OrderRequestController extends Controller {


	public function __construct(Order $order,User $user,UserProfile $profile,CategoryRepository $category,SubscribePackage $subscribeobj,OrderService $orderservice,Category $cat,Services $service)
	{
		//$this->middleware('admin', ['except' => ['create', 'store']]);

		$this->model = $order;
		$this->user = $user;
		$this->catobj=$category;
		$this->subscribeobj=$subscribeobj;
		$this->orderservice=$orderservice;
		$this->userprofile = $profile;
		$this->service = $service;
		$this->category = $cat;
		//$this->orderuploadpath = Config::get('app.uploadPath').'uploads/orders/';

		$this->middleware('auth');

	}

	
	public function getOrders(CategoryRepository $category,Order $orderobj,Request $request,Branches $branchobj)	{

	$order_id=strtolower($request->input('order_id'));
	$first_name=$request->input('first_name');
	$last_name=$request->input('last_name');
	$email=$request->input('email');
	$mobile=$request->input('mobile');
	$service_type=$request->input('service_type');
	$addressType=$request->input('addressType');
	$flatandstreet=$request->input('flatandstreet');
	$landmark=$request->input('landmark');
	$pickupDate=$request->input('pickupDate');
	$deliveryDate=$request->input('deliveryDate');
	$orderDate=$request->input('createdAt');
	$status=$request->input('status');
	$payment_status=$request->input('payment_status');
	$payment_date=$request->input('payment_date');
	$branch=$request->input('branch');

 	$mainorderobj=$orderobj;
	if($request->input('filtertype')=='simple'){
			$mainorderobj=$mainorderobj->whereYear('createdAt', '=', $request->input('year'))->whereBetween('createdAt', [ $request->input('from'),  $request->input('to')]);
			if($service_type!=''){
				$mainorderobj=$mainorderobj->where('service_cat_id', '=',$service_type);

			}

			if($status!=''){
				$mainorderobj=$mainorderobj->where('status', '=',$status);

			}
			if($payment_status!=''){
				$mainorderobj=$mainorderobj->whereDate('payment_status', '=',$payment_status);

			}
	}
	else if($request->input('filtertype')=='advance'){
		if($email!=''){
			$mainorderobj=$mainorderobj->whereHas(
					'user', function($q) use($email){
						$q->where('email', 'LIKE', '%'.$email.'%');
					}
			);
		}
		if($mobile!=''){
		$mainorderobj=$mainorderobj->whereHas(
				'user', function($q) use($mobile){
					$q->where('mobile', 'LIKE', '%'.$mobile.'%');
				}
			);

		}

		if($first_name!=''){
		$mainorderobj=$mainorderobj->whereHas(
				'userProfile', function($q) use($first_name){
					$q->where('first_name', 'LIKE', '%'.$first_name.'%');
				}
			);
		}
		if($last_name!=''){
		$mainorderobj=$mainorderobj->whereHas(
				'userProfile', function($q) use($last_name){
					$q->where('last_name', 'LIKE', '%'.$last_name.'%');
				}
			);
		}

		if($order_id!=''){
			if(strpos($order_id, 'iw') !== false){
				$mainorderobj=$mainorderobj->where('id', '=',explode('iw',$order_id)[1]);

			}else{
			$mainorderobj=$mainorderobj->where('id', '=',$order_id);
			}
		}
		if($status!=''){
			$mainorderobj=$mainorderobj->where('status', '=',$status);
		}
		if($service_type!=''){
			$mainorderobj=$mainorderobj->where('service_cat_id', '=',$service_type);
		}
		if($addressType!=''){
			$mainorderobj=$mainorderobj->where('addressType', '=',$addressType);
		}
		if($flatandstreet!=''){
			$mainorderobj=$mainorderobj->where('flatandstreet', '=',$flatandstreet);
		}
		if($landmark!=''){
			$mainorderobj=$mainorderobj->where('landmark', '=',$landmark);
		}
		if($pickupDate!=''){
			$mainorderobj=$mainorderobj->whereDate('pickupDate', '=',$pickupDate);
		}
		if($deliveryDate!=''){
			$mainorderobj=$mainorderobj->whereDate('deliveryDate', '=',$deliveryDate);
		}
		if($orderDate!=''){
			$mainorderobj=$mainorderobj->whereDate('createdAt', '=',$orderDate);
		}
		if($payment_status!=''){
			$mainorderobj=$mainorderobj->whereDate('payment_status', '=',$payment_status);
		}
		if($payment_date!=''){
			$mainorderobj=$mainorderobj->whereDate('payment_date', '=',$payment_date);
		}
	}
    $role=Auth::user()->role->slug;
	if(Auth::user()->isAdmin()){
		if($branch!=''){
			$mainorderobj=$mainorderobj->where('branch', '=',$branch);
		}
		$orders =$mainorderobj->orderBy('createdAt', 'DESC')->paginate(10);
	}
	else{
        $orders =$mainorderobj->where('branch','=',Auth::user()->location)->orderBy('createdAt', 'DESC')->paginate(10);
	}

	//$orders =$orderobj->where('parent_id', '=', 0)->paginate(10);
	$url="localhost:3000";
	$categories=$category->parentCats();
	$branches=$branchobj->get();
	return view('admin.orderrequest', compact('orders','url','categories','branches'))->with('title','Orders');
}


public function getOrderDetails(Order $orderobj,$orderid){

	$order= $orderobj->find($orderid);
	$url=Config::get('app.api_url');

		return view('admin.orderdetails', compact('order','url'))->with('title','Order Details');


	}

public function update(Request $request,$id,User $user){
	$inputs=$request->all();
	$obj=$this->model->find($id);

	if(empty($obj->qr)){
		$path=DNS1D::getBarcodePNGPath('Iw'.$obj->id, "C39");
	$filename = basename($path);


	$obj->qr='uploads/orders/'.$filename ;
	}

	

	if($inputs['flatandstreet']){
	$obj->flatandstreet= $inputs['flatandstreet'];
	} if($inputs['landmark']){
	$obj->landmark= $inputs['landmark'];

	} if($inputs['addressType']){
	$obj->addressType= $inputs['addressType'];

	} if( $inputs['pickupDate']){
		$obj->pickupDate= $inputs['pickupDate'];

	} if($inputs['deliveryDate']){
		$obj->deliveryDate= $inputs['deliveryDate'];

	} if($inputs['timeOfDelivery']){
		$obj->timeOfDelivery= $inputs['timeOfDelivery'];

	} if($inputs['timeOfPickup']){
		$obj->timeOfPickup= $inputs['timeOfPickup'];

	} if($inputs['totalamount']){
		$obj->totalamount= $inputs['totalamount'];

	} if( $inputs['qty']){
		$obj->qty= $inputs['qty'];

	} if($inputs['status']){
		$obj->status= $inputs['status'];

	} if($inputs['payment_status']){
		$obj->payment_status= $inputs['payment_status'];

	} if($inputs['payment_status'] && $inputs['payment_date']){
		$obj->payment_date= $inputs['payment_date'];

	}
	if(Auth::user()->isAdmin()){
			$branch=$inputs['branch'];
			$added_by=Auth::user()->id;
	}else{

		$branch=Auth::user()->location;
		$added_by=Auth::user()->id;

	}

	$obj->branch= $branch;
$obj->added_by= $added_by;

	$obj->save();
	
		if($obj->order_type=='simple'){
						
		$services=$inputs['services'];
		foreach($services as $val){
			$quantity=$inputs['quantity_'.$val];							   
			if($quantity!='' &&!empty($quantity)){
				
				$this->orderservice->where('order_id','=',$obj->id)->where('service_id','=',$val)->update(['quantity' => $quantity]);
			}
		}
							
		
}else{

	//here we write code for subscription based 

}



	return redirect()->back()->with('ok', 'Order updated');
}


public function edit($id,CategoryRepository $category,Pickuptime $pick,Deliverytime $del,Branches $branch){
		$branches=$branch->get();

	$order=$this->model->find($id);
	$categories=$category->parentCats();
	$pickup=$pick->get();
	$delivery=$del->get();
	return view('admin.edit-order', compact('order','categories','pickup','delivery','branches'))->with('title','Update Order');
}



public function delete($id){
	$this->model->destroy($id);
	return redirect('orders');
}



public function create(CategoryRepository $category,Pickuptime $pick,Deliverytime $del,Branches $branch,Request $request){
	$subscription=false;
	if($request->input('email')){
		// $user_id=$request->input('user_id');
		// $subscription=DB::select(DB::raw("SELECT subscribe_package.*,CURRENT_DATE() as currentdate,(DATE(createdAt) + INTERVAL duration MONTH) as expdate FROM subscribe_package where status=1 and payment_status=1 and user_id=".$user_id." HAVING currentdate < expdate limit 1"));
		// if(count($subscription)){
		// 	$subscription_id=$subscription[0]->id;
		// 	$subscriptionlimit=DB::select(DB::raw("SELECT  subscribe_package.id as package, subscribe_services.*, sum(subscribe_services.quantity) as remaining FROM `subscribe_package` join subscribe_services on subscribe_package.id=subscribe_services.subscribe_id WHERE subscribe_package.payment_status=1 && subscribe_package.id=".$subscription_id." having sum(subscribe_services.quantity) <=0"));
		// 	if(count($subscriptionlimit)){
		// 		//limit used
		// 		$subscription=false;
		// 	}
		// 	else{
		// 		$subscription=$this->subscribeobj->where('id','=',$subscription_id)->first();
		// 	}
		// }
		// else{
		// 	$subscription=false;
		// }
	}
	$branches=$branch->get();
	$categories=$category->parentCats();
	$pickup=$pick->get();
	$delivery=$del->get();
	$tableview = $users = DB::table('services')
            		    ->get(['services.*']);
	return view('admin.add-orderrequest', compact('tableview','categories','pickup','delivery','branches','subscription'))->with('title','Create order');
}


public function save(Request $request,User $user,Branches $branchobj){
	$inputs=$request->all();
	//$order=$orderobj->find( $inputs['branch']);
	$totalQty = 0;
	$totalPrice = 0;
	$items_w = [];
	$itemqty_w = [];
	$itemprice_w = [];
	$items_dc = [];
	$itemqty_dc = [];
	$itemprice_dc = [];
	$items_ir = [];
	$itemqty_ir = [];
	$itemprice_ir = [];

	$length = count($inputs['product_id']);
	for($i=0;$i<$length;$i++){
		if($inputs['qty_w'][$i]!="" && $inputs['qty_w'][$i]!=0){
			$totalQty +=$inputs['qty_w'][$i];
			array_push($items_w,$inputs['product_id'][$i]);
			array_push($itemqty_w,$inputs['qty_w'][$i]);
			array_push($itemprice_w,$inputs['price_w'][$i]);
		}
		if($inputs['qty_dc'][$i]!="" && $inputs['qty_dc'][$i]!=0){
			$totalQty +=$inputs['qty_dc'][$i];
			array_push($items_dc,$inputs['product_id'][$i]);
			array_push($itemqty_dc,$inputs['qty_dc'][$i]);
			array_push($itemprice_dc,$inputs['price_dc'][$i]);
		}
		if($inputs['qty_ir'][$i]!="" && $inputs['qty_ir'][$i]!=0){
			$totalQty +=$inputs['qty_ir'][$i];
			array_push($items_ir,$inputs['product_id'][$i]);
			array_push($itemqty_ir,$inputs['qty_ir'][$i]);
			array_push($itemprice_ir,$inputs['price_ir'][$i]);
		}
	
		if($inputs['price_w'][$i]!="" && $inputs['price_w'][$i]!=0){
			$totalPrice+=$inputs['price_w'][$i] * $inputs['qty_w'][$i];
		}
		if($inputs['price_dc'][$i]!="" && $inputs['price_dc'][$i]!=0){
			$totalPrice +=$inputs['price_dc'][$i]* $inputs['qty_dc'][$i];
		}
		if($inputs['price_ir'][$i]!="" && $inputs['price_ir'][$i]!=0){
			$totalPrice +=$inputs['price_ir'][$i]* $inputs['qty_ir'][$i];
		}
		
	}

	$userobj=$user->where('active', '=', '1')->where('email', '=',$inputs['email'])-> whereHas(
				'role', function($q){

					$q->where('slug','user');
				}
		)->first();
	if(!$userobj){
		if(Auth::user()->isAdmin()){
			$branch=$inputs['branch'];
			$added_by=Auth::user()->id;
		}
		else{
			$branch=Auth::user()->location;
			$added_by=Auth::user()->id;
		}
		$this->user->email = $inputs['email'];
		$this->user->mobile = $inputs['mobile'];
		$this->user->location = $branch;
		$this->user->added_by = $added_by;
		$this->user->active = 1;
		$this->user->role_id = 2;
		$this->user->confirmed = 1;
		$this->user->password = "iwash1234";
		$this->user->mobile_confirmed = 1;
		$this->user->save();
		if(isset($inputs['fname'])){
		$this->userprofile->first_name = $inputs['fname'];
		$this->userprofile->last_name = $inputs['lname'];
		$this->userprofile->user_id = $this->user->id;
		$this->userprofile->save();
		}

	}	

	if($userobj || $this->user->id){
			$obj = new $this->model;
			$obj->user_id= $userobj?$userobj->id:$this->user->id;
			
			
			if(isset($inputs['use_subscription']) && $inputs['use_subscription']==1){
				$obj->order_type='subscription';
				$obj->subscription_id= $inputs['subscription_id'];
			}else{
			$obj->order_type='simple';
			$obj->service_cat_id= 2;
			}
			


				$obj->qty= $totalQty;
				
				$obj->totalamount= $totalPrice;
				$obj->flatandstreet= $inputs['flatandstreet'];
				$obj->landmark= $inputs['landmark'];
				$obj->addressType= $inputs['addressType'];
				$obj->pickupDate= $inputs['pickupDate'];
				$obj->deliveryDate= $inputs['deliveryDate'];
				$obj->timeOfDelivery= $inputs['timeOfDelivery'];
				$obj->timeOfPickup= $inputs['timeOfPickup'];
				$obj->status= $inputs['status'];
				$obj->payment_mode= $inputs['payment_mode'];
				if($inputs['payment_status']){
					$obj->payment_status= $inputs['payment_status'];

				}
				if( $inputs['payment_status'] && $inputs['payment_date']){
					$obj->payment_date=$inputs['payment_date'];


				}
				if(Auth::user()->isAdmin()){

				$branch=$inputs['branch'];
				$added_by=Auth::user()->id;
				}else{

				$branch=Auth::user()->location;
				$added_by=Auth::user()->id;

				}

				$obj->branch= $branch;
				$obj->added_by= $added_by;

				$obj->updatedAt=date('Y-m-d H:i:s');

				$obj->save();
				//comment out for server 
				// $path=DNS1D::getBarcodePNGPath('Iw'.$obj->id, "C39");
				// 	$filename = basename($path);
				$obj->qr='uploads/orders/' ;
				$obj->save();
				

				if( $obj->id){
					$records=[];
					if($obj->order_type=='simple'){
					//saving srvices for each order
					$dc = [];
					$wa = [];
					$ir = [];
					for($i=0;$i<count($items_dc);$i++){
						$dc[$i] = array('order_id'=>$obj->id,'service_id'=>$items_dc[$i],'qty_dc'=>$itemqty_dc[$i],'price_dc'=>$itemprice_dc[$i]);
					}
					for($i=0;$i<count($items_w);$i++){
						$wa[$i] = array('order_id'=>$obj->id,'service_id'=>$items_w[$i],'qty_w'=>$itemqty_w[$i],'price_w'=>$itemprice_w[$i]);
					}
					for($i=0;$i<count($items_ir);$i++){
						$ir[$i] = array('order_id'=>$obj->id,'service_id'=>$items_ir[$i],'qty_ir'=>$itemqty_ir[$i],'price_ir'=>$itemprice_ir[$i]);
					}

					
					$this->orderservice->insert($dc);
					$this->orderservice->insert($wa);
					$this->orderservice->insert($ir);		

					}
					
				}
			return redirect()->back()->with('ok', 'Order is Created');
			}
			else{
				return redirect()->back()->with('error', 'User Does Not exist!');
			}
	}
}
