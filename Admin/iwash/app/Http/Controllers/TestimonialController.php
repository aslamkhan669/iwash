<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\TestimonialsRepository;
use Illuminate\Http\Request;
use Config;

class  TestimonialController extends Controller {

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;

    /**
     * Create a new AdminController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion
     * @return void
     */
    public function __construct(UserRepository $user_gestion)
    {
		$this->user_gestion = $user_gestion;
			$this->testimonialpath = Config::get('app.uploadPath').'uploads/testimonials/';
	
		$this->middleware('admin');
    }

	/**
	* Show the admin panel.
	*
	* @param  App\Repositories\ContactRepository $contact_gestion
	* @param  App\Repositories\BlogRepository $blog_gestion
	* @param  App\Repositories\CommentRepository $comment_gestion
	* @return Response
	*/
	
    public function addTestimonial(){
		 return view('admin.add-testimonial')->with('title','Add Testimonial');
	}	

	
	 public function saveTestimonial(TestimonialsRepository $category,Request $request){
		  $file = $request->file('profile');
  
               $destinationPath =$this->testimonialpath ;

       $extension = $file->getClientOriginalExtension();
       $filename ='testimonial_'.date('Y-m-d H:i:s') . '.' . $extension;
        $file->move($destinationPath, $filename);
	       $category->store($request->all(),'uploads/testimonials/'.$filename);
       return redirect()->back()->with('ok', 'Testimonial is saved');
	}
	
	 public function getTestimonials(TestimonialsRepository $testimonial){
	         $testimonials=$testimonial->getTestimonials();
	                    $url=Config::get('app.api_url');

	   		 return view('admin.testimonials', compact('testimonials','url'))->with('title','Testimonials');;

	}	
	
		
	
	public function deleteTestimonial(TestimonialsRepository $testimonial,$id){
	      $testimonial->deleteTestimonial($id);
        return redirect()->back()->with('ok', 'Testimonial Deleted');
	}
	
	public function editTestimonial(TestimonialsRepository $testimonial,$id){
	                    $url=Config::get('app.api_url');

	     $testimonial=$testimonial->editTestimonial($id);
	   return view('admin.edit-testimonial', compact('testimonial','url'))->with('title','Edit Testimonial');;
	}
	
	public function updateTestimonial(TestimonialsRepository $testimonial,Request $request,$id){
			$filename='';
		$testobj= $testimonial->getbyId($id);
		if ($request->hasFile('profile')) {
			$file = $request->file('profile');
  
                      $destinationPath =$this->testimonialpath ;

       $extension = $file->getClientOriginalExtension();
       $filename ='testimonial_'.date('Y-m-d H:i:s') . '.' . $extension;
        $file->move($destinationPath, $filename);
			}
		  
	    $testimonial->updateTestimonial($request->all(),$testobj,'uploads/testimonials/'.$filename);
	  return redirect()->back()->with('ok', 'testimonial updated');
	}
	
	
	
	
	
	
}
