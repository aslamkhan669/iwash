<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnlistSubCat extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'enlist_subcat';
	 protected $fillable = array('category_id', 'subcat_id', 'user_id');

public function user() 
	{
		return $this->belongsTo('App\Models\UserProfile','user_id','user_id');
	}

}
