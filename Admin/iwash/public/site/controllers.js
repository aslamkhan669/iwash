var api_url='http://54.202.108.46:3000/api/';
var siteurl='http://54.202.108.46/';


//var api_url='http://localhost:3000/api/';
//var siteurl='http://localhost/';
var items_added = [], quantity_added=0, total=0, tax=0,category_selected ='';

angular.module('iwashApp.controllers', [])
.directive('datepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                dateFormat: 'yy-mm-dd',
                  minDate: new Date(),

                onSelect: function (date) {
                    ngModelCtrl.$setViewValue(date);
                    ngModelCtrl.$render();
                    scope.$apply();
                }
            });
        }
    };
})

.service('LoginService', function($q, $http, $window) {
    var serviceToken, serviceHost, tokenKey;
    tokenKey = 'xaccesstoken';
    if (localStorage.getItem(tokenKey)) {
        serviceToken = $window.localStorage.getItem(tokenKey);
    }
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var credentials = {};
            credentials.username = name;
            credentials.password = pw;
            var credentials = JSON.stringify(credentials);
            $http.post(api_url + 'user/authenticate', credentials).then(function(resp) {

                if (resp.data.success === true) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject(resp.data);
                }

            }, function(err) {
                deferred.reject(resp.data);

            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;

        },
        signup: function(fname, lname, email,password) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            var credentials = {};
            credentials.email = email;
            credentials.password = password;
            credentials.fname = fname;
            credentials.lname = lname;
            var credentials = JSON.stringify(credentials);
            $http.post(api_url + 'user/register', credentials).then(function(resp) {
                if (resp.data.success === true) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject(resp.data);
                }

            }, function(err) {
                deferred.reject(resp.data);

            });
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;

        },
        setAuth: function(authuser) {
            $window.localStorage.setItem('authuser', JSON.stringify(authuser));
        },

        getAuth: function() {
            if (localStorage.getItem('authuser')) {
                var retrievedObject = $window.localStorage.getItem('authuser');
                return JSON.parse(retrievedObject);
            }
        },

        setToken: function(token) {
            serviceToken = token;
            $window.localStorage.setItem(tokenKey, token);
        },

        getToken: function() {
            return serviceToken;
        },

        removeToken: function() {
            serviceToken = undefined;
            $window.localStorage.removeItem(tokenKey);
            $window.localStorage.removeItem('authuser');
        },

        get: function(uri, params) {
            return $http({
                url: uri,
                method: "GET",
                params: {
                    xaccesstoken: serviceToken
                }
            });
        },

        post: function(uri, params) {
            if (params) {
                params = JSON.parse(params);

            } else {
                params = params || {};

            }
            params['xaccesstoken'] = serviceToken;
            params = JSON.stringify(params);
            return $http.post(uri, params);
        }
    };

})

.controller('AppCtrl', function($state,$scope,$rootScope,LoginService,$timeout,GoogleSignin,$http,$anchorScroll,$location,toastr) {
     $rootScope.currentorder = {};
     $scope.currentservice = "";
     $scope.api_url = api_url;
     $rootScope.currentorder.timslot = {};
      $rootScope.isLoggedIn=false;

      $scope.settype=function(type){

            if(type==1){
           $('.login').removeClass('hidden');
             $('.signup').addClass('hidden');


            }else{
                  $('.signup').removeClass('hidden');
             $('.login').addClass('hidden');


            }
        
      }

    $scope.goto= function(id) {
      // set the location.hash to the id of
      // the element you wish to scroll to.
      $location.hash('howitworks');

      // call $anchorScroll()
      $anchorScroll();
    };
    if(LoginService.getAuth()){
        $rootScope.isLoggedIn=true;

    }

    $scope.getauthuser=function(){
		authuser=LoginService.getAuth();
		$scope.authuserid=authuser.userId;
		LoginService.post(api_url+'user/details/'+$scope.authuserid).then(function(resp) {
			$scope.authrole=resp.data.role.id;
			$scope.myinfo = resp.data;
		
			if(resp.data.avatar!='' && resp.data.avatar!=null){
				$scope.profileimage=api_url+''+resp.data.avatar;
				$scope.profileimagemainurl=resp.data.avatar;
				$scope.noprofile=0;
			}else{
				$scope.noprofile=1;

				$scope.profileimage='img/avatar.jpg';
			}
			console.log(resp.data);
		}, function(err) {
						console.error('ERR', err);

		})
    }
 if ($rootScope.myinfo == undefined && LoginService.getToken()) {

        $scope.getauthuser();

    }
 	$scope.logOut = function () {
        LoginService.removeToken();
        $scope.myinfo=undefined;
        $scope.isAuthenticated = false;
        $timeout(function() {
               $state.go('home');
        }, 1000);
        gapi.auth.signOut();

    };
           $scope.data={};
         $scope.register = {};
          
	 $scope.sociallogin = function(social) {
			var social=JSON.stringify(social);
		alert(social);
			$http.post(api_url+'user/loginsocial',social).then(function(resp) {
						//alert(JSON.stringify(social));
						if(resp.data.success==true){
								LoginService.setAuth(resp.data);
								LoginService.setToken(resp.data.token);
								$scope.signedIn = true;
							 $rootScope.isLoggedIn=true;
								$scope.getauthuser();
								//$scope.$apply();
							  

						}else{

                           toastr.error(resp.data.message, 'Error');

							
						}
			}, function(err) {


			});
        }
        


   $rootScope.facebookLogin = function() {
        //alert(333333333333333);
        FB.login(function(response) {
            if (response.authResponse) {
                //console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function(response) {
                    //console.log(response);
                    //console.log('Good to see you, ' + response.name + '.');
                    var accessToken = FB.getAuthResponse().accessToken;
                    ////console.log(accessToken);
                    var obj = {};
                    obj.network = 'facebook';
                    obj.socialToken = accessToken;
                    $scope.sociallogin(obj);

                });
            } else {
                //console.log('User cancelled login or did not fully authorize.');
            }
        });

    }
	

    $rootScope.googleLogin = function() {
        GoogleSignin.signIn().then(function(result) {

            var obj = {};
            obj.network = 'gplus';
            obj.socialToken = result.Zi.access_token;
            $scope.sociallogin(obj);
        }, function(error) {
            //console.log(error);
        });
    }



  
	$scope.login = function(){
			if($scope.data.username!=null && $scope.data.password!=null){
				
					var mobile=$scope.data.username;
					var password=$scope.data.password;
					LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
						  if(data.success==true && data.message=='mobile_verify'){
								sessionkey=data.session_key;
								verifymob=mobile;
								afterresetuser.username=mobile;
								afterresetuser.password=password;
								console.log(afterresetuser);						  
								$state.go('resetmobile');

							}else if(data.success==true){
								LoginService.setAuth(data);
                                LoginService.setToken(data.token);
                                 $rootScope.isLoggedIn=true;
                               	$scope.getauthuser();

                                $scope.data.username='';
                                $scope.data.password='';
                                $scope.signedIn = true;
                                     

								

							}else{
							 
								 if(data.success==false){

                                      toastr.error(data.message, 'Error');


                                    
                                 }

							 }
							
									

						}).error(function(data) {
							
                              toastr.error(data.message, 'Error');
						});
			}else{
					
			    toastr.error('All fields required', 'Error');

			}
	};
		$scope.signup = function() {
                     var  registermob=$scope.register.mobile;
                      var  registeremail=$scope.register.email;
				 var  registerpassword=$scope.register.password;
				if($scope.register.fname!=null && $scope.register.email!=null && $scope.register.lname!=null && $scope.register.password!=null ){
						LoginService.signup($scope.register.fname, $scope.register.lname,$scope.register.email,$scope.register.password).success(function(data) {
								
						    if(data.success==true && data.message=='mobile_verify'){
								verifymob=registermob;
								sessionkey=data.session_key;
								$scope.register={};							 
								afterresetuser.username=registermob;
								afterresetuser.password=registerpassword;
								console.log(afterresetuser);
								$state.go('resetmobile');
                            }else if(data.success==true && data.message!='mobile_verify'){
                                $scope.register={};
                                $scope.data.username=registeremail;
                                $scope.data.password=registerpassword;    
                                $scope.login(); 
                                //$state.go('app.home');

                            }else{
								 if(data.success==false){

                                      toastr.error(data.message, 'Error');


                                    
                                 }
						       

							 }
						}).error(function(data) {
                              toastr.error(data.message, 'Error');
							  
						});
				}else{
								    toastr.error('All fields required', 'Error');
	
				}

		}; 


  
	
  
})



.controller('FaqCtrl', function($scope, $http, $state,$stateParams) {

    $scope.getFaq=function(){
        $http.get(api_url+'faqs/catwithQuestion').then(function(resp) {    
                $scope.faqs=resp.data;
            }, function(err) {
                    console.error('ERR', err);
                    console.log(err);

            });
        }
    $scope.getFaq();
})

.controller('LoginCtrl', function($scope,$http,$scope, LoginService,$state,$cordovaOauth,$timeout) {
 

    })
.controller('HomeCtrl', function($scope, $http, $state,$stateParams) {

   $scope.getbanner = function() {
        $http.get(api_url + "banner").then(function(resp) {
            console.log(resp);
            $scope.banners = resp.data;
        }, function(err) {
            console.error("ERR", err);
            console.log(err);
        });
    };

  //  $scope.getbanner();


	$scope.getTestimonial = function() {
        $http.get(api_url + "testimonials").then(function(resp) {
          console.log(resp);
          $scope.testimonials = resp.data.result;
        }, function(err) {
          console.error("ERR", err);
          console.log(err);
        });
    };

    $scope.getTestimonial();
    


    $scope.getserviceshome=function(catid){
        $http.get(api_url+'services/'+catid).then(function(resp) {     
                $scope.serviceshome=resp.data;
            }, function(err) {
                console.error('ERR', err);
                console.log(err);

        });
    }


		$scope.getserviceshome(1);


}).controller('ServiceFeaturesCtrl', function($scope, $http, $state,$stateParams) {
  //$ionicSideMenuDelegate.canDragContent(false); 

	$scope.getserviceFeatures=function(catid){
        $http.get(api_url+'services/features/list').then(function(resp) {
                $scope.servicesfeatures=resp.data;    
            }, function(err) {
                console.error('ERR', err);
                console.log(err);

        });
	}


		$scope.getserviceFeatures();

})
    
.controller('PlaceOrderCtrl', function($scope, $http, $state,$stateParams,$rootScope,LoginService,$stateParams,$location) {

    $scope.$on('$ionicView.enter', function(ev) {
        if(ev.targetScope !== $scope)
            return;
        if($rootScope.currentorder.service==undefined || $rootScope.currentorder.service=='' || $rootScope.currentorder.service==null){
        
                $location.path('/app/home');

                }
    });
    

    $scope.getservicesforOrder=function(catid){
        $http.get(api_url+'services/'+catid).then(function(resp) {     
                $scope.services=resp.data;
            }, function(err) {
                console.error('ERR', err);
                console.log(err);

        });
    }


		$scope.getservicesforOrder(1);

    $scope.getdeliverytime=function(){
        LoginService.get(api_url+'order/deliverytime').then(function(resp) {      
        $scope.deliverytime=resp.data;
        }, function(err) {
            console.error('ERR', err);
            console.log(err);

        });
    }
  //  $scope.deliverytime();
    $scope.getpickuptime=function(){
            LoginService.get(api_url+'order/pickuptime').then(function(resp) {      
            $scope.pickuptime=resp.data;

            }, function(err) {
                console.error('ERR', err);
                console.log(err);

            });
    }
  //  $scope.pickuptime();

  $scope.placeOrder=function(order){

            var obj={};
            obj.flatandstreet=order.address.street;
            obj.landmark=order.address.landmark;
            obj.addressType=order.address.addresstype;
            obj.pickupDate=order.timslot.pickupdate;
            obj.deliveryDate=order.timslot.deliverydate;
            obj.timeOfDelivery=order.timslot.deliverytime.id;
            obj.timeOfPickup=order.timslot.pickuptime.id;
             obj.service_id=order.service.id;

                   var data=JSON.stringify(obj);
              LoginService.post(api_url+'order/place',data).then(function(resp) {
                if(resp.data){

                   $rootScope.currentorder={};
                               $location.path('/app/order/success/'+resp.data.id);

                }
        /*    $cordovaToast.showLongCenter('Order placed successfully!').then(function(success) {
                  



                  }, function (error) {

                  });*/

              }, function(err) {
                  console.error('ERR', err);

              });

  }
            

})
.controller('orderSuccessCtrl', function($scope, $http, $state,$stateParams,$ionicNavBarDelegate) {
     // $ionicNavBarDelegate.showBackButton(false);

$scope.placedorder=$stateParams.orderid;

})
.controller('OrderDetailsCtrl', function($scope, $http, $state,$stateParams,LoginService,$cordovaToast) {


    $scope.getOrderDetails=function(){              

        LoginService.get(api_url+'order/get/'+$stateParams.orderid).then(function(resp) {      
            
                $scope.order=resp.data

            }, function(err) {
                console.error('ERR', err);
                console.log(err);

            });
    }

    if($stateParams.orderid){
    $scope.getOrderDetails();

    }

    $scope.orderCancel=function(orderid){
        LoginService.get(api_url+'order/cancelled/'+orderid).then(function(resp) {      
            $cordovaToast.showLongCenter('Order cancelled').then(function(success) {
                        $scope.order.status='cancelled';
                }, function (error) {

                });
        

            }, function(err) {
                console.error('ERR', err);
                console.log(err);

            });
    }
})


.controller('AllOrderCtrl', function($scope, $http, $state,$stateParams,LoginService) {

    var page=1;
    $scope.moreresults =true;


$scope.getAllorders=function(pageno){
            var obj={};
            obj.page=pageno;
            obj.limit=3;  
             var data=JSON.stringify(obj);

        LoginService.post(api_url+'order/getAll',data).then(function(resp) {   
          
          if(resp.data.length==0 && page==1 ){
                $scope.notfound=true;
            }
          if(resp.data.length){
               $scope.notfound=false;
              if(resp.data.length < obj.limit){
                  $scope.moreresults =true;
              }else{
                  $scope.moreresults =false;
              }
          if(page!=1 ){
                  for (var i=0; i<resp.data.length; i++){
                      $scope.allorders.push(resp.data[i]);
                  }
                  $scope.$broadcast('scroll.infiniteScrollComplete');
              }else{
                  $scope.allorders=resp.data;
              }

          }else{
             $scope.moreresults =true;
          }
         }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }
    $scope.getAllorders(page);
    $scope.loadMoreOrders= function() {
        page++;
        $scope.getAllorders(page);

    };

    $scope.orderCancel=function($index,orderid){
        LoginService.get(api_url+'order/cancelled/'+orderid).then(function(resp) {      

                        $scope.allorders[$index]['status']='cancelled';


             
            }, function(err) {
                console.error('ERR', err);
                console.log(err);

            });
    }
  //  $scope.getAllorders();

})

.controller('RateCtrl', function($scope, $http, $state,$stateParams) {

    function setCartData(){
  
        $("#quantity").text(quantity_added);
        $("#total").text(total);
        var tax = (total * 14.5)/100;
        $("#tax").text(tax);
    }
    function vibrateCart(){
        var i=0;
        var myInterval = setInterval(function(){
            if(i++>=10)
                clearInterval(myInterval);
            if(i%2==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(15deg)",
                    "-moz-transform": "rotate(15deg)",
                    "transform": "rotate(15deg)",
                    "-o-transform": "rotate(15deg)",
                    "-ms-transform": "rotate(15deg)"
                });        
            }
            else if(i%3==0){
                $(".estimator img").css({
                    "-webkit-transform": "rotate(-15deg)",
                    "-moz-transform": "rotate(-15deg)",
                    "transform": "rotate(-15deg)" ,
                    "-o-transform": "rotate(-15deg)",
                    "-ms-transform": "rotate(-15deg)"
                });   
            }
            else{
                $(".estimator img").css({
                    "-webkit-transform": "rotate(0deg)",
                    "-moz-transform": "rotate(0deg)",
                    "transform": "rotate(0deg)",
                    "-o-transform": "rotate(0deg)",
                    "-ms-transform": "rotate(0deg)"
                });   
            }
        }, 100);
     }  
     
   $scope.getservices=function(catid){
        $http.get(api_url+'services/'+catid).then(function(resp) {      
          console.log($scope.categories);
                        $scope.services=resp.data;
                        $scope.categories[0].service=$scope.services;
                      //  $ionicSlideBoxDelegate.update();
          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }
    $scope.getcats=function(){
        $http.get(api_url+'categories/parent').then(function(resp) {      
          
             $scope.categories=resp.data.result;
              $scope.getservices(1);
            for (i=0; i< $scope.categories.length;i++){

              console.log($scope.categories);
                     items_added[$scope.categories[i]['id']] = new Array();   
            }
            console.log('items_added');
console.log(items_added);
//$ionicSlideBoxDelegate.update();
     console.log('items_added');
          }, function(err) {
              console.error('ERR', err);
              console.log(err);

          });
     }


    $scope.getcats();

      $scope.increment=function(categoryid,itemid,price){

          var item_label =itemid;

          if(items_added[categoryid][item_label]){
          items_added[categoryid][item_label]++;
          this.service.quantity=items_added[categoryid][item_label];
          }else{
          items_added[categoryid][item_label]=1;
          this.service.quantity=items_added[categoryid][item_label];

          }    
          if(quantity_added==0){
             $(".estimator").fadeIn(1500);

          }
             
          quantity_added++;
          total +=  Number(price);
          setCartData();
          vibrateCart();
        //  $ionicSlideBoxDelegate.update();

       }


$scope.decrement=function(categoryid,itemid,price){

 var item_label =itemid;

                if(items_added[categoryid][item_label]>0){
                    items_added[categoryid][item_label]--;
                    quantity_added--;
                    total -= Number(price);
                    setCartData();
                    if(quantity_added==0)
                        $(".estimator").fadeOut(1000);
                }
              this.service.quantity=items_added[categoryid][item_label];
                vibrateCart();
              //  $ionicSlideBoxDelegate.update();

}

    $scope.getservicesbycat=function($index,catid){
         console.log($index);
         var curr= $scope.categories[$index];
        if(curr.service==undefined){
            $http.get(api_url+'services/'+catid).then(function(resp) {      
                $scope.services=resp.data;
                if(resp.data.length){
                    curr.service=resp.data;

                }
            //  $ionicSlideBoxDelegate.update();
            }, function(err) {
                    console.error('ERR', err);
                    console.log(err);

            });

        }
     }


}); 