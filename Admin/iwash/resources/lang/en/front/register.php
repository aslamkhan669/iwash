<?php

return [
	'title' => 'Sign Up',
	'infos' => 'To register please fill this form :',
	'first_name' => 'Your  First Name',
	'last_name' => 'Your Last Name',
	'mobile' => 'Your Mobile Number',
	'email' => 'Your email',
	'pseudo' => 'Your Nickname',
	'password' => 'Your password',
	'confirm-password' => 'Confirm your password',
	'warning' => 'Attention',
	'warning-name' => '30 characters maximum',
	'warning-password' => 'At least 8 characters',
	'ok' => 'You have been registered !',
	'error'=> 'These credentials do not match our records.'
];