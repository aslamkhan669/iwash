<?php

return [
	'title' => 'Contact form',
	'text' => 'If you wish to send a message please fill this form :',
	'name' => 'Your name',
	'email' => 'Your email',
	'message' => 'Your message',
	'ok' => 'Thanks for contacting us, we will respond as soon as possible.'
];