
@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" action=''>
								{{ csrf_field() }}

                  <div class="box-body">
                    <div class="form-group">
                      <label >Package Name</label>
                      <input type="text" required class="form-control" placeholder="For eg.(Traditional)" name='package' id="exampleInputEmail1" required >
                    </div>
                    <!-- <div class="form-group">
                      <label >Usage Limit</label>
                      <input type="text" required class="form-control"  placeholder="For eg.(50 Thobs,50 Srwal)"  name='usage_limit' id="exampleInputEmail1" required >
                    </div> -->
                    <div class="form-group">
                      <label >Pickup Options(Enter value as Comma Seperated)</label>
                      <input type="text" required class="form-control" placeholder="For eg.(Alternative,Weekly)"  name='pickup_options' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Available Packages(Enter value as Comma Seperated)</label>
                      <input type="text" required class="form-control" placeholder="For eg.(1,3,6,12)"  name='available_packages' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Pricings(For each month)</label>
                      <input type="text" required class="form-control" placeholder="For eg.(700)"   name='pricing_for_each_month' id="exampleInputEmail1" required >
                    </div>
                 

                    <div class="form-group">
                      <label >Category</label>
                      <?php foreach($categories as $category){ ?>
                        
                      <div class="checkbox packagecategory"> 
                      <label for="<?= $category->id;?>"><input  id="<?= $category->id;?>" type="checkbox" class="category" name='category_id[]'  value="<?= $category->id;?>"><?= $category->name;?></label>
                    </div>
                    <div class="services">


                    </div>
                    <?php } ?>
                    
                    </div>
                    

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                
      
    <style>
    .services .quantity {


      width:45px;
    }
    .services{

      margin-left: 2%;
    }
    </style>
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
