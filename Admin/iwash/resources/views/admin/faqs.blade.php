@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">FAQS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>Faq Category</th>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Date</th>

                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
            
            @foreach ($faqs as $message)



                      <tr>
                   <td>{{ @$message->FaqCategory->name}}</td>
                          <td>{{ @$message->question}}</td>
                        <td>{{ @$message->answer}}</td>
                     
                         <td> 
                    {{ $message->created_at}}
                        
                        </td>
                        <td>
                          
                            <a class="btn btn-danger" href="{{URL::to('faqs/'.$message->id.'/edit')}}">Edit</a>
                                <a class="btn btn-danger" href="{{URL::to('faqs/'.$message->id.'/delete')}}">Delete</a>
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                  {!! $faqs->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        
@stop
