@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          
              <div class="box">
                <div class="box-header">
                <button onclick="goBack()">Go Back</button>

                </div><!-- /.box-header -->
                <div class="box-body">
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Branch</th>
                          <th>Registration Date </th>
                         
                          <th>Status</th>
                      <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						@foreach ($branchadmins as $admin)

   
   



                      <tr>
                      <td>{{$admin->userProfile->first_name}}</td>
                      <td>{{$admin->userProfile->last_name}}</td>
                      <td>{{$admin->email}}</td>
                       <td>{{$admin->mobile}}</td>
                       <td>{{@$admin->userlocation->branch}}({{@$admin->userlocation->location}})</td>
                        <td>{{$admin->createdAt}}</td>
                         
                           
                            <td>
                              
                                                        <span class="label label-<?php echo $admin->active==1?'success':'danger' ?>"><?php echo $admin->active==1?'Active':'Inactive' ?></span>

                       </td>
                     <td>


                     <a class="btn btn-success" href="/branch/admin/{{ $admin->id }}/edit">Edit</a>
                            <a class="btn btn-danger" href="/branch/admin/{{ $admin->id }}/delete">Delete</a>

                     </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                  {!! $branchadmins->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
			    $('.getsubcat').click(function(){
			    $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
		});
     
        
        </script>
        
@stop
