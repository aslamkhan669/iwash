@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                </div><!-- /.box-header -->
                <!-- form start -->
<form role="form" method='post' enctype="multipart/form-data" action='/add-category'>
								{{ csrf_field() }}

                  <div class="box-body">
                    <div class="form-group">
                      <label >Category Name</label>
                      <input type="text" class="form-control"  name='name' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Category Description</label>
                    <textarea class="form-control"  name='description' required></textarea>
                    </div>
                      <div class="form-group">
                      <label >Category Image</label>
                      <input type="file" class="form-control"  name='cat_image' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label > Banner Image</label>
                      <input type="file" class="form-control"  name='image_banner' id="exampleInputEmail1" required >
                    </div>

                     <div class="form-group">
                      <label > Image Website Home</label>
                      <input type="file" class="form-control"  name='image_website' id="exampleInputEmail1" required >
                    </div>
                      <div class="form-group">
                      <label > Image App Home</label>
                      <input type="file" class="form-control"  name='image_app' id="exampleInputEmail1" required >
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
