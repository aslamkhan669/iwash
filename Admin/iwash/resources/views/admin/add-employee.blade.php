@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->

  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                <button onclick="goBack()">Go Back</button>

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method='post' action=''>
								{{ csrf_field() }}

                  <div class="box-body">

       
            <div class="form-group">
              <label>First Name</label>
              <input type="text" class="form-control" name='first_name' required>
            </div>

            <div class="form-group">
              <label>Last Names</label>
              <input type="text" class="form-control" name='last_name' required>
            </div>


                    <div class="form-group">
                      <label >Email</label>
                      <input type="email" class="form-control"  name='email' autocomplete='off' required >
                    </div>
                      <div class="form-group">
              <label>Mobile</label>
              <input type="tel" class="form-control" name='mobile' required>
            </div>
                    <div class="form-group">
                      <label >Password</label>
                       <input type="text" class="form-control"  name='password' autocomplete='off' required >

                    </div>
                  <?php   if(Session::get('status')=='admin'){ ?>
                    <div class="form-group">
                      <label >Branch </label>
                      <select name='location' class="form-control">
						  <?php foreach($branches as $parent){ ?>
							   <option value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>
                  
							 <?php } ?>
                       </select>
                    </div>
         <?php } ?>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                
          @if(session()->has('ok'))
          @include('partials/error', ['type' => 'success', 'message' => session('ok')])
          @endif	
          @if(isset($info))
          @include('partials/error', ['type' => 'info', 'message' => $info])
          @endif
          @if(session()->has('error'))
          @include('partials/error', ['type' => 'danger', 'message' => session('error')])
          @endif	

              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
