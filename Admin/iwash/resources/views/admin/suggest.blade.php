@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">

                </div><!-- /.box-header -->
                <div class="box-body">
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>From</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Location</th>
                        <th>Category</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						@foreach ($suggest as $value)

                      <tr>
                        <td>
                  <?php 
                            $usertype=$value->users->role->slug;
                             if($usertype=='admin'){
                              $url='';
                            }else{
                           $url='service-'.$usertype.'/'.$value->from_id;
                            }

                             ?>
                            <a href="{{$url}}">	 {{ $value->users->userProfile->first_name.' '.$value->users->userProfile->last_name }}	
                        
                         </a>
                        </td>
                          <td>
						 {{ $value->name }}	
                        </td>
                           <td>
							 {{ $value->mobile }}	
                        </td>
                           <td>
							 {{ $value->email }}	
                        </td>
                           <td>
							 {{ $value->location }}	
                        </td>
                        <td>
							 {{ $value->category->name }}	
                        </td>
                        <td> 
							{{ $value->created_at }}
						 </td>
							   <td>
								    <div class="checkbox icheck">
                <label>
                 								   <input type='checkbox'  name="active" <?php if($value->active==1){ echo 'checked';} ?> >

                </label>
              </div>
								   
                    
                        
                        </td>
                        <td>
                            <a class="btn btn-danger" href="/suggested-service-provider/{{ $value->id }}/delete">Delete</a>
                          
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                     <div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	{!! $suggest->render() !!} </div>   
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
			    $('.getsubcat').click(function(){
			    $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
		});
     
        
        </script>
        
@stop
