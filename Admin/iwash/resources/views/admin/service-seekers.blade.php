@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Service Seekers</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Location</th>
                        
                        <th>Reviewed </th>
                           <th>Followers </th>
                       
                            <th>Qoute Requested by me</th>
                            <th>Registration Date </th>
                          
                            <th>Status </th>
                        <th>Action</th> 

                      </tr>
                    </thead>
                    <tbody>
                  
                   @foreach ($seekers as $seeker)
                      <tr>
                     <td>{{$seeker->userProfile->first_name.' '. $seeker->userProfile->last_name }}</td>
                          <td>{{$seeker->userProfile->location}}
                          </td>
                      
                          <td>{{$seeker->reviewGiven->count()}}</td>
                           
                           <td>{{$seeker->followers->count()}}</td>
                     
                         <td>{{$seeker->requestQuote->count()}}</td>
                         <td>{{$seeker->created_at}}</td>
                        
                        	 <td>
                                 <input type="checkbox" <?php if($seeker->active==1){ echo 'checked';} ?>>
                        </td>

                      <td>
                         
                           <a class="btn btn-default" href="service-seeker/{{$seeker->id}}">View Profile</a>
                          
                        
                        </td>
                      </tr>
                  @endforeach
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             <div class="col-lg-6"> {!! $seekers->render() !!} </div>   

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        
@stop
