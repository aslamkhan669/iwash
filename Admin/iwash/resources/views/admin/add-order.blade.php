@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->


  <div class="row">
            <div class="col-md-12">

<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" name="orderform" id="orderform" enctype="multipart/form-data" action=''>
								{{ csrf_field() }}

                  <div class="box-body">

                  <div class="col-lg-12">

                <div class="col-lg-4">
                <label >Customer Email </label>

    <input type="text" class="form-control" id="emailcustomer" value="<?php echo @$_REQUEST['email'] ; ?>"  autocomplete="off" required name="emailcustomer" placeholder="">
                </div>
  <?php
//print_r($subscription->SubscribeServices);
if($subscription!=false){
 ?>
  <div class="col-lg-4">
                <label >Use Subscription</label>
    <select name='use_subscription' id="use_subscription" class="form-control" required placeholder="Use Subscription">
    <option value="">Select</option>
    <option value="1">Yes</option>
    <option value="0">No</option>
    </select>

                </div>
    <div class="col-lg-4 subscriptions hidden">
                <label >Subscription</label>
				 <input type="hidden" value="<?php echo $subscription->id; ?>"  readonly class="form-control" id="subscription_id" required name="subscription_id" >
                <input type="text" value="<?php echo $subscription->Package->package;?>"  readonly class="form-control" id="subscription" required name="subscription" >


                </div>
			<?php } ?>	
		<?php if(isset($_REQUEST['email'])){?>		
	 <div class="col-lg-4 servicecat <?php if($subscription!=false){ echo 'hidden';} ?> ">
                <label >Service Type</label>
    <select id="service_cat_id" name='service_cat_id' class="form-control" required placeholder="Service Type">
	<option value="" >Select</option>
    <?php

    foreach($categories as $category){ ?>
    <option  value="<?= $category->id;?>" ><?= $category->name;?></option>

    <?php } ?>
    </select>

                </div>

<?php } ?>

                </div>
				 <div class="otherinfo <?php if(!isset($_REQUEST['email'])){ echo 'hidden';} ?> ">
            <div class="col-lg-12 services">
<?php if($subscription!=false){ ?>
   <div class="col-lg-6 subscriptonservices hidden ">
        <label >Services: </label>

        <?php foreach( $subscription->SubscribeServices as $services){?>
                <div>
                <input type="hidden" name="services[]" value="<?php echo $services->service_id;?>"> 
                <input  type="number" min=0 price="<?php echo trim($services->Service->price); ?>" name="quantity_<?php echo $services->service_id;?>"  class="quantity" max=<?php echo (int)$services->quantity; ?> required>
                
                <?php 
                  echo $services->Service->price ." QRS Per {$services->Service->per}-". $services->Service->name.'('.$services->category->name.')   ';
				  echo 'Total Remaining Quantity: '.$services->quantity;

?>
      </div>  <?php } ?>

        </div> 
		<?php } ?>
                <div class="col-lg-6 categoryservices hidden">
  <label >Services: </label>
  
<div class="servicelist">
	  
</div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Address Type</label>
  <select class="form-control" required id="sel1" name="addressType" placeholder="Address type">
    <option value="">Select Address type</option>
    <option value="home" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='home'){ echo 'selected';} ?>>Home</option>
    <option value="office" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='office'){ echo 'selected';} ?>>Office</option>
    <option value="other" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='other'){ echo 'selected';} ?>>Other</option>
  </select>
                </div>
                <div class="col-lg-6">
                <label >Flat and Street</label>
    <input type="text" required class="form-control" name="flatandstreet" placeholder="Flat and Street" value="<?php echo @$_GET['flatandstreet']; ?>">

                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Landmark</label>

    <input type="text" required class="form-control"  name="landmark"  placeholder="Landmark" >
                </div>
                <div class="col-lg-6">
                <label >Pickup Date</label>

    <input type="date" required class="form-control" name="pickupDate"  placeholder="Pickup Date"  >
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Pickup  Time</label>
    <select name='timeOfPickup' required class="form-control" >
    <option value="">Select</option>
    <?php

    foreach($pickup as $pick){ ?>
    <option  value="<?= $pick->id;?>" ><?= $pick->timebetween;?></option>

    <?php } ?>
    </select>
                </div>
                <div class="col-lg-6">
                <label >Delivery Date</label>

    <input type="date"  required class="form-control" name="deliveryDate"  placeholder="Delivery Date"  >
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Delivery  Time</label>
    <select name='timeOfDelivery' required class="form-control" >
    <option value="">Select</option>
    <?php

    foreach($delivery as $del){ ?>
    <option  value="<?= $del->id;?>" ><?= $del->timebetween;?></option>

    <?php } ?>
    </select>
                </div>
                <div class="col-lg-6">
                <label >Total Amount</label>

    <input type="text"   class="form-control totalamount" name="totalamount"  placeholder="Total Amount"  >
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Quantity </label>

    <input type="number"  required class="form-control totalquantity" name="qty"  placeholder="Quantity"  >
                </div>
                <div class="col-lg-6">
                <label >Order Status</label>
    <select class="form-control" required id="sel1" name="status" >
    <option value="">Select Order Status</option>
    <option value="placed" >Placed</option>
    <option value="accepted" >Accepted</option>
    <option value="picked up" >Picked Up</option>
    <option value="processing" >Processing</option>
    <option value="ready to deliver" >Ready to delivery</option>
    <option value="delivered" >Delivered</option>
    <option value="cancelled">Cancelled</option>
  </select>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-6">
                <label >Payment Status</label>
    <select class="form-control"  id="payment_status"  name="payment_status"  required>
    <option value="">Select Payment Status</option>
    <option value="0" >Pending</option>
    <option value="1" >Paid</option>


  </select>
                </div>
                <div class="col-lg-6">
                <label >Payment Date: </label>

    <input type="date"   class="form-control" value="<?php echo  date('Y-m-d');?>"  name="payment_date"   placeholder="Payment Date"  >
                </div>
            </div>
 <?php   if(Session::get('status')=='admin'){ ?>
                    <div class="col-lg-12">
                         <div class="col-lg-6">
                      <label >Branch </label>
                      <select name='branch' class="form-control">
                          <?php foreach($branches as $parent){ ?>
                               <option value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>

                             <?php } ?>
                       </select>
                   </div>
                          <div class="col-lg-6">
                               </div>
                    </div>
         <?php } ?>


 </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer otherinfo  <?php if(!isset($_REQUEST['email'])){ echo 'hidden';} ?> ">
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   </div>
                  </div>
                </form>


              </div><!-- /.box -->


            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <style>

.col-lg-12 {
    padding-bottom: 1%;

}

</style>
  <style>
    .services .quantity {


      width:45px;
    }
   
    </style>
@stop
