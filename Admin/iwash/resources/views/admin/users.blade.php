@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  



  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
              <form class="form-inline" method="get">

              <div class="form-group">
              <input type="text" class="form-control" placeholder="First Name" name='first_name' value="<?php echo @$_GET['first_name']; ?>">
            </div>
             <div class="form-group">
              <input type="text" class="form-control" placeholder="Last Name" name='last_name' value="<?php echo @$_GET['last_name']; ?>">
            </div>
             <div class="form-group">
              <input type="text" class="form-control" placeholder="Mobile" name='mobile' value="<?php echo @$_GET['mobile']; ?>">
            </div>
              <div class="form-group">
              <input type="text" class="form-control" placeholder="Email" name='email' value="<?php echo @$_GET['email']; ?>">
            </div>
  <button type="submit" class="btn btn-default">Submit</button>

</form>
  
                <div class="box-header">
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th>UserId</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                                          <th>Branch</th>

                               <th>Source</th>

                               
                            <th>Registration Date </th>
                           
<!--
                        <th>Status</th>
-->

                      </tr>
                    </thead>

                    <tbody>
                          @foreach ($users as $user)
                      <tr>
                      <td>{{$user->id}}</td>
                       <td>{{$user->userProfile->first_name}}</td>
                       <td>{{$user->userProfile->last_name}}</td>
                       <td>{{$user->email}}</td>
                        <td>{{$user->mobile}}</td>
                       <td>{{@$user->userlocation->branch}}-{{@$user->userlocation->location}}</td>

                               <td>
                                <?php 
                              if($user->added_by!=NULL && $user->location!=NULL){

                                     if($user->addedby->role->slug=='admin'){
                                       echo 'Added by Super Admin';

                                     }else{
                                       echo $user->addedby->email.'(' .$user->addedby->userlocation->branch.'-'.$user->addedby->userlocation->location.')<br>';
                                      echo 'Branch Admin';
                                     }
                               }else{

                                  echo 'Website/App';

                               }

                               
                              ?>

                                </td>
                                 
                         <td>{{$user->createdAt}}</td>
                          
                            
                             <td>
                                                             
                              <a class="btn btn-default btn-sm" href="{{URL::to('user/'.$user->id.'/view')}}">View Order Details</a>
                        </td>
                      
                      </tr>
                  @endforeach
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                           <div class="col-lg-6"> {!! $users->render() !!} </div>   


             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        
@stop
