
@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->

 <div class="box box-primary">
 @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" method='post'  enctype='multipart/form-data'>
        {{ csrf_field() }}
        <div class="box-body">
          <div class="form-group">
          <div class="col-lg-12">
            <div class="col-lg-6">
          <input type="hidden" name="email" class="form-control" id="email">
          <input type="hidden" name="id" id="id" class="form-control">
          </div>
          </div>
            <div class="col-lg-12">
            <div class="col-lg-6">
              <label>Email</label>
              <input type="text" class="form-control" name='searchemail' id='searchemail' required>
              </div>
             
          
              <div class="col-lg-6">
              <label>Mobile</label>
              <input type="text" class="form-control" name='mobile' id='mobile' >
              </div>
            </div>
            <div class="col-lg-12" >
            <div class="col-lg-6" id="isuserexist" >
           <!-- <span  id="isuserexist" ></span>-->
            </div>
            <div class="col-lg-6">
            </div>
            </div>
            <div class="col-lg-12">
            <div class="col-lg-6">
              <label>First Name</label>
              <input type="text" class="form-control" name='first_name' id='firstname' >
              </div>
              <div class="col-lg-6">
              <label>Last Name</label>
              <input type="text" class="form-control" name='last_name' id='lastname'  >
              </div>
            </div>
            <div class="col-lg-12">
            <div class="col-lg-6">
              <label>Package name</label>
              <select name="package" class="form-control" id="item" required>
              <option value="">Package</option>
              @foreach($itemlist as $item)
            <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
          </select >
              </div>
              <div class="col-lg-6">
              <label>Qty</label>
              <input type="text" class="form-control" name='qty_clothes' id="qty_clothes"  readonly="readonly">
              </div>
            </div>
            <div class="col-lg-12">
            <div class="col-lg-6" id="ispackageexist">
            </div>
            <div class="col-lg-6">
            </div>
            </div>
        <div class="col-lg-12">
            <div class="col-lg-6">
              <label>Description</label>
              <input type="text" class="form-control" name='description' id="description"  readonly="readonly" >
              </div>
              <div class="col-lg-6">
              <label>Price</label>
              <input type="number" class="form-control" name="price" id="price"  readonly="readonly" >
              </div>
            </div>
          <!--  <div class="col-lg-12">
            <div class="col-lg-6">
            <label>Start Date</label>
            <input type="text" class="form-control" name="start_date" id="startdate" placeholder="Start Date" onfocus="(this.type='date')" value="">
            </div>
            <div class="col-lg-6">
            <label>End Date</label>
            <input type="text" class="form-control" name="end_date" id="enddate" placeholder="End Date" onfocus="(this.type='date')" value="">
            </div>
           </div> -->
           <div class="col-lg-12">
           <div class="col-lg-6">
            <label>Duration</label>
            <select name="duration" class="form-control" id="duration" required>
              <option value="">Duration</option>
            <option value="1">1 Month</option>
            <option value="2">2 Month</option>
            <option value="3">3 Month</option>
            <option value="4">4 Month</option>
            <option value="5">5 Month</option>
            <option value="6">6 Month</option>
            <option value="7">7 Month</option>
            <option value="8">8 Month</option>
            <option value="9">9 Month</option>
            <option value="10">10 Month</option>
            <option value="11">11 Month</option>
            <option value="12">12 Month</option>
            </select >
            </div>
            <div class="col-lg-6">
            <label>Amount Paid</label>
            <input type="number" class="form-control" name="amount_paid" id="amountpaid"  readonly="readonly">
            </div>
            </div>
        </div>
        </div>
      
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" id ="buttonclick" class="btn btn-primary">Add Subscription this users</button>
        </div>
      </form>
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      <script>


$(document).ready(function(){

$("#amountpaid").change(function(){
  var amountpaid = $("#amountpaid").val();
  var priceofpackage = $("#price").val();
  var comp = priceofpackage - amountpaid;
  if( comp < 0)
{
  $("#buttonclick").attr('disabled','disabled');
alert("Amount paid shouldn't be greater than the price of package price")
}
else
{
  $("#buttonclick").removeAttr('disabled');
}
})


    $("#item").change(function(){
        var $id = $(this).val();
       
       
            $.ajax({
                type: "GET",
                url: "/Simplesubscription/"+$id,
                cache: false,
                contentType:"application/json",
                success: function(data) {
                    
                    $("#qty_clothes").val( data.qty_clothes );
                    $("#description").val( data.description );
                    $("#price").val( data.price );
                    
                }
            });
        
    })

    $("#duration").change(function(){
      debugger;
        var duration = $(this).val();
        var price = $("#price").val();
        $('#amountpaid').val(duration * price);        
    })

    $("#searchemail").change(function(){
      debugger;
        var $email = $(this).val();
       
       
            $.ajax({
                type: "GET",
                url: "/GetCustomerInfo/"+$email,
                cache: false,
                contentType:"application/json",
                success: function(data,isUserExist) {
                  debugger;
                  
                  $("#isuserexist").text("User Exist !!! Please Proceed to choose the package")
                  $("#isuserexist").css('background-color', "#50bf50");

                  $("#isuserexist").css('font-size', "20px");
                    $("#mobile").val( data.mobile );
                    $("#firstname").val( data.first_name );
                    $("#lastname").val( data.last_name );

                    $.ajax({
                type: "GET",
                url: "/GetPackageInfo/"+$email,
                cache: false,
                contentType:"application/json",
                success: function(data) {
                  debugger;
                          if(data == "No Package Exist")  
                          { 
                  $("#ispackageexist").text("Active package doesn't exist ... please choose a package");
                  $("#ispackageexist").css('background-color', "#ff0000 ");
                  $("#ispackageexist").css('font-size', "20px");
                          }  
                          else{  
                            debugger;
                    $("#qty_clothes").val( data.qty_clothes );
                    $("#price").val( data.price );
                    $("#description").val( data.description );
                    $("#item").val(data.id);
                    $("#duration").val(data.duration);
                   // $("#startdate").val(data.start_date);
                   // $("#enddate").val(data.end_date);

                    $("#amountpaid").val(data.amount_paid);
                    $("#ispackageexist").text("");
                  $("#ispackageexist").css('background-color','white');
                  $("#ispackageexist").css('font-size', "0px");
                          }
                          
                }
            });

                }
            });
               
        
    })
});
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 </section><!-- /.content -->
@stop





