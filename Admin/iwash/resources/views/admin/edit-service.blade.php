@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
      
                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
    
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" >
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Service  Name</label>
                      <input type="text" class="form-control" value="{{ @$service->name }}" name="name" id="exampleInputEmail1" placeholder="Enter Service name">
                    </div>
                        <div class="form-group">
                      <label for="exampleInputEmail1">Category </label>
                     <select class="form-control" name="category_id">
                        <?php
              
               foreach($categories as $category){ ?>
                 <option value="<?= $category->id;?>" <?php if($service->category_id==$category->id){echo 'selected';} ?>><?= $category->name;?></option>
                  
               <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Service Description</label>
                    <textarea class="form-control"   name='description' required>{{ @$service->description }}</textarea>
                    </div>

                     <div class="form-group">
                      <label >Service Image</label>
                      <input type="file" class="form-control"  name='image' id="exampleInputEmail1"  >
                                             <img src=" {{$url. $service->image }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">

                    </div>
                    <!--<div class="form-group">
                      <label >Service Banner Image</label>
                      <input type="file" class="form-control"  name='image_banner' id="exampleInputEmail1"  >
                                             <img src=" {{$url. $service->image_banner }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">

                    </div>
                      <div class="form-group">
                      <label >Service Image Website Home</label>
                      <input type="file" class="form-control"  name='image_website' id="exampleInputEmail1"  >
                          <img src=" {{$url. $service->image_website }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">
                    </div>
                      <div class="form-group">
                      <label >Service Image App Home</label>
                      <input type="file" class="form-control"  name='image_app' id="exampleInputEmail1"  >
                        <img src=" {{$url. $service->image_app }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">
                    </div>-->

                     <div class="form-group">
                      <label for="exampleInputEmail1">Price Wash & Iron</label>
                      <input type="text" name="price_wash" value="{{ @$service->price_wash }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Service price">
                    </div>
                      <div class="form-group">
                      <label for="exampleInputEmail1">Price Dry Clean</label>
                      <input type="text" name="price_dryclean" value="{{ @$service->price_dryclean }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Service price">
                    </div>
                      <div class="form-group">
                      <label for="exampleInputEmail1">Price Iron</label>
                      <input type="text" name="price_iron" value="{{ @$service->price_iron }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Service price">
                    </div>
                     <div class="form-group">
                      <label for="exampleInputEmail1">Per</label>
                      <input type="text" name="per"  value="{{ @$service->per }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Service Per">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop