@include('admin/includes/header')
@include('admin/includes/sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          {{ $title }}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{ $title }}</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		@yield('container')
      </div><!-- /.content-wrapper -->
	  @include('admin/includes/footer')

