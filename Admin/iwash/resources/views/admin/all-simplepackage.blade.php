@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
			  				  <div class="box-header with-bsubscription">
<a href="/all-simplepackage"><h3>ALL packages </h3></a>
                </div><!-- /.box-header -->
  
                <div class="box-body">

                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
                  <table id="example1" class="table table-bsubscriptioned table-striped">
                    <thead>
                      <tr>
                       <th>PackageId</th>
                       <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Description</th>         
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($package as $data)
                      <tr>
                  
                        <td> {{ @$data->id}}</td>
                        <td>{{ @$data->name}}</td>
                        <td>  {{ @$data->qty_clothes}}</td>
                        <td> {{ @$data->price}}</td>
                        <td> {{ @$data->description}}</td>
                       
                         <td>
                         <!-- <a class="btn btn-danger" href="/all-simplepackage/{{ $data->id }}/delete">Delete</a> -->
                         </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                  {!! $package->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        

@stop
