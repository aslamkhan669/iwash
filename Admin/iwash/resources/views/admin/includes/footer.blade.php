
      <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0 Iwash
      </div>
      <strong>Copyright &copy; 2017 <a href="">Iwash</a>.</strong> All rights reserved.
    </footer>


    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

  </div><!-- ./wrapper -->

  <!-- jQuery 2.1.4 -->
  <script src="{{ URL::asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ URL::asset('AdminLTE/plugins/fastclick/fastclick.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ URL::asset('AdminLTE/dist/js/app.min.js') }}"></script>
  <!-- Sparkline -->
  <script src="{{ URL::asset('AdminLTE/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
  <!-- jvectormap -->
  <script src="{{ URL::asset('AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
  <script src="{{ URL::asset('AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{ URL::asset('AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <!-- ChartJS 1.0.1 -->
  <script src="{{ URL::asset('AdminLTE/plugins/chartjs/Chart.min.js') }}"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

  <!-- AdminLTE for demo purposes -->
  <script src="{{ URL::asset('AdminLTE/dist/js/demo.js') }}"></script>



  <script src="{{ URL::asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
          <script src="{{ URL::asset('AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

       <script>
    $(function () {
      $("#example1").DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "order": [],
        "ordering": true,
        "info": false,
        "autoWidth": false
      });

    });
  </script>
  <script type="text/javascript">
$.ajaxSetup({
// headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});


   function goBack() {
  window.history.back();
}
</script>
</body>
</html>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
</script>


<script>


function swipe(largeImage) {

 largeImage.style.display = 'block';
 //largeImage.style.width=200+"px";
// largeImage.style.height=200+"px";
 var url=largeImage.getAttribute('src');
 window.open(url,'Image','width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
}



function addmoreservice(){
   var str=' <div class="form-group row col-lg-12 servicebox"><div class="col-lg-6"> <label >Services</label><input type="text" class="form-control"  name="service_name[]" placeholder="Servie Name"  > </div> <div class="col-lg-5"><label ></label>  <input type="text" class="form-control"  name="service_price[]" placeholder="Servie Price" ></div>';
str+='<div <div class="col-lg-1"><label ></label><button type="button" class="btn btn-danger closeme " ><i class="fa fa-trash-o fa-lg"></i> Delete</button></div></div>';
       jQuery(".servicebox:last").after(str);
  }

  jQuery("body").on('click','.closeme',function(){
     jQuery(this).parents('.servicebox').remove();
  });

  var subcaturl="<?php echo URL::to('/api/getsubcat'); ?>";

     $('#prod_cat').change(function(){
    var parent_id=$(this).val();
    if(parent_id!=''){
      $.ajax({
        url:subcaturl+'/'+parent_id,
        type: "get",
        success: function(data){
          var str='<option value="">Select</option>';
         $.each(data, function(i, item) {
             str+='<option value="'+item.id+'" >'+item.name +'</option>';
          })
        $('#prod_sub_cat').html(str);

        }
      });


      }
    });






      var providerupdate="<?php echo URL::to('/api/update-provider'); ?>";
$('#updateprofile').submit(function(event){

  var userid=$(this).find('.userid').val();


var token="<?php echo csrf_token();  ?>";
  var fields = $(this).serializeArray();
    event.preventDefault();
      $.ajax({
        url:providerupdate+'/'+userid,
        type: "post",
        data:fields,
        success: function(data){

        if(data.success){
         window.location.reload();

        }else{

                     alert(data.message);

        }
        }
      });

    });


   var verify="<?php echo URL::to('/api/verify-user'); ?>";
$('#setstatus').click(function(event){
  var status=$(this).attr('status');
var verificationids=$('.verification:checked').map(function(){return $(this).val();}).get();
console.log(verificationids);
var token="<?php echo csrf_token();  ?>";
    event.preventDefault();
      $.ajax({
        url:verify+'?_token='+token,
        type: "post",
        data:{verification:verificationids,status:status},
        success: function(data){

        if(data.success){
      window.location.reload();

        }else{

          alert('Error occured');
        }
        }
      });

    });
</script>


  <script type="text/javascript">

$("#selectlocation").change(function(){
$("#branchchoose").submit();
});


$(".packagecategory .category").change(function(){

  if($(this).is(":checked")){
    var service=$(this).parents('.packagecategory').next('.services');
          var catid=$(this).val();

          var servicesbycaturl="<?php echo URL::to('/api/services/'); ?>/";
          var api_url="<?php echo Config::get('app.api_url'); ?>";
          console.log(api_url);
            $.ajax({
                    url:servicesbycaturl+catid,
                    type: "get",
                    success: function(data){
                    console.log(data);
                    if(data.success==true){
                      var str='<b>Choose Service </b> <br>';
                      $.each(data.data, function(i, item) {
                        str+='<div class="checkbox"> <label for="'+item.id+'"><input id="'+item.id+'"  type="checkbox" name="service_id'+catid+'[]" class="servicechek" value="'+item.id+'">'+item.name+'</label> <input type="number"  class="quantity hidden" name="quantity_'+catid+'_'+item.id+'"></div>';
                      })

                      service.html(str);
                      $(".servicechek").change(function(){
                          var qty=$(this).parent().next('.quantity');

                          if($(this).is(":checked")){
                                qty.removeClass('hidden');

                          }else{
                            qty.addClass('hidden');


                          }

                          });

                    }



                    }
                  });
  }else{
    $(this).parents('.packagecategory').next('.services').empty();


  }




});





function getServices(catid){
console.log($('#'+catid).find('.row').is(':empty'));
if($('#'+catid).find('.row').is(':empty')){

var servicesbycaturl="<?php echo URL::to('/api/services/'); ?>/";
var api_url="<?php echo Config::get('app.api_url'); ?>";
console.log(api_url);
$.ajax({
        url:servicesbycaturl+catid,
        type: "get",
        success: function(data){
         console.log(data);
         if(data.success==true){
           var str='';
          $.each(data.data, function(i, item) {
             str+=" <div class='col-md-12'> <div class='dash'> <div class='col-md-2'> <div class='dashboard'> <a href=''> <img src='"+api_url+ item.image+"'> </a> </div> </div> <div class='col-md-8'> <div> <h4>"+item.name+" (Per "+item.per+" )</h4> <p> QRS "+item.price+"</p> <span class='iw-rate' method='POST' action='#'> <input type='button' value='-' class='iw-qtyminus' field='quantity'  onclick='decrement($(this),"+item.category_id+","+item.id+","+item.price+")'  /> <input type='text' readonly name='quantity'value='0' class='iw-qty' /> <input type='button' value='+' class='iw-qtyplus' field='quantity' onclick='increment($(this),"+item.category_id+","+item.id+","+item.price+")' /></span> </div> </div> </div> <hr> </div>";
          })
        $('#'+catid).find('.row').html(str);

         }



        }
      });




}
}
getServices(1);

var items_added = [], quantity_added=0, total=0, tax=0,category_selected ='';

function initialize_data(){
      $(".header_bar li").each(function(){
          var category = $(this).attr("category");
          items_added[category] = new Array();
      });
  }

  initialize_data();
function setCartData(){

      $("#quantity").text(quantity_added);
      $("#total").text(total);
      var tax = (total * 14.5)/100;
      $("#tax").text(tax);
  }
  function vibrateCart(){
      var i=0;
      var myInterval = setInterval(function(){
          if(i++>=10)
              clearInterval(myInterval);
          if(i%2==0){
              $(".estimator img").css({
                  "-webkit-transform": "rotate(15deg)",
                  "-moz-transform": "rotate(15deg)",
                  "transform": "rotate(15deg)",
                  "-o-transform": "rotate(15deg)",
                  "-ms-transform": "rotate(15deg)"
              });
          }
          else if(i%3==0){
              $(".estimator img").css({
                  "-webkit-transform": "rotate(-15deg)",
                  "-moz-transform": "rotate(-15deg)",
                  "transform": "rotate(-15deg)" ,
                  "-o-transform": "rotate(-15deg)",
                  "-ms-transform": "rotate(-15deg)"
              });
          }
          else{
              $(".estimator img").css({
                  "-webkit-transform": "rotate(0deg)",
                  "-moz-transform": "rotate(0deg)",
                  "transform": "rotate(0deg)",
                  "-o-transform": "rotate(0deg)",
                  "-ms-transform": "rotate(0deg)"
              });
          }
      }, 100);
   }

function increment(curr,categoryid,itemid,price){
console.log(curr.prev('.iw-qty').val());
        var item_label =itemid;

        if(items_added[categoryid][item_label]){
        items_added[categoryid][item_label]++;
        curr.prev('.iw-qty').val(items_added[categoryid][item_label]);

        }else{
        items_added[categoryid][item_label]=1;
        curr.prev('.iw-qty').val(items_added[categoryid][item_label]);


        }
        if(quantity_added==0){
           $(".estimator").fadeIn(1500);

        }

        quantity_added++;
        total +=  Number(price);
        setCartData();
        vibrateCart();

     }


function decrement(curr,categoryid,itemid,price){

var item_label =itemid;

              if(items_added[categoryid][item_label]>0){
                  items_added[categoryid][item_label]--;
                  quantity_added--;
                  total -= Number(price);
                  setCartData();
                  if(quantity_added==0)
                      $(".estimator").fadeOut(1000);
              }
              curr.next('.iw-qty').val(items_added[categoryid][item_label]);

              vibrateCart();

}





</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<script>
    $( "#use_subscription" ).change(function(){
	var usesub=$(this).val();

		if(usesub==1){
		$('#payment_status').val(1);
		
			
			$('.subscriptions').removeClass('hidden');
			$('.subscriptonservices').removeClass('hidden');
			
			
			$('.servicecat').addClass('hidden');
			$('.categoryservices').addClass('hidden');
		}else{
		   	$('.subscriptions').addClass('hidden');
		   	$('.subscriptonservices').addClass('hidden');
			if(usesub==''){
			$('.categoryservices').addClass('hidden');
				$('.servicecat').addClass('hidden');
			}else{
					   	$('.servicecat').removeClass('hidden');

			}
			

		}
	
		
	});
	
	
	$("#orderform").submit(function(){
	  $(":hidden").prop('required',null);
	
	});
  $('.input-grid').on("change",function(){
    var qty = parseInt($(this).val());
    var price = $(this).next().val();
    var qw = $(this).parent().parent().find('input[name^="qty_w"]').val();
    var qd = $(this).parent().parent().find('input[name^="qty_dc"]').val();
    var qr = $(this).parent().parent().find('input[name^="qty_ir"]').val();
    var pw = $(this).parent().parent().find('input[name^="price_w"]').val();
    var pd = $(this).parent().parent().find('input[name^="price_dc"]').val();
    var pr = $(this).parent().parent().find('input[name^="price_ir"]').val();
    qw = qw?parseInt(qw):0;
    qd = qd?parseInt(qd):0;
    qr = qr?parseInt(qr):0;
    var st = (pw*qw)+(pd*qd)+(pr*qr);
    if(!qty){
      qty=0;
    }
    $(this).parent().parent().find(".sub-total").text(st);

    var tc =0;
    var ta=0.00;
    $('.input-grid').each(function (index, value) { 
       
        if(this.value!=="" && this.value!==undefined && this.value!==NaN){
        tc += parseInt(this.value)
         
         $(".tc").text(tc);

        }
        
    });
     $('.sub-total').each(function (index, value) { 
       
        if($(this).html()!=="" && $(this).html()!==undefined && $(this).html()!==NaN){
        ta += parseFloat($(this).html())
         
         $(".ta").text(ta);
        }
    });

    var a =$(".tc").text();
    var b=$("#quantity_used").val();
    var c = b - a;
    if(c < 0 && b != "" )
    {
          alert("Your Subscription limit of using quantity is exceeded by the limit !!! now subscription will not used...");
   }

  })
	$("#service_cat_id").change(function(){
    var service=$('.categoryservices').find('.servicelist');

  if($(this).val!=''){
          var catid=$(this).val();

          var servicesbycaturl="<?php echo URL::to('/api/services/'); ?>/";
          var api_url="<?php echo Config::get('app.api_url'); ?>";
          console.log(api_url);
            $.ajax({
                    url:servicesbycaturl+catid,
                    type: "get",
                    success: function(data){
                    
                    if(data.success==true){
					$('.categoryservices').removeClass('hidden');
                      var str='';
                      $.each(data.data, function(i, item) {
                        					
						str+='<div> <input  type="hidden" name="services[]" value="'+item.id+'"> <input type="number" name="quantity_'+item.id+'" class="quantity" price="'+item.price+'" min="0" required="">  '+item.name +'(QRS '+item.price +')'+'</div>';
                      })

                      service.html(str);
                     

                    }

jQuery('.quantity').on('change',function(){
	var totalamount=0;
	
	var totalquantity=0;
	
	$('.quantity').each(function(i,e){
	    if (e.value !== ""){
				var qty=e.value;
			var price=$(this).attr('price');
			console.log(qty);
			totalquantity+=parseInt(qty);
			totalamount+=parseInt(qty * price);
			
			
			
			}
		
	});

	$('.totalamount').val(totalamount);
	$('.totalquantity').val(totalquantity);
  
});

                    }
                  });
  }else{
   service.empty();


  }




});

	
	
var custurl="<?php echo URL::to('/api/getCustomers'); ?>";


    $( "#emailcustomer" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url:custurl,
          dataType: "json",
          data: {
            email: request.term,
            maxRows:12
          },
          success: function( data ) {
            console.log(data);
            response( data );
          }
        } );
      },
      minLength: 2,
      select: function( event, ui ) {
        window.location.href="?email="+ ui.item.value +"&user_id=" + ui.item.id;
      //  log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }
    } );
jQuery('.quantity').on('change',function(){
	var totalamount=0;
	
	var totalquantity=0;
	
	$('.quantity').each(function(i,e){
    
	    if (e.value !== ""){
			var qty=e.value;
			var price=$(this).attr('price');
			console.log(qty);
			totalquantity+=parseInt(qty);
			totalamount+=parseInt(qty * price);
			
			
			
			}
		
	});
	$('.totalamount').val(totalamount);
	$('.totalquantity').val(totalquantity);
  

});

$("#card").on("click",function(){
    $("#cash").removeClass("activepayment");
    $(this).addClass("activepayment");
    $("#payment_mode").val("card");
})
$("#cash").on("click",function(){
     $("#card").removeClass("activepayment");
     $(this).addClass("activepayment");
    $("#payment_mode").val("cash");
})
$("#totalqty").on("change",function(){
  if($("#totalqty").text() > $("#quantity_used").val())
{
  alert("hello");
}
})
 



$("#mobilecust").on("change",function(){
  
  var servicesbycaturl="<?php echo URL::to('/getuserinfo/'); ?>/";
          var mob = $(this).val();
         
            $.ajax({
                    url:servicesbycaturl+mob,
                    type: "get",
                    dataType:"json",
                    success: function(data){
              
                    var userdata = JSON.parse(data)
                      if(userdata.ispackageexist == "NoPackageExist")  
                          { 
                  $("#ispackageexist").text("Active package doesn't exist ... please choose a package");
                  $("#ispackageexist").css('background-color', "#ff0000 ");
                  $("#ispackageexist").css('font-size', "14px");
                  $("#ispackageexist").css('font-weight', "600");
                  $("#fname").val(userdata.firstname);
                        $("#fname , #lname").prop("readonly", true);
                        $("#lname").val(userdata.lastname);
                        $("#qty_clothes").val(userdata.qty_clothes);
                        $("#quantity_used").val(userdata.quantity_used);
                        $("#end_date").val(userdata.end_date);
                          }  
                          else{ 
                      var user = JSON.parse(data);
                      if(user.message) return;
                      else{
                        $("#fname").val(user.firstname);
                        $("#fname , #lname").prop("readonly", true);
                        $("#lname").val(user.lastname);
                        $("#qty_clothes").val(user.qty_clothes);
                        $("#quantity_used").val(user.quantity_used);
                        $("#end_date").val(user.end_date);
                      }
                      $("#ispackageexist").text("");
                  $("#ispackageexist").css('background-color','white');
                  $("#ispackageexist").css('font-size', "0px");
                          }

                   }
            });

})


  </script>
 <style>
 #chartdiv {
   width		: 100%;
   height		: 500px;
   font-size	: 11px;
 }
 .ui-autocomplete {
z-index: 100;
}
 </style>

 <!-- Resources -->
 <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
 <script src="https://www.amcharts.com/lib/3/serial.js"></script>
 <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
 <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
 <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<?php
if(Request::is('order/graphs')){

?>
<script>

var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "light",
                "marginRight": 70,
                "dataProvider":<?php echo json_encode($orders);; ?>,
                "valueAxes": [{
                  "axisAlpha": 0,
                  "position": "left",
                  "title": "Total Orders"
                }],
                "startDuration": 1,
                "graphs": [{
                  "balloonText": "<b>[[category]]: [[value]]</b>",
                  "fillColorsField": "color",
                  "fillAlphas": 0.9,
                  "lineAlpha": 0.2,
                  "type": "column",
                  "valueField": "totalrecord"
                }],
                "chartCursor": {
                  "categoryBalloonEnabled": false,
                  "cursorAlpha": 0,
                  "zoomable": false
                },
                "categoryField": "category",
                "categoryAxis": {
                  "gridPosition": "start",
                  "labelRotation": 45
                },
                "export": {
                  "enabled": true
                }

              });
			  
			  

</script>
<?php

            }
?>
<style>
.pac-container{

    z-index: 1077!important;

}

</style>