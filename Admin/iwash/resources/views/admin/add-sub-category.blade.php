@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method='post' enctype="multipart/form-data" action='{{URL::to("add-sub-category")}}'>
								{{ csrf_field() }}

                  <div class="box-body">
                    <div class="form-group">
                      <label >Parent Category Name</label>
                      <select name='parent' class="form-control">
						  <?php
						  
						   foreach($categories as $category){ ?>
							   <option value="<?= $category->id;?>" <?php if($category->id==$parent){ echo 'selected';} ?>><?= $category->name;?></option>
                  
							 <?php } ?>
                       </select>
                    </div>
                      <div class="form-group">
                      <label >Sub Category Name</label>
                      <input type="text" class="form-control"  name='name' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Sub Category Description</label>
                    <textarea class="form-control"  name='description' required></textarea>
                    </div>
                      <div class="form-group">
                      <label >Sub Category Image</label>
                      <input type="file" class="form-control"  name='cat_image' id="exampleInputEmail1" required >
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
