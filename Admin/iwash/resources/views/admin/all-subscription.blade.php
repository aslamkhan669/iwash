@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
			  				  <div class="box-header with-bsubscription">
<a href="/all-subscription"><h3>ALL subscriptions </h3></a>
                </div><!-- /.box-header -->
  
                <div class="box-body">

                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
                  <table id="example1" class="table table-bsubscriptioned table-striped">
                    <thead>
                      <tr>
                       <th>Name</th>
                       <th>Mobile</th>
                        <th>Package</th>
                        <th>Amount Paid</th>
                              <th>Remaining Quantity</th>
                              <th>Duration</th>
                              <th>Start Date</th>

                              <th>End Date</th> 
							                <th>Action</th>          
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($subscription as $data)
                      <tr>
                  
                        <td> {{ @$data->first_name}} {{ @$data->last_name}}</td>
                        <td>{{ @$data->mobile}}</td>
                        <td>  {{ @$data->name}}</td>
                        <td> {{ @$data->amount_paid}}</td>
                        <td> {{ @$data->quantity_used}}</td>
                        <td> {{ @$data->duration}} Month</td>
                         <td> {{ @$data->start_date}}</td>
                         <td> {{ @$data->end_date}}</td>
                         <td>
                         <a class="btn btn-default btn-sm" href="{{URL::to('all-subscription/'.$data->id.'/edit')}}">Edit</a>
                         </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                  {!! $subscription->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        

@stop
