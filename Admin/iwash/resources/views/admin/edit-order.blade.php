@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                <button onclick="goBack()">Go Back</button>

    <script>
function goBack() {
    window.history.back();
}
</script>
           
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" action=''>
								{{ csrf_field() }}
               
      <div class="box-body">
          <div class="col-lg-12">
            
            <div class="otherinfo">
              <div class="col-lg-12 services">
                <div class="col-md-4 categoryservices">
                  <div class = "singleview">
                    <table>
                      <thead>
                        <th>
                        </th>
                        <th>Wash & Iron 
                        </th>
                        <th>Dry clean
                        </th>
                        <th>Iron
                        </th>
                        <th>Total
                        </th>
                      </thead>
                      <tbody>
                        <?php
                              $first = "";
                              $second = "";
                              $third = "";
                              $html ="";
                              $number = 1;
                              foreach($tableview as $rateList){
                              $number++;
                              $qw = ($rateList->qty_w==0?"":$rateList->qty_w);
                              $qdc = ($rateList->qty_dc==0?"":$rateList->qty_dc);
                              $qir = ($rateList->qty_ir==0?"":$rateList->qty_ir);
                              $st = 0;
                              

                              if($number==15 || $number==28){
                              $html ="";
                              }
                              $html .= "";
                              $html .="<tr><td>".$rateList->name."<input type='hidden' name='product_id[]' value='".$rateList->id."'</td>";
                              if($qw>0){
                                    $st += $qw*$rateList->price_wash;
                                   $html .="<td><input type='number' min='0' class='input-grid' name=qty_w[]' value='".$qw."' autocomplete='off'/><input type='hidden' name='price_w[]' value='".$rateList->price_wash."'/></td>";
                              }else{
                                     $html .="<td><input type='number' min='0' class='input-grid' name=qty_w[]' value='' autocomplete='off'/><input type='hidden' name='price_w[]' value='".$rateList->price_wash."'/></td>";
                              }
                              if($qdc>0){
                                $st += $qdc*$rateList->price_dryclean;
                                   $html .="<td><input type='number' min='0' class='input-grid' name=qty_dc[]' value='".$qdc."' autocomplete='off'/><input type='hidden' name='price_dc[]' value='".$rateList->price_dryclean."'/></td>";
                              }else{
                                     $html .="<td><input type='number' min='0' class='input-grid' name=qty_dc[]' value='' autocomplete='off'/><input type='hidden' name='price_dc[]' value='".$rateList->price_dryclean."'/></td>";
                              }
                              if($qir>0){
                                  $st += $qir*$rateList->price_iron;
                                   $html .="<td><input type='number' min='0' class='input-grid' name=qty_ir[]' value='".$qir."' autocomplete='off'/><input type='hidden' name='price_ir[]' value='".$rateList->price_iron."'/></td>";
                              }else{
                                     $html .="<td><input type='number' min='0' class='input-grid' name=qty_ir[]' value='' autocomplete='off'/><input type='hidden' name='price_ir[]' value='".$rateList->price_iron."'/></td>";
                              }
                             $st = $st==0?"":$st;
                           
                              $html .="<td class='sub-total'>".$st."</td></tr>";
                              if($number<=14){
                              $first = $html;
                              }
                              else if($number>13 && $number<=27){
                              $second = $html;
                              }
                              else{
                              $third = $html;
                              }
                              }
                              echo $first;
                              ?>
                      </tbody>
                    </table>
                  </div> 
                </div>
                <div class="col-md-4 categoryservices">
                  <div class = "singleview">
                    <table>
                      <thead>
                        <th>
                        </th>
                        <th>Wash & Iron 
                        </th>
                        <th>Dry clean
                        </th>
                        <th>Iron
                        </th>
                        <th>Total
                        </th>
                      </thead>
                      <tbody>
                        <?php
                          echo $second;
                          ?>
                      </tbody>
                    </table>
                  </div> 
                </div>
                <?php if($third!=""){?>
                <div class="col-md-4 categoryservices">
                  <div class = "singleview">
                    <table>
                      <thead>
                        <th>
                        </th>
                        <th>Wash & Iron 
                        </th>
                        <th>Dry clean
                        </th>
                        <th>Iron
                        </th>
                        <th>Total
                        </th>
                      </thead>
                      <tbody>
                        <?php
                        echo $third;
                        ?>
                      </tbody>
                    </table>
                  </div> 
                </div>
                <?php }?>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="col-xs-12">
                <label > 
                </label>
                
                <div class="col-xs-2">
                  <label>Total Quantity
                  </label>
                  <p class="tc">{{$order->qty}}
                  </p>
                </div>
                <div class="col-xs-2">
                  <label>Total Amount (QRS)
                  </label>
                  <p class="ta">{{$order->totalamount}}
                  </p>
                </div>
                <div class="col-xs-2">
                <label>Package Quantity </label>
                <input type="text"  readonly="readonly"  class="form-control" id="qty_clothes" name="qty_clothes" placeholder="0" value="{{ $packinfo ? $packinfo->qty_clothes : '' }}">
                </div>
                <div class="col-xs-2">
                <label>Remaining Quantity</label>
            <input type="text"  readonly="readonly"  class="form-control" id="quantity_used" name='quantity_used' placeholder="0" value="{{$subscription ? $subscription->quantity_used : '' }}">
                </div>
                <div class="col-xs-2">
                <label>End Date</label>
            <input type="text"  readonly="readonly"  class="form-control" name='end_date' placeholder="0" value="{{ $end_date }}">
                </div>
                <div class="col-lg-2">
                <label >Order Status: </label>
    <select class="form-control"  id="sel1" name="status" >
    <option value="">Select Order Status</option>
    
    <option value="accepted" <?php if($order->status=='accepted'){echo 'selected';} ?>>Accepted</option>
    <option value="delivered" <?php if($order->status=='delivered'){echo 'selected';} ?>>Delivered</option>
    <option value="cancelled" <?php if($order->status=='cancelled'){echo 'selected';} ?>>Cancelled</option>
  </select>
                </div>
              </div>
              
            </div> 
          
      

        </div>
    

        <div class="col-lg-6">
                
                <div class="col-lg-6">
                <label >Order Date: </label>
                    <span>
                    <?php
        $d = new DateTime(explode(" ",$order->createdAt)[0]);  
        $formatted_date = $d->format('d-F-Y'); 
         echo $formatted_date;
       ?> 
                    </span>
                </div>
                <div class="col-lg-6">
        </div> 
             
        </div> 
        
          <div class="col-lg-12">
        <div class="col-lg-3">
                    <label >Mobile </label>
                    <input type="text"  class="form-control" value="{{$order->user->mobile}}" name="mobile" placeholder="Mobile" readonly="readonly">

                </div>
                <div class="col-lg-3">
                 <label >First Name </label>
                    <input type="text"  class="form-control" value="{{@$order->userProfile->first_name}}" name="fname" placeholder="First Name">
               <!--<label >Flat and Street: </label>
                    <input type="text"  class="form-control" value="{{$order->flatandstreet}}" name="flatandstreet" placeholder="Flat and Street" value="<?php echo @$_GET['flatandstreet']; ?>">-->

                </div>
              <input type="hidden" name="oid" value="{{$order->id}}"/>
       
      
                <div class="col-lg-3">
                 <label >Last Name </label>
                    <input type="text"  class="form-control" value="{{@$order->userProfile->last_name}}" name="lname" placeholder="Last Name">
                <!--<label >Landmark: </label>
                <input type="text" value="{{$order->landmark}}"  class="form-control"  name="landmark"  placeholder="Landmark" >-->

                </div>
                 <div class="col-lg-3">

                     <?php   if(Session::get('status')=='admin'){ ?>
                 
                      <label >Branch </label>
                      <select name='branch' class="form-control">
                          <?php foreach($branches as $parent){ ?>
                               <option <?php if($parent->id==$order->branch){echo 'selected';} ?> value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>
                  
                             <?php } ?>
                       </select>
                 
         <?php } ?>
                </div>
              
        </div> 

        <div class="col-lg-6">
            <div class="col-lg-6" id="ispackageexist">
            </div>
          
            </div>
         
        <div class="col-lg-12">
               
                <div class="col-lg-6">
                <label >Payment Mode
                </label><br>
                  <div class="col-xs-8">
                  <input type="hidden" name="payment_mode" id="payment_mode" value="<?php echo $order->payment_mode;?>" />
                  <p>  <a class="btn btn-primary btn-lg <?php if($order->payment_mode=="cash")echo'activepayment';?>" style="width:45%!important;float:left; margin-top: 10px;" id="cash" >Cash</a>
                  <a class="btn btn-primary btn-lg <?php if($order->payment_mode=="card")echo'activepayment';?>" style="width:45%!important;margin-right:10px; margin-top: 10px;"  id="card" >Card</a>
                  </p>
                  
                </div>
                


                <!--<select class="form-control"  id="payment_mode"  name="payment_mode"  >
                  <option value="">Select Payment Mode
                  </option>
                  <option value="cash" <?php if($order->payment_mode=="cash")echo'selected';?> >Cash
                  </option>
                  <option value="card" <?php if($order->payment_mode=="card")echo'selected';?> >Card
                  </option>
                </select>-->
              </div>
              <label >Print Mode
                </label><br>
              <div class="col-xs-4 member-detail">
                  
                  <p>  <a class="btn btn-primary btn-lg" style="width:50%!important;float:left; margin-top: 10px;" onclick="PrintBill({{$order->id}})" >Print Reciept</a>
                  <a class="btn btn-primary btn-lg" style="width:49%!important; margin-top: 10px;background-color: tomato;border-color: tomato;"  onclick="PrintElem({{$order->id}})" >Print Label</a>
                  </p>
                  
                </div>

    <input type="hidden"  class="form-control" name="pickupDate" value="{{$order->pickupDate}}"  placeholder="Pickup Date"  >
             
               
        </div> 
     
             

    <input type="hidden"   class="form-control" name="deliveryDate"  value="{{$order->deliveryDate}}"  placeholder="Delivery Date"  >
              

       
               
        </div> 
       
    


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                  <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   </div>
                  </div>
                </form>
     
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  <div class="col-sm-12 hidden" id="print">
           <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
       <link rel="stylesheet" href="{{ URL::asset('admin/style.css') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ URL::asset('AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset('AdminLTE/dist/css/skins/_all-skins.min.css') }}">
  <div class="p-head"><span> <img src="{{ URL::asset('AdminLTE/dist/img/logo2.jpg') }}"  alt="User Image" width="50px"></span>
      <span><h3>Iwash Laundry Services</h3></span>
  </div><br>
  Order Number: IW{{$order->id}}<br>
  Date : <?php
        $d = new DateTime(explode(" ",$order->createdAt)[0]);  
        $formatted_date = $d->format('d-F-Y'); 
         echo $formatted_date;
       ?>        <br>
        Branch : <?php foreach($branches as $parent){ 
            if($parent->id==$order->branch){echo $parent->branch;} 
       }?>    
       <br>
       Mobile: {{$order->user->mobile}}
       
       <table>
          <thead><th>Clothes </th><th>Quantity</th><th>Service Type</th><th>Rate(per item)</th></thead>
          <tbody><?php        $html ="";
                              $number = 1;
                              foreach($tableview as $rateList){
                              $number++;
                              $qw = ($rateList->qty_w==0?"":$rateList->qty_w);
                              $qdc = ($rateList->qty_dc==0?"":$rateList->qty_dc);
                              $qir = ($rateList->qty_ir==0?"":$rateList->qty_ir);

                              $html .= "";
                              if($qw>0||$qdc>0||$qir>0){
                             
                              }
                              if($qw>0){
                                   $html .="<tr><td>".$rateList->name."</td>";
                                   $html .="<td>$qw</td>";
                                   $html .="<td>Wash</td>";
                                   $html .="<td>".$rateList->price_wash."</td>";
                                   $html .="<td class='sub-total'></td></tr>";
                              }
                              if($qdc>0){
                                   $html .="<tr><td>".$rateList->name."</td>";
                                   $html .="<td>$qdc</td>";
                                   $html .="<td>Dry Clean</td>";
                                   $html .="<td>".$rateList->price_dryclean."</td>";
                                   $html .="<td class='sub-total'></td></tr>";
                              }
                              if($qir>0){
                                   $html .="<tr><td>".$rateList->name."</td>";
                                   $html .="<td>$qir</td>";
                                   $html .="<td>Iron</td>";
                                   $html .="<td>".$rateList->price_iron."</td>";
                                   $html .="<td class='sub-total'></td></tr>";
                              }
                             
                           
                              
                            
                              }
                              $html .="<tr style='border-top:1px solid silver;'><td></td><td></td><td>Total</td><td>".$order->totalamount."(QRS)</td></tr>";
                              echo $html;
                              ?>
                      </tbody>
                    </table>
                    Total Items :  <?php echo $order->qty; ?><br>
                    Total Amount Payable : <?php echo $order->totalamount."(QRS)"; ?>


 <style>
    @media print and (width: 8.5in) and (height: 11in) {
      @page {
          margin: 1in;
      }
     
    }
    table tbody tr td {
      padding-left: 2%;
    }
     table thead th{
           font-weight: 600!important;
    font-size: 14px;
    width: 20%;
    padding-left: 2%;
     border-bottom: solid 1px silver;
      }
      .p-head span {
        display:inline-block;
      }
      .p-head span img{
            margin-right: 10px;
      }
    </style>
</div>
<style>
  .col-lg-12 {
    padding-bottom: 1%;
  }
  .member-detail{
    background:skyblue;
    
    text-align: center;
  }
  .member-detail h5{
    background: blue;
    padding: 5px;
    color:white;
  }
  .services .quantity {
    width:45px;
  }
  .singleview input{
    width:70px!important;
  }
  .singleview th{
    text-align: center;
    background: gainsboro;
    font-size: 12px;
    font-weight: 400;
  }
  .singleview td:first-child {
    font-size:11px!important;
  }
</style>
<script>
		   
function PrintBill(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    var is_chrome = Boolean(mywindow.chrome);

    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('</head><body >');
   
    mywindow.document.write(document.getElementById("print").innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
 if (is_chrome) {
        mywindow.onload = function() { // wait until all resources loaded 
            mywindow.focus(); // necessary for IE >= 10
            mywindow.print();  // change window to mywindow
            mywindow.close();// change window to mywindow
        };
    }
    else {
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        mywindow.close();
    }

    return true;

}
 </script>

 <div class="hidden" id="{{$order->id}}">
	 		  <style>
  @page{
      size: 6cm 2.4cm;
      size: landscape;
      margin:none;

    }  
   .card-pricing h2{
    text-align: center;
    font-size: 12px;
    font-weight: 900;
    margin: 0;
    padding: 2px;
    text-transform: uppercase;
     } 
     .card-pricing h3{
    text-align: center;
    margin-top: 0px;
    font-weight: 900;
    text-transform: uppercase;
    font-size: 6px;
    }  
    .card-pricing ul li {
      text-align:center;
    margin-right:0px;
    font-weight: 600;
    text-transform: uppercase;
    font-size: 6px;  
    } 
    .card ul{
      padding:0;
    } 
    .img-thumbnail {
    display: inline-block;
    max-width: 100%;
    height: auto;
    padding: 0px;
    line-height: 1.42857143;
    background-color: #fff; 
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    margin-top:5px;
}
img {
    -webkit-print-color-adjust: exact;
} 
</style>
			  
		<div class="card card-raised card-pricing"> 
    <div class="card-content text-center"> 
    <h2 style="text-transform:none!important;">iWash</h2>  
    <h3 style="text-transform:none!important;">Job No. IW{{ @$order->id}}</h3> 
    <ul style = " list-style: none;margin-top:10px"> <li>Date: <?php echo explode(" ",$order->createdAt)[0];?></li>
    <li style="">Mob: <?php if($order->user->mobile){ echo $order->user->mobile;}else{echo $order->user->email;} ?></li>
	  <li></li> <li style=""> <img src="{{$url. $order->qr }}"   class="img-thumbnail" ></li> 
    <li style="">IW{{ @$order->id}}</li></ul> </div> </div>
	 </div>   
   <script>
   function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    var is_chrome = Boolean(mywindow.chrome);

    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('</head><body style="padding-left:0.3cm!important;display: table-cell; width: 100%">');
   
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
 if (is_chrome) {
        mywindow.onload = function() { // wait until all resources loaded 
            mywindow.focus(); // necessary for IE >= 10
            mywindow.print();  // change window to mywindow
            mywindow.close();// change window to mywindow
        };
    }
    else {
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        mywindow.close();
    }

    return true;

    return true;
}



   </script>   
<style>
.activepayment{
  background-color:tomato;
  border-color:tomato;
}
.btn.btn-primary.btn-lg:hover{
    background-color:tomato;
  border-color:tomato;
}
.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: white!important;
    opacity: 1;
    border: none!important;
}
.form-control{
  padding: 6px 6px!important;
}
</style>
@stop
