@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
          
                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>Image</th>
                     
                        <th>Name</th>
                                                 <th>Category</th>

                        <th>Description</th>
                        <th>Price Wash & Iron</th>
                        <th>Price Dry Clean</th>
                        <th>Price Iron</th>
                         <th>Per</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
            
            
            @foreach ($services as $service)

     <tr>
      <td>
      <img src=" {{ $url. $service->image }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">
             
                        </td>
                     
                 
                        <td> {{ $service->name }}      </td>
                                <td> {{ $service->Category->name }}      </td>
                        <td> 
                  {{ $service->description }}
                        </td>
                                                 <td> {{ $service->price_wash }}      </td>
                                                 <td> {{ $service->price_dryclean }}      </td>
                                                 <td> {{ $service->price_iron }}      </td>
                                                 <td> {{ $service->per }}      </td>
                                                 <td>

                            <a class="btn btn-default" href="{{URL::to('service/'.$service->id.'/edit')}}">Edit</a>
                            <a class="btn btn-danger" href="{{URL::to('service/'.$service->id.'/delete')}}">Delete</a>
                          
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                    {!! $services->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
          $('.getsubcat').click(function(){
          $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
    });
     
        
        </script>
        
@stop
