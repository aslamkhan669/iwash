@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->

  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
  @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif
    	
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data" action=''>
								{{ csrf_field() }}

                  <div class="box-body">
                    <div class="form-group">
                      <label >Package Name</label>
                      <input type="text" class="form-control" placeholder="For eg.(Traditional)" name='name'>
                    </div>
                    <div class="form-group">
                      <label > Package Qty</label>
                      <input type="text" class="form-control" placeholder="For eg.(1,3,6,12)"  name='qty_clothes'>
                    </div>
                    <div class="form-group">
                      <label >Pricing</label>
                      <input type="text" class="form-control" placeholder="For eg.(700)"   name='price'>
                    </div>
                    <div class="form-group">
                      <label >Description</label>
                      <input type="text" class="form-control" placeholder="weekly,montly"   name='description'>
                    </div>
                 
                 
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                
      
    <style>
    .services .quantity {


      width:45px;
    }
    .services{

      margin-left: 2%;
    }
    </style>
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
