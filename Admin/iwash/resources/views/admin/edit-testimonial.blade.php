@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" method='post' action=''>
								{{ csrf_field() }}

                  <div class="box-body">
                    <div class="form-group">
                      <label >Name</label>
                      <input type="text" class="form-control"  value="{{ @$testimonial->name }}" name='name' id="exampleInputEmail1" required >
                    </div>
                     <div class="form-group">
                      <label >Designation</label>
                      <input type="text" class="form-control"  value="{{ @$testimonial->designation }}" name='designation' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Message</label>
                    <textarea class="form-control"  name='message' required>{{ @$testimonial->message }}</textarea>
                    </div>


                <div class="form-group">
                      <label >Profile</label>
                      <input type="file" class="form-control"  name='profile' id="exampleInputEmail1" required >
                       <img src=" {{$url. $testimonial->profile }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
                
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
