@extends('admin.layouts.main') @section('container')
<!-- Small boxes (Stat box) -->

<div class="row">
  <div class="col-md-12">

    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        @if(session()->has('ok')) @include('partials/error', ['type' => 'success', 'message' => session('ok')]) @endif @if(isset($info))
        @include('partials/error', ['type' => 'info', 'message' => $info]) @endif @if(session()->has('error')) @include('partials/error',
        ['type' => 'danger', 'message' => session('error')]) @endif
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" method='post'  enctype='multipart/form-data'>
        {{ csrf_field() }}
        <div class="box-body">

          <div class="form-group">
            <div class="col-lg-12">
              <label>First Name</label>
              <input type="text" class="form-control" name='first_name' required>
            </div>

            <div class="col-lg-12">
              <label>Last Names</label>
              <input type="text" class="form-control" name='last_name' required>
            </div>


          </div>

          <div class="form-group">
              <div class="col-lg-12">
           
   <label>Email</label>
              <input type="email" class="form-control" name='email'></div>

            </div>
            <div class="col-lg-12">
              <label>Mobile</label>
              <input type="tel" class="form-control" name='mobile' required>
            </div>

          



          <div class="form-group ">
            <div class="col-lg-12">
                 <label>Password</label>
              <input type="password" class="form-control" name='password' required>
            
            </div>
             </div>
    
  <?php   if(Session::get('status')=='admin'){ ?>
                    <div class="form-group">
                       <div class="col-lg-12">
                      <label >Branch </label>
                      <select name='location' class="form-control">
              <?php foreach($branches as $parent){ ?>
                 <option value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>
                  
               <?php } ?>
                       </select>
                    </div>
                      </div>
         <?php } ?>


       




        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>



    </div>
    <!-- /.box -->


  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@stop