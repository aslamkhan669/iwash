@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Banners</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				
		
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Banner Image</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>

						
						@foreach ($banners as $banner)

                      <tr>
                        <td>
							<img src=" {{ $url.$banner->url }}"  onClick="swipe(this);"sclass="img-thumbnail" style="width:150px;height:150px">
                    
                        
                        </td>
                   
							<!--    <td>
								    <div class="checkbox icheck">
                <label>
                 								   <input type='checkbox'  name="active" <?php if($banner->status==1){ echo 'checked';} ?> >

                </label>
              </div>
								   
                    
                        
                        </td> -->
                        <td>
                            <a class="btn btn-danger" href="/banners/{{ $banner->id }}/delete">Delete</a>
                          
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
<div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	{!! $banners->render() !!} </div>         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
     
@stop
