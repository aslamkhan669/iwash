@extends('admin.layouts.main')
@section('container')
<!-- Small boxes (Stat box) -->

<div class="row">
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
    
      <div class="box-header with-border">
        @if(session()->has('ok'))
        @include('partials/error', ['type' => 'success', 'message' => session('ok')])
        @endif
        @if(isset($info))
        @include('partials/error', ['type' => 'info', 'message' => $info])
        @endif
        @if(session()->has('error'))
        @include('partials/error', ['type' => 'danger', 'message' => session('error')])
        @endif
        <?php   if(Session::get('status')!='admin'){ ?>
        <h4>Branch : {{@$userbranch}}</h4>
        <?php } ?>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" name="orderform" id="orderform" enctype="multipart/form-data" action=''>
        {{ csrf_field() }}
        <div class="box-body">
          <div class="col-lg-12">
            <div class="col-lg-4">
              <?php
              //print_r($subscription->SubscribeServices);
              // if($subscription!=false){
              // 
              // <div class="col-lg-4">
              //   <label >Use Subscription
              //   </label>
              //   <select name='use_subscription' id="use_subscription" class="form-control" required placeholder="Use Subscription">
              //     <option value="">Select
              //     </option>
              //     <option value="1">Yes
              //     </option>
              //     <option value="0">No
              //     </option>
              //   </select>
              // </div>
              // <div class="col-lg-4 subscriptions hidden">
              //   <label >Subscription
              //   </label>
              //   <input type="hidden" value="<?php echo $subscription->id; "  readonly class="form-control" id="subscription_id" required name="subscription_id" >
              //   <input type="text" value="<?php echo $subscription->Package->package;"  readonly class="form-control" id="subscription" required name="subscription" >
              // </div>
              //  } ?>	
            </div>
            <div class="otherinfo">
              <div class="col-lg-12 services">
                <div class="col-md-4 categoryservices">
                  <div class = "singleview">
                    <table>
                      <thead>
                        <th>
                        </th>
                        <th>Wash & Iron 
                        </th>
                        <th>Dry clean
                        </th>
                        <th>Iron
                        </th>
                        <th>Total
                        </th>
                      </thead>
                      <tbody>
                        <?php
                              $first = "";
                              $second = "";
                              $third = "";
                              $html ="";
                              $number = 1;
                              foreach($tableview as $rateList){
                              $number++;
                              if($number==15 || $number==28){
                              $html ="";
                              }
                              $html .= "";
                              $html .="<tr><td>".$rateList->name."<input type='hidden' name='product_id[]' value='".$rateList->id."'</td>";
                              $html .="<td><input type='number' min='0' class='input-grid' name=qty_w[]' value='' autocomplete='off'/><input type='hidden' name='price_w[]' value='".$rateList->price_wash."'/></td>";
                              $html .="<td><input type='number' min='0' class='input-grid' name='qty_dc[]' value='' autocomplete='off'/><input type='hidden' name='price_dc[]' value='".$rateList->price_dryclean."'/></td>";
                              $html .="<td><input type='number' min='0' class='input-grid' name='qty_ir[]' value=''autocomplete='off'/><input type='hidden' name='price_ir[]' value='".$rateList->price_iron."'/></td>";
                              $html .="<td class='sub-total'></td></tr>";
                              if($number<=14){
                              $first = $html;
                              }
                              else if($number>13 && $number<=27){
                              $second = $html;
                              }
                              else{
                              $third = $html;
                              }
                              }
                              echo $first;
                              ?>
                      </tbody>
                    </table>
                  </div> 
                </div>
                <div class="col-md-4 categoryservices">
                  <div class = "singleview">
                    <table>
                      <thead>
                        <th>
                        </th>
                        <th>Wash & Iron 
                        </th>
                        <th>Dry clean
                        </th>
                        <th>Iron
                        </th>
                        <th>Total
                        </th>
                      </thead>
                      <tbody>
                        <?php
                          echo $second;
                          ?>
                      </tbody>
                    </table>
                  </div> 
                </div>
                <?php if($third!=""){?>
                <div class="col-md-4 categoryservices">
                  <div class = "singleview">
                    <table>
                      <thead>
                        <th>
                        </th>
                        <th>Wash & Iron 
                        </th>
                        <th>Dry clean
                        </th>
                        <th>Iron
                        </th>
                        <th>Total
                        </th>
                      </thead>
                      <tbody>
                        <?php
                        echo $third;
                        ?>
                      </tbody>
                    </table>
                  </div> 
                </div>
                <?php }?>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="col-xs-12">
              
              
                <div class="col-xs-3">
                <label >Total Quantity
                  </label >
                  <p id='totalqty' class="tc">0
                  </p>
                </div>
                <div class="col-xs-3">
                <label >Total Amount (QRS)
                  </label >
                  <p class="ta">0
                  </p>
                </div>
                <div class="col-xs-2">
                <label>Package Quantity </label>
                <input type="text"  readonly="readonly" required class="form-control" id="qty_clothes" name="qty_clothes" placeholder="0" value="<?php echo @$_GET['qty_clothes']; ?>">
                </div>
                <div class="col-xs-2">
                <label>Remaining Quantity</label>
                <input type="text"  readonly="readonly" required class="form-control" id="quantity_used" name="quantity_used" placeholder="0" value="<?php echo @$_GET['quantity_used']; ?>">
                </div>
                <div class="col-xs-2">
                <label>End Date </label>
                <input type="text"  readonly="readonly" required class="form-control" id="end_date" name="" placeholder="0" value="<?php echo @$_GET['end_date']; ?>">
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-12 messages" id="ispackageexist">
            </div>
            </div>          
            <div class="col-lg-6">
              <div class="col-lg-6">
                <label >Mobile 
                </label>
                <input type="text" class="form-control" id="mobilecust" value="<?php echo @$_REQUEST['email'] ; ?>"  autocomplete="off"  name="email" placeholder="">
              </div>
              <div class="col-lg-6">
              <label >First Name</label>  
      <input type="text"  required class="form-control" id="fname" name="fname" placeholder="First Name" value="<?php echo @$_GET['fname']; ?>">
                <!--<label >Flat and Street
                </label>-->
                <input type="hidden"  class="form-control" name="flatandstreet" placeholder="Flat and Street" value="<?php echo @$_GET['flatandstreet']; ?>">
              </div>
            </div>
          
            <div class="col-lg-6">
              <div class="col-lg-6">
                <!--<label >Landmark
                </label>-->
                <input type="hidden"  class="form-control"  name="landmark"  placeholder="Landmark" >
                <label >Last Name</label>  
      <input type="text"  required class="form-control" id="lname" name="lname" placeholder="Last Name" value="<?php echo @$_GET['lname']; ?>">
              </div>
              <div class="col-lg-6">
              <input type="hidden"   class="form-control" value="<?php echo  date('Y-m-d');?>"  name="payment_date"   placeholder="Payment Date"  >
                <?php   if(Session::get('status')=='admin'){ ?>
                <label >Branch 
                </label>
                
              
                <select name='branch' class="form-control">
                  <?php foreach($branches as $parent){ ?>
                  <option value="<?= $parent->id;?>" >
                    <?= $parent->branch;?>(
                    <?= $parent->location;?>)
                  </option>
                  <?php } ?>
                </select>
                <?php } ?>
              </div>
            </div>
            
           
            <div class="col-lg-12">
              <div class="col-lg-6">
               
                <label >Payment Mode
                </label><br>
                  <div class="col-xs-8">
                  <input type="hidden" name="payment_mode" id="payment_mode" value="cash" />
                  <p>  <a class="btn btn-primary btn-lg activepayment" style="width:45%!important;float:left; margin-top: 10px;" id="cash" >Cash</a>
                  <a class="btn btn-primary btn-lg" style="width:45%!important;margin-right:10px; margin-top: 10px;"  id="card" >Card</a>
                  </p>
                  
                </div>
              </div>
              <input type="hidden"  class="form-control" value="<?php echo  date('Y-m-d');?>" name="pickupDate"  placeholder="Pickup Date"  >
              <div class="col-lg-6">
              </div>
            </div>
            <div class="col-lg-12">
              <div class="col-lg-6">
                <label >
                </label>
                <input type="hidden"  class="form-control" name="deliveryDate"  placeholder="Delivery Date" value="<?php echo  date('Y-m-d');?>" >
              </div>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="col-lg-6">
              <input type="hidden" value="accepted" name="status" >
            </div>
            <div class="col-lg-6">
              <input type="hidden" value="1" name="payment_status" >
            </div>
          </div>
        </div>
        </div>
      <!-- /.box-body -->
      <div class="box-footer otherinfo">
        <div class="col-lg-12">
          <button type="submit" id="submitbutton" class="btn btn-primary">Submit
          </button>
        </div>
      </div>
      </form>
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
<style>
.messages{
    padding: 3px!important;
    color: white;
    font-weight: 600!important;
    width: 90%;
    margin-left: 50px;
}
  .col-lg-12 {
    padding-bottom: 1%;
  }
  .member-detail{
    background:skyblue;
    height: 150px;
    text-align: center;
  }
  .member-detail h5{
    background: blue;
    padding: 5px;
    color:white;
  }
  .services .quantity {
    width:45px;
  }
  .singleview input{
    width:70px!important;
  }
  .singleview th{
    text-align: center;
    background: gainsboro;
    font-size: 12px;
    font-weight: 400;
  }
  .singleview td:first-child {
    font-size:11px!important;
  }

.activepayment{
  background-color:tomato;
  border-color:tomato;
}
.btn.btn-primary.btn-lg:hover{
    background-color:tomato;
  border-color:tomato;
}
.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: white!important;
    opacity: 1;
    border: none!important;
}
.form-control{
  padding: 6px 6px!important;
}
</style>
@stop
