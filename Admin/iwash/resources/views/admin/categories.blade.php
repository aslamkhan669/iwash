@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Categories</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
	        
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>Image</th>
                       <th>Image Banner</th>
                        <th>Image Website Home</th>
                         <th>Image App Home</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						
						@foreach ($categories as $category)

     <tr>
      <td>
      <img src=" {{$url. $category->image }}"  onClick="swipe(this);"class="img-thumbnail" style="width:150px;height:150px">
             
                        </td>
                        <td>
      <img src=" {{ $url. $category->image_banner }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">
             
                        </td>
                        <td>
      <img src=" {{ $url. $category->image_website }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">
             
                        </td>
                        <td>
      <img src=" {{ $url. $category->image_app }}"  onClick="swipe(this);" class="img-thumbnail" style="width:150px;height:150px">
             
                        </td>
                        <td><i class="fa fa-cog"></i>  {{ $category->name }}
                        
                        @foreach ($category->subcategory as $subcat)
                       <ul >
                <li>
				<i class="fa fa-angle-double-right"></i>
					{{ $subcat->name }} 
				
					<a class="" href="{{URL::to('sub-category/'.$subcat->id.'/edit')}}">Edit</a>
					<a class=" " href="{{URL::to('sub-category/'.$subcat->id.'/delete')}}">Delete</a></li>
              
             </ul>
                         @endforeach 
                       
                        </td>
                        <td> 
							    {{ $category->description }}
                        <td>
                            <a class="btn btn-default" href="{{URL::to('category/'.$category->id.'/edit')}}">Edit</a>
                            <a class="btn btn-danger" href="{{URL::to('category/'.$category->id.'/delete')}}">Delete</a>
                           
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                    {!! $categories->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
			    $('.getsubcat').click(function(){
			    $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
		});
     
        
        </script>
        
@stop
