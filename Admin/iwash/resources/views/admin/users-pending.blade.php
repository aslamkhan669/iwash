@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  



  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Verified Users</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>

                        <th><input type="button"  status='1' name="setstatus" id="setstatus" value="Verify" class="btn btn-primary"></th>
                        <th>First Name</th>
                        <th>Last Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                            <th>Registration Date </th>
                           

                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
                          @foreach ($users as $user)
                      <tr>
                       <td>
                   <input type="checkbox" name="verification"  class="verification" value="<?php echo $user->id; ?>">


                       </td>
                       <td>{{@$user->userProfile->first_name}}</td>
                       <td>{{@$user->userProfile->last_name}}</td>
                       <td>{{$user->email}}</td>
                        <td>{{@$user->mobile}}</td>
                         <td>{{@$user->createdAt}}</td>
                          
                            
                             <td>
                                                         <span class="label label-danger">Not Verified</span>

                        </td>
                    
                      </tr>
                  @endforeach
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                           <div class="col-lg-6"> {!! $users->render() !!} </div>   


             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        
@stop
