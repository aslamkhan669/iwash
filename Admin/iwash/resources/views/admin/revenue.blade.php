@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       <script>
		   
		   function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    var is_chrome = Boolean(mywindow.chrome);

    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('</head><body >');
   
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
 if (is_chrome) {
        mywindow.onload = function() { // wait until all resources loaded 
            mywindow.focus(); // necessary for IE >= 10
            mywindow.print();  // change window to mywindow
            mywindow.close();// change window to mywindow
        };
    }
    else {
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        mywindow.close();
    }

    return true;

    return true;
}
		   </script>

  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
			  				  <div class="box-header with-border">
                  <div class="col-lg-12">
                    <div class="col-lg-3">
                    Revenue : <h3>QRS {{$total}}</h3>
                    </div> 
                    <div class="col-lg-3">
                    Subscription Revenue : <h3>{{$subtotal}}</h3>
                    </div>   
                    <div class="col-lg-3">
                    Total Clothes Processed : <h3>{{$clothes}}</h3>
                    </div>  
                    <div class="col-lg-3">
                    Total Customers : <h3>{{$customers}}</h3>
                    </div>   
                    
                  </div>



                </div><!-- /.box-header -->
                <div class="box-header">

                <form class="form-inline" name="branchchoose" id="branchchoose" method="get" action="">
   <?php   if(Session::get('status')=='admin'){ ?>
              
                      <select name='branch' id="selectlocation" class="form-control">
                        <option value="" >All Branches</option>
              <?php foreach($branches as $parent){ ?>
                 <option <?php if(isset($_GET['branch']) && ($_GET['branch']==$parent->id)){echo 'selected';} ?> value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>
                  
               <?php } ?>
                       </select>
                 
         <?php } ?>
</form>
                <form class="form-inline" method="get" action="">
                <div class="form-group">
                
              </div>
              <div class="form-group">
    <input type="text" class="form-control" name="from"   placeholder="Order From Date" onfocus="(this.type='date')" value="<?php echo @$_GET['from']; ?>">
    <input type="hidden" name="filtertype" value="simple">

  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="to"   placeholder="Order To Date" onfocus="(this.type='date')" value="<?php echo @$_GET['to']; ?>">
  </div>
  
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="payment_mode" >
            <option value="">Select Payment Mode</option>
           
            <option value="cash" <?php if(isset($_GET['payment_mode']) && $_GET['payment_mode']=='cash'){ echo 'selected';} ?>>Cash</option>
            <option value="card" <?php if(isset($_GET['payment_mode']) && $_GET['payment_mode']=='card'){ echo 'selected';} ?>>Card</option>
           
           
            </select>
            </div>


            <div class="form-group">
        
            <select class="form-control" id="sel1" name="status" >
            <option value="">Select Order Status</option>
           
            <option value="accepted" <?php if(isset($_GET['status']) && $_GET['status']=='accepted'){ echo 'selected';} ?>>Accepted</option>
           
           
            
            <option value="delivered" <?php if(isset($_GET['status']) && $_GET['status']=='delivered'){ echo 'selected';} ?>>Delivered</option>
            <option value="cancelled" <?php if(isset($_GET['status']) && $_GET['status']=='cancelled'){ echo 'selected';} ?>>Cancelled</option>
          </select>
            </div>
         
                 
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="payment_status" >
            <option value="">Select Payment Status</option>
            <option value="1" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']=="1"){ echo 'selected';} ?>>Paid</option>
            <option value="0" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']=="0"){ echo 'selected';} ?>>Pending</option>
          
          </select>
            </div>
  <button type="submit" class="btn btn-default">Submit</button>
  

</form>
<script>
var start = 2017;
var end = new Date().getFullYear();
var options = "<option value=''>Order Year</option>";
for(var year = start ; year <=end; year++){
  options += "<option>"+ year +"</option>";
}
document.getElementById("year").innerHTML = options;

</script>
   <!--Modal 
  <div class="modal fade" id="advance" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Advance Filter</h4>
        </div>
        <div class="modal-body">
            <form  method="get" action="{{URL::to('orders')}}">
            <input type="hidden" name="filtertype" value="advance">

<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Order Id" name='order_id' value="<?php echo @$_GET['order_id']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="First Name" name='first_name' value="<?php echo @$_GET['first_name']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Last Name" name='last_name' value="<?php echo @$_GET['last_name']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Mobile" name='mobile' value="<?php echo @$_GET['mobile']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
    
            <div class="form-group">
            <input type="text" class="form-control" placeholder="Email" name='email' value="<?php echo @$_GET['email']; ?>">
          </div>
          
          
            </div>
            <div class="col-lg-6">
            <div class="form-group">
                              <select name='service_type' class="form-control" placeholder="Service Type">
                    <option value="">Select</option>
                      <?php
                      
                       foreach($categories as $category){ ?>
                         <option <?php if(isset($_GET['service_type'])&&$_GET['service_type']==$category->id){ echo 'selected';} ?> value="<?= $category->id;?>" ><?= $category->name;?></option>
                          
                       <?php } ?>
                               </select>
                            </div>  
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
        
          <select class="form-control" id="sel1" name="addressType" placeholder="Address type">
            <option value="">Select Address type</option>
            <option value="home" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='home'){ echo 'selected';} ?>>Home</option>
            <option value="office" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='office'){ echo 'selected';} ?>>Office</option>
            <option value="other" <?php if(isset($_GET['addressType']) && $_GET['addressType']=='other'){ echo 'selected';} ?>>Other</option>
          </select>
          </div>
            </div>
            <div class="col-lg-6">
               
          <div class="form-group">
            <input type="text" class="form-control" name="flatandstreet" placeholder="Flat and Street" value="<?php echo @$_GET['flatandstreet']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control"  name="landmark"  placeholder="Landmark" value="<?php echo @$_GET['landmark']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" name="pickupDate"  placeholder="Pickup Date" onfocus="(this.type='date')" value="<?php echo @$_GET['pickupDate']; ?>" >
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" name="deliveryDate"  placeholder="Delivery Date" onfocus="(this.type='date')" value="<?php echo @$_GET['deliveryDate']; ?>">
          </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <input type="text" class="form-control" name="createdAt"  placeholder="Order Date" onfocus="(this.type='date')" value="<?php echo @$_GET['createdAt']; ?>">
          </div>
            </div>


</div>
<div class="col-lg-12">
            <div class="col-lg-6">
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="status" >
            <option value="">Select Order Status</option>
            <option value="placed" <?php if(isset($_GET['status']) && $_GET['status']=='placed'){ echo 'selected';} ?>>Placed</option>
            <option value="accepted" <?php if(isset($_GET['status']) && $_GET['status']=='accepted'){ echo 'selected';} ?>>Accepted</option>
            <option value="picked up" <?php if(isset($_GET['status']) && $_GET['status']=='picked up'){ echo 'selected';} ?>>Picked Up</option>
            <option value="processing" <?php if(isset($_GET['status']) && $_GET['status']=='processing'){ echo 'selected';} ?>>Processing</option>
            <option value="ready to deliver" <?php if(isset($_GET['status']) && $_GET['status']=='ready to deliver'){ echo 'selected';} ?>>Readt to delivery</option>
            <option value="delivered" <?php if(isset($_GET['status']) && $_GET['status']=='delivered'){ echo 'selected';} ?>>Delivered</option>
            <option value="cancelled" <?php if(isset($_GET['status']) && $_GET['status']=='cancelled'){ echo 'selected';} ?>>Cancelled</option>
          </select>
            </div>
            </div>
            <div class="col-lg-6">
                 
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="payment_status" >
            <option value="">Select Payment Status</option>
            <option value="1" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==1){ echo 'selected';} ?>>Paid</option>
            <option value="0" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==0){ echo 'selected';} ?>>Pending</option>
          
          </select>
            </div>
            </div>


</div>

<div class="col-lg-12">
     
            <div class="form-group">
            <input type="text" class="form-control" name="payment_date"  placeholder="Payment Date" onfocus="(this.type='date')" >
          </div>
        </div>



           
         
         
       
        
        
        
        
       
        
       
        
       
        
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
</form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>





-->




















                <form class="form-inline" method="get">
                
                </div><!-- /.box-header -->
                <div class="box-body">

                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					    <th>Order ID</th>
                       <th>Mobile/Email</th>
                        <th>First Name</th>
                              <th>Last Name</th>
                        
                  <!--
                          <th>Landmark</th>
                            <th>Address Type</th>
               -->
                            
                              <th>Order Placed Date</th>
                              <th>Order Status</th>
                              <th>Quantity</th>
                              <th>Total Amount</th>
                              <th>Payment Status</th>

                              


							                          
							                           <th>Action</th>


                      </tr>
                    </thead>
                    <tbody>
            
            @foreach ($orders as $order)



                      <tr>
					                          <td> IW{{ @$order->id}}</td>

					
                      <?php 
                     $str='Name: '."<br />";
                     $str.='Email: '.$order->user->email."<br />";
                      $str.='Mobile : '.$order->user->mobile."<br />";
                       $str.='Register Date : '.$order->user->createdAt."<br />";


                      ?>
                   <td ><?php if($order->user->mobile){ echo $order->user->mobile;}else{echo $order->user->mobile;} ?></td>


                         <td>{{@$order->userProfile->first_name}}</td>
                        <td>{{@$order->userProfile->last_name}}</td>
                  <!--      <td> {{ @$order->landmark}}</td>
                        <td> {{ @$order->addressType}}</td>
               -->
                       
                     
                         <td> {{ @$order->createdAt}}</td>
                       
					   
                                    <?php 
                                      $status='';
                                    if($order->status=='delivered'){
                                    $status='label-success"';
                                      
                                    }else  if($order->status=='cancelled'){
                                    $status='label-danger"';
                                      
                                    }else  if($order->status=='placed'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='accepted'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='picked up'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='processing'){
                                    $status='label-info"';
                                      
                                    }
                                          else  if($order->status=='ready to deliver'){
                                    $status='label-info"';
                                      
                                    }
                                    else  if($order->status=='delivered'){
                                    $status='label-success"';
                                      
                                    }
                                    
                                    ?>
                                             <td><span class="label <?php echo $status;?>">{{ @ucfirst($order->status)}}</span> </td>
                                             <td> {{ @$order->qty}}</td>
                                             <?php 
                                      $status='';
                                    if($order->payment_status==1){
                                         $status='label-success"';
                                      
                                    }else{
                                        $status='label-danger"';
                                      
                                    }
                                    
                                    ?>
                                             <td> {{ @$order->totalamount}}</td>
                                             <td><span class="label <?php echo $status;?>"><?php if($order->payment_status==1){echo 'Paid';}else{echo 'Pending';} ?></span> </td>

                                             <!--<td> <?php if($order->payment_status==0){echo '<span class="label label-default">NA</span>';}else{ echo $order->payment_date;} ?></td>-->

											 
                                             <td>
											 <div class="hidden" id="{{$order->id}}">
											 		   <style>
			   .card {
    display: inline-block;
    position: relative;
    width: 25%;
    margin-bottom: 30px;
    border-radius: 0;
    color: rgba(0,0,0, 0.87);
    background: #fff;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
}
.card-pricing .card-content {
    padding: 15px !important;
        margin-top: 50px;
}
.card-pricing ul {
    list-style: none;
    padding: 0;
    max-width: 240px;
    margin: 10px auto;
}
.card-pricing h2{
        text-align: center;
    font-size: 18px;
    font-weight: 900;
    margin: 0;
        padding: 2px;
    text-transform: uppercase;
}
.card-pricing h3{
    text-align: center;
    margin-top: 0px;
    font-weight: 900;
    text-transform: uppercase;
    font-size: 22px;
}
.card-pricing ul li {
        color: black;
    text-align: center;
    font-size: 16px;
    font-weight: bold;
    padding: 4px 0px;
}
table {
  border-collapse: collapse;
  margin: 0 auto;
}
td {
  width: 5px;
  height: 60px;
}
	img {
    -webkit-print-color-adjust: exact;
}	
.img-thumbnail {
    display: inline-block;
    max-width: 100%;
    height: auto;
    padding: 4px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}

bootstrap.min.css:5
img {
    vertical-align: middle;
}
			   </style>
			  
												 <div class="card card-pricing card-raised"> <div class="card-content"> <h2>Iwash Clean</h2> <h2>Laundry Service</h2> <h3>Job No. IW{{ @$order->id}}</h3> <ul> <li><b>{{ @$order->createdAt}}</b></li> <li><?php if($order->user->mobile){ echo $order->user->mobile;}else{echo $order->user->mobile;} ?></li>
													  
			    <?php 
	  
	  foreach($order->orderservices as $service){
	
	  ?>
	  <li><?php 
		 $servicetxt=$service->service->name; 
		  if($order->order_type=='subscription'){
			  @$servicetxt.=' ('.$service->service->Category->name.')';
		  }
		  echo $servicetxt."<br>";
		  ?></li> 
	  <?php } ?>
													 <li>{{ @$order->servicecat->name}}</li> <li> <img src="{{$url. $order->qr }}"   class="img-thumbnail" ></li> <li>IW{{ @$order->id}}</li></ul> </div> </div>
												 </div>
                                             <a class="btn btn-default btn-sm" href="{{URL::to('order/'.$order->id.'/view')}}">View</a>
                            <a class="btn btn-default btn-sm" href="{{URL::to('order/'.$order->id.'/edit')}}">Edit</a>
                            <a class="btn btn-danger btn-sm" href="{{URL::to('order/'.$order->id.'/delete')}}">Delete</a>
                            <a class="btn btn-primary btn-sm" onclick="PrintElem({{$order->id}})" >Print Label</a>
                          
                        
                        </td>

                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                  {!! $orders->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        

@stop
