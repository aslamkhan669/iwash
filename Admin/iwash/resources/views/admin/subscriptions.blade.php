@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
			  				  <div class="box-header with-bsubscription">
<a href="/subscriptions"><h3>ALL subscriptions </h3></a>
                </div><!-- /.box-header -->
                <div class="box-header">

                <form class="form-inline" method="get" action="{{URL::to('subscriptions')}}">
                <div class="form-group">
                <select class="form-control" required name="year" id="year">
    
 
  </select>
              </div>
              <div class="form-group">
    <input type="text" class="form-control" name="from" required  placeholder="Subscripton From Date" onfocus="(this.type='date')" value="<?php echo @$_GET['from']; ?>">
    <input type="hidden" name="filtertype" value="simple">

  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="to"  required placeholder="Subscripton To Date" onfocus="(this.type='date')" value="<?php echo @$_GET['to']; ?>">
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
  <a  data-toggle="modal" data-target="#advance">Open Advance Filter</a>

</form>
<script>
var start = 2017;
var end = new Date().getFullYear();
var options = "<option value=''>Subscripton Year</option>";
for(var year = start ; year <=end; year++){
  options += "<option>"+ year +"</option>";
}
document.getElementById("year").innerHTML = options;

</script>
  <!-- Modal -->
  <div class="modal fade" id="advance" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Advance Filter</h4>
        </div>
        <div class="modal-body">
            <form  method="get" action="{{URL::to('subscriptions')}}">
            <input type="hidden" name="filtertype" value="advance">

  <div class="form-group">
    <input type="text" class="form-control" placeholder="First Name" name='first_name' value="<?php echo @$_GET['first_name']; ?>">
  </div>
   <div class="form-group">
    <input type="text" class="form-control" placeholder="Last Name" name='last_name' value="<?php echo @$_GET['last_name']; ?>">
  </div>
   <div class="form-group">
    <input type="text" class="form-control" placeholder="Mobile" name='mobile' value="<?php echo @$_GET['mobile']; ?>">
	</div>
    <div class="form-group">
    <input type="text" class="form-control" placeholder="Email" name='email' value="<?php echo @$_GET['email']; ?>">
	</div>
  
  
  <div class="form-group">
  <div class="form-group">
                      <select name='package_id' class="form-control" placeholder="Packages">
					  <option value="">Select Package</option>
						  <?php
						  
						   foreach($packages as $val){ ?>
							   <option <?php if(isset($_GET['package_id'])&&$_GET['package_id']==$val->id){ echo 'selected';} ?> value="<?= $val->id;?>" ><?= $val->package;?></option>
                  
							 <?php } ?>
                       </select>
                    </div>  </div>


    <div class="form-group">

    <select class="form-control" id="sel1" name="status" >
    <option value="">Select subscription Status</option>
    <option value="0" <?php if(isset($_GET['status']) && $_GET['status']=='0'){ echo 'selected';} ?>>Inactive</option>
    <option value="1" <?php if(isset($_GET['status']) && $_GET['status']=='1'){ echo 'selected';} ?>>Pending</option>
  </select>
    </div>

    <div class="form-group">

    <select class="form-control" id="sel1" name="payment_status" >
    <option value="">Select Payment Status</option>
    <option value="1" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==1){ echo 'selected';} ?>>Paid</option>
    <option value="0" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==0){ echo 'selected';} ?>>Pending</option>
  
  </select>
    </div>
    <div class="form-group">
    <input type="text" class="form-control" name="payment_date"  placeholder="Payment Date" onfocus="(this.type='date')" >
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

              
                </div><!-- /.box-header -->
                <div class="box-body">

                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
                  <table id="example1" class="table table-bsubscriptioned table-striped">
                    <thead>
                      <tr>
                       <th>User</th>
                        <th>Package</th>
                        <th>Pickup Option</th>
                        <th>Duration</th>
                        <th>Monthly Price(QRS )</th>
                              <th>Subscription  Date</th>
                              <th>Subscription Status</th>
                              <th>Is Expired</th>
                              <th>Package limit used</th>
                              <th>Total Amount(QRS)</th>
                              <th>Payment Status</th>

                              <th>Payment Date</th>


							                          
							                           <th>Action</th>


                      </tr>
                    </thead>
                    <tbody>
            
            @foreach ($subscriptions as $subscription)



                      <tr>

				
                      <?php 
                     $str='Name: '.$subscription->userProfile->first_name.' '.$subscription->userProfile->last_name."<br />";
                     $str.='Email: '.$subscription->user->email."<br />";
                      $str.='Mobile : '.$subscription->user->mobile."<br />";
                       $str.='Register Date : '.$subscription->user->createdAt."<br />";


                      ?>
                   <td data-html="true"  title="{{$subscription->userProfile->first_name.' '.$subscription->userProfile->last_name}}" data-toggle="popover" data-trigger="hover" data-content="<?php echo $str; ?>">{{ @$subscription->userProfile->first_name}}</td>


                        <td> {{ @$subscription->Package->package}}

 <?php
//print_r( $subscription->SubscribeServices);
// foreach( $subscription->SubscribeServices as $services){
// echo '<p><a>&raquo; '.$services->Service->name.'('.$services->category->name.')</a></p>';

// }
 
 ?>


                        </td>
                   
                        <td> {{ @$subscription->pickup_option}}</td>
                        <td> {{ @$subscription->duration}} Month</td>
                        <td> {{ @$subscription->Package->pricing_for_each_month}}</td>

                         <td> {{ @$subscription->createdAt}}</td>
                       
					   
                                    <?php 
                                      $status='';
                                    if($subscription->status===1 && $subscription->payment_status==1){
                                    $status='label-success"';
                                    $subsStatus="Active";
                                      
                                    }else  if($subscription->status==1 && $subscription->payment_status==0){
                                      $status='label-warning"';
                                      $subsStatus="Pending";
                                      }else {
                                    $status='label-danger"';
                                    $subsStatus="Inactive";
                                      
                                    }
                                  
                                    ?>
                                             <td><span class="label <?php echo $status;?>"><?php echo $subsStatus;?></span> </td>
                                         

<?php 
 $time = strtotime($subscription->createdAt);
$final = date("Y-m-d", strtotime("+".$subscription->duration." month", $time));
if(date("Y-m-d")>=$final  && $subscription->payment_status==1){
  $isExpired=1;
}else{

  $isExpired=0;
}

//print_r($subscription->SubscribeServices->toArray());

if($subscription->payment_status==1){

  $remaining=$subscription->SubscribeServices->sum('quantity');
}
?>

                                             <td><span class="label  <?php if($isExpired==1){ echo 'label-danger';}else{echo 'label-default' ;} ?>"><?php if($isExpired==1){ echo 'Yes';}else if($subscription->payment_status==1){ echo 'No';}else{echo 'NA' ;} ?></span> </td>
                                             <td><span class="label  <?php if($subscription->payment_status==1 &&  $remaining<=0){ echo 'label-danger';}else{echo 'label-default' ;} ?>"><?php if($subscription->payment_status==1 &&  $remaining<=0){ echo 'Yes';}else if($subscription->payment_status==1 ){ echo 'No';}else{echo 'NA' ;} ?></span> </td>

                                         
                                         <?php 
                                      $status='';
                                    if($subscription->payment_status==1){
                                         $status='label-success"';
                                      
                                    }else{
                                        $status='label-danger"';
                                      
                                    }
                                    
                                    ?>
                                             <td><?php if($subscription->payment_status==0){echo '<span class="label label-default">NA</span>';}else{  echo $subscription->payment_amount;} ?></td>
                                             <td><span class="label <?php echo $status;?>"><?php if($subscription->payment_status==1){echo 'Paid';}else{echo 'Pending';} ?></span> </td>

                                             <td><?php if($subscription->payment_status==0){echo '<span class="label label-default">NA</span>';}else{ echo $subscription->payment_date;} ?> </td>

											 
                                             <td>
                                             <a class="btn btn-default btn-sm" href="{{URL::to('subscription/'.$subscription->id.'/view')}}">View</a>
                            <a class="btn btn-default btn-sm" href="{{URL::to('subscription/'.$subscription->id.'/edit')}}">Edit</a>
                          
                        
                        </td>

                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                  {!! $subscriptions->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        

@stop
