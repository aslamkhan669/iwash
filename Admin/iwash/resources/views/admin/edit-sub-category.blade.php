@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" method='post' action=''>
								{{ csrf_field() }}

                  <div class="box-body">
					   <div class="form-group">
                      <label >Parent Category Name</label>
                      <select name='parent' class="form-control">
						  <?php foreach($parentcategories as $parent){ ?>
							   <option value="<?= $parent->id;?>" <?php if($parent->id==$category->id){echo 'selected';} ?>><?= $parent->name;?></option>
                  
							 <?php } ?>
                       </select>
                    </div>
                    <div class="form-group">
                      <label >Sub Category Name</label>
                      <input type="text" class="form-control"  value="{{ @$category->name }}" name='name' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label >Sub Category Description</label>
                    <textarea class="form-control"  name='description' required>{{ @$category->description }}</textarea>
                    </div>
                       <div class="form-group">
                      <label >Sub Category Image</label>
                      <input type="file" class="form-control"  name='cat_image' id="exampleInputEmail1" required >
                       <img src=" {{URL::to('/').'/'.  $category->image }}"  onClick="swipe(this);"class="img-thumbnail" style="width:150px;height:150px">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
                
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
