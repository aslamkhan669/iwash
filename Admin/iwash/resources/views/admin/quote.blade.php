@extends('admin.layouts.main')
 @section('container')
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">

                </div><!-- /.box-header -->
                <div class="box-body">
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>From</th>
                            <th>Email</th>
                        <th>Mobile</th>
                          <th>Category</th>
                        <th>Max Budget</th>
                 
                       <th>Min Budget</th>
                       <th>Description</th>
                        <th>Date</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						@foreach ($quote as $value)

                      <tr>
                        <td>
                           <?php 
                            $usertype=$value->users->role->slug;
                             if($usertype=='admin'){
                              $url='';
                            }else{
                           $url='service-'.$usertype.'/'.$value->from_id;
                            }

                             ?>
                            <a href="{{$url}}">
							  {{ $value->users->userProfile->first_name.' '.$value->users->userProfile->last_name }} 
                        
                         </a>
                        </td>
                                       <td>{{ @$value->user->email}}</td>
                        <td>{{ @$value->userProfile->mobile}}</td>
                          <td>
					  {{ $value->category->name }} 
                        </td>
                           <td>
							 {{ $value->max_budget }}	
                        </td>
                           <td>
							 {{ $value->min_budget }}	
                        </td>
                           <td>
							 {{ $value->description }}	
                        </td>
                       
                        <td> 
							{{ $value->created_at }}
						 </td>
							 
                        <td>
                            <a class="btn btn-danger" href="/quote/{{ $value->id }}/delete">Delete</a>
                          
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                     <div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	</div>   
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
       
        
@stop
