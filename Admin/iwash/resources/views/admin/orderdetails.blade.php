@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          
            <button onclick="goBack()">Go Back</button>
   
            <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i>
           &nbsp;&nbsp;&nbsp;
           <span>Branch: {{@$order->orderbranch->branch}}-{{@$order->orderbranch->location}}</span>
           &nbsp;&nbsp;&nbsp;
           <span>Order Source: 
<?php 
 if($order->added_by!=NULL && $order->branch!=NULL){

                                     if($order->addedby->role->slug=='admin'){
                                       echo 'by Super Admin';

                                     }else{
                                       echo $order->addedby->email.'(' .$order->addedby->userlocation->branch.'-'.$order->addedby->userlocation->location.')<br>';
                                      echo 'Branch Admin';
                                     }
                               }else{

                                  echo 'Website/App';

                               }
?>
           </span>
          <small class="pull-right"><strong>ORDER Date: {{ @$order->createdAt}}</strong></small>
        </h2>
        <a class="btn btn-default btn-sm" href="{{URL::to('order/'.$order->id.'/edit')}}">Edit</a>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
      <h3>ORDER ID : IW{{ @$order->id}}</h3>
      <div>

      <img src=" {{ $url. $order->qr }}"  onClick="swipe(this);" class="img-thumbnail" ></td>

      </div>

       
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
  <h4> Customer Details</h4>
        <address>
          <strong>{{ @$order->userProfile->first_name}} {{ @$order->userProfile->last_name}} </strong><br>
          <b>Address Type: </b> {{$order->addressType}}<br>
          <b>flatandstreet: </b> {{$order->flatandstreet}}<br>
          <b>landmark: </b> {{$order->landmark}}<br>
          <b> Email: </b> {{ @$order->user->email }} <br>
          <b>Mobile: </b>  {{ @$order->user->mobile }} <br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
      <h4> Order Details</h4>
	  
        <b>Order Type:</b> <?php if($order->order_type=='simple'){ echo 'Simple'; }else{ echo 'Subscription';}?><br>
        <b>Pickup Date:</b>  {{ @$order->pickupDate}}<br>
        <b>Pickup Time:</b>  {{@$order->pickup->timebetween}}<br>
        <b>Delivery Date:</b> {{ @$order->deliveryDate}}<br>
        <b>Delivery Time:</b> {{@$order->delivery->timebetween}}<br>

        <?php 
         $status="";
					   if($order->status=='delivered'){
					   $status='label-success"';
						   
						 }else  if($order->status=='cancelled'){
					   $status='label-danger"';
						   
						 }else  if($order->status=='placed'){
					   $status='label-warning"';
						   
						 }else  if($order->status=='accepted'){
					   $status='label-warning"';
						   
						 }else  if($order->status=='picked up'){
					   $status='label-warning"';
						   
						 }else  if($order->status=='processing'){
					   $status='label-info"';
						   
						 }
                  else  if($order->status=='ready to deliver'){
					   $status='label-info"';
						   
						 }
						 else  if($order->status=='delivered'){
					   $status='label-success"';
						   
						 }
					   
					   ?>
                   <b>Order Status:</b>                     <span class="label <?php echo $status;?>">{{ @ucfirst($order->status)}}</span> 
											 
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
	<div class="col-md-12 text-left"> <h3>
		<?php if($order->order_type=='simple'){
			echo 'Item Details: ';
			}else if($order->order_type=='subscription'){
			echo 'Subscription Package: '. @$order->subscription->Package->package;
			}
			?>
		</h3></div>
      <div class="col-xs-12 table-responsive">
	 
     
      <table class="table table-striped">
          <thead>
          <tr>
		    <th>Item</th>
            <th>Qty Wash</th>
            <th>Qty Dry Clean</th>
            <th>Qty Iron</th>

            <th>Price Wash(per item)</th>
            <th>Price Dry Clean(per item)</th>
            <th>Price Iron(per item)</th>

          </tr>
          </thead>
          <tbody>
       
            <?php 
	  
	  foreach($order->orderservices as $service){
    
      $html = "<tr><td>".$service->service->name."</td>";
      if($service->qty_w!=null){
        $html.= "<td>".$service->qty_w."</td>";
       
      }
      else{
         $html.= "<td>0</td>";
       

      }
        if($service->qty_dc!=null){
        $html.= "<td>".$service->qty_dc."</td>";
        
      }
       else{
         $html.= "<td>0</td>";
       

      }
      if($service->qty_ir!=null){
        $html.= "<td>".$service->qty_ir."</td>";
        
      }
       else{
         $html.= "<td>0</td>";
       

      }
    
       $html.= "<td>".$service->price_w."</td>";
       $html.= "<td>".$service->price_dc."</td>";
       $html.= "<td>".$service->price_ir."</td>";
      $html.="</tr>";
      echo $html;


	   } ?>
          
        
         
          </tbody>
        </table>

         
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
    
      <!-- /.col -->
      <div class="col-xs-6">
        <p class="lead">Payment Date: {{$order->payment_date}}</p>
        <p class="lead"><span class="label label-<?php if($order->payment_status==1){echo 'success';}else{ echo 'danger';} ?>"><?php if($order->payment_status==1){echo 'Payment Completed';}else{ echo 'Payment Pending';} ?></span></p>
        <div class="table-responsive">
          <table class="table">
            <tbody>
            <tr>
              <th>Quantity: {{$order->qty}}</th>
              <td></td>
            </tr>
            <tr>
              <th>Total:QRS {{$order->totalamount}}</th>
              <td></td>
            </tr>
          </tbody></table>
        </div>
      </div>
      <div class="col-xs-6">
     
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
          

            </div><!-- /.col -->
          </div><!-- /.row -->


        </section><!-- /.content -->
    <script>
function goBack() {
    window.history.back();
}
</script>
        
@stop
