@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
			  				  <div class="box-header with-border">
<a href="{{URL::to('order/graphs')}}"><h3>ALL Orders </h3></a>
                </div><!-- /.box-header -->
                <div class="box-header">


                <form class="form-inline" method="get" action="{{URL::to('order/graphs')}}">
                <div class="form-group">
                <select class="form-control" required name="year" id="year">
    
 
  </select>
              </div>
              <div class="form-group">
              <select name="month" id="month" required  class="form-control">
              <option value="">Select month</option>
              <?php
           for ($m=1; $m<=12; $m++) {
            $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
            echo "<option value='$m'>$month</option>";
            }?>
              </select>  
                <input type="hidden" name="filtertype" value="simple">

  </div>

  <div class="form-group">
        <select name='service_type' class="form-control" placeholder="Service Type">
            <option value="">Select Service Type</option>
            <?php

            foreach($categories as $category){ ?>
            <option <?php if(isset($_GET['service_type'])&&$_GET['service_type']==$category->id){ echo 'selected';} ?> value="<?= $category->id;?>" ><?= $category->name;?></option>

            <?php } ?>
        </select>
   </div>  



            <div class="form-group">
        
            <select class="form-control" id="sel1" name="status" >
            <option value="">Select Order Status</option>
            <option value="placed" <?php if(isset($_GET['status']) && $_GET['status']=='placed'){ echo 'selected';} ?>>Placed</option>
            <option value="accepted" <?php if(isset($_GET['status']) && $_GET['status']=='accepted'){ echo 'selected';} ?>>Accepted</option>
            <option value="picked up" <?php if(isset($_GET['status']) && $_GET['status']=='picked up'){ echo 'selected';} ?>>Picked Up</option>
            <option value="processing" <?php if(isset($_GET['status']) && $_GET['status']=='processing'){ echo 'selected';} ?>>Processing</option>
            <option value="ready to deliver" <?php if(isset($_GET['status']) && $_GET['status']=='ready to deliver'){ echo 'selected';} ?>>Readt to delivery</option>
            <option value="delivered" <?php if(isset($_GET['status']) && $_GET['status']=='delivered'){ echo 'selected';} ?>>Delivered</option>
            <option value="cancelled" <?php if(isset($_GET['status']) && $_GET['status']=='cancelled'){ echo 'selected';} ?>>Cancelled</option>
          </select>
            </div>
         
                 
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="payment_status" >
            <option value="">Select Payment Status</option>
            <option value="1" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==1){ echo 'selected';} ?>>Paid</option>
            <option value="0" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']==0){ echo 'selected';} ?>>Pending</option>
          
          
          </select>
            </div>
  <button type="submit" class="btn btn-default">Submit</button>

</form>
<script>
var start = 2017;
var end = new Date().getFullYear();
var options = "<option value=''>Order Year</option>";
for(var year = start ; year <=end; year++){
  options += "<option>"+ year +"</option>";
}
document.getElementById("year").innerHTML = options;

</script>

                
                </div><!-- /.box-header -->
                <div class="box-body">
              
                
                
                                <div id="chartdiv"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        

@stop
