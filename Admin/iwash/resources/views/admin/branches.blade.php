@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				
		
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Branch Name</th>
                        <th>Location</th>

                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>

						
						@foreach ($branches as $branch)

                      <tr>
                        <td>    {{$branch->branch}} </td>
                        <td>    {{$branch->location}} </td>
             
                        <td>
                      

                            <a class="btn btn-success btn-sm" href="/branch/{{ $branch->id }}/edit">Edit</a>
                            <a class="btn btn-danger btn-sm" href="/branch/{{ $branch->id }}/delete">Delete</a>
                            <a class="btn btn-info btn-sm"  href="/branch/add-admin"> Add Branch Admin</a>
                            <a  class="btn btn-info btn-sm"  href="/branch/admins"> View Branch Admins </a>
                            <a class="btn btn-info btn-sm"  href="/branch/add-employee"> Add Employee</a>
                            <a  class="btn btn-info btn-sm"  href="/branch/employees"> View Employees </a>
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
<div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	{!! $branches->render() !!} </div>         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
     
@stop
