@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Categories</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
	        
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>Profile</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Message</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						
						@foreach ($testimonials as $testimonial)

     <tr>
      <td>
      <img src=" {{ $url.$testimonial->profile }}"  onClick="swipe(this);"class="img-thumbnail" style="width:150px;height:150px">
             
                        </td>
                        <td> {{ $testimonial->name }}      </td>
                         <td> {{ $testimonial->designation }}      </td>
                        <td> 
							    {{ $testimonial->message }}
                        <td>
                            <a class="btn btn-default" href="{{URL::to('testimonial/'.$testimonial->id.'/edit')}}">Edit</a>
                            <a class="btn btn-danger" href="{{URL::to('testimonial/'.$testimonial->id.'/delete')}}">Delete</a>
                          
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                    {!! $testimonials->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        <script>
        $(document).ready(function(){
			    $('.getsubcat').click(function(){
			    $.get("/delete/"+id, function(data) {

    if(data=="OK"){
      var target = $("#"+id);

      target.hide('slow', function(){ target.remove(); });

    }
    
  });
  });
		});
     
        
        </script>
        
@stop
