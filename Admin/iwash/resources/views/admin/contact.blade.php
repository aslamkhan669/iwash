@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Contacts</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Message</th>
                        <th>Date</th>

                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>
						
						@foreach ($messages as $message)



                      <tr>
                        <td> 
                         
               <a href="">  {{ $message->name }} 
                        
                         </a> 
                        </td>
                        <td>{{ @$message->email}}</td>
                        <td>{{ @$message->mobile}}</td>
                       <td> 
                    {{ $message->message}}
                        
                        </td>
                         <td> 
                    {{ $message->createdAt}}
                        
                        </td>
                        <td>
                          
                            <a class="btn btn-danger" href="{{URL::to('/contacts/'.$message->id.'/delete')}}">Delete</a>
                          
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                  {!! $messages->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        
@stop
