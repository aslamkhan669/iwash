@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       <script>
		   
		   function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=24cm,width=60cm');
    var is_chrome = Boolean(mywindow.chrome);

    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('</head><body style="padding-left:0.3cm!important;display: table-cell; width: 100%">');
   
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
 if (is_chrome) {
        mywindow.onload = function() { // wait until all resources loaded 
            mywindow.focus(); // necessary for IE >= 10
            mywindow.print();  // change window to mywindow
            mywindow.close();// change window to mywindow
        };
    }
    else {
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        mywindow.close();
    }

    return true;

    
}
		   </script>

  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
			  				  <div class="box-header with-border">
<a href="/orders"><h3>Orders </h3></a>

<form class="form-inline" name="branchchoose" id="branchchoose" method="get" action="">
   <?php   if(Session::get('status')=='admin'){ ?>
              
                      <select name='branch' id="selectlocation" class="form-control">
                        <option value="" >All Branches</option>
              <?php foreach($branches as $parent){ ?>
                 <option <?php if(isset($_GET['branch']) && ($_GET['branch']==$parent->id)){echo 'selected';} ?> value="<?= $parent->id;?>" ><?= $parent->branch;?>(<?= $parent->location;?>)</option>
                  
               <?php } ?>
                       </select>
                 
         <?php } ?>
</form>
                </div><!-- /.box-header -->
                <div class="box-header">


                <form class="form-inline" method="get" action="">
                <div class="form-group">
                <select class="form-control hidden"  name="year" id="year">
    
 
  </select>
              </div>
              <div class="form-group">
    <input type="text" class="form-control" name="orderid"   placeholder="Enter Order Id IW-" value="<?php echo @$_GET['orderid']; ?>">
    <input type="hidden" name="filtertype" value="simple">

  </div>
              <div class="form-group">
    <input type="text" class="form-control" name="from"   placeholder="Order From Date" onfocus="(this.type='date')" value="<?php echo @$_GET['from']; ?>">
    <input type="hidden" name="filtertype" value="simple">

  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="to"   placeholder="Order To Date" onfocus="(this.type='date')" value="<?php echo @$_GET['to']; ?>">
  </div>
  



            <div class="form-group">
        
            <select class="form-control" id="sel1" name="status" >
            <option value="">Select Order Status</option>
            <option value="accepted" <?php if(isset($_GET['status']) && $_GET['status']=='accepted'){ echo 'selected';} ?>>Accepted</option>
            
            <option value="delivered" <?php if(isset($_GET['status']) && $_GET['status']=='delivered'){ echo 'selected';} ?>>Delivered</option>
            <option value="cancelled" <?php if(isset($_GET['status']) && $_GET['status']=='cancelled'){ echo 'selected';} ?>>Cancelled</option>
          </select>
            </div>
         
                 
            <div class="form-group">
        
            <select class="form-control" id="sel1" name="payment_status" >
            <option value="">Select Payment Status</option>
            <option value="1" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']=="1"){ echo 'selected';} ?>>Paid</option>
            <option value="0" <?php if(isset($_GET['payment_status']) && $_GET['payment_status']=="0"){ echo 'selected';} ?>>Pending</option>
          
          </select>
            </div>
  <button type="submit" class="btn btn-default">Submit</button>
  
  

</form>
<script>
var start = 2017;
var end = new Date().getFullYear();
var options = "<option value=''>Order Year</option>";
for(var year = start ; year <=end; year++){
  options += "<option>"+ year +"</option>";
}
document.getElementById("year").innerHTML = options;

</script>
 

























                <form class="form-inline" method="get">
                
                </div><!-- /.box-header -->
                <div class="box-body">

                @if(session()->has('ok'))
      @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif  
    @if(isset($info))
      @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
    @if(session()->has('error'))
      @include('partials/error', ['type' => 'danger', 'message' => session('error')])
    @endif  
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					    <th>Order ID</th>
                       <th>Mobile/Email</th>
                        
                          <!--<th>Flat and Street</th>-->
                  <!--
                          <th>Landmark</th>
                            <th>Address Type</th>
               -->
                             <th>First Name</th>
                              <th>Last Name</th>
                              <th>Order Placed Date</th>
                              <th>Order Status</th>
                              <th>Quantity</th>
                              <th>Total Amount</th>
                              <th>Payment Status</th>

                              <!--<th>Payment Date</th>-->


							                          
							                           <th>Action</th>


                      </tr>
                    </thead>
                    <tbody>
            
            @foreach ($orders as $order)



                      <tr>
					                          <td> IW{{ @$order->id}}</td>

					
                      <?php 
                     $str='Name: '."<br />";
                     $str.='Email: '.$order->user->email."<br />";
                      $str.='Mobile : '.$order->user->mobile."<br />";
                       $str.='Register Date : '.$order->user->createdAt."<br />";


                      ?>
                   <td ><?php if($order->user->mobile){ echo $order->user->mobile;}else{echo $order->user->mobile;} ?></td>


                        
                        <!--<td> {{ @$order->flatandstreet}}</td>-->
                  <!--      <td> {{ @$order->landmark}}</td>
                        <td> {{ @$order->addressType}}</td>
               -->
                        <!--<td> {{ @$order->pickupDate}} ({{@$order->pickup->timebetween}})</td>
                        <td> {{ @$order->deliveryDate}} ({{@$order->delivery->timebetween}})</td>-->
                        <td>{{@$order->userProfile->first_name}}</td>
                        <td>{{@$order->userProfile->last_name}}</td>
                     
                         <td> {{ @$order->createdAt}}</td>
                       
					   
                                    <?php 
                                      $status='';
                                    if($order->status=='delivered'){
                                    $status='label-success"';
                                      
                                    }else  if($order->status=='cancelled'){
                                    $status='label-danger"';
                                      
                                    }else  if($order->status=='placed'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='accepted'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='picked up'){
                                    $status='label-warning"';
                                      
                                    }else  if($order->status=='processing'){
                                    $status='label-info"';
                                      
                                    }
                                          else  if($order->status=='ready to deliver'){
                                    $status='label-info"';
                                      
                                    }
                                    else  if($order->status=='delivered'){
                                    $status='label-success"';
                                      
                                    }
                                    
                                    ?>
                                             <td><span class="label <?php echo $status;?>">{{ @ucfirst($order->status)}}</span> </td>
                                             <td> {{ @$order->qty}}</td>
                                             <?php 
                                      $status='';
                                    if($order->payment_status==1){
                                         $status='label-success"';
                                      
                                    }else{
                                        $status='label-danger"';
                                      
                                    }
                                    
                                    ?>
                                             <td> {{ @$order->totalamount}}</td>
                                             <td><span class="label <?php echo $status;?>"><?php if($order->payment_status==1){echo 'Paid';}else{echo 'Pending';} ?></span> </td>

                                             <!--<td> <?php if($order->payment_status==0){echo '<span class="label label-default">NA</span>';}else{ echo $order->payment_date;} ?></td>-->

											 
                                             <td>
											 <div class="hidden" id="{{$order->id}}">
	 		  <style>
            @page{
                size: 6cm 2.4cm;
      size: landscape;
      margin:none;
    
             }  
   .card-pricing h2{
    text-align: center;
    font-size: 12px;
    font-weight: 900;
    margin: 0;
    padding: 2px;
    text-transform: uppercase;
     } 
     .card-pricing h3{
    text-align: center;
    margin-top: 0px;
    font-weight: 900;
    text-transform: uppercase;
    font-size: 6px;
    }  
    .card-pricing ul li {
      text-align:center;
    margin-right:0px;
    font-weight: 600;
    text-transform: uppercase;
    font-size: 6px;  
    } 
    .card ul{
      padding:0;
    } 
    .img-thumbnail {
    display: inline-block;
    max-width: 100%;
    height: auto;
    padding: 0px;
    line-height: 1.42857143;
    background-color: #fff; 
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    margin-top:5px;
}
img {
    -webkit-print-color-adjust: exact;
} 
</style>
			  
		<div class="card card-raised card-pricing"> 
    <div class="card-content text-center"> 
    <h2 style="text-transform:none!important;">iWash</h2>  
    <h3 style="text-transform:none!important;">Job No. IW{{ @$order->id}}</h3> 
    <ul style = " list-style: none;margin-top:10px"> <li>Date: <?php echo explode(" ",$order->createdAt)[0];?></li>
    <li style="">Mob: <?php if($order->user->mobile){ echo $order->user->mobile;}else{echo $order->user->email;} ?></li>
		<?php 	  
	    foreach($order->orderservices as $service){
	
	  ?>
	  <li><?php 
		  $servicetxt=$service->service->name; 
		  if($order->order_type=='subscription'){
			  @$servicetxt.=' ('.$service->service->Category->name.')';
		  }
		 // echo $servicetxt."<br>";
		  ?></li> 
	  <?php } ?>
	  <li></li> <li style=""> <img src="{{$url. $order->qr }}"   class="img-thumbnail" ></li> 
    <li style="">IW{{ @$order->id}}</li></ul> </div> </div>
												 </div>
                            <a class="btn btn-default btn-sm" href="{{URL::to('order/'.$order->id.'/view')}}">View</a>
                            <a class="btn btn-default btn-sm" href="{{URL::to('order/'.$order->id.'/edit')}}">Edit</a>
                            <a class="btn btn-danger btn-sm" href="{{URL::to('order/'.$order->id.'/delete')}}">Delete</a>
                            <a class="btn btn-primary btn-sm" onclick="PrintElem({{$order->id}})" >Print Label</a>
                          
                        
                        </td>

                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
                  {!! $orders->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    
        

@stop
