@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-bsubscription">
                <button onclick="goBack()">Go Back</button>

    <script>
function goBack() {
    window.history.back();
}
</script>
           
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method='post' enctype='multipart/form-data'>
                {{ csrf_field() }}
             <!--      <input type="hidden" name="id"
                   value="{{ ($infosubscription) ? $infosubscription['id'] : ''}}"> -->
                <div class="box-body">
                  <div class="form-group">
                    <div class="col-lg-12">
                    <div class="col-lg-6">
                      <label>Email</label>
                      <input type="text" class="form-control" name='email' id='email' value="{{ ($usersubs) ? $usersubs['email'] : ''}}" readonly="readonly">
                      </div>
                      <div class="col-lg-6">
                      <label>Mobile</label>
                      <input type="text" class="form-control" name='mobile' id='mobile' value="{{ ($usersubs) ? $usersubs['mobile'] : ''}}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-lg-12" >
                    <div class="col-lg-6" >
                   <!-- <span  id="isuserexist" ></span>-->
                    </div>
                    <div class="col-lg-6">
                    </div>
                    </div>
                    <div class="col-lg-12">
                    <div class="col-lg-6">
                      <label>First Name</label>
                      <input type="text" class="form-control" name='first_name'  value="{{ ($userprofile) ? $userprofile['first_name'] : ''}}" readonly="readonly">
                      </div>
                      <div class="col-lg-6">
                      <label>Last Name</label>
                      <input type="text" class="form-control" name='last_name' id='lastname' value="{{ ($userprofile) ? $userprofile['last_name'] : ''}}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-lg-12">
                    <div class="col-lg-6">
                      <label>Package name</label>
                      <select name="package_id" class="form-control" id="package">
                      <option value="">Select Package</option>
              @foreach($itemlist as $item)
              <option value="{{ $item->id }}" {{ $selectedpackage == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
            
            @endforeach
          
                  </select >
                      </div>
                      <div class="col-lg-6">
                      <label>Qty</label>
                      <input type="text" class="form-control" name='qty_clothes' id="qty_clothes" value="{{ ($packagesubs) ? $packagesubs['qty_clothes'] : ''}}" readonly="readonly">
                      </div>
                    </div>
                <div class="col-lg-12">
                    <div class="col-lg-6">
                      <label>Description</label>
                      <input type="text" class="form-control" name='description' id="description" value="{{ ($packagesubs) ? $packagesubs['description'] : ''}}" readonly="readonly">
                      </div>
                      <div class="col-lg-6">
                      <label>Price</label>
                      <input type="number" class="form-control" name='price' id="price"  value="{{ ($packagesubs) ? $packagesubs['price'] : ''}}" readonly="readonly">
                      </div>
                    </div>
                   <div class="col-lg-12">
                   <div class="col-lg-6">
            <label>Duration</label>
            <select name="duration" class="form-control" id="duration">
           
                       <?php for($i=0;$i<12;$i++){
            echo "<option value='".($i+1)."'". (($infosubscription['duration']==($i+1))? "selected": "" ). ">". ($i+1)." Month </option>"; 
            } ?>
              </select >
          
            </div>
                    <div class="col-lg-6">
                    <label>Amount Paid</label>
                    <input type="number" class="form-control" id="amount" readonly="readonly" name="amount_paid" value="{{ ($infosubscription) ? $infosubscription['amount_paid'] : ''}}">
                    </div>
                    </div>
                </div>
                </div>
              
                <!-- /.box-body -->
        
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary" value="all-subscription/{id}/edit">Update Subscription</button>
                </div>
              </form>
     
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
          <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      <script>


$(document).ready(function(){
    $("#package").change(function(){
        var $id = $(this).val();
       
       debugger;
            $.ajax({
                type: "GET",
                url: "/Simplesubscription/"+$id,
                cache: false,
                contentType:"application/json",
                success: function(data) {
                    $("#qty_clothes").val( data.qty_clothes );
                    $("#description").val( data.description );
                    $("#price").val( data.price );
                    $("#amount").val("0");
                    $("#duration").val( data.duration );
                }
            });
        

          

    })
    $("#duration").change(function(){
      debugger;
        var duration = $(this).val();
        var price = $("#price").val();
        $('#amount').val(duration * price);        
    })

});
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        </section><!-- /.content -->
        <style>

.col-lg-12 {
    padding-bottom: 1%;

}
.quantity {


      width:45px;
    }
</style>
@stop
