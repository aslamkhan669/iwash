@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       
  <div class="row">
            <div class="col-md-12">
          

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				
		
					      @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Package Name</th>
                        <th>Usage Limit</th>
                        <th>Pickup Options</th>
                        <th>Available Packages</th>
                        <th>Pricings(For each month)</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>

						
						@foreach ($packages as $val)

                      <tr>
                        <td>  {{$val->package}}</td>
                        <td>
                          <?php
                      foreach($val->PackageServices as $servicepack){

                       
                          echo '<p>'.$servicepack->quantity.' '. $servicepack->Service->name .'('.$servicepack->Category->name.')'.'</p>';


                      }
                      ?>
                      

                      </td>

                        <td>  {{$val->pickup_options}}</td>

                        <td>  {{$val->available_packages}}</td>

                        <td>  {{$val->pricing_for_each_month}}</td>

             
                        <td>
                  

                            <a class="btn btn-success" href="/package/{{ $val->id }}/edit">Edit</a>
                            <a class="btn btn-danger" href="/package/{{ $val->id }}/delete">Delete</a>
                          
                        
                        </td>
                      </tr>
          @endforeach
                    </tbody>
                   
                  </table>
<div class="col-lg-6">		
	 	<a href="javascript:void(0)"  onclick="goBack()" class="btn btn-primary"><b><i class="fa fa-backward"></i>Back</b></a>
	
</div>
<div class="col-lg-6">	{!! $packages->render() !!} </div>         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
     
@stop
