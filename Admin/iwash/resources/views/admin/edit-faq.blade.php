@extends('admin.layouts.main')
 @section('container')
 <!-- Small boxes (Stat box) -->
       

         



  <div class="row">
            <div class="col-md-12">
          
<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" method='post' action=''>
								{{ csrf_field() }}

                  <div class="box-body">
                      <div class="form-group">
                      <label >FAQ Category </label>
                      <select name='faq_category' class="form-control">
						  <?php foreach($categories as $parent){ ?>
							   <option value="<?= $parent->id;?>" <?php if($parent->id==$faq->faq_category){echo 'selected';} ?>><?= $parent->name;?></option>
                  
							 <?php } ?>
                       </select>
                    </div>
                    <div class="form-group">
                      <label >Question</label>
                      <input type="text" class="form-control"  value="{{ @$faq->question }}" name='question' id="exampleInputEmail1" required >
                    </div>
                    <div class="form-group">
                      <label > Answer</label>
                    <textarea class="form-control"  name='answer' required>{{ @$faq->answer }}</textarea>
                    </div>


                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
                
                @if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif	
		@if(isset($info))
			@include('partials/error', ['type' => 'info', 'message' => $info])
		@endif
		@if(session()->has('error'))
			@include('partials/error', ['type' => 'danger', 'message' => session('error')])
		@endif	
		
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
@stop
