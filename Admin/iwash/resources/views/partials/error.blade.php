
<div class="alert alert-{{ $type }} fade in">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    	{!! $message !!}
</div>