-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2017 at 10:20 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iwash`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `url`, `status`, `createdAt`, `updatedAt`) VALUES
(5, 'uploads/banners/banner_2017-08-02 10:57:44.jpg', '1', '2017-08-02 10:57:44', '2017-08-02 10:57:44'),
(6, 'uploads/banners/banner_2017-08-02 10:57:48.jpg', '1', '2017-08-02 10:57:48', '2017-08-02 10:57:48');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch`, `location`, `createdAt`, `updatedAt`) VALUES
(1, 'Wakra', 'Wakra', '2017-08-17 10:43:44', '2017-09-25 15:02:12'),
(2, 'Doha', 'Doha', '2017-09-20 11:08:14', '2017-09-25 15:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_app` varchar(255) NOT NULL,
  `image_website` varchar(255) NOT NULL,
  `image_banner` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `per` varchar(100) NOT NULL,
  `rank_weight` int(10) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `description`, `image`, `image_app`, `image_website`, `image_banner`, `price`, `per`, `rank_weight`, `createdAt`, `updatedAt`) VALUES
(1, 'Wash & Fold', 0, 'Wash & Fold', 'uploads/categories/cat_6347d6e215803bbe7b46885bc1c46cb5.png', 'uploads/categories/cat_appc881b4db550e5859e86a971da87df25b.png', 'uploads/categories/cat_web6347d6e215803bbe7b46885bc1c46cb5.jpg', 'uploads/categories/cat_banner97ddaa95a8c8030ba186c76b6f0ed9c9.jpg', 0.00, 'Piece', 0, '2017-06-29 07:31:49', '0000-00-00 00:00:00'),
(2, 'Wash & Iron', 0, 'Wash & Iron', 'uploads/categories/cat_499efbe825d993c93abdbd588aa1ad18.png', 'uploads/categories/cat_appb903526d5598dd0b99433f8e270038d7.png', 'uploads/categories/cat_webebd4cc96ba5f67217a84e455448590d4.jpg', 'uploads/categories/cat_bannerc50aa7be2bf3abf7095a877bafdab7b6.jpg', 0.00, 'Piece', 0, '2017-06-29 07:33:17', '0000-00-00 00:00:00'),
(3, 'Premium Laundry', 0, 'Premium Laundry', 'uploads/categories/cat_ed89033621d3fce0fdded313720514fc.png', 'uploads/categories/cat_appab900a71d13dbb4c538a9c4c9b882318.png', 'uploads/categories/cat_web1a831f0f175a3ef3775f1c94db969111.jpg', 'uploads/categories/cat_banner6405b1853142e9c10bab5ab738c28ca0.jpg', 0.00, 'Piece', 0, '2017-06-29 07:33:32', '0000-00-00 00:00:00'),
(4, 'Dry Cleaning', 0, 'Dry Cleaning', 'uploads/categories/cat_97ab038dc0bce966a3bed6474da6843b.png', 'uploads/categories/cat_app3d40fadaec3884db9ef515343b968d68.png', 'uploads/categories/cat_web1b7c6b556050cc0ad35c8b79d2054c8e.jpg', 'uploads/categories/cat_bannere414162fa32899a682e2b7a3a465eea2.jpg', 0.00, 'Piece', 0, '2017-06-29 07:33:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `mobile`, `email`, `seen`, `message`, `createdAt`, `updatedAt`) VALUES
(1, 'Rakesh Bisht', '9650719414', 'raks.bisht@gmail.com', 0, 'hello testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `deliverytime`
--

CREATE TABLE `deliverytime` (
  `id` int(11) NOT NULL,
  `timebetween` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `deliverytime`
--

INSERT INTO `deliverytime` (`id`, `timebetween`, `status`, `createdAt`, `updatedAt`) VALUES
(1, '12:00 PM to 03:00 PM', 1, '2017-07-27 11:10:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `faq_category` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `faq_category`, `question`, `answer`, `createdAt`, `updatedAt`) VALUES
(3, 1, 'Q. How to reschedule a missed pickup/delivery?', 'Ans: If you happen to have missed your pickup/delivery, you can simply reschedule it by calling at our call centre number and we will reschedule it.', '2017-06-30 11:02:56', '0000-00-00 00:00:00'),
(4, 1, 'Q. What’s your turnaround time?', 'Ans: We take 48 Hours for laundry and 72 hours for dry cleaning. But in case of an emergency, we can make special arrangements.', '2017-07-08 10:34:57', '0000-00-00 00:00:00'),
(5, 1, 'Q.  How to contact iWash in case of a query/complaint?', 'Ans: You can always call at our call centre number or write us at info@iwashqatar.com. We will be more than happy to hear from you.', '2017-07-20 07:44:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `name`, `parent_id`, `description`, `image`, `createdAt`, `updatedAt`) VALUES
(1, 'Pickup/Delivery', 0, 'faq category 1', 'uploads/faqcat/', '2017-07-03 09:28:25', '0000-00-00 00:00:00'),
(3, 'Washing', 0, 'testfaq', 'uploads/faqcat/', '2017-08-02 11:37:42', '0000-00-00 00:00:00'),
(4, 'Payment', 0, 'payment', 'uploads/faqcat/faqcat_e7b12ac1478c85a0f3bc66231b3da7d3.png', '2017-08-25 11:03:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `monthly_packages`
--

CREATE TABLE `monthly_packages` (
  `id` int(11) NOT NULL,
  `package` varchar(255) NOT NULL,
  `usage_limit` varchar(255) NOT NULL,
  `pickup_options` varchar(255) NOT NULL,
  `available_packages` varchar(255) NOT NULL,
  `pricing_for_each_month` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monthly_packages`
--

INSERT INTO `monthly_packages` (`id`, `package`, `usage_limit`, `pickup_options`, `available_packages`, `pricing_for_each_month`, `createdAt`, `updatedAt`) VALUES
(1, 'Traditional', '50 Thobs, 50 Srwal', 'Alternative,Weekly', '1 , 3 , 6 , 12', 600, '2017-08-22 08:43:58', '2017-09-22 11:09:14'),
(2, 'Casual', '50 Tops 50 Formal Shirt 50 Pants', 'Alternative,Weekly', '1 , 3 , 6 , 12', 1000, '2017-08-25 09:34:45', '2017-09-22 11:08:09'),
(3, 'Smart', '50 Tops  50 bottom 50 Suit Jackets', 'Weekly', '1 , 3 , 6 , 12', 800, '2017-08-25 09:35:09', '2017-09-22 11:09:01'),
(4, 'Lady\'s', '50 Tops  50 bottom 10 Abaya\'s', 'Alternative,Weekly', '1 , 3 , 6 , 99', 400, '2017-08-25 09:36:05', '2017-09-22 11:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_cat_id` int(11) NOT NULL,
  `qr` varchar(255) DEFAULT NULL,
  `totalamount` float(10,2) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `flatandstreet` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `addressType` enum('home','office','other') NOT NULL,
  `pickupDate` date NOT NULL,
  `deliveryDate` date NOT NULL,
  `timeOfDelivery` tinyint(1) NOT NULL,
  `timeOfPickup` tinyint(1) NOT NULL,
  `status` enum('placed','accepted','picked up','processing','ready to deliver','delivered','cancelled') NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_status` tinyint(1) NOT NULL DEFAULT '0',
  `payment_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `service_cat_id`, `qr`, `totalamount`, `qty`, `branch`, `added_by`, `flatandstreet`, `landmark`, `addressType`, `pickupDate`, `deliveryDate`, `timeOfDelivery`, `timeOfPickup`, `status`, `createdAt`, `updatedAt`, `payment_status`, `payment_date`) VALUES
(112, 6, 4, 'uploads/orders/bda54eb878c5ce50eaab42095e985d80.png', NULL, 0, 2, 21, 'a', 'afa', 'other', '2017-08-26', '2017-08-31', 1, 1, 'picked up', '2017-08-25 07:48:00', '2017-09-06 12:53:05', 0, '0000-00-00'),
(113, 6, 1, 'uploads/orders/01055302a0988a66cb55cb7d55bf3aed.png', NULL, 0, 0, 0, 'bs vah', 'bandh', 'home', '2017-08-25', '2017-08-26', 1, 1, 'placed', '2017-08-25 09:26:24', '0000-00-00 00:00:00', 0, '0000-00-00'),
(114, 6, 2, 'uploads/orders/fb5e26422aa1f9ec8009573c01756c8f.png', NULL, 0, 0, 0, '123', 'landmark', 'office', '2017-08-26', '2017-08-30', 1, 1, 'placed', '2017-08-25 09:39:06', '0000-00-00 00:00:00', 0, '0000-00-00'),
(115, 6, 3, 'uploads/orders/0c70f654ecab5c643ed4ce3e09d86f39.png', NULL, 0, 0, 0, '234', 'landmark', 'other', '2017-08-26', '2017-08-31', 1, 1, '', '2017-08-25 09:43:15', '0000-00-00 00:00:00', 0, '0000-00-00'),
(116, 15, 1, 'uploads/orders/dcbd209e55397b037b91bbc038e8eab9.png', NULL, 0, 0, 0, 'hah', 'ha', 'office', '2017-08-31', '2017-08-31', 1, 1, 'placed', '2017-08-25 09:51:23', '0000-00-00 00:00:00', 0, '0000-00-00'),
(117, 6, 4, 'uploads/orders/c59d28d7e43f2496765059ef192c70a9.png', NULL, 0, 0, 0, '1233aaa', 'landmark', 'office', '2017-08-26', '2017-08-31', 1, 1, 'placed', '2017-08-25 10:15:39', '0000-00-00 00:00:00', 0, '0000-00-00'),
(118, 6, 1, 'uploads/orders/c61c3924df6b138f712340b40ff71484.png', NULL, 0, 0, 0, 'aa', 'a', 'home', '2017-08-29', '2017-08-29', 1, 1, 'placed', '2017-08-25 10:22:48', '0000-00-00 00:00:00', 0, '0000-00-00'),
(119, 6, 2, 'uploads/orders/50890c5397431f4453746a392b7c638f.png', NULL, 0, 0, 0, '124', 'aligarh', 'office', '2017-08-25', '2017-08-31', 1, 1, 'placed', '2017-08-25 10:30:15', '0000-00-00 00:00:00', 0, '0000-00-00'),
(120, 6, 2, 'uploads/orders/5d3d4ef027393fbc1cb3411a64ac1b74.png', NULL, 0, 0, 0, 'aa', 'Andhra Pradesh, India', 'office', '2017-08-31', '2017-08-26', 1, 1, 'placed', '2017-08-25 10:40:55', '0000-00-00 00:00:00', 0, '0000-00-00'),
(121, 6, 3, 'uploads/orders/016fc9ca186a50024450806412aac9e4.png', NULL, 0, 0, 0, 'gg', 'ggg', 'office', '2017-08-30', '2017-08-31', 1, 2, '', '2017-08-25 10:41:42', '0000-00-00 00:00:00', 0, '0000-00-00'),
(122, 6, 2, 'uploads/orders/4386e1436b46d7673b3767dca4f14d36.png', NULL, 0, 0, 0, 'ha', 'ha', 'office', '2017-08-29', '2017-08-30', 1, 2, 'placed', '2017-08-25 10:54:59', '0000-00-00 00:00:00', 0, '0000-00-00'),
(123, 6, 4, 'uploads/orders/11a3795477fa3d18ff1fd78e0e803e70.png', NULL, 0, 0, 0, 'Hshsh', 'Badarbagh', 'office', '2017-08-25', '2017-08-25', 1, 1, 'placed', '2017-08-25 11:03:29', '0000-00-00 00:00:00', 0, '0000-00-00'),
(124, 6, 3, 'uploads/orders/60bd1f8eb1fd36f1365c9279c30a67bd.png', NULL, 0, 0, 0, 'hhh', 'Pink City, Jaipur, Rajasthan, India', 'office', '2017-08-31', '2017-08-31', 1, 2, 'placed', '2017-08-25 11:06:35', '0000-00-00 00:00:00', 0, '0000-00-00'),
(125, 6, 1, 'uploads/orders/e7eae1f549479af65da4d11ada251d26.png', NULL, 0, 0, 0, '1123aa', 'aligarh', 'office', '2017-08-25', '2017-08-31', 1, 1, 'placed', '2017-08-25 11:09:49', '0000-00-00 00:00:00', 0, '0000-00-00'),
(126, 6, 2, 'uploads/orders/510c1e174e8d8a52b7f064a256c6664d.png', NULL, 0, 0, 0, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-31', '2017-08-25', 1, 1, 'placed', '2017-08-25 11:21:41', '0000-00-00 00:00:00', 0, '0000-00-00'),
(127, 6, 3, 'uploads/orders/8afc13b95c90b6402a4bab3252ba7db5.png', NULL, 0, 0, 0, 'sdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-26', 1, 2, 'placed', '2017-08-25 11:24:09', '0000-00-00 00:00:00', 0, '0000-00-00'),
(128, 6, 2, 'uploads/orders/4d437629fa6770c338f38eab2224cf32.png', NULL, 0, 0, 0, 'sdfs', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-31', '2017-08-26', 1, 1, 'placed', '2017-08-25 11:26:30', '0000-00-00 00:00:00', 0, '0000-00-00'),
(129, 6, 3, 'uploads/orders/80e9b07ebdf4fcbe5c2a2c81da51ad28.png', NULL, 0, 0, 0, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 1, 'placed', '2017-08-25 11:27:15', '0000-00-00 00:00:00', 0, '0000-00-00'),
(130, 6, 3, 'uploads/orders/a470185269bb0b553d030afe45e3e737.png', NULL, 0, 0, 0, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-25', 1, 2, 'placed', '2017-08-25 11:28:09', '0000-00-00 00:00:00', 0, '0000-00-00'),
(131, 6, 3, 'uploads/orders/5edb36447e6bab1c12e839e480efa4c6.png', NULL, 0, 0, 0, 'fsdfs', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-26', 1, 1, 'placed', '2017-08-25 11:29:05', '0000-00-00 00:00:00', 0, '0000-00-00'),
(132, 6, 2, 'uploads/orders/35ed3fe4cefaeb9a0684f9b189160d97.png', NULL, 0, 0, 0, 'fdsf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 2, 'placed', '2017-08-25 11:32:40', '0000-00-00 00:00:00', 0, '0000-00-00'),
(133, 6, 2, 'uploads/orders/76f7c3bed1c7f70211214ca24852d563.png', NULL, 0, 0, 0, 'sdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-31', '2017-08-26', 1, 1, 'placed', '2017-08-25 11:33:49', '0000-00-00 00:00:00', 0, '0000-00-00'),
(134, 6, 3, 'uploads/orders/a1f17c6295526f1e2c756882b78e3aad.png', NULL, 0, 0, 0, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-31', 1, 1, 'placed', '2017-08-25 11:37:21', '0000-00-00 00:00:00', 0, '0000-00-00'),
(135, 6, 3, 'uploads/orders/0eff7cf7c873935397a63ac0ff70250f.png', NULL, 0, 0, 0, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-31', 1, 1, 'placed', '2017-08-25 11:37:22', '0000-00-00 00:00:00', 0, '0000-00-00'),
(136, 6, 3, 'uploads/orders/380b95d863c8ebb071c1c13cfe3de9a4.png', NULL, 0, 0, 0, 'sdfsdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 1, 'placed', '2017-08-25 11:37:56', '0000-00-00 00:00:00', 0, '0000-00-00'),
(137, 6, 2, 'uploads/orders/bfa32dc3437116631ccf11e7086b6c15.png', NULL, 0, 0, 0, 'sdfsdfsfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-30', '2017-08-31', 1, 2, 'placed', '2017-08-25 11:39:26', '0000-00-00 00:00:00', 0, '0000-00-00'),
(138, 6, 2, 'uploads/orders/bfa32dc3437116631ccf11e7086b6c15.png', NULL, 0, 0, 0, 'sdfsdfsfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-30', '2017-08-31', 1, 2, 'placed', '2017-08-25 11:39:26', '0000-00-00 00:00:00', 0, '0000-00-00'),
(139, 6, 2, 'uploads/orders/734e704771896ab3f70b09fba20b7aee.png', NULL, 0, 0, 0, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-31', 1, 1, 'placed', '2017-08-25 11:42:58', '0000-00-00 00:00:00', 0, '0000-00-00'),
(140, 6, 2, 'uploads/orders/d8ab762db4ac2cd5e5d8314d0bb83986.png', NULL, 0, 0, 0, 'sdsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-25', '2017-08-25', 1, 1, 'placed', '2017-08-25 11:45:04', '0000-00-00 00:00:00', 0, '0000-00-00'),
(141, 6, 2, 'uploads/orders/3e548a784c035140dc8605f032d86240.png', NULL, 0, 0, 0, 'fsdfsd', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-25', 1, 1, 'placed', '2017-08-25 11:47:29', '0000-00-00 00:00:00', 0, '0000-00-00'),
(142, 6, 2, 'uploads/orders/3d1db336463a9d387dd06b251bb664e4.png', NULL, 0, 0, 0, 'sdfsdf', 'D225, Gali Number 4, Block E, Sangam Vihar, New Delhi, Delhi 110062, India', 'office', '2017-08-26', '2017-08-26', 1, 1, 'cancelled', '2017-08-25 11:48:11', '0000-00-00 00:00:00', 0, '0000-00-00'),
(143, 15, 1, 'uploads/orders/12ab48fce418dee39f963fd368053dae.png', NULL, 0, 0, 0, 'hahah', 'SAAD APARTMENT, Loco Rd, Aligarh, Uttar Pradesh 202001, India', 'office', '2017-08-31', '2017-08-27', 1, 1, '', '2017-08-26 05:35:17', '0000-00-00 00:00:00', 0, '0000-00-00'),
(144, 6, 4, 'uploads/orders/0af6510331403531b9d2371be34a38f5.png', NULL, 0, 0, 0, '123aa', 'landmark', 'home', '2017-08-26', '2017-08-31', 1, 2, '', '2017-08-26 06:57:12', '0000-00-00 00:00:00', 0, '0000-00-00'),
(145, 6, 3, 'uploads/orders/a0f2a44229b84e9b1dec8e9162943b86.png', NULL, 0, 0, 0, '123', 'landmark', 'office', '2017-08-26', '2017-08-31', 1, 1, 'cancelled', '2017-08-26 08:31:18', '0000-00-00 00:00:00', 0, '0000-00-00'),
(146, 6, 1, 'uploads/orders/676af7b34d39772cfd97532c7201d6df.png', NULL, 0, 0, 0, '111', 'aligarh', 'office', '2017-08-26', '2017-08-31', 1, 1, 'cancelled', '2017-08-26 09:20:39', '0000-00-00 00:00:00', 0, '0000-00-00'),
(147, 6, 1, 'uploads/orders/c6537a32d661db2694d7fbe5c53b55c5.png', NULL, 0, 0, 0, 'hhnn', 'nnn', 'office', '2017-08-31', '2017-08-30', 1, 1, 'cancelled', '2017-08-28 09:42:57', '0000-00-00 00:00:00', 0, '0000-00-00'),
(148, 24, 2, 'uploads/orders/00e3d90ea640da62ec1443488bd2c777.png', NULL, 0, 0, 0, '112', 'landmark', 'office', '2017-08-30', '2017-08-31', 1, 1, 'cancelled', '2017-08-29 11:29:32', '0000-00-00 00:00:00', 0, '0000-00-00'),
(149, 24, 2, 'uploads/orders/73db3471b31f27a9b035d6a936edd36e.png', NULL, 0, 0, 0, '88', 'aligarh', 'home', '2017-08-29', '2017-08-31', 1, 1, 'placed', '2017-08-29 11:34:36', '0000-00-00 00:00:00', 0, '0000-00-00'),
(150, 6, 1, 'uploads/orders/8926fa4f98a022b45fba81d7e6397149.png', NULL, 0, 0, 0, 'my flat', 'agarwal sweets', 'home', '2017-09-08', '2017-09-13', 1, 1, 'cancelled', '2017-09-07 07:16:56', '0000-00-00 00:00:00', 0, '0000-00-00'),
(151, 6, 1, 'uploads/orders/2a96a2ef30b100a0c099a2733c80c0f9.png', NULL, 0, 0, 0, 'hhh', 'hhh', 'home', '2017-09-14', '2017-09-13', 1, 1, 'placed', '2017-09-07 07:44:02', '0000-00-00 00:00:00', 0, '0000-00-00'),
(152, 6, 1, 'uploads/orders/9217f5e5e6c2d77c33353d06d43f2bbe.png', NULL, 0, 0, 0, 'jajaja', 'Hyderabad, Telangana, India', 'home', '2017-09-19', '2017-09-20', 1, 1, 'placed', '2017-09-08 13:57:44', '0000-00-00 00:00:00', 0, '0000-00-00'),
(153, 15, 4, 'uploads/orders/88f52d4bcc8966ba0a08e66640ef0813.png', NULL, 0, 0, 0, 'jaj', 'Jajaja Enclave Colony, Indore, Madhya Pradesh, India', 'home', '2017-09-23', '2017-09-19', 1, 1, 'cancelled', '2017-09-11 15:25:42', '0000-00-00 00:00:00', 0, '0000-00-00'),
(154, 6, 1, 'uploads/orders/13798b59c707983ec5189d72e707fa85.png', NULL, 0, 0, 0, 'gwg', 'hhshs', 'home', '2017-09-22', '2017-09-20', 1, 2, 'cancelled', '2017-09-12 10:09:20', '0000-00-00 00:00:00', 0, '0000-00-00'),
(155, 6, 1, 'uploads/orders/0ade6db36066de30e695762c9fa54b2c.png', NULL, 0, 0, 0, '305 badar bagh', 'aligarh', 'office', '2017-09-14', '2017-09-28', 1, 1, 'cancelled', '2017-09-13 10:12:46', '0000-00-00 00:00:00', 0, '0000-00-00'),
(156, 6, 4, 'uploads/orders/b9d778f92b297c0681e6fb1c24ebe9e0.png', NULL, 0, 0, 0, '56', 'Lansdowne, Uttarakhand, India', 'other', '2017-09-14', '2017-09-30', 1, 2, 'placed', '2017-09-13 10:15:15', '0000-00-00 00:00:00', 0, '0000-00-00'),
(157, 6, 4, 'uploads/orders/a71af6cb959c29307d5421c315b49030.png', NULL, 0, 0, 0, '123', 'Aligarh, Uttar Pradesh, India', 'other', '2017-09-13', '2017-09-14', 1, 2, 'placed', '2017-09-13 10:17:35', '0000-00-00 00:00:00', 0, '0000-00-00'),
(158, 6, 1, 'uploads/orders/20bc5736a39ecd8c87297ddad3a7c8a3.png', NULL, 0, 0, 0, '12', 'Aligarh, Uttar Pradesh, India', 'other', '2017-09-14', '2017-09-27', 1, 2, 'placed', '2017-09-13 10:18:56', '0000-00-00 00:00:00', 0, '0000-00-00'),
(159, 6, 1, 'uploads/orders/d8f0be2f093a15784006818a6655126c.png', NULL, 0, 0, 0, '56', 'Andhra Pradesh, India', 'other', '2017-09-13', '2017-09-21', 1, 2, 'cancelled', '2017-09-13 10:19:49', '0000-00-00 00:00:00', 0, '0000-00-00'),
(160, 6, 1, 'uploads/orders/4d5370f61b4d00570586ad9087a9fc1e.png', NULL, 0, 0, 0, '12', 'Landmark City, Kunadi, Rajasthan, India', 'home', '2017-09-15', '2017-09-27', 1, 1, 'placed', '2017-09-13 10:25:27', '0000-00-00 00:00:00', 0, '0000-00-00'),
(161, 37, 1, 'uploads/orders/7a3b7d9874458bf17d6d97ad27d310b9.png', NULL, 0, 0, 0, 'hshsh', 'Jhajjar, Haryana, India', 'home', '2017-09-22', '2017-09-17', 1, 1, 'placed', '2017-09-14 07:15:00', '0000-00-00 00:00:00', 0, '0000-00-00'),
(162, 6, 2, 'uploads/orders/bcb9fbf3125001364f0ecacb9d49939d.png', NULL, 0, 0, 0, '111', 'landmark', 'other', '2017-09-20', '2017-09-21', 1, 1, 'placed', '2017-09-20 10:43:05', '0000-00-00 00:00:00', 0, '0000-00-00'),
(163, 6, 1, 'uploads/orders/a7324a0de43b4a73a8d000c29304b28a.png', NULL, 0, 0, 0, 'house no-1,south delhi', 'Saket, New Delhi, Delhi, India', 'office', '2017-09-21', '2017-09-27', 1, 1, 'placed', '2017-09-20 11:50:07', '0000-00-00 00:00:00', 0, '0000-00-00'),
(164, 6, 3, 'uploads/orders/9271a6483e2d75a6d558c419e2707a08.png', NULL, 0, 0, 0, '1112', 'delhi', 'home', '2017-09-21', '2017-09-30', 1, 1, 'placed', '2017-09-20 11:54:31', '0000-00-00 00:00:00', 0, '0000-00-00'),
(165, 6, 4, 'uploads/orders/fcd80d559432e2f16bdb18d50722f637.png', NULL, 0, 0, 0, '1234', 'delhi', 'office', '2017-09-20', '2017-09-21', 1, 1, 'placed', '2017-09-20 12:07:04', '0000-00-00 00:00:00', 0, '0000-00-00'),
(166, 6, 3, 'uploads/orders/04ca430238c6ba4832e4e62083c5e438.png', NULL, 0, 0, 0, '987', 'delhi', 'office', '2017-09-20', '2017-09-21', 1, 1, 'placed', '2017-09-20 12:40:08', '0000-00-00 00:00:00', 0, '0000-00-00'),
(167, 19, 2, 'uploads/orders/e877cbe666791cb8cd347ed15d541797.png', NULL, 0, 0, 0, '90/93', 'Unnamed Road, Umm ?al?l \'Al?, Qatar', 'home', '2017-09-27', '2017-09-28', 1, 1, 'placed', '2017-09-25 15:37:08', '0000-00-00 00:00:00', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `order_service`
--

CREATE TABLE `order_service` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_service`
--

INSERT INTO `order_service` (`id`, `order_id`, `service_id`, `quantity`, `price`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, 1, 0.00, '2017-07-26 08:39:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `package_services`
--

CREATE TABLE `package_services` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_services`
--

INSERT INTO `package_services` (`id`, `category_id`, `service_id`, `package_id`, `quantity`, `createdAt`, `updatedAt`) VALUES
(1, 1, 15, 2, 50, '2017-09-01 10:15:58', '2017-09-22 11:08:09'),
(2, 1, 24, 2, 50, '2017-09-01 10:15:58', '2017-09-22 11:08:09'),
(3, 2, 8, 4, 50, '2017-09-01 10:16:08', '2017-09-22 11:08:36'),
(4, 2, 9, 4, 50, '2017-09-01 10:16:08', '2017-09-22 11:08:36'),
(5, 3, 12, 3, 50, '2017-09-01 10:16:57', '2017-09-22 11:09:01'),
(6, 3, 14, 3, 50, '2017-09-01 10:16:57', '2017-09-22 11:09:01'),
(7, 4, 13, 1, 50, '2017-09-01 10:17:05', '2017-09-22 11:09:14'),
(8, 4, 17, 1, 50, '2017-09-01 10:17:05', '2017-09-22 11:09:14');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pickuptime`
--

CREATE TABLE `pickuptime` (
  `id` int(11) NOT NULL,
  `timebetween` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickuptime`
--

INSERT INTO `pickuptime` (`id`, `timebetween`, `status`, `createdAt`, `updatedAt`) VALUES
(1, '12:00 PM to 02:00 PM', 1, '2017-07-27 11:10:14', '0000-00-00 00:00:00'),
(2, '12:00 PM to 03:00 PM', 1, '2017-08-07 11:38:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'User', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Employee', 'employee', '2017-08-16 12:24:01', '0000-00-00 00:00:00'),
(4, 'Branch Admin', 'branch_admin', '2017-09-26 08:01:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `per` varchar(100) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image_website` varchar(255) DEFAULT NULL,
  `image_app` varchar(255) DEFAULT NULL,
  `image_banner` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `category_id`, `description`, `image`, `price`, `per`, `createdAt`, `updatedAt`, `image_website`, `image_app`, `image_banner`) VALUES
(8, 'Pants', 2, 'some ', 'uploads/services/service_7c9f77d931c13b2dec39ca1066050f23.jpg', 5.00, 'item', '2017-08-18 10:15:41', '0000-00-00 00:00:00', 'uploads/services/service_webb60753d2a98f1652c221bdcb55b94b1a.jpg', 'uploads/services/service_appb60753d2a98f1652c221bdcb55b94b1a.jpg', 'uploads/services/service_banner61e48ebb928c3501b601d78912d5e657.png'),
(9, 'Jeans', 2, 'some', 'uploads/services/service_76825781ae82b4a0bbb2cf5cebb5e00f.jpg', 5.00, 'item', '2017-08-18 10:18:22', '0000-00-00 00:00:00', NULL, NULL, NULL),
(10, 'Formal Pants', 2, 'some', 'uploads/services/service_8bf18fa5f855f64700432972986342fd.jpg', 5.00, 'item', '2017-08-18 10:19:19', '0000-00-00 00:00:00', NULL, NULL, NULL),
(11, 'Formal Pants', 2, 'some', 'uploads/services/service_09c75fc19475689dfcf8d3fef03c0db6.jpg', 5.00, 'item', '2017-08-18 10:26:44', '0000-00-00 00:00:00', NULL, NULL, NULL),
(12, 'Abayas', 3, 'some', 'uploads/services/service_595ed86c9476970912fe3a13233c6bb6.jpg', 5.00, 'item', '2017-08-18 10:28:14', '0000-00-00 00:00:00', NULL, NULL, NULL),
(13, 'Towels', 4, 'some', 'uploads/services/service_e491974c4e283f54a422fcb6746931c7.jpg', 3.00, 'item', '2017-08-18 10:29:10', '0000-00-00 00:00:00', NULL, NULL, NULL),
(14, 'Tops', 3, 'some', 'uploads/services/service_43368bc0ce18998606367e2cfd5bda56.jpg', 5.00, 'item', '2017-08-18 10:30:08', '0000-00-00 00:00:00', NULL, NULL, NULL),
(15, 'Shorts', 1, 'some', 'uploads/services/service_79445eff3413f7d548d2066fc970b69d.jpg', 5.00, 'item', '2017-08-18 10:32:13', '0000-00-00 00:00:00', NULL, NULL, NULL),
(16, 'Suit Jackets', 3, 'some', 'uploads/services/service_c8f2dc7d0e0baca63f012eeb21cca322.jpg', 15.00, 'item', '2017-08-18 10:37:44', '0000-00-00 00:00:00', NULL, NULL, NULL),
(17, 'Pillow Case', 4, 'some', 'uploads/services/service_68db50e7b2f6c3e2e22da49eca05e484.jpg', 3.00, 'item', '2017-08-18 10:41:59', '0000-00-00 00:00:00', NULL, NULL, NULL),
(18, 'Bed Cover', 4, 'some', 'uploads/services/service_3b02f69aa707796530a293c5775b0c54.jpg', 5.00, 'item', '2017-08-18 10:42:48', '0000-00-00 00:00:00', NULL, NULL, NULL),
(19, 'Comforter', 4, 'some', 'uploads/services/service_bc631807d8f0fc5873b9fb58dfe2dba3.jpg', 10.00, 'i', '2017-08-18 10:43:31', '0000-00-00 00:00:00', NULL, NULL, NULL),
(20, 'Bedsheets', 4, 'some', 'uploads/services/service_a9f16222440888c7b0104ddf99e44edf.jpg', 6.00, 'item', '2017-08-18 10:44:20', '0000-00-00 00:00:00', NULL, NULL, NULL),
(21, 'Formal Shirts', 2, 'some', 'uploads/services/service_3f5b4d20bcf2f7f9aad3eb3536d34c7c.jpg', 5.00, 'item', '2017-08-18 10:45:15', '0000-00-00 00:00:00', NULL, NULL, NULL),
(22, 'Formal Shirts', 2, 'some', 'uploads/services/service_be87188b12a2122740159638dd7021b1.jpg', 5.00, 'item', '2017-08-18 10:45:21', '0000-00-00 00:00:00', NULL, NULL, NULL),
(23, 'Thobes', 2, 'some', 'uploads/services/service_8956916c2ca9173eca3ac79724455c1c.jpg', 5.00, 'item', '2017-08-18 10:47:39', '0000-00-00 00:00:00', NULL, NULL, NULL),
(24, 'Qtrah', 1, 'some', 'uploads/services/service_47022791d60b677cd68f2e2666a26095.jpg', 3.00, 'item', '2017-08-18 10:50:01', '0000-00-00 00:00:00', NULL, NULL, NULL),
(25, 'Fnyla', 1, 'some', 'uploads/services/service_62cf50a6ed4be3f4540f878e7de9f90e.jpg', 3.00, 'item', '2017-08-18 10:50:58', '0000-00-00 00:00:00', NULL, NULL, NULL),
(26, 'Srwal', 3, 'some', 'uploads/services/service_76b249a8039af5641c47be0e83af46ba.jpg', 3.00, 'item', '2017-08-18 10:51:32', '0000-00-00 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_features`
--

CREATE TABLE `service_features` (
  `id` int(11) NOT NULL,
  `service_cat_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_features`
--

INSERT INTO `service_features` (`id`, `service_cat_id`, `title`, `image`, `description`, `createdAt`, `updatedAt`) VALUES
(3, 1, 'Garments', 'uploads/servicefeatures/service_630359e397c21d9984362775201a8edf.png', 'For Household Wears', '2017-07-22 06:55:17', '0000-00-00 00:00:00'),
(5, 2, 'Garments', 'uploads/servicefeatures/service_c6d73f786d20411535fbaabd2f3ee278.png', 'For Household wears', '2017-08-02 13:40:59', '0000-00-00 00:00:00'),
(6, 3, 'Wash & Iron', 'uploads/servicefeatures/service_0d4b5fb2539a501d16ea265ab8330d8d.png', 'This is some Demo Description', '2017-08-23 08:44:43', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `souce_account_source_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`id`, `provider_id`, `username`, `avatar`, `souce_account_source_id`, `user_id`, `createdAt`, `updatedAt`) VALUES
(1, '116032362042541444460', 'bisht.raks@gmail.com', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 2, 6, '2017-09-08 11:21:47', '2017-09-08 11:21:47'),
(2, '503195943367725', 'waseemzaidi85@gmail.com', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c10.0.50.50/p50x50/20953460_497036647316988_3333295034162566021_n.jpg?oh=767d84f510136b7414915cd243082a54&oe=5A51C4B7', 1, 15, '2017-09-11 15:21:34', '2017-09-11 15:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `social_account_sources`
--

CREATE TABLE `social_account_sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_account_sources`
--

INSERT INTO `social_account_sources` (`id`, `source`, `createdAt`, `updatedAt`) VALUES
(1, 'facebook', '2016-04-15 02:08:58', '0000-00-00 00:00:00'),
(2, 'gplus', '2016-04-15 02:09:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_package`
--

CREATE TABLE `subscribe_package` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `pickup_option` varchar(255) NOT NULL,
  `duration` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_status` tinyint(1) NOT NULL,
  `payment_amount` int(11) NOT NULL,
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isExpired` tinyint(1) NOT NULL DEFAULT '0',
  `isLimitUsed` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe_package`
--

INSERT INTO `subscribe_package` (`id`, `package_id`, `pickup_option`, `duration`, `user_id`, `payment_status`, `payment_amount`, `payment_date`, `isExpired`, `isLimitUsed`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Alternative', 1, 6, 0, 600, '2017-09-01 10:17:55', 0, 0, 0, '2017-08-31 13:29:16', '0000-00-00 00:00:00'),
(2, 2, 'Weekly', 12, 6, 0, 12000, '2017-09-04 07:27:18', 0, 0, 0, '2017-09-04 07:12:50', '0000-00-00 00:00:00'),
(3, 4, 'Alternative', 1, 6, 0, 400, '2017-09-07 07:27:10', 0, 0, 0, '2017-09-04 07:27:42', '0000-00-00 00:00:00'),
(4, 1, 'Alternative', 12, 6, 0, 7200, '2017-09-07 08:16:20', 0, 0, 0, '2017-09-07 07:40:16', '0000-00-00 00:00:00'),
(5, 2, 'Weekly', 3, 6, 0, 3000, '2017-09-08 14:01:19', 0, 0, 0, '2017-09-08 13:58:20', '0000-00-00 00:00:00'),
(6, 2, 'Weekly', 3, 15, 0, 3000, '2017-09-17 12:51:15', 0, 0, 0, '2017-09-11 15:27:46', '0000-00-00 00:00:00'),
(7, 2, 'Alternative', 1, 6, 0, 1000, '2017-09-12 10:13:20', 0, 0, 0, '2017-09-12 10:10:41', '0000-00-00 00:00:00'),
(8, 2, 'Weekly', 3, 6, 0, 3000, '2017-09-13 10:45:17', 0, 0, 0, '2017-09-13 10:44:33', '0000-00-00 00:00:00'),
(9, 2, 'Alternative', 1, 6, 0, 1000, '2017-09-13 10:47:01', 0, 0, 1, '2017-09-13 10:47:01', '0000-00-00 00:00:00'),
(10, 2, 'Alternative', 3, 37, 0, 3000, '2017-09-14 07:16:22', 0, 0, 0, '2017-09-14 07:15:58', '0000-00-00 00:00:00'),
(11, 2, 'Alternative', 1, 19, 0, 1000, '2017-09-25 15:31:17', 0, 0, 1, '2017-09-25 15:31:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_services`
--

CREATE TABLE `subscribe_services` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subscribe_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe_services`
--

INSERT INTO `subscribe_services` (`id`, `user_id`, `subscribe_id`, `category_id`, `service_id`, `package_id`, `quantity`, `createdAt`, `updatedAt`) VALUES
(1, 6, 2, 1, 15, 2, 50, '2017-09-04 07:12:50', '0000-00-00 00:00:00'),
(2, 6, 2, 1, 24, 2, 50, '2017-09-04 07:12:50', '0000-00-00 00:00:00'),
(3, 6, 3, 2, 8, 4, 50, '2017-09-04 07:27:42', '0000-00-00 00:00:00'),
(4, 6, 3, 2, 9, 4, 50, '2017-09-04 07:27:42', '0000-00-00 00:00:00'),
(5, 6, 4, 4, 13, 1, 50, '2017-09-07 07:40:16', '0000-00-00 00:00:00'),
(6, 6, 4, 4, 17, 1, 50, '2017-09-07 07:40:16', '0000-00-00 00:00:00'),
(7, 6, 5, 1, 15, 2, 50, '2017-09-08 13:58:20', '0000-00-00 00:00:00'),
(8, 6, 5, 1, 24, 2, 50, '2017-09-08 13:58:20', '0000-00-00 00:00:00'),
(9, 15, 6, 1, 15, 2, 50, '2017-09-11 15:27:46', '0000-00-00 00:00:00'),
(10, 15, 6, 1, 24, 2, 50, '2017-09-11 15:27:46', '0000-00-00 00:00:00'),
(11, 6, 7, 1, 15, 2, 50, '2017-09-12 10:10:41', '0000-00-00 00:00:00'),
(12, 6, 7, 1, 24, 2, 50, '2017-09-12 10:10:41', '0000-00-00 00:00:00'),
(13, 6, 8, 1, 15, 2, 50, '2017-09-13 10:44:33', '0000-00-00 00:00:00'),
(14, 6, 8, 1, 24, 2, 50, '2017-09-13 10:44:33', '0000-00-00 00:00:00'),
(15, 6, 9, 1, 15, 2, 50, '2017-09-13 10:47:01', '0000-00-00 00:00:00'),
(16, 6, 9, 1, 24, 2, 50, '2017-09-13 10:47:01', '0000-00-00 00:00:00'),
(17, 37, 10, 1, 15, 2, 50, '2017-09-14 07:15:58', '0000-00-00 00:00:00'),
(18, 37, 10, 1, 24, 2, 50, '2017-09-14 07:15:58', '0000-00-00 00:00:00'),
(19, 19, 11, 1, 15, 2, 50, '2017-09-25 15:31:17', '0000-00-00 00:00:00'),
(20, 19, 11, 1, 24, 2, 50, '2017-09-25 15:31:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `profile` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `designation`, `message`, `profile`, `createdAt`, `updatedAt`) VALUES
(2, 'Waseem Ahmad', 'Software Engineer', 'Iwash doing a great job!', 'uploads/testimonials/testimonial_2017-08-25 09:53:43.jpg', '2017-08-02 11:28:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED NOT NULL,
  `confirmed` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `mobile_confirmed` tinyint(1) NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `mobile`, `password`, `location`, `added_by`, `active`, `role_id`, `confirmed`, `mobile_confirmed`, `confirmation_code`, `remember_token`, `createdAt`, `updatedAt`) VALUES
(1, 'raks.bisht@gmail.com', '', '$2y$10$VAbzPbeTcGKp5OspvuezlOXaAhPQxPj1O0PPtsA/.AAOTo.ka57wK', NULL, 0, '1', 1, '1', 1, NULL, 'VWiO6m0KAIqJRtsrYzuQrS3H85PPwsYfRsrxUWfW4X0wAZAhwFiyaLgTgalv', '2017-06-30 09:10:52', '2017-09-27 02:46:23'),
(2, 'raks@test.com', '', 'db59555ae86e3ca1626305eb1b8b8d7a', NULL, 0, '1', 2, '1', 1, NULL, NULL, '2017-07-12 10:52:57', '0000-00-00 00:00:00'),
(5, 'bisht.raksA@gmail.com', NULL, 'cd9d48e9f242ee364e39de55a811dcf1', NULL, 0, '1', 2, '', 0, '35ce6f72f4b5e11a0b8ed35f685132ca', NULL, '2017-07-24 10:48:08', '0000-00-00 00:00:00'),
(6, 'bisht.raks@gmail.com', NULL, 'cd9d48e9f242ee364e39de55a811dcf1', NULL, 0, '1', 2, '', 0, '35ce6f72f4b5e11a0b8ed35f685132ca', NULL, '2017-07-24 10:50:35', '0000-00-00 00:00:00'),
(7, 'raks.bishtsw@gmail.com', NULL, 'db59555ae86e3ca1626305eb1b8b8d7a', NULL, 0, '1', 2, '1', 1, NULL, NULL, '2017-08-02 13:51:44', '0000-00-00 00:00:00'),
(8, 'aslam@gmail.com', NULL, 'bc0595a6c1a1d7246c23333b0201eea6', NULL, 0, '1', 2, '1', 1, NULL, NULL, '2017-08-03 09:04:51', '0000-00-00 00:00:00'),
(9, 'junaid@sofyrus.com', NULL, 'bc0595a6c1a1d7246c23333b0201eea6', NULL, 0, '1', 2, '1', 1, NULL, NULL, '2017-08-03 09:05:26', '0000-00-00 00:00:00'),
(10, 'rakesh@sofyrus.com', NULL, '0fccae940ba99190da5507470fca915b', NULL, 0, '0', 2, '0', 0, '17bf03043247afaf4d8283a1515fe5e2', NULL, '2017-08-05 08:14:41', '2017-08-05 08:14:41'),
(11, 'raks.bisht@outlook.com', NULL, '89f2868773bff8b9669c7784da388098', NULL, 0, '0', 2, '0', 0, '0955706dd7d85b6f3706b6708a27a7aa', NULL, '2017-08-05 08:17:12', '2017-08-05 08:17:12'),
(12, 'test@test.com', NULL, '7303ac0b9d74e6b23f8db16ab760aa26', NULL, 0, '0', 2, '0', 0, '2b276d70000f20362c75bce9e601527e', NULL, '2017-08-05 08:26:34', '2017-08-05 08:26:34'),
(13, 'aslamkhan669@gmail.com', NULL, NULL, NULL, 0, '0', 2, '0', 0, NULL, NULL, '2017-08-05 11:56:57', '2017-08-05 11:56:57'),
(14, 'test@tester.com', NULL, 'password', NULL, 0, '1', 4, '1', 1, NULL, NULL, '2017-08-06 16:55:10', '2017-09-26 07:42:38'),
(15, 'waseemzaidi85@gmail.com', NULL, '3c1ef886bbf276b2bafb68225b1beca8', NULL, 0, '0', 2, '0', 0, '67ecc812693e190de811a77d4a31d35f', NULL, '2017-08-07 13:57:21', '2017-08-07 13:57:21'),
(16, 'sid.allthebest91@gmail.com', NULL, NULL, NULL, 0, '0', 2, '0', 0, NULL, NULL, '2017-08-08 13:34:11', '2017-08-08 13:34:11'),
(17, NULL, NULL, NULL, NULL, 0, '0', 2, '0', 0, NULL, NULL, '2017-08-08 13:39:16', '2017-08-08 13:39:16'),
(18, 'aliawasif1983@gmail.com', NULL, 'd00f763cf69a1c093d962d63e2df23fe', NULL, 0, '0', 2, '0', 0, '1722cb732e3269cdce24dd63a6d0f1f4', NULL, '2017-08-08 19:21:10', '2017-08-08 19:21:10'),
(19, 'gilaniwasif@gmail.com', NULL, 'e4eb11936dd8a70cb6c01cd6b002a16f', NULL, 0, '0', 2, '0', 0, 'c44b55e28b5afabde033bed8e4df3ca8', NULL, '2017-08-14 06:55:54', '2017-08-14 06:55:54'),
(20, 'aamir@iul.ac.in', NULL, 'ac9f8befe1aa0844b2ba26fd9cf503d8', NULL, 0, '0', 2, '0', 0, '65d854107216faf8e7373c8267722d80', NULL, '2017-08-17 10:01:54', '2017-08-17 10:01:54'),
(21, 'employee1@test.com', '9643896566', '$2y$10$RT6lkRU2OFX8u.40ZLRV4uMbV9Zk/fOD8eQUXB4KINrPG6.JQOVdy', 2, 0, '1', 3, '1', 1, NULL, 'Zrm8Q3cBIx4uFEKEDHih31rgqOVrWpoNGXewii9l9Jpregm2NzcWrZZusuB0', '2017-08-17 10:44:09', '2017-09-27 02:45:46'),
(22, 'al.naimi97@hotmail.com', NULL, 'bfdc4cf9fad1a42275555c50cefbe3a5', NULL, 0, '0', 2, '0', 0, 'bdb3c63a0bd1261073c35244d7fbeef8', NULL, '2017-08-21 19:01:14', '2017-08-21 19:01:14'),
(23, 'qatari._.97@hotmail.com', '33300033', '$2y$10$rbCe2r5F.5FWO/4L5zZr2uIYob50j6.xx1bYrdMJWepPe9llDpSJG', 1, 0, '1', 3, '1', 1, NULL, 'kU45OuZH7rrdnPqBPsQaqZeU44ebQGACPvoXbLiefCbAxSHosNoLZiQ75QIt', '2017-08-22 23:17:37', '0000-00-00 00:00:00'),
(24, 'amiransari.etawah@gmail.com', NULL, '1b408bf76796cc7de860ddfd8a2367ee', NULL, 0, '0', 2, '0', 0, '06f5fb62426d9a107379140629010ea0', NULL, '2017-08-29 11:19:10', '2017-08-29 11:19:10'),
(26, 'mohdjunaidsiddiqui91@gmail.com', '8273903927', '036d6a0001ddb09b82e822da51012f14', NULL, 0, '0', 2, '0', 0, '0bff9c5a8994d1fdc0770bc463de2a59', NULL, '2017-09-06 07:23:14', '0000-00-00 00:00:00'),
(27, 'allthebest91@gmail.com', '1234567890', '95e5e32ea4499b1cde18154d60851b31', NULL, 0, '0', 2, '0', 0, 'afb04f41f4e913bba477e786c9ddc0bc', NULL, '2017-09-06 07:44:28', '0000-00-00 00:00:00'),
(28, 'abc@gmail.com', '1234565656', '10295e6933c8112a6e3cba7d1b1af681', NULL, 0, '0', 2, '0', 0, '67a4ff61d39a1f9f87d66da0616b4409', NULL, '2017-09-06 07:59:58', '0000-00-00 00:00:00'),
(29, 'a@a.com', '1111111111', '93dc500bedf40acbef7e7e6d835ca74d', NULL, 0, '0', 2, '0', 0, '5143c9dcd76496ab3ab2fb37d086e494', NULL, '2017-09-06 08:39:47', '0000-00-00 00:00:00'),
(30, 'aa@a.com', '22222222', 'f766d516ecd0e66983cf3e236fe765e9', NULL, 0, '0', 2, '0', 0, '4ab314688c9044934723b0d47706e930', NULL, '2017-09-06 08:43:09', '0000-00-00 00:00:00'),
(31, 'aaa@a.com', '333333333333', 'cafb6dbbdb53b5326626c14ebc5746c6', NULL, 0, '0', 2, '0', 0, '7c7d8316311ea2d2a4bc7042c23deb37', NULL, '2017-09-06 08:47:25', '0000-00-00 00:00:00'),
(32, 'aaaaaa@a.com', '5555555555', '258221a53313dfe36a182ae724d959af', NULL, 0, '0', 2, '0', 0, '551244d865fd45477522c5073c7d4480', NULL, '2017-09-06 08:49:01', '0000-00-00 00:00:00'),
(33, 'b@b.com', '9999999999', '36690080a8f9bc17d2e02b072832bf72', NULL, 0, '0', 2, '0', 0, '59e8fbc40ee8f0a2d4c523bba3ac72bf', NULL, '2017-09-06 08:52:21', '0000-00-00 00:00:00'),
(34, 'c@c.com', '7777777777', '2f74a9534a3a259634e719d57cc13455', NULL, 0, '1', 2, '0', 0, '894e2e0dae1c056b96ed7f7ddc92e6bf', NULL, '2017-09-06 08:56:21', '0000-00-00 00:00:00'),
(35, 'bilal.hasan.khan@hotmail.com', NULL, NULL, NULL, 0, '1', 2, '0', 0, NULL, NULL, '2017-09-08 07:04:24', '0000-00-00 00:00:00'),
(36, 'waseem_zaidi123@gmail.com', '9540594018', '77b71f1dee3ce1c03d3e294b009edb7e', NULL, 0, '1', 2, '0', 0, '5428543ab58a8f36079fd87053bcbcc0', NULL, '2017-09-13 10:24:12', '0000-00-00 00:00:00'),
(37, 'waseeem_zaidi123@rediff.com', '8439425242', '77b71f1dee3ce1c03d3e294b009edb7e', 1, 21, '1', 2, '0', 0, '5428543ab58a8f36079fd87053bcbcc0', NULL, '2017-09-14 07:09:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_me` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `first_name`, `last_name`, `about_me`, `avatar`, `user_id`, `createdAt`, `updatedAt`) VALUES
(1, 'Rakesh', 'Bisht', '', '', 1, '2017-06-30 09:12:26', '0000-00-00 00:00:00'),
(2, 'Rakesh', 'Bisht', '', '', 2, '2017-07-12 10:52:57', '0000-00-00 00:00:00'),
(3, 'ra', 'faf', '', '', 5, '2017-07-24 10:48:08', '0000-00-00 00:00:00'),
(4, 'RA', 'RA', '', '', 6, '2017-07-24 10:50:35', '0000-00-00 00:00:00'),
(5, 'Rakesh', 'Bisht', '', '', 7, '2017-08-02 13:51:44', '0000-00-00 00:00:00'),
(6, 'aslam', 'khan', '', '', 8, '2017-08-03 09:04:51', '0000-00-00 00:00:00'),
(7, 'junaid', 'siddiqui', '', '', 9, '2017-08-03 09:05:26', '0000-00-00 00:00:00'),
(8, 'fsdfsd', 'fsdf', '', '', 12, '2017-08-05 08:26:34', '0000-00-00 00:00:00'),
(9, 'Aslam', 'Khan', '', '', 13, '2017-08-05 11:56:57', '0000-00-00 00:00:00'),
(10, 'test', 'testaaaaaaaaa', '', '', 14, '2017-08-06 16:55:10', '2017-09-26 07:42:38'),
(11, 'Waseem', 'Ahmad', '', '', 15, '2017-08-07 13:57:21', '0000-00-00 00:00:00'),
(12, 'Junaid', 'Sid', '', '', 16, '2017-08-08 13:34:11', '0000-00-00 00:00:00'),
(13, 'Amir', 'Ansari', '', '', 17, '2017-08-08 13:39:16', '0000-00-00 00:00:00'),
(14, 'Alia', 'Khan', '', '', 18, '2017-08-08 19:21:10', '0000-00-00 00:00:00'),
(15, 'Wasif', 'Gilani', '', '', 19, '2017-08-14 06:55:54', '0000-00-00 00:00:00'),
(16, 'md', 'amir', '', '', 20, '2017-08-17 10:01:54', '0000-00-00 00:00:00'),
(17, 'newemp1', 'newemp1', '', '', 21, '2017-08-17 10:44:09', '2017-09-27 01:11:09'),
(18, 'hassan', 'alnaimi', '', '', 22, '2017-08-21 19:01:14', '0000-00-00 00:00:00'),
(19, 'h', 'h', '', '', 23, '2017-08-22 23:17:37', '0000-00-00 00:00:00'),
(20, 'md', 'amir', '', '', 24, '2017-08-29 11:19:10', '0000-00-00 00:00:00'),
(21, 'a', 'a', '', '', 32, '2017-09-06 08:49:01', '0000-00-00 00:00:00'),
(22, 'a', 'a', '', '', 33, '2017-09-06 08:52:21', '0000-00-00 00:00:00'),
(23, 'a', 'a', '', '', 34, '2017-09-06 08:56:21', '0000-00-00 00:00:00'),
(24, 'Bilal', 'Khan', '', '', 35, '2017-09-08 07:04:24', '0000-00-00 00:00:00'),
(25, 'waseem', 'ahmad', '', '', 36, '2017-09-13 10:24:12', '0000-00-00 00:00:00'),
(26, 'waseem', 'zaidi', '', '', 37, '2017-09-14 07:09:17', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliverytime`
--
ALTER TABLE `deliverytime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthly_packages`
--
ALTER TABLE `monthly_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_service`
--
ALTER TABLE `order_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_services`
--
ALTER TABLE `package_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `pickuptime`
--
ALTER TABLE `pickuptime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_features`
--
ALTER TABLE `service_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_accounts_provider_id_unique` (`provider_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `souce_account_source_id` (`souce_account_source_id`);

--
-- Indexes for table `social_account_sources`
--
ALTER TABLE `social_account_sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_package`
--
ALTER TABLE `subscribe_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_services`
--
ALTER TABLE `subscribe_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_profiles_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `deliverytime`
--
ALTER TABLE `deliverytime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `monthly_packages`
--
ALTER TABLE `monthly_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
--
-- AUTO_INCREMENT for table `order_service`
--
ALTER TABLE `order_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `package_services`
--
ALTER TABLE `package_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pickuptime`
--
ALTER TABLE `pickuptime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `service_features`
--
ALTER TABLE `service_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `social_account_sources`
--
ALTER TABLE `social_account_sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subscribe_package`
--
ALTER TABLE `subscribe_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `subscribe_services`
--
ALTER TABLE `subscribe_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `social_accounts_ibfk_2` FOREIGN KEY (`souce_account_source_id`) REFERENCES `social_account_sources` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
